 angular.module('ehatidApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}).controller('ApplicationController', ['$scope', function($scope) {

	$scope.subClassifications = [
        {id:'G1', value:'National Government'},
        {id:'G2', value:'Local Government Unit'},
        {id:'G3', value:'Department of National Defense'},
        {id:'G4', value:'Philippine National Police'},
        {id:'G5', value:'Department of Justice'},
        {id:'G6', value:'State Universities and Colleges'},
        {id:'G7', value:'Government-owned and Controlled Corporations'},
        {id:'G8', value:'Others'},		
	];
  	
    $scope.todos = [
      {text:'learn angular', done:true},
      {text:'build an angular app', done:false}];
 
    $scope.addTodo = function() {
      $scope.todos.push({text:$scope.todoText, done:false});
      $scope.todoText = '';
    };
 
    $scope.remaining = function() {
      var count = 0;
      angular.forEach($scope.todos, function(todo) {
        count += todo.done ? 0 : 1;
      });
      return count;
    };
 
    $scope.archive = function() {
      var oldTodos = $scope.todos;
      $scope.todos = [];
      angular.forEach(oldTodos, function(todo) {
        if (!todo.done) $scope.todos.push(todo);
      });
    };

    $scope.healthFacilityType = 1;
    $scope.ownershipMajorClassification = 1;
    $scope.ownershipSubClassification1 = 'G1';
    $scope.ownershipSubClassification2 = 'P1';

    $scope.others = function() {
    	return (($scope.ownershipSubClassification1 == 'G8' && $scope.ownershipMajorClassification == 1) || ($scope.ownershipSubClassification2 == 'P8' && $scope.ownershipMajorClassification == 2));
    };


  }]);