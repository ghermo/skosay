function BackBtn() {
	window.history.back();
}

/*
* Variable used in Locations and Search pages
*/
var map, placesList, results, pName, pId, pNameDisplay, pAddress, pType, d, lat, lng, temp,place_names,rl, pyrmont,sw,ne,cur_bounds,rd,temp_arr=[],sorted=[],count=0, add='1', ctlID = 0;
var markers = Array();
var infos = Array();
var r = Array();
var reverseGeocoder = new google.maps.Geocoder();
var googKy = 'AIzaSyCP1sn-eN7_Fjk0KzvKU4lmpmU2GVcoJ8U';
//debug: 'ios',           // this can be deleted it's for debugging purposes on laptop browser
//displayPace: 720,		// display again after 12hrs, not really necessary

//initialize
function initialize() 
{
	// set initial position
	if (navigator.geolocation) 
	{
    var timeoutVal = 10 * 1000 * 1000;
    navigator.geolocation.getCurrentPosition
    (
        displayPosition,
        displayError,
        { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 }
    );  
    
}

function displayPosition(position)
{
	//alert('lat: ' + position.coords.latitude + ' lng: ' + position.coords.longitude);
	
    $('#lat').val(position.coords.latitude);
    $('#lng').val(position.coords.longitude); 
}

//display error
function displayError(error){}
    var myLatlng = new google.maps.LatLng(document.getElementById('lat').value, document.getElementById('lng').value);

    var myOptions = { // default map options
        zoom: 10,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
}

// clear overlays function
function clearOverlays() 
{
    if (markers) 
    {
        for (i in markers) 
        {
            markers[i].setMap(null);
        }
        markers = [];
        infos = [];
    }
}

// clear infos function
function clearInfos() 
{
    if (infos) 
    {
        for (i in infos) 
        {
            if (infos[i].getMap()) 
            {
                infos[i].close();
            }
        }
    }
}

// create markers (from 'findPlaces' function)
function createMarkers(results, status, pagination) 
{
	 //Sdocument.getElementById('loadDiv').style.display = '';
	var clat =document.getElementById('lat').value;//36.2753807;
    var clng =document.getElementById('lng').value;//-94.2971973;
    
	var coords = new google.maps.LatLng(clat, clng);//current location
	var newResults;
	var sko_customer;
	var flag=false;
	var sc_coords;
	var goog_latlng;
	var cur_d;
	//clearOverlays();
    if (status == google.maps.places.PlacesServiceStatus.OK) 
    {
        // if we have found something - clear map (overlays)
        clearOverlays();

        // and create new markers by search result
        if(getParam()=="location")
        {
        	sko_customer=JSON.parse(sessionStorage.sc);//get customers from database
        	
        	for(var i = 0; i < sko_customer.length; i++)
        	{
	        	for (var j = 0; j < results.length; j++) 
	            {
	        		cur_d=calc_distance(coords,results[j].geometry.location);
	    			sc_coords= new google.maps.LatLng(sko_customer[i].latitude,sko_customer[i].longitude); 
	    			goog_latlng=new google.maps.LatLng(results[j].geometry.location.lat(), results[j].geometry.location.lng());
	        
	        		if((goog_latlng.equals(sc_coords))&&(cur_d<800))
	        		{
	        			sorted.push(results[j]);
	        			results.splice(j,1);
	        			
	        		}
	    	           
	            }
        	}
      
        	
        	if (pagination.hasNextPage) 
        	{
        		//flag=true;
        		//document.getElementById('places').style.display = '';
        		pagination.nextPage();
        	    
        	}
        	
    
        	
        	if(temp_arr.length===0)
        	{
        		temp_arr=results;
        	}
        	
        	else
        	{
        		
        		newResults=temp_arr.concat(results);
        		 placesList = document.getElementById('places');
        		    placesList.innerHTML = "";
        		for(var l=0; l<sorted.length; l++)
        		{
        			insertHtml(sorted[l]);
        		}
        		for(var k=0; k<newResults.length; k++)
        		{
        			insertHtml(newResults[k]);
        		}
        	}
        }
        
        else if(getParam()=="search")
        {
        	sortresults(coords,results);//sort places based on distance and prominence
        	for (var i = 0; i < results.length; i++) 
            {
        		//alert(results[i].geometry.location.lat()+","+results[i].geometry.location.lng());
    	            //createMarker(results[i]);
    	            insertHtml(results[i]);
    	           // placesList = document.getElementById('places');
    	        	//document.getElementById('loadDiv').style.display = '';
    			    //placesList.innerHTML = "";
            }
        	
        }
        
        else
        {
        	alert("radius not set");
        }
        
    } 
    else if (status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS) 
    {
    	//alert("hi");
        document.getElementById('loadDiv').style.display = 'none';
        document.getElementById('places').style.display = 'none';
        document.getElementById('errorDiv').style.display = '';
    }
    
    else if(status==google.maps.places.PlacesServiceStatus.OVER_QUERY_LIMIT)
    {
    	alert("over query limit");
    	document.getElementById('places').style.display = 'none';
    	document.getElementById('loadDiv').style.display = 'none';
        document.getElementById('errorDiv').style.display = '';
    }
    
    else if(status==google.maps.places.PlacesServiceStatus.REQUEST_DENIED)
    {
    	alert("request denied");
    	document.getElementById('places').style.display = 'none';
    	document.getElementById('loadDiv').style.display = 'none';
        document.getElementById('errorDiv').style.display = '';
    }
    
    else if(status==google.maps.places.PlacesServiceStatus.INVALID_REQUEST)
    {
    	alert("invalid request");
    	document.getElementById('places').style.display = 'none';
    	document.getElementById('loadDiv').style.display = 'none';
        document.getElementById('errorDiv').style.display = '';
    }
}


// create single marker function
function createMarker(obj) 
{
    // prepare new Marker object
    var mark = new google.maps.Marker
    ({
        position: obj.geometry.location,
        map: map,
        title: obj.name
    });
    
    markers.push(mark);
    
    // prepare info window
    var infowindow = new google.maps.InfoWindow
    ({
        content: '<img src="' + obj.icon + '" /><font style="color:#000;">' + obj.name + '<br />Rating: ' + obj.rating + '<br />Vicinity: ' + obj.vicinity + '</font>'
    });

    // add event handler to current marker
    google.maps.event.addListener(mark, 'click', function() 
    {
        clearInfos();
        infowindow.open(map,mark);
    });
    
    infos.push(infowindow);
}
 
function checkPrev(plat,plng)
{
	var pl=0;
	var pln=0;
	
	if((plat===undefined) && (plng===undefined))
	{
		pl=0;
		pln=0;
	}
	else
	{
		pl= plat;
		pln= plng;
	}
	
	return new google.maps.LatLng(pl,pln);
	
}

function setPosition(poslat,poslng) 
{
	if (typeof(Storage) != undefined) 
	{
	    // Store
		sessionStorage.prevlat = poslat;
		sessionStorage.prevlng = poslng;
	} 
	
	else 
	{
	    alert("Sorry, your browser does not support Web Storage; please upgrade browser.");
	}
	
}

var rad = function(x) 
{
	  return x * Math.PI / 180;
};

var degrees = function(x) 
{
	  return x * 180 / Math.PI;
	  
	  //radians * (180/pi)
};

function calc_distance(p1, p2) 
{
	  var R = 6378137; // Earth’s mean radius in meter
	  var dLat = rad(p2.lat() - p1.lat());
	  var dLong = rad(p2.lng() - p1.lng());
	  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	    Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
	    Math.sin(dLong / 2) * Math.sin(dLong / 2);
	  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	  var d = R * c;
	  return d; // returns the distance in meter
};

function computeOffset(from, distance, heading) 
{
	var EARTH_RADIUS=6378137;
    distance /= EARTH_RADIUS;
    heading = rad(heading);
    
    var fromLat = rad(from.lat());
    var fromLng = rad(from.lng());
    var cosDistance = Math.cos(distance);
    var sinDistance = Math.sin(distance);
    var sinFromLat = Math.sin(fromLat);
    var cosFromLat = Math.cos(fromLat);
    var sinLat = cosDistance * sinFromLat + sinDistance * cosFromLat * Math.cos(heading);
   var dLng = Math.atan2(
            sinDistance * cosFromLat * Math.sin(heading),
            cosDistance - sinFromLat * sinLat);
    return new google.maps.LatLng(degrees(Math.asin(sinLat)), degrees(fromLng + dLng));
};

function sortresults(cur_loc, coords) 
{
    coords.sort(function ( a, b ) {
        return calc_distance( cur_loc, a.geometry.location ) - calc_distance( cur_loc, b.geometry.location ); 
    });
    
};

/*function sortByConsumer(myLoc, goog_locs)
{
	var sc= JSON.parse(sessionStorage.sc);//get customers from database
	//alert(sc);
	var cur_d;
	//var prev_d=0;
	var consumerArray = [];
	var goog_latlng,sc_coords;
	//var test_arr=["Mr Burger","Tan with TLC","Harp's Deli", "Ay Caramba", "Beat the Bookstore"];//for testing
	//var test=["Tan with TLC"];
	var all_locs;
	
	//alert("hi");
	for(var i=0;i<sc.length;i++)
	{
		for(var j=0;j<goog_locs.length;j++)
		{
			cur_d=calc_distance(myLoc,goog_locs[j].geometry.location);
			sc_coords= new google.maps.LatLng(sc[i].latitude,sc[i].longitude); 
			//alert(goog_locs[j].place_id);//sc[i].latitude+","+sc[i].longitude);
			goog_latlng=new google.maps.LatLng(goog_locs[j].geometry.location.lat(), goog_locs[j].geometry.location.lng());
			//alert(goog_locs[j].geometry.location.lat()+","+goog_locs[j].geometry.location.lng());
			//(goog_locs[j].geometry.location.lat()==sc[i].latitude)&&(goog_locs[j].geometry.location.lng()==sc[i].longitude)
			//(sc_coords.equals(goog_latlng))

			if((goog_locs[j].place_id===sc[i].place_id)&&(cur_d<1000))//if any google location match any store name in database push into the new array
			{
				//alert(cur_d);
					consumerArray.push(goog_locs[j]);
					goog_locs.splice(j,1);//remove the location that has already been placed in new array
				
			}
			
		}
	}
	
	if(consumerArray.length===0)
	{
		alert("hello");
		return goog_locs;// if no locations were placed in new array simply return the old array with google locations
	}
	
	else
	{
		sortresults(myLoc, consumerArray); //sort the locations by distance in the new array
		all_locs=consumerArray.concat(goog_locs);//join both the new and old array and store in variable 
		return all_locs;//return the new sorted array
	}
	
}*/

/*function checkConnection()
{
	var sc;
	var xmlhttp;
	
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	
	else
	{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	    //var xmlhttp=new XMLHttpRequest();
	    xmlhttp.onreadystatechange=function() 
	    {
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) 
	        {
	        	return true;
	            //document.getElementById("txtHint").innerHTML
	        	//sc=xmlhttp.responseText;
	        }
	        
	        else
	        {
	        	return false
	        }
	    }
	    //xmlhttp.open("GET",file,true);
	    //xmlhttp.send();
	    //return sc;
	    //"gethint.php?q="+str
	//}    
}*/


/* Find custom places function */
function findPlaces(param)
{
    // prepare variables (filter)
    //placesList = document.getElementById('places');
    //placesList.innerHTML = "";
    
	lat =document.getElementById('lat').value;
    lng = document.getElementById('lng').value;
    
    var cur_location = new google.maps.LatLng(lat, lng);  
    temp = checkPrev(sessionStorage.prevlat,sessionStorage.prevlng);
	var distance = calc_distance(cur_location,temp);
	
	
    if (param == 'search')
  	{
    	//document.getElementById('loadDiv').style.display = '';
  		var radius = 50000;
      	var keyword = document.getElementById('gmap_keyword').value;
      	setPosition(lat,lng);
      	setParam(param);
      	
      	sw= computeOffset(cur_location, radius, 225);
    	ne= computeOffset(cur_location, radius, 45);
    	cur_bounds=new google.maps.LatLngBounds(sw,ne);
    	//alert(sw.lat()+","+sw.lng()+"  "+ne.lat()+","+ne.lng());
      	
      	//alert('radius: ' + radius + ' keyword: ' + keyword + ' cur_location: ' + cur_location);
      	var request = {
      		//bounds: cur_bounds,
      		location: cur_location,
      		radius: radius,
  			query: keyword,
  			key: googKy
  		};
  		//alert(request.query);
      	placesList = document.getElementById('places');
    	//document.getElementById('loadDiv').style.display = 'none';
        placesList.innerHTML = "";
  		// send request
  	    service = new google.maps.places.PlacesService(map);
  	    service.textSearch(request, createMarkers);
  	  	
  	}
    
  	else if (param == 'location')
  	{
  		if(!(cur_location.equals(temp))&&((Math.abs(distance))>15.23996952))
  		{
  			document.getElementById('errorDiv').style.display = 'none';
  			//document.getElementById('loadDiv').style.display = '';
  			//document.getElementById('places').style.display = '';
  			setPosition(lat,lng);
  			setParam(param);
  
	      	var keyword = '';
	      	
	  	    var request = 
	  	    {
	  	        location: cur_location,
	  	        //radius: radius,
	  	        //keyword: 'skosay',
	  	        rankBy: google.maps.places.RankBy.DISTANCE,
	  	        types: ['accounting','airport','amusement_park','aquarium','art_gallery','atm','bakery','bank','bar','beauty_salon','bicycle_store','book_store','bowling_alley','bus_station','cafe','campground','car_dealer','car_rental','car_repair','car_wash','casino','cemetery','city_hall','clothing_store','convenience_store','courthouse','dentist','department_store','doctor','electrician','electronics_store','embassy','establishment','finance','fire_station','florist','food','funeral_home','furniture_store','gas_station','general_contractor','grocery_or_supermarket','gym','hair_care','hardware_store','health','home_goods_store','hospital','insurance_agency','jewelry_store','laundry','lawyer','library','liquor_store','local_government_office','locksmith','lodging','meal_delivery','meal_takeaway','movie_rental','movie_theater','moving_company','museum','night_club','painter','park','parking','pet_store','pharmacy','physiotherapist','plumber','police','post_office','real_estate_agency','restaurant','roofing_contractor','rv_park','school','shoe_store','shopping_mall','spa','stadium','storage','store','subway_station','taxi_stand','train_station','travel_agency','university','veterinary_care','zoo'],
	  	        key: googKy
	  	        
	  	    };
	  	    
	  	    if (keyword) 
	  	    {
	  	        request.keyword = [keyword];
	  	    }

	  	    // send request
	  	    service = new google.maps.places.PlacesService(map);
	  	    service.nearbySearch(request, createMarkers);
	  	    
	  	    
	  	    
	  	}
  		
  		else //current position is less than 50 feet of previous position (do not get new list from Google Places, just re-display)
  		{
  			//alert('current and previous within 50 feet of each other - no need to reload GPA');
  			setPosition(lat,lng);
  			
  			if(sessionStorage.pList===undefined)//if no locations are stored due an error ask google for locations 
  			{
  				document.getElementById('errorDiv').style.display = 'none';
  				//document.getElementById('errorDiv2').style.display = 'none';
  	  			//document.getElementById('loadDiv').style.display = '';
  	  			//document.getElementById('places').style.display = '';
  	  			
	  	  		setPosition(lat,lng);
	  			setParam(param);
	  			
		  		//var radius = 305;
		      	var keyword = '';
		  	    // prepare request to Places
		  	    var request = 
		  	    {
		  	        location: cur_location,
		  	        //radius: radius,
		  	        rankBy: google.maps.places.RankBy.DISTANCE,
		  	        types: ['accounting','airport','amusement_park','aquarium','art_gallery','atm','bakery','bank','bar','beauty_salon','bicycle_store','book_store','bowling_alley','bus_station','cafe','campground','car_dealer','car_rental','car_repair','car_wash','casino','cemetery','city_hall','clothing_store','convenience_store','courthouse','dentist','department_store','doctor','electrician','electronics_store','embassy','establishment','finance','fire_station','florist','food','funeral_home','furniture_store','gas_station','general_contractor','grocery_or_supermarket','gym','hair_care','hardware_store','health','home_goods_store','hospital','insurance_agency','jewelry_store','laundry','lawyer','library','liquor_store','local_government_office','locksmith','lodging','meal_delivery','meal_takeaway','movie_rental','movie_theater','moving_company','museum','night_club','painter','park','parking','pet_store','pharmacy','physiotherapist','plumber','police','post_office','real_estate_agency','restaurant','roofing_contractor','rv_park','school','shoe_store','shopping_mall','spa','stadium','storage','store','subway_station','taxi_stand','train_station','travel_agency','university','veterinary_care','zoo'],
		  	        key: googKy
		  	        
		  	    };
		  	    
		  	    if (keyword) 
		  	    {
		  	        request.keyword = [keyword];
		  	    }
		  	    
  				service = new google.maps.places.PlacesService(map);
  		  	    service.nearbySearch(request, createMarkers);
  		  	    
  			}
  			
  			else//display whatever has been stored already
  			{
  				//document.getElementById('loadDiv').style.display = 'none';
	  			document.getElementById('errorDiv').style.display = 'none';
	  			//document.getElementById('errorDiv2').style.display = 'none';
  				placesList = document.getElementById('places');
  			    //placesList.innerHTML = "";
	  			placesList.innerHTML = sessionStorage.pList;
	  			//document.getElementById('places').style.display = '';
  			}
  		}
  		

  		 var ath = addToHomescreen({
 			skipFirstVisit: true,	// skip first visit to website
 			maxDisplayCount: 1,      // show only once
	 		debug: 'ios',
	 		autostart: false, 		//don't automatically call the popup only when I call the function
	 		startDelay: 10,          // display the popup after 7 seconds
	 		lifespan: 10,            // kill the popup after 10 seconds
	 		message: "Digging Skosay's web app? Add it to your homescreen.\n" +//customized message, it can be changed if needed
	 				"Tap %icon and then <strong>Add to Home Screen</strong>."
  		 });
  		
  		 ath.show();//call the popup
    }
}

function setParam(pr)
{
	sessionStorage.param = pr;
}

function getParam()
{
	var p;
	p=sessionStorage.param;
	return p;
}

function checkParam(par,pList)
{
	if (par == 'location')
	{
		sessionStorage.pList = pList;
	}
	
}

/*
* Used to display location icons
* in the Location and Search pages
*/
function getIcon(icon){
	var result = "";
	var icons = {
		'airport': function(){
			return 'airport.png';
		},
		'liquor_store': function(){
			return 'bar.png';
		},
		'casino': function(){
			return 'bar.png';
		},
		'night_club': function(){
			return 'bar.png';
		},
		'cafe': function(){
			return 'cafe.png';
		},
		'store': function(){
			return 'deptstore.png';
		},
		'shopping_mall': function(){
			return 'deptstore.png';
		},
		'church': function(){
			return 'info.png';
		},
		'gas_station': function(){
			return 'deptstore.png';
		},
		'gym': function(){
			return 'gym.png';
		},
		'city_hall': function(){
			return 'info.png';
		},
		'hospital': function(){
			return 'info.png';
		},
		'school': function(){
			return 'info.png';
		},
		'park': function(){
			return 'park.png';
		},
		'pet_store': function(){
			return 'petshop.png';
		},
		'campground': function(){
			return 'park.png';
		},
		'restaurant': function(){
			return 'restaurant.png';
		},
		'food': function(){
			return 'restaurant.png';
		},
		'train_station': function(){
			return 'trainstation.png';
		},
		'subway_station': function(){
			return 'trainstation.png';
		},
		'convenience_store': function(){
			return 'supermarket.png';
		},
		'veterinary_care': function(){
			return 'petshop.png';
		},
		'zoo': function(){
			return 'petshop.png';
		},
		'grocery_or_supermarket': function(){
			return 'supermarket.png';
		}
	};

	if (typeof icons[icon] !== 'function'){
		return 'business.png';
	}
	return icons[icon]();
}



/*
* Used to display location information
* such as Business Name, Address
* in the Location and Search pages
*/
function insertHtml(obj){

		document.getElementById('loadDiv').style.display = 'none';
	
	    //document.getElementById('places').style.display = '';
	    //document.getElementById('demoDay').style.display = '';
	    //document.getElementById('demoDay').className = "demoDay";
	    
		
	    pId = obj.id;
	    pName = obj.name.replace(/ /g,'_');
	    pNameDisplay = obj.name;
	    //pAddress = p.formatted_address;	
	    
	    if (isStrEmpty(obj.vicinity))
	    {
			pAddress = obj.formatted_address;	
			var FinalpAddress = obj.formatted_address.replace(/ /g,'_');
		}
	    else if (isStrEmpty(obj.formatted_address))
	    {
			pAddress = obj.vicinity;	
			var FinalpAddress = obj.vicinity.replace(/ /g,'_');
	    }
	    
	    pType = obj.types;
	    
	    //alert("pId: " + pId + " pName: " + pName + " pNameDisplay: " + pNameDisplay + " pAddress: " + pAddress + " pType: " + pType);
	   
		var iconToUse = getIcon(obj.types[0]);
	
	    place_names = [pName, pId, pNameDisplay, pAddress];
	
		//alert(place_names.length);
	  
	    var key = ctlID++;
	
	    /* Set the local storage item */
	    for (var i = 0, place; place = place_names[i]; i++)
	    {
	        localStorage.setItem(key, place_names);
	    }
	    
	    placesList.innerHTML += '<form method="POST" action="feedback" id="'+ place_names[0] +'"><input type="hidden" name="question" value="1" /><input type="hidden" name="iconsrc" value="images/'+iconToUse+'" /><input type="hidden" name="pId" value="'+place_names[1]+'" /><input type="hidden" name="pType" value="'+pType+'" /><input type="hidden" name="pAddress" value="'+place_names[3]+'" /><input type="hidden" name="pName" value="'+place_names[0]+'" /><button type="submit" class="item item-thumbnail-left" style="text-align:left; padding-left:25px; width: 100%;"><img src="images/' + iconToUse + '" style="width:50px;"><img src="images/right.png" style="height:30px;vertical-align:center;" align="right"><img src="images/spcr.png" style="width:40px;padding-right:30px;"><font style="font-size:15px;vertical-align:top;">' + place_names[2] + '</font><br /><img src="images/spcr.png" style="width:40px;padding-right:30px;"><font color="grey" style="font-size:12px;vertical-align:top;">'+ place_names[3] +'</font></button></form>';
	    
	   checkParam(sessionStorage.param,placesList.innerHTML);
	   
}

function isStrEmpty(value)
{
	return (typeof value === "undefined" || value === null);	
}