<?php namespace Ehatid\Mailers;

class FeedbackMailer extends Mailer {

	public function resendDetailsTo($email, $data) {

		$subject = "SKOSAY Feedback";
		$view = 'emails.feedback.feedback';

		return $this->sendTo($email, $subject, $view, $data);
	}
	
}