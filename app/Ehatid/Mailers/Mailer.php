<?php namespace Ehatid\Mailers;

use Illuminate\Mail\Mailer as Mail;

abstract class Mailer {

	private $mail;

	function __construct(Mail $mail) {

		$this->mail = $mail;		

	}
	
	public function sendTo($email, $subject, $view, $data = [] )
	{
		//dd($data);
		$this->mail->queue(
        $view,
        $data,
        function ($message) use ($email, $subject) {
           $message
           	->to($email)
           	->subject($subject);
             });
		// $this->mail->queue($view, $data, function (message) use ($user, $subject) 
		// {
		// 	$message->to($user->email)->subject($subject);
		// });
	}
}