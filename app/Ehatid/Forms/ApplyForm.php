<?php namespace Ehatid\Forms;

use Laracasts\Validation\FormValidator;

class ApplyForm extends FormValidator {
	
	protected $rules = [
            'username' => 'required|alpha_dash',
            'email'    => 'required|email',
	];

}
