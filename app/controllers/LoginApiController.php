<?php
class LoginApiController extends \ApiController {
    
    public function Login(){
        if (DB::table('users')->where('email', Input::get('email'))->count() == 0){
            return $this->respondUnauthorized('Users does not exist.');
        }
        else{
            $confirmation = DB::table('users')->where('email', Input::get('email'))->pluck('confirmation');
            if ($confirmation == 1){
                if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
  
                    return $this->respondOk('Successfully logged in.');
                    /*return array(
                        'secret: '     => DB::table('oauth_clients')->where('id', Input::get('email'))->pluck('secret'),
                        );*/
                }
                else {
                    return $this->respondUnauthorized('Wrong Password.');
                }
            } 
            else{
                return $this->respondUnauthorized('email not confirmed.');
            }
        } 
    }

    public function managerLogin(){
            $type = DB::table('users')->where('email', Input::get('username'))->pluck('type');
            if (DB::table('users')->where('email', Input::get('username'))->count() == 0){
                return $this->respondInternalError('User does not exist.');
            }
            else{
                    if($type == 'manager'){

                        if (Auth::attempt(array('email'=>Input::get('username'), 'password'=>Input::get('password')))) {                        
                            
                            return $this->setStatusCode(200)->respond([
                                'userid'     => Auth::user()->id,
                                'name'       => Auth::user()->name,
                                'email'      => Input::get('username'),
                                'status_code'=> $this->getStatusCode(),
                                'token'      => Authorizer::issueAccessToken()
                               
                            ]);
                            //return Response::json(Authorizer::issueAccessToken());                                                   
                        }
                        else{
                            return $this->respondUnauthorized('Wrong Password.');
                        }
                    }
                    else if($type == 'admin'){
                        
                        if (Auth::attempt(array('email'=>Input::get('username'), 'password'=>Input::get('password')))) {                        
                            
                            return $this->setStatusCode(200)->respond([
                                'userid'     => Auth::user()->id,
                                'name'       => Auth::user()->name,
                                'email'      => Input::get('username'),
                                'status_code'=> $this->getStatusCode(),
                                'token'      => Authorizer::issueAccessToken()
                            ]);
                            //return Response::json(Authorizer::issueAccessToken());                                                    

                        
                        
                        }
                        else{
                            return $this->respondUnauthorized('Wrong Password.');
                        }
                    }                

                    else{
                        return $this->respondUnauthorized('Invalid user type.');
                    }
            }              
    }
}   