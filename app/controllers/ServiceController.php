<?php
class ServiceController extends \BaseController {

	public function addDepartmentGroup(){
	 	$status = 'FAILED';
	    $msg = '';


	    $deptGroup = DepartmentGroup::all();
	    $count = 0;
	    foreach ($deptGroup as $d) {
	    	if ($d->groupid == Input::get('groupid') && $d->deptid == Input::get('deptid')) {
	    		$count = 1;
	    	}
	    }

	    if ($count == 0) {
	    	
	    	$try = new DepartmentGroup;
	    	$try->groupid = Input::get('groupid');
	    	$try->deptid = Input::get('deptid');
	    	$try->save();

		    $msg = 'Success';
		    $status = 'SUCCESS';

			$groupName = Group::where('id',Input::get('groupid'))->pluck('groupname'); 
			$deptName = Department::where('id',Input::get('deptid'))->pluck('name');   

		    $deptData = '<tr bgcolor="#E2E2E2"><td>'.$try->id.'</td><td>'.$groupName.'</td><td>'.$deptName.'</td><td></td>';
	    }

	    return array(
	        'status' => $status,
	        'msg' 	 => $msg,
	        'ddata'  =>	$deptData
	    );
	}
	
	public function deleteDGroup(){
		DepartmentGroup::where('id', Input::get('dGroupid'))->delete();
	}

	public function deleteDepartment(){
		$getstoreid = DB::table('departments')->where('id', Input::get('deptId'))->pluck('storeid');
		$count = DB::table('departments')->where('storeid', $getstoreid)->count();

		if($count <= 1){

			$storeid = DB::table('departments')->where('id', Input::get('deptId'))->pluck('storeid');
			DB::table('stores')->where('id', $storeid)->update(array('hasDept' => 0));
			DepartmentGroup::where('deptid',Input::get('deptId'))->delete();
			Department::where('id', Input::get('deptId'))->delete();

		}
		else{
			DepartmentGroup::where('deptid',Input::get('deptId'))->delete();
			Department::where('id', Input::get('deptId'))->delete();
		}

	}

	public function addDepartment(){
		$msg = '';
		$status = 'FAILED';

		$departments = Department::all();
		$count = 0;

		foreach ($departments as $d) {
		 	if ($d->name == Input::get('deptname') && $d->storeid == Input::get('storeAdd')) {
		 		$count =+ 1;
		 	}
		 } 

		if ($count == 0) {
			Department::insert(array(
	        'name' => Input::get('deptname'),
	        'storeid' => Input::get('storeAdd'),
	        'qGroupId' => Input::get('questionAdd')
		    ));

		   	DB::table('stores')->where('id', Input::get('storeAdd'))->update(array('hasDept' => 1));
		    $msg = 'Department Added Successfully!';
		    $status = 'SUCCESS';
		}else{
			$msg = "Duplicates are not allowed";
		}

		return array(
			'status' => $status, 
			'msg'	 =>	$msg
		);
	}

	public function addUserGroupConnection(){
	 	$status = 'FAILED';
	    $msg = '';

	    $userG = UserGroup::all();
	    $count = 0;
	    foreach ($userG as $u) {
	    	if ($u->groupid == Input::get('groupid') && $u->userid == Input::get('userid')) {
	    		$count =+ 1;
	    	}
	    }

	    if ($count == 0) {
	    	UserGroup::insert(array(
	        'groupid' => Input::get('groupid'),
	        'userid' => Input::get('userid')
		    ));
		    $msg = 'Success';
		    $status = 'SUCCESS';
	    }

	    return array(
	        'status' => $status,
	        'msg' => $msg
	    );
	}

	public function create(){
		$userData = '';
	    $msg = '';
	    $status = 'FAILED';
	    if (strlen(Input::get('name')) == '') {
	    	$msg = 'Name Field is Required';
	    }
	    elseif(strlen(Input::get('email')) == '' && strlen(Input::get('password')) == ''){
	    	$msg = 'Email and Password fields are required';
	    }elseif(strlen(Input::get('password')) < 7){
	        if($msg == ''){
	            $msg = 'Password must be at least 7 characters';
	        }
	        else{
	            $msg .= ' and password must be<br/> at least 7 characters';
	        }
	    }elseif(!filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL)){
	    	if($msg == ''){
	        	$msg = 'Invalid email';
	    	}
	        else{
	            $msg .= ' and Invalid email';
	        }
	    }
	    elseif(User::where('email', Input::get('email'))->count() == 1){
	        $msg = 'Email is already taken';
	    }
	    elseif(strlen(Input::get('mobile')) == ''){
	    	$msg = 'Mobile Number is required';
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    
	    if($status == 'SUCCESS'){
	    	
	    	if(Input::get('actType') == 'manager'){
		        $dataArray = [
							    ['email' => Input::get('email'),
					             'password' => Hash::make(Input::get('password')),
					             'name' => Input::get('name'),
					             'type' => Input::get('actType'),
					             'confirmation' => '1',
					             'mobile' =>  Input::get('mobile'),
					             'district_manager' => Input::get('checkvalue')
							    ]
							 ];
							 
				$user= new ClientRegister;
                $user->id = Input::get('email');
                $user->secret = '12345';
                $user->name = Input::get('name');
                $user->save();
				$ids = [];

				foreach($dataArray as $data){
						$ids[] = DB::table('users')->insertGetId($data);
				}
					UserGroup::insert(array(
							array(
									'userid' => $ids[0],
									'groupid' 
									=> Input::get('getGroup')
								)
						));
			}else{
				$dataArray = [
							    ['email' => Input::get('email'),
					             'password' => Hash::make(Input::get('password')),
					             'name' => Input::get('name'),
					             'type' => Input::get('actType'),
					             'confirmation' => '1',
					             'mobile' =>  Input::get('mobile'),
					             'district_manager' => Input::get('checkvalue')

							    ]
	
							 ];

				$user= new ClientRegister;
                $user->id = Input::get('email');
                $user->secret = '12345';
                $user->name = Input::get('name');
                $user->save();
				$ids = [];

				foreach($dataArray as $data){
						$ids[] = DB::table('users')->insertGetId($data);
				}
			}

			$ucfInput = ucfirst(Input::get('actType'));

			if(Input::get('actType') == 'manager' && Input::get('checkvalue') == '1'){
					$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.'District Manager'.'</td><td></td>';
			}elseif(Input::get('actType') == 'manager' && Input::get('checkvalue') != '1'){
				$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.$ucfInput.'</td><td></td>';
			}elseif(Input::get('actType') == 'admin' && Input::get('checkvalue') != '1'){
				$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.'Administrator'.'</td><td></td>';
			}else{
				$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.$ucfInput.'</td><td></td>';
			}
	    }
	    return array(
	        'msg' => $msg,
	        'status' => $status,
	        'user' => $userData
	    );
	}

	public function delete(){
		
		$email = DB::table('users')->where('id', Input::get('userId'))->pluck('email');
		DB::table('oauth_clients')->where('id', $email)->delete();
		User::where('id', Input::get('userId'))->delete();
	}

	public function edit(){

		$msg = '';
	    $status = 'FAILED';
	    
	    $count = count(DB::table('users')->where('email', Input::get('emailHidden_'.Input::get('userIdEdit')))->first());
	    $id = Input::get('userIdEdit');
		$email = DB::table('users')->find($id);
		$oldEmail = $email->email;

	    if(Input::get('emailHidden_'.Input::get('userIdEdit')) == ''){
		    $msg = 'Email cannot be empty';
	    }
		else if(!filter_var(Input::get('emailHidden_'.Input::get('userIdEdit')), FILTER_VALIDATE_EMAIL)){
			$msg = 'Invalid email';
		}
		else if($count != 0 && Input::get('emailHidden_'.Input::get('userIdEdit')) != $oldEmail)
		{
			$msg = 'Email Address Already Taken';
		}
		else
		{
			$status = 'SUCCESS';
		}

	    if($status == 'SUCCESS'){
	    	$email = DB::table('users')->where('id', Input::get('userIdEdit'))->pluck('email');
	    	DB::table('oauth_clients')->where('id', $email)->update( array(
	            'id' => Input::get('emailHidden_'.Input::get('userIdEdit')),
	            'name' => Input::get('emailHidden_'.Input::get('userIdEdit')),
	  
	        ));

	        User::where('id', Input::get('userIdEdit'))->update( array(
	            'email' => Input::get('emailHidden_'.Input::get('userIdEdit')),
	            'type' => Input::get('typeHidden_'.Input::get('userIdEdit')),
	            'name' => Input::get('nameHidden_'.Input::get('userIdEdit')),
	            'mobile' => Input::get('mobileHidden_'.Input::get('userIdEdit')),
	            'district_manager' => Input::get('districtHidden_'.Input::get('userIdEdit'))
	        ));

	    }

	    return array(
	        'msg' => $msg,
	        'status' => $status,
	        'id' => Input::get('userIdEdit')
	    );
	}

	public function addStore(){
		$status = 'FAILED';
	    $msg = '';
	    $string = Input::all();

	    $rules = array(
		    'storeid'  => 'alpha_num',		    
		);

		$validation = Validator::make($string, $rules);
 
	   	if (Input::get('storename') == ' ' || Input::get('storename') == '' || trim(Input::get('storename')) == ''){
	    	$msg = 'Please input store name';
	   	}
	    elseif(Input::get('storeadd') == ' ' || Input::get('storeadd') == '' || trim(Input::get('storeadd')) == ''){
	        $msg = 'Please input address';
	    }
	    elseif(Input::get('storeid') == ' ' || Input::get('storeid') == '' || trim(Input::get('storeid')) == ''){
	        if($msg == ''){
	            $msg = 'Please input store id';
	        }
	        else{
	            $msg .= ' and store id';
	        }
	    }elseif(Store::where('id', Input::get('storeid'))->count() == 1){
	        $msg = 'Store id already exist';
	    }
	    elseif ($validation->fails()){
		    $msg = 'Store id should not contain special characters';
		}else{
	    	$status = 'SUCCESS';
		}

	    if($status == 'SUCCESS'){
	        Store::insert(array(
	            'id' => Input::get('storeid'),
	            'storename' => Input::get('storename'),
	            'address' => Input::get('storeadd')
	        ));
	        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.Input::get('storeid').'</p></td>'.'<td><p>'.Input::get('storename').'</p></td><td><p>'.Input::get('storeadd').'</p></td><td></td></tr>';
	    }

	    return array(
	        'status' => $status,
	        'msg' => $msg
	    );
	}

	public function deleteStore(){

		Store::where('id', Input::get('storeId'))->delete();
	}

	public function editStore(){
		$msg = '';
	    $status = 'FAILED';

	    if(Input::get('hiddenStorename_'.Input::get('storeid')) == ''){
	        $msg = 'Store name cannot be empty';
	    }
	    else if(Input::get('hiddenAddress_'.Input::get('storeid')) == ''){
	        if($msg == ''){
	            $msg = 'Address cannot be empty';
	        }
	        else{
	            $msg = 'Store name and address cannot be empty';
	        }
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {
	        Store::where('id', Input::get('storeid'))->update(array(
	            'storename' => Input::get('hiddenStorename_'.Input::get('storeid')),
	            'address' => Input::get('hiddenAddress_'.Input::get('storeid'))
	        ));
	    }

	    return array(
	        'msg' => $msg,
	        'status' => $status
	    );
	}

	public function addGroup(){
		
		$status = 'FAILED';
	    $msg = '';

	    if (Input::get('nameStore') == ' ' || Input::get('nameStore') == '' || trim(Input::get('nameStore')) == ''){
	    	$msg = 'Please input group name';
	    }
		else if(Group::where('groupname', Input::get('nameStore'))->count() == 1){
	    	$msg = 'Group name already taken';
		}
	    elseif(Input::get('nameStore') == ' '){
	    	$msg = 'Group name field cannot be empty';
	    }
	    else{
	    	$status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {

	        $dataArray = [
							['groupname' => Input::get('nameStore')]
  
						 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('groups')->insertGetId($data);
					}

					$msg = '<tr ><td>'.$ids[0].'</td><td>'.Input::get('nameStore').'</td><td></td><td></td></tr>';
	    }

	    return array(
	        'status' => $status,
	        'msg' => $msg
	    );
	}

	public function deleteGroup(){
		//dd(Input::get('groupId'));
		Group::where('id', Input::get('groupId'))->delete();
	}

	public function editGroup(){
		$msg = '';
	    $status = 'FAILED';

	    if(Input::get('hiddenGroupname_'.Input::get('groupid')) == ''){
	        $msg = 'Group name cannot be empty';
	    }
	    elseif(Group::where('groupName',Input::get('hiddenGroupname_'.Input::get('groupid')))->count() == 1){
	    	$msg = 'Group name already taken';
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {
	        Group::where('id', Input::get('groupid'))->update(array(
	            'groupName' => Input::get('hiddenGroupname_'.Input::get('groupid'))
	            
	        ));
	    }

	    return array(
	        'msg' => $msg,
	        'status' => $status
	    );
	}

	public function deleteDetail(){
		
		DB::table('store-groups')->where('id',Input::get('groupid'))->delete();
	}

	function myFunction() 
	{
    location.reload();
	}

	public function addDetail(){
			
	    $msg = '';
	    $dataArray = [
						[	

							'groupid' => Input::get('gId'),
	            			'storeid' => Input::get('selected')
	         			]
   					 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('store-groups')->insertGetId($data);
					}

	        $msg = '<tr><td>'.$ids[0].'</td><td>'.Input::get('selected_name').'</td><td></td></tr>';

	    return array(	        
	        'msg' => $msg
	    );
	}

	public function editUserGroup(){
		$msg = '';
	        UserGroup::where('userid', Input::get('userid'))->update(array(
	            'groupid' => Input::get('groupHidden_'.Input::get('userid'))
	            
	        ));

	    return array(
	        'msg' => $msg,
	    );
	}

	public function deleteUserGroup(){
		UserGroup::where('id', Input::get('userId'))->delete();
	}

	public function addUserGroup(){
		$status = 'FAILED';
	    $msg = '';

		if(Group::where('groupname', '!=', Input::get('gname'))){
	        $status = 'SUCCESS';
		}
	    if($status == 'SUCCESS')
	    {
	        Group::insert(array(
	            
	            'groupname' => Input::get('gname'),
	            
	        ));

	        $msg = '<tr bgcolor="#E2E2E2"><td></td><td>'.Input::get('gname').'</p></td><td><div id="actionButtons1_'.Input::get('groupid').'"><a href="#" style="color : #27AE60" onclick="initEdit1('.Input::get('groupid').')"><span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span></a><form id="actionForm_{{ $group->id }}" method="POST" action="" style="display: inline;"><a href="#" style="color : #C0392B" onclick="deleteUser1("actionForm_'.Input::get('groupid').')"><span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span></a><input type="hidden" id="userId" name="userId" value="'.Input::get('groupid').'"></form></div></td></tr>';
	    }

	    return array(
	        'status' => $status,
	        'msg' => $msg
	    );
	}

	public function deleteQuestion(){
		Question::where('id', Input::get('qId'))->delete();
		QuestionGroup::where('qid', Input::get('qId'))->delete();
	}
			
	public function editQuestion(){
		$msg = '';
	    $status = 'FAILED';
	    $string = Input::all();

	    $rules = array(
		    'orderId'  => 'numeric',
		    
		);

		$validation = Validator::make($string, $rules);

	    if(Input::get('hiddenOrder_'.Input::get('qid')) == ''){
	        $msg = 'Order Id cannot be empty';
	    }
	    elseif(Input::get('hiddenOrder_'.Input::get('qid')) > 5){
	    	$msg = 'Order Id cannot be greater than 5';
	    }
	    elseif(Input::get('hiddenOrder_'.Input::get('qid')) < 1){
	    	$msg = 'Invalid Order Id';
	    }
	    elseif ($validation->fails())
		{
		    $msg = 'Order Id should be numeric';
		}
	    else if(Input::get('hiddenQuestion_'.Input::get('qid')) == ''){
	        if($msg == ''){
	            $msg = 'Question cannot be empty';
	        }
	        else{
	            $msg = 'Order Id and Question cannot be empty';
	        }
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {
	        Question::where('id', Input::get('qid'))->update(array(
	            'orderid' => Input::get('hiddenOrder_'.Input::get('qid')),
	            'question' => Input::get('hiddenQuestion_'.Input::get('qid'))
	        ));

	        QuestionGroup::where('qid', Input::get('qid'))->update(array(
	            'qgroupid' => Input::get('hiddenQuestionGroup_'.Input::get('qid'))       
	        ));
	    }

	    return array(
	        'msg' => $msg,
	        'status' => $status
	    );
	}

	public function addQuestion(){
		$status = 'FAILED';
	    $msg = '';

	    $quesGrp = Input::get('getGroup');
	    $num = DB::table('question-groups')->where('qgroupid', '=', $quesGrp)->count();
	    $count1 = 5-$num;

	    if($num == 0)
	    {
	    	if((trim(Input::get('qname1')) != '' && Input::get('qNum') == 1) || (trim(Input::get('qname1')) != '' && trim(Input::get('qname2')) != '' && Input::get('qNum') == 2) ||
		    	(trim(Input::get('qname3')) != '' && trim(Input::get('qname2')) != '' && trim(Input::get('qname1')) != '' && Input::get('qNum') == 3) || 
		    	(trim(Input::get('qname4')) != '' && trim(Input::get('qname3')) != '' && trim(Input::get('qname2')) != '' && trim(Input::get('qname1')) != '' && Input::get('qNum') == 4) ||
		    	(trim(Input::get('qname5')) != '' && trim(Input::get('qname4')) != '' && trim(Input::get('qname3')) != '' && trim(Input::get('qname2')) != '' && trim(Input::get('qname1')) != '' && Input::get('qNum') == 5))
			    {
			    	$status = 'SUCCESS';
			    }
			else if(Input::get('qname1') == ' ' || trim(Input::get('qname1')) == '' || Input::get('qname2') == ' ' || trim(Input::get('qname2')) == '' || 
		    	Input::get('qname3') == ' ' || trim(Input::get('qname3')) == '' || Input::get('qname4') == ' ' || trim(Input::get('qname4')) == '' ||
		    		Input::get('qname5') == ' ' || trim(Input::get('qname5')) == '')
		    {
		    	$msg = 'Question field/s cannot be Empty';
		    }
		}

	    else if($num != 0)
	    {
	    	$grp = DB::table('question-groups')->where('qgroupid', '=', $quesGrp)->max('qid');
		    $maxid = DB::table('questions')->where('id', '=', $grp)->first();
		    $maxordid = $maxid->orderId;

	    	if(Input::get('qNum') > $count1)
	    	{
	    		$msg = 'Up to 5 questions only per group';
	    	}
		    else if((trim(Input::get('qname1')) != '' && Input::get('qNum') == 1) || (trim(Input::get('qname1')) != '' && trim(Input::get('qname2')) != '' && Input::get('qNum') == 2) ||
		    	(trim(Input::get('qname3')) != '' && trim(Input::get('qname2')) != '' && trim(Input::get('qname1')) != '' && Input::get('qNum') == 3) || 
		    	(trim(Input::get('qname4')) != '' && trim(Input::get('qname3')) != '' && trim(Input::get('qname2')) != '' && trim(Input::get('qname1')) != '' && Input::get('qNum') == 4) ||
		    	(trim(Input::get('qname5')) != '' && trim(Input::get('qname4')) != '' && trim(Input::get('qname3')) != '' && trim(Input::get('qname2')) != '' && trim(Input::get('qname1')) != '' && Input::get('qNum') == 5))
		    {
		    	$status = 'SUCCESS';
		    }
		    else if(Input::get('qname1') == ' ' || trim(Input::get('qname1')) == '' || Input::get('qname2') == ' ' || trim(Input::get('qname2')) == '' || 
		    	Input::get('qname3') == ' ' || trim(Input::get('qname3')) == '' || Input::get('qname4') == ' ' || trim(Input::get('qname4')) == '' ||
		    		Input::get('qname5') == ' ' || trim(Input::get('qname5')) == '')
		    {
		    	$msg = 'Question field/s cannot be Empty';
		    }
		}

	    if($status == 'SUCCESS')
	    {
	    	if(Input::get('qNum') == 1 && $num == 0)
			{

		       	$dataArray = [
							    ['orderId' => Input::get('orderId1'), 'question' => Input::get('qname1')]

							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(
							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								)
						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.Input::get('orderId1').'</p></td><td><p>'.Input::get('qname1').'</p></td><td><div id="actionButtons4_'.Input::get('qid').'"></div></td></tr>';
	    	}

	    	if(Input::get('qNum') == 1 && $num != 0)
			{
				$OrderID1 = $maxordid + 1;

		       	$dataArray = [
							    ['orderId' => $OrderID1, 'question' => Input::get('qname1')]   
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(
							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								)
						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.$OrderID1.'</p></td><td><p>'.Input::get('qname1').'</p></td><td><div id="actionButtons4_'.Input::get('qid').'"></div></td></tr>';
	    	}

	    	if(Input::get('qNum') == 2 && $num == 0)
			{

				$dataArray = [
							    ['orderId' => Input::get('orderId1'), 'question' => Input::get('qname1')],  
							    ['orderId' => Input::get('orderId2'), 'question' => Input::get('qname2')]				    
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(

							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								)
						));


		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.Input::get('orderId1').'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.Input::get('orderId2').'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td></tr>';
	    	}

	    	if(Input::get('qNum') == 2 && $num != 0)
			{
				$OrderID1 = $maxordid + 1;
				$OrderID2 = $OrderID1 + 1;

				$dataArray = [
							    ['orderId' => $OrderID1, 'question' => Input::get('qname1')], 
							    ['orderId' => $OrderID2, 'question' => Input::get('qname2')]
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(
							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								)
						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.$OrderID1.'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.$OrderID2.'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td></tr>';
	    	}

	    	if(Input::get('qNum') == 3 && $num == 0)
			{
		        $dataArray = [
							    ['orderId' => Input::get('orderId1'), 'question' => Input::get('qname1')],
							    ['orderId' => Input::get('orderId2'), 'question' => Input::get('qname2')],
							    ['orderId' => Input::get('orderId3'), 'question' => Input::get('qname3')]   
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(

							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[2],
									'qgroupid' => Input::get('getGroup')
								)
						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.Input::get('orderId1').'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td><td><input type="hidden" id="userId" name="userId" value="'.Input::get('qid').'"></form></div></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.Input::get('orderId2').'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td><td><input type="hidden" id="userId" name="userId" value="'.Input::get('qid').'"></form></div></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[2].'</p></td>'.'<td><p>'.Input::get('orderId3').'</p></td><td><p>'.Input::get('qname3').'</p></td><td></td><td></td></tr>';
	    	}

	    	if(Input::get('qNum') == 3 && $num != 0)
			{
				$OrderID1 = $maxordid + 1;
				$OrderID2 = $OrderID1 + 1;
				$OrderID3 = $OrderID2 + 1;

		        $dataArray = [
							    ['orderId' => $OrderID1, 'question' => Input::get('qname1')],
							    ['orderId' => $OrderID2, 'question' => Input::get('qname2')],
							    ['orderId' => $OrderID3, 'question' => Input::get('qname3')]   
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(

							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[2],
									'qgroupid' => Input::get('getGroup')
								)


						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.$OrderID1.'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td><td><input type="hidden" id="userId" name="userId" value="'.Input::get('qid').'"></form></div></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.$OrderID2.'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td><td><input type="hidden" id="userId" name="userId" value="'.Input::get('qid').'"></form></div></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[2].'</p></td>'.'<td><p>'.$OrderID3.'</p></td><td><p>'.Input::get('qname3').'</p></td><td></td><td></td></tr>';
	    	}

	    	 if(Input::get('qNum') == 4 && $num == 0)
			{
		        $dataArray = [
							    ['orderId' => Input::get('orderId1'), 'question' => Input::get('qname1')],
							    ['orderId' => Input::get('orderId2'), 'question' => Input::get('qname2')],
							    ['orderId' => Input::get('orderId3'), 'question' => Input::get('qname3')],
							    ['orderId' => Input::get('orderId4'), 'question' => Input::get('qname4')]
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(

							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[2],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[3],
									'qgroupid' => Input::get('getGroup')
								)


						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.Input::get('orderId1').'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.Input::get('orderId2').'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[2].'</p></td>'.'<td><p>'.Input::get('orderId3').'</p></td><td><p>'.Input::get('qname3').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[3].'</p></td>'.'<td><p>'.Input::get('orderId4').'</p></td><td><p>'.Input::get('qname4').'</p></td><td></td></tr>';
	    	}

	    	if(Input::get('qNum') == 4 && $num != 0)
			{
				$OrderID1 = $maxordid + 1;
				$OrderID2 = $OrderID1 + 1;
				$OrderID3 = $OrderID2 + 1;
				$OrderID4 = $OrderID3 + 1;

		        $dataArray = [
							    ['orderId' => $OrderID1, 'question' => Input::get('qname1')],
							    ['orderId' => $OrderID2, 'question' => Input::get('qname2')],
							    ['orderId' => $OrderID3, 'question' => Input::get('qname3')],
							    ['orderId' => $OrderID4, 'question' => Input::get('qname4')]
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(

							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[2],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[3],
									'qgroupid' => Input::get('getGroup')
								)


						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.$OrderID1.'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.$OrderID2.'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[2].'</p></td>'.'<td><p>'.$OrderID3.'</p></td><td><p>'.Input::get('qname3').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[3].'</p></td>'.'<td><p>'.$OrderID4.'</p></td><td><p>'.Input::get('qname4').'</p></td><td></td></tr>';
	    	}

	    	 if(Input::get('qNum') == 5 && $num == 0)
			{
		        $dataArray = [
							    ['orderId' => Input::get('orderId1'), 'question' => Input::get('qname1')],
							    ['orderId' => Input::get('orderId2'), 'question' => Input::get('qname2')],
							    ['orderId' => Input::get('orderId3'), 'question' => Input::get('qname3')],
							    ['orderId' => Input::get('orderId4'), 'question' => Input::get('qname4')],
							    ['orderId' => Input::get('orderId5'), 'question' => Input::get('qname5')]
							 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('questions')->insertGetId($data);
					}

					QuestionGroup::insert(array(

							array(
									'qid' => $ids[0],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[1],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[2],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[3],
									'qgroupid' => Input::get('getGroup')
								),

							array(
									'qid' => $ids[4],
									'qgroupid' => Input::get('getGroup')
								)


						));

		        $msg = '<tr bgcolor="#E2E2E2"><td><p>'.$ids[0].'</p></td>'.'<td><p>'.Input::get('orderId1').'</p></td><td><p>'.Input::get('qname1').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[1].'</p></td>'.'<td><p>'.Input::get('orderId2').'</p></td><td><p>'.Input::get('qname2').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[2].'</p></td>'.'<td><p>'.Input::get('orderId3').'</p></td><td><p>'.Input::get('qname3').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[3].'</p></td>'.'<td><p>'.Input::get('orderId4').'</p></td><td><p>'.Input::get('qname4').'</p></td><td></td></tr><tr bgcolor="#E2E2E2"><td><p>'.$ids[4].'</p></td>'.'<td><p>'.Input::get('orderId5').'</p></td><td><p>'.Input::get('qname5').'</p></td><td></td></tr>';
	    	}
	    }

	    return array(
	        'status' => $status,
	        'msg' => $msg
	    );
	}

	public function deleteQuestionGroup(){		
		Qgroup::where('id', Input::get('qId'))->delete();
	}

	public function deleteQuestionGroupDetail(){

        $qid = Input::get('qId');
        $count = Input::get('count');
        $erasedOrderId = Input::get('erasedOrderId');
        $groupid = Input::get('groupid');

        if($count == 5){
        	if($erasedOrderId == 5){
				QuestionGroup::where('qid', Input::get('qId'))
				->delete();
			}
			else if($erasedOrderId == 4){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 5)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
			else if($erasedOrderId == 3){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 4)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 5)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
			else if($erasedOrderId == 2){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 3)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 4)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 5)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
			else if($erasedOrderId == 1){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 2)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 3)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 4)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 5)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
		}
		else if($count == 4){
        	if($erasedOrderId == 4){
				QuestionGroup::where('qid', Input::get('qId'))
				->delete();
			}
			else if($erasedOrderId == 3){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 4)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
			else if($erasedOrderId == 2){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 3)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 4)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
			else if($erasedOrderId == 1){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 2)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 3)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 4)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
		}
		else if($count == 3){
        	if($erasedOrderId == 3){
				QuestionGroup::where('qid', Input::get('qId'))
				->delete();
			}
			else if($erasedOrderId == 2){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 3)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
			else if($erasedOrderId == 1){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 2)
				->decrement('questions.orderId');

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 3)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
		}
		else if($count == 2){
        	if($erasedOrderId == 2){
				QuestionGroup::where('qid', Input::get('qId'))
				->delete();
			}
			else if($erasedOrderId == 1){

				DB::table('question-groups')
				->join('questions', 'questions.id', '=', 'question-groups.qid')
				->where('question-groups.qgroupid', $groupid)
				->where('questions.orderId', '=', 2)
				->decrement('questions.orderId');

				QuestionGroup::where('qid', Input::get('qId'))->delete();
			}
		}
		else if($count == 1){
        	if($erasedOrderId == 1){
				QuestionGroup::where('qid', Input::get('qId'))
				->delete();
			}
		}
	}

	public function editQuestionGroup(){
		// dd(Input::all());
		$i = Input::get('qHidden_'.Input::get('qid'));
	   
				Qgroup::where('id', Input::get('qid'))->update(array(
					            'groupName' => Input::get('qHidden1_'.Input::get('qid'))          
					        ));
	}

	public function editQuestionGroupDetail(){
		// dd(Input::all());
		QuestionGroup::where('id', Input::get('qid'))->update(array(
			'qgroupid' => Input::get('qHidden_'.Input::get('qid'))		            
			));
	}

	public function addQuestionGroupDetail(){
		// dd(Input::all());
		$status = 'FAILED';
	    $msg = '';
	    $qid = [];
	    $i = 0;
	    $ord = 1;

	    $count2 = DB::table('question-groups')->where('qgroupid', '=', Input::get('qValId'))->count();

	    if($count2 >= 5)
	    {
	    	$msg = 'Up to 5 questions Only';
	    }
	    else if(Input::get('qval') == ' ')
	    {
	    	$msg = 'No Question selected';
	    }
	  	else{
	  		$status = 'SUCCESS';
	  	}

	  	if($status == 'SUCCESS')
	  	{
	        $dataArray = [
							['qid' => Input::get('qval'), 
							 'qgroupid' => Input::get('qValId')	
							]
						 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('question-groups')->insertGetId($data);
					}

	  		$ques = DB::table('question-groups')->where('qgroupid', '=', Input::get('qValId'))->get();
	  		
	  		foreach ($ques as $key) 
	  		{
	  			$qid[] = $key->qid;
	  			DB::table('questions')->where('id', $qid[$i])->update(array('orderId' => $ord));
	  			$i++;
	  			$ord++;
	  		}

	        $msg = '<tr><td>'.$ids[0].'</td><td>'.Input::get('qname').'</td><td></td></tr>';
	    }

	    return array(
	        'status'=> $status,
	        'msg' => $msg
	    );				
	}

	public function addQuestionGroup(){
		// dd(Input::all());
		$status = 'FAILED';
	    $msg = '';

	    if (Input::get('qgname') == ' ' || Input::get('qgname') == '' || trim(Input::get('qgname')) == ''){
	    	$msg = 'Please input group name';
	    }
		else if(Qgroup::where('groupName', Input::get('qgname'))->count() == 1){
	    	$msg = 'Group name already taken';
		}
	    elseif(Input::get('qgname') == ''){
	    	$msg = 'Group name field cannot be empty';
	    }
	    else{
	    	$status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {

	        $dataArray = [
							['groupName' => Input::get('qgname')]
  
						 ];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('qgroups')->insertGetId($data);
					}

	        $msg = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('qgname').'</td><td></td><td></td></tr>';
	    }

	    return array(
	        'status' => $status,
	        'msg' => $msg
	    );
	}

	public function sample()
	{
		foreach($_GET['listItem'.Input::get('qid')] as $position =>$item)
		{
			Question::where('id', $item)->update(array('orderId' => $position)); 
		}
	}

	public function deleteQStoreGroup(){
		Store::where('qGroupId', Input::get('qgroupid'))->where('id',Input::get('sid'))->update(array('qGroupId' => ' '));
	}

	public function addQStoreGroup()
	{
		$msg = '';
	    $dataArray = [
						[	
					    	'qGroupId' => Input::get('getGroup1')
	            		]
  					];

				$ids = [];

				foreach($dataArray as $data)
					{
						$ids[] = DB::table('stores')->where('id',Input::get('getStore'))->update($data);
					}

	        $msg = '<tr bgcolor="#E2E2E2"><td>'.Input::get('getStorename').'</td><td>'.Input::get('getGroupname').'</td><td></td></tr>';
	    
	    return array(    
	        'msg' => $msg
	    );
	}
	
	public function addGroup2(){
		Input::merge(array_map('trim', Input::all()));
		
		$rules = array(
			'GroupName' => 'required|unique:groups,groupname',
			'StoreName' => 'required|unique:stores,storename',
			'StoreAddress' => 'required|unique:stores,address',
			'StoreID' => 'required|alpha_num|unique:stores,id',
			'latitude' => 'required|regex:/^[+-]?\d{1,3}\.\d{1,14}$/',
			'longitude' => 'required|regex:/^[+-]?\d{1,3}\.\d{1,14}$/'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful
		// insert to groups table
		$groupid = DB::table('groups')->insertGetId([
			'groupname' => Input::get('GroupName')
		]);

		// insert to stores table
		DB::table('stores')->insert([
			'id' => Input::get('StoreID'),
			'storename' => Input::get('StoreName'),
			'address' => Input::get('StoreAddress'),
			'hasDept' => Input::get('hasDept') == 'yes' ? 1 : 0 // if hasDept = yes, 1 else 0
		]);

		// insert to customers table
		$customerid = DB::table('customers')->insertGetId([
			'storename' => Input::get('StoreName'),
			'latitude' => Input::get('latitude'),
			'longitude' => Input::get('longitude')
		]);

		// insert to store-group connection
		DB::table('store-groups')->insertGetId([
			'groupid' => $groupid,
			'storeid' => Input::get('StoreID')
		]);

		return array(
			'success' => true,
			'storeid' => Input::get('StoreID'),
			'groupid' => $groupid,
			'name' => Input::get('GroupName'),
			'customerid' => $customerid,
			'storename' => Input::get('StoreName'),
			'address' => Input::get('StoreAddress'),
			'latitude' => Input::get('latitude'),
			'longitude' => Input::get('longitude'),
			'hasDept' => Input::get('hasDept')
		);
	}

	public function editGroup2(){
		Input::merge(array_map('trim', Input::all()));

		$rules = array(
			'GroupName' => 'required|unique:groups,groupname,'.Input::get('hiddengroupid'),
			'StoreName' => 'required|unique:stores,storename,'.Input::get('StoreID'),
			'StoreAddress' => 'required|unique:stores,address,'.Input::get('StoreID'),
			'latitude' => 'required|regex:/^[+-]?\d{1,3}\.\d{1,14}$/',
			'longitude' => 'required|regex:/^[+-]?\d{1,3}\.\d{1,14}$/'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful
		// update groups table
		DB::table('groups')->where('id', Input::get('hiddengroupid'))
			->update([ 'groupname' => Input::get('GroupName') ]);

		// update stores table
		DB::table('stores')->where('id', Input::get('StoreID'))
			->update([
				'storename' => Input::get('StoreName'),
				'address' => Input::get('StoreAddress'),
				'hasDept' => Input::get('hasDept') == 'yes' ? 1 : 0 // if hasDept = yes, 1 else 0
			]);

		// update customers table
		DB::table('customers')->where('id', Input::get('hiddencustomerid'))
			->update([
				'storename' => Input::get('StoreName'),
				'latitude' => Input::get('latitude'),
				'longitude' => Input::get('longitude')
			]);

		if(Input::get('hasDept') == 'no'){// delete departments
			// delete departments from department-groups
			$depts = DB::table('department-groups')->where('groupid', Input::get('hiddengroupid'))->get();
			DB::table('department-groups')->where('groupid', Input::get('hiddengroupid'))->delete();
			// delete departments
			foreach($depts as $dept){
				DB::table('departments')->where('id', $dept->deptid)->delete();
			}
		}

		return array(
			'success' => true,
			'groupid' => Input::get('hiddengroupid'),
			'name' => Input::get('GroupName'),
			'hasDept' => Input::get('hasDept'),
			'storename' => Input::get('StoreName'),
			'address' => Input::get('StoreAddress'),
			'latitude' => Input::get('latitude'),
			'longitude' => Input::get('longitude')
		);
	}

	public function addQuestion2(){
		Input::merge(array_map('trim', Input::all()));
		$output = [];

		$rules = array(
			'questiongroupname' => 'required|unique:qgroups,groupName',
		);

		for($i = 1; $i <= Input::get('qNum'); $i++){
			$rules['question'.$i] = 'required';
		}

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful
		// insert to qgroups
		$qgroupid = DB::table('qgroups')->insertGetId([
			'groupName' => Input::get('questiongroupname')
		]);

		// insert to questions
		for($i = 1; $i <= 5; $i++){
			if($i <= Input::get('qNum')){
				$id[$i] = DB::table('questions')->insertGetId([
					'orderId' => $i,
					'question' => Input::get('question'.$i)
				]);
				$output['id'.$i] = $id[$i];
				$output['q'.$i] = Input::get('question'.$i);
			}else{
				$output['id'.$i] = -1;
				$output['q'.$i] = '';
			}
		}

		// insert to question-groups
		for($i = 1; $i <= Input::get('qNum'); $i++){
			DB::table('question-groups')->insertGetId([
				'qid' => $id[$i],
				'qgroupid' => $qgroupid
			]);
		}

		// insert to store (qgroupsid)
		DB::table('stores')->where('id', Input::get('hiddenstoreid'))->update(['qGroupId' => $qgroupid]);

		$output['success'] = true;
		$output['id'] = $qgroupid;
		$output['name'] = Input::get('questiongroupname');
		$output['qnum'] = Input::get('qNum');

		return $output;
	}

	public function editQuestion2(){
		Input::merge(array_map('trim', Input::all()));
		$output = [];

		$rules = array(
			'questiongroupname' => 'required|unique:qgroups,groupName,'.Input::get('hiddenqgroupid')
		);

		for($i = 1; $i <= Input::get('qNum'); $i++){
			$rules['question'.$i] = 'required';
		}

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful
		// update qgroups
		DB::table('qgroups')->where('id', Input::get('hiddenqgroupid'))
			->update([
				'groupName' => Input::get('questiongroupname')
			]);

		for($j = 1; $j <= 5; $j++){
			DB::table('questions')->where('id', '=', Input::get('hiddenqid'.$j))->delete();	
			DB::table('question-groups')->where('qid', '=', Input::get('hiddenqid'.$j))->delete();	
		}

		// insert to questions
		for($i = 1; $i <= 5; $i++){
			if($i <= Input::get('qNum')){
				$id[$i] = DB::table('questions')->insertGetId([
					'orderId' => $i,
					'question' => Input::get('question'.$i)
				]);
				$output['id'.$i] = $id[$i];
				$output['q'.$i] = Input::get('question'.$i);
			}else{
				$output['id'.$i] = -1;
				$output['q'.$i] = '';
			}
		}

		// insert to question-groups
		for($i = 1; $i <= Input::get('qNum'); $i++){
			DB::table('question-groups')->insertGetId([
				'qid' => $id[$i],
				'qgroupid' => Input::get('hiddenqgroupid')
			]);
		}

		$output['success'] = true;
		$output['id'] = Input::get('hiddenqgroupid');
		$output['name'] = Input::get('questiongroupname');
		$output['qnum'] = Input::get('qNum');

		return $output;
	}

	public function addDept2(){
		Input::merge(array_map('trim', Input::all()));

		$rules = array(
			'dept' => 'required'
		);

		if(Input::get('groupid') == 'new'){
			$rules['newgroupinput'] = 'required|unique:groups,groupname';
		}

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful

		// if new group is created
		if(Input::get('groupid') == 'new'){
			// insert to groups table
			$groupid = DB::table('groups')->insertGetId([
				'groupname' => Input::get('newgroupinput')
			]);
		}else{
			$groupid = Input::get('groupid');
		}

		// insert to departments
		$deptid = DB::table('departments')->insertGetId([
			'name' => Input::get('dept'),
			'storeid' => Input::get('hiddenstoreid2'),
			'qGroupId' => Input::get('questiongroupid')
		]);
		// insert to department groups
		DB::table('department-groups')->insertGetId([
			'groupid' => $groupid,
			'deptid' => $deptid
		]);

		return array(
			'success' => true,
			'id' => $deptid,
			'name' => Input::get('dept'),
			'groupid' => $groupid,
			'groupname' => Input::get('newgroupinput'),
			'qgroupid' => Input::get('questiongroupid')
		);
	}


	public function editDept2(){
		Input::merge(array_map('trim', Input::all()));

		$rules = array(
			'dept' => 'required'
		);

		if(Input::get('groupid') == 'new'){
			$rules['newgroupinput'] = 'required|unique:groups,groupname';
		}

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful	
		// if new group is created
		if(Input::get('groupid') == 'new'){
			// insert to groups table
			$groupid = DB::table('groups')->insertGetId([
				'groupname' => Input::get('newgroupinput')
			]);
		}else{
			$groupid = Input::get('groupid');
		}

		// update departments
		DB::table('departments')->where('id', Input::get('hiddendeptid'))
			->update([
				'name' => Input::get('dept'),
				'qGroupId' => Input::get('questiongroupid')
			]);

		// insert to department groups
		DB::table('department-groups')->where('deptid', Input::get('hiddendeptid'))
			->update([
				'groupid' => $groupid,
			]);

		return array(
			'success' => true,
			'name' => Input::get('dept'),
			'id' => Input::get('hiddendeptid'),
			'groupid' => $groupid,
			'groupname' => Input::get('newgroupinput'),
			'qgroupid' => Input::get('questiongroupid')
		);
	}


	public function addUser2(){
		Input::merge(array_map('trim', Input::except('groupid2')));
		$rules = array(
			'username' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:7',
			'confirmpassword' => 'required|min:7|same:password',
			'mobilenum' => 'numeric',
			'groupid2' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful

		// insert to users table
		$userid = DB::table('users')->insertGetId([
			'name' => Input::get('username'),
			'email' => Input::get('email'),
			'mobile' => Input::get('mobilenum') == '' ? 0 : Input::get('mobilenum'),
			'password' => Hash::make(Input::get('password')),
			'type' => 'manager',
			'confirmation' => '1',
			'district_manager' => '0'
		]);
					             
		$user= new ClientRegister;
        $user->id = Input::get('email');
        $user->secret = '12345';
        $user->name = Input::get('username');
        $user->save();

		// insert to groups table
		$i = 0;
		$id = [];
		foreach(Input::get('groupid2') as $groupid){
			$id[$i++] = DB::table('user-groups')->insertGetId([
				'userid' => $userid,
				'groupid' => $groupid
			]);
		}

		return array(
			'success' => true,
			'id' => $userid,
			'name' => Input::get('username'),
			'email' => Input::get('email'),
			'mobile' => Input::get('mobilenum'),
			'groups' => Input::get('groupid2')
		);
	}

	public function editUser2(){
		Input::merge(array_map('trim', Input::except('groupid2')));
		$rules = array(
			'username' => 'required',
			'email' => 'required|email|unique:users,email,'.Input::get('hiddenuserid'),
			'password' => 'required|min:7',
			'confirmpassword' => 'required|min:7|same:password',
			'mobilenum' => 'numeric',
			'groupid2' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return array(
		        'success' => false,
		        'errors' => $validator->messages()->toJson()
		    );
		}

		// validation successful

		// insert to users table
		DB::table('users')->where('id', Input::get('hiddenuserid'))
			->update([
				'name' => Input::get('username'),
				'email' => Input::get('email'),
				'mobile' => Input::get('mobilenum') == '' ? 0 : Input::get('mobilenum'),
				'password' => Hash::make(Input::get('password')),
				'type' => 'manager'
			]);
		
		DB::table('user-groups')->where('userid', '=', Input::get('hiddenuserid'))->delete();

		// insert to groups table
		foreach(Input::get('groupid2') as $groupid){
			DB::table('user-groups')->insertGetId([
				'userid' => Input::get('hiddenuserid'),
				'groupid' => $groupid
			]);
		}

		return array(
			'success' => true,
			'name' => Input::get('username'),
			'id' => Input::get('hiddenuserid'),
			'email' => Input::get('email'),
			'mobile' => Input::get('mobilenum'),
			'groups' => Input::get('groupid2')
		);
	}
	
}
?>
