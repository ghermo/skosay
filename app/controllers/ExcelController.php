<?php
class ExcelController extends \BaseController {

	public function expexcel(){
			Excel::create('Reports', function ($excel) {

    		$excel->sheet('Sheetname', function ($sheet){
    			$sheet->setAutoSize(true);
      			$sheet->loadView('webservice.reportsexcel');    
    		});

		})->export('csv');
	}
}