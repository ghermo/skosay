<?php
class RegisterApiController extends \ApiController {

    public function mConfirmAccount($id){
        DB::table('users')->where('confirmation_code', $id)->update(array('confirmation' => '1'));
        $email = User::where('confirmation_code', $id)->pluck('email');
        return Redirect::to('login2')->with('email', $email);
    }

    public function Register(){
        {
            function detectDuplicate(){
                $randVar = str_random(20);
                $bool = DB::table('users')->where('confirmation_code', '=', $randVar)->count();
                if($bool == 0){
                    return $randVar;
                }
                else{
                    detectDuplicate();
                }
            }
                $unusedCode = detectDuplicate();    

                $bool = DB::table('users')->where('email', Input::get('email'))->count();
                if($bool == 0){
                    $input = Input::all();
                    $name = $input['name'];
                    $email = $input['email'];

                    $user = new Register;
                    $user->email = $email;
                    $user->name = $name;
                    $user->type = 'consumer';
                    $user->confirmation_code = $unusedCode;
                    $user->password = Hash::make($input['password']);
                    $user->confirmation = 0;
                    $user->save();

                    //oauth_client
                    $user= new ClientRegister;
                    $user->id = $email;
                    $user->secret = '12345';
                    $user->name = $name;
                    $user->save(); 

                    $confirmationCode = $unusedCode;
                    $url = URL::to('/').'/mConfirmAccount/'.$confirmationCode;
                    $data = ['url' => $url];

                    Mail::send('emails.template', $data, function($message) use($email){
                        $message->from('support@skosay.com', 'Skosay - Say it anywhere!');
                        $message->to($email)->subject('Skosay account confirmation');
                    });

                    return $this->respondCreated('Account Created');   
                }

                else{
                    return $this->respondInternalError('e-mail is already taken.');
                }
                return $responseArray;
            }
        }
    }
?>