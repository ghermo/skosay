<?php

class CustomersApiController extends \ApiController {

	public function addIndustry(){
		if(Input::get('name') == ''){
			return $this->setStatusCode(500)->respond([
				'message' => 'Name is required.',
        		'status_code' => $this->getStatusCode()
       		]);
		}
		else{
			$industry = new Industry;
        	$industry->name = Input::get('name');
        	$industry->save();

        	return $this->setStatusCode(201)->respond([
				'message' => 'Successfully Added.',
        		'status_code' => $this->getStatusCode()
       		]);
        }
	}

	public function editIndustry(){
		if(Input::get('editId') == ''){
			return $this->setStatusCode(500)->respond([
				'message' => 'Id is required.',
        		'status_code' => $this->getStatusCode()
       		]);
		}
		else{
			if(Input::get('nameEdit_'.Input::get('editId')) == ''){
				return $this->setStatusCode(500)->respond([
					'message' => 'Name is required.',
        			'status_code' => $this->getStatusCode()
       			]);
			}
			else{
				DB::table('industries')->where('id', Input::get('editId'))->update( array(
					'id' => Input::get('editId'),
	    		    'name' => Input::get('nameEdit_'.Input::get('editId')) 
	    		));

	    		return $this->setStatusCode(201)->respond([
					'message' => 'Successfully changed.',
        			'status_code' => $this->getStatusCode()
       			]);
	    	}
	    }
	}

	public function deleteIndustry(){
		if(Input::get('deleteId') == ''){
			return $this->setStatusCode(500)->respond([
        		'message' => 'deleteId is required.',
        		'status_code' => $this->getStatusCode()
        	]);
        }
        else{
        	industry::where('id', Input::get('deleteId'))->delete();
        	return $this->setStatusCode(201)->respond([
        		'message' => 'Industry deleted.',
        		'status_code' => $this->getStatusCode()
        	]);
        }
	}



	//Customer registration
	public function addCustomer(){
		//selected
		$SelectedTier = '';
		$SelectedName = '';
		$SelectedIndustryName = '';
		$storeId = '';
		$address = '';
		$latitude = '';
		$longitude = '';
	
		//P1 tiers
		$SelectedTier = Input::get('tier');
		//P2 name
		$SelectedName = Input::get('storeName');
		//P2 industry
		$SelectedIndustryId = DB::table('industries')->where('name', Input::get('industryName'))->pluck('id');
		//p3
		$qGroupName = $SelectedName;
		//p3
		$q1 = Input::get('question1');
		$q2 = Input::get('question2');
		$q3 = Input::get('question3');

		//error handler
		if($SelectedTier == ""){
			return $this->setStatusCode(500)->respond([
        		'message' => 'Tier is required.',
        		'status_code' => $this->getStatusCode()
        	]);
		}
		else if($SelectedName == ""){
			return $this->setStatusCode(500)->respond([
        		'message' => 'Name is required.',
        		'status_code' => $this->getStatusCode()
        	]);
		}
		else if($SelectedIndustryId == ""){
			return $this->setStatusCode(500)->respond([
        		'message' => 'Industry is required.',
        		'status_code' => $this->getStatusCode()
        	]);
		}
		else if($q1 == "" || $q2 == "" || $q3 == ""){
			return $this->setStatusCode(500)->respond([
        		'message' => 'Questions are required.',
        		'status_code' => $this->getStatusCode()
        	]);
		}
		//successful
		else{
			if(Input::get('longitude') == ""){
				$longitude = 0.00000000000000;
			}

			if(Input::get('latitude') == ""){
				$longitude = 0.00000000000000;
			}
			$allCount = DB::table('customers')->count();
			$address = Input::get('address');
			$storeId = Input::get('storeId');
			$latitude = Input::get('latitude');
			$longitude = Input::get('longitude');
			$date = new DateTime();
			$timeStamp = $date->format('Y-m-d H:i:s');

			$customer = new Customer;
           	$customer->storename = $SelectedName;
            $customer->longitude = $longitude;
            $customer->latitude = $latitude;
            $customer->created_at = $timeStamp;
            $customer->updated_at = $timeStamp;
            $customer->tiers_id = $SelectedTier;
            $customer->industries_id = $SelectedIndustryId;
            $customer->save();
            
            $store = new Store;
            $store->id = $storeId;
            $store->storename = $SelectedName;
            $store->address = $address;
            $store->created_at = $timeStamp;
            $store->updated_at = $timeStamp;
            $store->save();

            $group = new Qgroup;
            $group->groupName = $qGroupName;
            $group->save();

            //question 1
            $question = new Question;
            $question->orderId = 1;
            $question->question = $q1;
            $question->save();

            $maxid = DB::table('questions')->where('question', $q1)->max('id');
            $maxid2 = DB::table('qgroups')->where('groupName', $qGroupName)->max('id');

            $qgroup = new QuestionGroup;
            $qgroup->qid = $maxid;
            $qgroup->qgroupid = $maxid2;
            $qgroup->save();

            //question2
            $question = new Question;
            $question->orderId = 2;
            $question->question = $q2;
            $question->save();

            $maxid = DB::table('questions')->where('question', $q2)->max('id');
            $maxid2 = DB::table('qgroups')->where('groupName', $qGroupName)->max('id');

            $qgroup = new QuestionGroup;
            $qgroup->qid = $maxid;
            $qgroup->qgroupid = $maxid2;
            $qgroup->save();

            //question3
			$question = new Question;
            $question->orderId = 3;
            $question->question = $q3;
            $question->save();

            $maxid = DB::table('questions')->where('question', $q3)->max('id');
            $maxid2 = DB::table('qgroups')->where('groupName', $qGroupName)->max('id');

            $qgroup = new QuestionGroup;
            $qgroup->qid = $maxid;
            $qgroup->qgroupid = $maxid2;
            $qgroup->save();

            $questionAll[] = ['Question 1: '.$q1, 'Question 2: '.$q2, 'Question 3: '.$q3];

            //temporary return *start
            $newCount = DB::table('customers')->count();
            $resultCustomer = DB::table('customers')->where('id', $newCount)->get();
            $tierInfo = DB::table('tiers')->where('id', $SelectedTier)->get();
            $industryInfo = DB::table('industries')->where('id', $SelectedIndustryId)->get();
            
            return $this->setStatusCode(201)->respond([
        		'message' => 'Successfully Added.',
        		'Customer Info' => $resultCustomer,
        		'Tier Info' => $tierInfo,
        		'Industry Info' => $industryInfo,
        		'Question Info' => $questionAll,
        		'status_code' => $this->getStatusCode()
        	]);
        	//*stop


		}
	}
}
?>