<?php

class ClientRegister extends Eloquent {
        protected $guarded = array();
        // table name
        protected $table = 'oauth_clients';
        // to disable default timestamp fields 
        public $timestamps = 'false' ; 

        // model function to store form data to database
        public static function saveFormData($data)
        {
            DB::table('oauth_clients')->insert($data);
        }

}

?>