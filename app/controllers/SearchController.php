<?php

class SearchController extends \BaseController {

    public function init(){
        if(Auth::check() || isset($_COOKIE['fbId']) || isset($_COOKIE['guestId'])){
            return View::make('search.search');
        }
        else{
            return Redirect::to('/');
        }

    }

    public function searchUser(){

    	$tags = Input::get('search');
     	
    	return View::make('webservice.search.userpagesearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchAssign(){

    	$tags = Input::get('search');
     	
    	return View::make('webservice.search.assignsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchDept(){

    	$tags = Input::get('search');
     	
    	return View::make('webservice.search.deptsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchDeptGrp(){

    	$tags = Input::get('search');
     	
    	return View::make('webservice.search.deptgroupsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchStores(){

        $tags = Input::get('search');
        
        return View::make('webservice.search.storesearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchGroups(){

        $tags = Input::get('search');
        
        return View::make('webservice.search.groupsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchQuestions(){

        $tags = Input::get('search');
        
        return View::make('webservice.search.questionsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchQGroups(){

        $tags = Input::get('search');
        
        return View::make('webservice.search.qgroupsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

     public function searchQGroupsToStores(){

        $tags = Input::get('search');
        
        return View::make('webservice.search.qgrouptostoresearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function searchReports(){

        $tags = Input::get('search');
        
        return View::make('webservice.search.reportsearch')->with('tags', $tags)->with('placeholder', Input::get('search'));
    }

    public function search() {

        $q = Input::get('name');
        $env = Input::get('env');

        if ($q == NULL || $q == '' || $q == ' ') {
            $results = 'none';
        }else{


            $searchTerms = explode(' ', $q);

             $query = DB::table('stores');

            foreach($searchTerms as $term)
            {
                $query->where('storename', 'LIKE', '%'. $term .'%')
                    ->orWhere('address', 'LIKE', '%'. $term .'%')
                    ->where('domain',$env);

            }


            $count = $query->count();

            if ($count == 0) {
                $results = 'none';
            }else{
                $results = $query->get();
            }

    }
        return View::make('search.search',['results'=>$results]);

    }

}