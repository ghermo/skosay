<?php

class ManagerApiController extends \ApiController {
    
    public function viewAll(){
        $x = Authorizer::getResourceOwnerId();

        if(DB::table('users')->where('id', $x)->pluck('type') == 'manager'){
                            
            $stores = DB::table('stores')
            ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
            ->join('groups', 'groups.id', '=', 'store-groups.groupid')
            ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
            ->join('issues', 'issues.storeid', '=', 'stores.id')
            ->whereNotIn('issues.status',['archive'])
            ->where('user-groups.userid', Auth::user()->id)->groupBy('stores.id')->orderBy('issues.updated_at','DESC')->get();

            $storeDept = DB::table('stores')
            ->join('issues', 'issues.storeid', '=','stores.id')
            ->join('departments', 'departments.id', '=', 'issues.isDept')
            ->join('department-groups', 'department-groups.deptid', '=','departments.id')
            ->join('groups', 'groups.id', '=','department-groups.groupid')
            ->join('user-groups', 'user-groups.groupid', '=','groups.id')
            ->where('user-groups.userid', Auth::user()->id)
            ->whereNotIn('issues.status',['archive'])
            ->groupBy('stores.id')
            ->orderBy('issues.updated_at','DESC')
            ->get();

            return $this->setStatusCode(200)->respond([
                'message' => 'All locations.',
                'stores' => $stores,
                'storeDept' => $storeDept,
                'status_code' => $this->getStatusCode()
             ]);
        }

        else if(DB::table('users')->where('id', $x)->pluck('type') == 'admin'){
                $stores = DB::table('stores')
                    ->join('issues', 'issues.storeid', '=', 'stores.id')
                    ->whereNotIn('issues.status',['archive'])
                    ->groupBy('stores.id')
                    ->orderBy('issues.updated_at','DESC')->get();

                return $this->setStatusCode(200)->respond([
                'message' => 'All locations.',
                'stores' => $stores,
                'status_code' => $this->getStatusCode()
            ]);
        }                

        else{
            return $this->respondForbidden('Forbidden');
        } 
    }

    public function viewAllHistoryDetails($id){
        if(Auth::check()){
            if(Auth::user()->type == 'admin'){

                $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->join('departments', 'issues.isDept', '=', 'departments.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at','issues.isDept', 'departments.name')
                    ->where('stores.id', $id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                return $this->setStatusCode(200)->respond([
                    'message' => 'All issues list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);
            }else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->join('messages', 'messages.issueid', '=','issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.updated_at','issues.isDept', 'departments.name')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('messages.msgType', 'feedback')
                    ->where('issues.storeid','=',$id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at','issues.isDept')
                    ->where('messages.msgType', 'feedback')
                    ->where('stores.id', $id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }

                return $this->setStatusCode(200)->respond([
                    'message' => 'All issues list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);
            }
        }
    }

    public function message($id,$fid){
        $uid = DB::table('messages')->where('issueid',$fid)->join('users', 'users.id', '=', 'messages.userid')->pluck('messages.userid');
        $ui = DB::table('users')->where('id',$uid)->count();

        
            $messages = DB::table('messages')->where('issueid',$fid)->select('messages.id','messages.userid','messages.issueid','messages.message','messages.msgType','messages.photo','messages.msgStatus','messages.created_at','messages.updated_at')->get();

            if(StoreGroup::where('storeid', $id)->count() != 0 && Auth::user()->type == 'manager'){
                DB::table('messages')->where('issueid', $fid)->whereNotIn('userid', [Auth::user()->id])->update(array('msgStatus' => 'read'));
            }
            else if(DepartmentGroup::where('deptid', $id)->count() == 0 && Auth::user()->type == 'manager'){
                DB::table('messages')->where('issueid', $fid)->whereNotIn('userid', [Auth::user()->id])->update(array('msgStatus' => 'read'));
        }

        $stat = DB::table('issues')->where('id',$fid)->pluck('status');

        return $this->setStatusCode(200)->respond([
                'message' => 'View message.',
                'messages' => $messages,
                'issuestat' => $stat,
                'status_code' => $this->getStatusCode()
        ]);
    }

    public function store(){
        if(Auth::check()){
            $date = new DateTime();
            $timeStamp = $date->format('Y-m-d H:i:s');
            
            $type = DB::table('users')->where('id', Auth::user()->id)->first();
            $type = $type->type;

            if(Input::file('qwe') != null){
            if(Input::file('qwe')->getSize()<2097152){
                $rand = rand(0, 1000);
                $destinationPath = 'uploads/';

                $newImageName= $rand.'MyImage.jpg';

                //Rename and move the file to the destination folder 
                Input::file('qwe')->move($destinationPath,$newImageName);

                //$quality = 60;  
                //$file=Input::file('qwe');
                //$rand = rand(0, 1000);
                //$orFile = $file->getClientOriginalName(); 
                //$filename = $rand.$orFile;
                //$img=Image::make($file);
                //$path = public_path('uploads/' . $filename);
                //$img->resize(300, null, function ($constraint){$constraint->aspectRatio();});
                //$img->save($path, $quality);
                $fphoto = 'uploads/'.$newImageName;
            }
            }else{
                $fphoto = '';
            }

            if($type == 'admin' || 'manager'){
                $message = new Messages;
                
                $message->message = strip_tags(Input::get('reply'));
                $message->issueid = Input::get('issueid');
                $message->userid = Auth::user()->id;
                $message->msgType = 'reply';
                $message->msgStatus = 'new';
                $message->created_at = $timeStamp;
                $message->updated_at = $timeStamp;
                $message->photo  = $fphoto;

               
                $message->save();

                return $this->setStatusCode(201)->respond([
                    'message' => 'Message sent.',
                    'status_code' => $this->getStatusCode()
                ]);
            }
        }
        return $this->respondUnauthorized('Not logged in');
    }

    public function change($id){
        
          $all = Issue::find($id);
          $stat = Input::get('statusChange');
        
        if( ! $all) {
            return $this->respondNotFound('Message does not exist.');
        }
        else{
            if(Auth::check()){
                if ($stat != NULL) {  
                    if ($all->status == 'archive'){
                        return $this->respondInternalError('Issue already archived.');
                    }else{
                        $all->status = $stat;
                        $all->save();

                        return $this->setStatusCode(201)->respond([
                            'message' => 'Changed status to '. $stat.'.',
                            'status_code'=>$this->getStatusCode()
                            ]);
                    }
                }else{
                    return $this->respondNotFound('statusChange is required.');
                }
            }
        }
    }

    public function newMessage(){
        
        $x = Authorizer::getResourceOwnerId();

        if(DB::table('users')->where('id', $x)->pluck('type') == 'manager'){
                            
            $stores = DB::table('stores')
            ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
            ->join('groups', 'groups.id', '=', 'store-groups.groupid')
            ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
            ->join('issues', 'issues.storeid', '=', 'stores.id')
            ->where('user-groups.userid', Auth::user()->id)->where('issues.status', 'new')->groupBy('stores.id')->orderBy('issues.updated_at','DESC')->get();

            $storeDept = DB::table('stores')
            ->join('issues', 'issues.storeid', '=','stores.id')
            ->join('departments', 'departments.id', '=', 'issues.isDept')
            ->join('department-groups', 'department-groups.deptid', '=','departments.id')
            ->join('groups', 'groups.id', '=','department-groups.groupid')
            ->join('user-groups', 'user-groups.groupid', '=','groups.id')
            ->where('user-groups.userid', Auth::user()->id)
            ->where('issues.status','new')
            ->groupBy('stores.id')
            ->orderBy('issues.updated_at','DESC')
            ->get();

            return $this->setStatusCode(200)->respond([
                'message' => 'New list',
                'stores' => $stores,
                'storeDept' => $storeDept,
                'status_code' => $this->getStatusCode()
             ]);
        }
        else if(DB::table('users')->where('id', $x)->pluck('type') == 'admin'){
                $stores = DB::table('stores')
                    ->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', 'new')->groupBy('stores.id')->orderBy('issues.updated_at','DESC')->get();

                return $this->setStatusCode(200)->respond([
                'message' => 'New list',
                'stores' => $stores,
                'status_code' => $this->getStatusCode()
            ]);
        }                

        else{
            return $this->respondForbidden('Forbidden');
        } 

    }

    public function inProgress(){
        $x = Authorizer::getResourceOwnerId();
        if(DB::table('users')->where('id', $x)->pluck('type') == 'admin'){
            $stores = DB::table('stores')->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', 'inprogress')->groupBy('stores.id')->orderBy('stores.updated_at','DESC')->get();

            return $this->setStatusCode(200)->respond([
                'message' => 'Inprogress list',
                'stores' => $stores,
                'status_code' => $this->getStatusCode()
            ]);
        }
        else if(DB::table('users')->where('id', $x)->pluck('type') == 'manager'){
            $stores = DB::table('stores')
                ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                ->join('issues', 'issues.storeid', '=', 'stores.id')->where('user-groups.userid', Auth::user()->id)->where('issues.status', 'inprogress')->groupBy('stores.id')->orderBy('issues.updated_at', 'DESC')->get();

            $storeDept = DB::table('stores')
                ->join('issues', 'issues.storeid', '=','stores.id')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                ->join('groups', 'groups.id', '=','department-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                ->where('user-groups.userid', Auth::user()->id)
                ->where('issues.status','inprogress')
                ->groupBy('stores.id')
                ->orderBy('issues.updated_at','DESC')
                ->get();

            
            return $this->setStatusCode(200)->respond([
                'message' => 'Inprogress list',
                'stores' => $stores,
                'storeDept' => $storeDept,
                'status_code' => $this->getStatusCode()
            ]);
        }else{
            return $this->respondForbidden('Forbidden');
        }
    }

    public function resolved(){
        $x = Authorizer::getResourceOwnerId();
        if(DB::table('users')->where('id', $x)->pluck('type') == 'admin'){
        
            $stores = DB::table('stores')->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', 'resolved')->groupBy('stores.id')->orderBy('stores.updated_at','DESC')->get();

            return $this->setStatusCode(200)->respond([
                'message' => 'Resolved list',
                'stores' => $stores,
                'status_code' => $this->getStatusCode()
            ]);
        }else if(DB::table('users')->where('id', $x)->pluck('type') == 'manager'){
             $stores = DB::table('stores')
                ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                ->join('issues', 'issues.storeid', '=', 'stores.id')->where('user-groups.userid', Auth::user()->id)->where('issues.status', 'resolved')->groupBy('stores.id')->orderBy('issues.updated_at', 'DESC')->get();

            $storeDept = DB::table('stores')
                ->join('issues', 'issues.storeid', '=','stores.id')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                ->join('groups', 'groups.id', '=','department-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                ->where('user-groups.userid', Auth::user()->id)
                ->where('issues.status','resolved')
                ->groupBy('stores.id')
                ->orderBy('issues.updated_at','DESC')
                ->get();

            return $this->setStatusCode(200)->respond([
                'message' => 'Resolved list',
                'stores' => $stores,
                'storeDept' => $storeDept,
                'status_code' => $this->getStatusCode()
            ]);
              
        }else{
            return $this->respondForbidden('Forbidden');
        }
    }

    public function issuelist($id){
        if(Auth::check()){
            if(Auth::user()->type == 'admin'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->join('departments', 'issues.isDept', '=', 'departments.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.status', 'issues.updated_at','issues.isDept', 'departments.name')
                    ->where('stores.id', $id)
                    ->where('messages.msgType', 'feedBack')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.status', 'issues.updated_at','issues.isDept')
                    ->where('messages.msgType', 'feedback')
                    ->where('stores.id', $id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }

                $new = 0;
                $inprogress = 0;
                $resolved = 0;

                for($ctr = 0; $ctr < count($issues); $ctr++){
                        if($issues[$ctr]->status == 'new')  
                            $new++;
                        else if($issues[$ctr]->status == 'inprogress')
                            $inprogress++;
                        else if($issues[$ctr]->status == 'resolved')
                            $resolved++;       
                }
                $count = array(
                    'new' => $new,
                    'inprogress' => $inprogress,
                    'resolved' => $resolved
                );

                return $this->setStatusCode(200)->respond([
                    'message' => 'All issues.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode(),
                    'count' => $count
                ]);
            }else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->join('messages', 'messages.issueid', '=','issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.status', 'issues.updated_at','issues.isDept', 'departments.name')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('messages.msgType', 'feedback')
                    ->where('issues.storeid','=',$id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.status', 'issues.updated_at','issues.isDept')
                    ->where('messages.msgType', 'feedback')
                    ->where('stores.id', $id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }

                $new = 0;
                $inprogress = 0;
                $resolved = 0;

                for($ctr = 0; $ctr < count($issues); $ctr++){
                        if($issues[$ctr]->status == 'new')  
                            $new++;
                        else if($issues[$ctr]->status == 'inprogress')
                            $inprogress++;
                        else if($issues[$ctr]->status == 'resolved')
                            $resolved++;       
                }
                $count = array(
                    'new' => $new,
                    'inprogress' => $inprogress,
                    'resolved' => $resolved
                );
                
                return $this->setStatusCode(200)->respond([
                    'message' => 'All issues.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode(),
                    'count' => $count
                ]);
            }
        }
    }

    public function historyDetails($id){
        if(Auth::check()){
            if(Auth::user()->type == 'admin'){

                $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at','issues.isDept')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','new')
                    ->where('messages.msgStatus','=','new')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                return $this->setStatusCode(200)->respond([
                    'message' => 'New details list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);
            }else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->join('messages', 'messages.issueid', '=','issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.updated_at','issues.isDept')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('issues.status','=','new')
                    ->where('messages.msgType', 'feedback')
                    ->where('issues.storeid','=',$id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=', 'issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at','issues.isDept')
                    ->where('messages.msgType', 'feedback')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','new')
                    ->where('messages.msgStatus','=','new')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }

                return $this->setStatusCode(200)->respond([
                    'message' => 'New details list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);
            }
        }
    }

    public function inProgressDetails($id){
        
        if(Auth::user()->type == 'admin'){
            $issues = DB::table('issues')
                ->join('stores', 'stores.id', '=', 'issues.storeid')
                ->join('messages', 'messages.issueid', '=','issues.id')
                ->where('messages.msgType', 'feedback')
                ->where('stores.id', $id)
                ->where('issues.status','=','inprogress')
                ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at')
                ->orderBy('issues.updated_at', 'DESC')
                ->get();

                return $this->setStatusCode(200)->respond([
                    'message' => 'Inprogress details list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);
        }
        else if(Auth::user()->type == 'manager'){

            $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

            if($hasDept == 1) {
                $issues = DB::table('issues')
                ->join('stores', 'stores.id', '=', 'issues.storeid')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                ->join('groups', 'groups.id', '=','department-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                ->join('messages', 'messages.issueid', '=','issues.id')
                ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at','issues.isDept')
                ->where('messages.msgType', 'feedback')
                ->where('user-groups.userid', Auth::user()->id)
                ->where('issues.status','=','inprogress')
                ->where('stores.id','=',$id)
                ->groupBy('issues.id')
                ->orderBy('issues.updated_at', 'DESC')
                ->get();
            }
            else{
                $issues = DB::table('issues')
                ->join('stores', 'stores.id', '=', 'issues.storeid')
                ->join('messages', 'messages.issueid', '=','issues.id')
                ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at','issues.isDept')
                ->where('messages.msgType', 'feedback')
                ->where('stores.id', $id)
                ->where('issues.status','=','inprogress')
                ->orderBy('issues.updated_at', 'DESC')
                ->get();
            }

            return $this->setStatusCode(200)->respond([
                'message' => 'Inprogress details list.',
                'issues' => $issues,
                'status_code' => $this->getStatusCode()
            ]);

        }
        else{
            return $this->respondUnauthorized('Not logged in');
        }
    }

    public function resolvedDetails($id){
        if(Auth::user()->type == 'admin'){
            $issues = DB::table('issues')
                ->join('stores', 'stores.id', '=', 'issues.storeid')
                ->join('messages', 'messages.issueid', '=','issues.id')
                ->where('messages.msgType', 'feedback')
                ->where('stores.id', $id)
                ->where('issues.status','=','resolved')
                ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message','issues.updated_at')
                ->orderBy('issues.updated_at', 'DESC')->get();
                return $this->setStatusCode(200)->respond([
                    'message' => 'Resolved details list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);
        }else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->join('messages', 'messages.issueid', '=','issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.updated_at','issues.isDept')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('messages.msgType', 'feedback')
                    ->where('issues.status','=','resolved')
                    ->where('stores.id','=',$id)
                    ->groupBy('issues.id')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('messages', 'messages.issueid', '=','issues.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'messages.message', 'issues.updated_at','issues.isDept')
                    ->where('messages.msgType', 'feedback')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','resolved')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();
                }

                return $this->setStatusCode(200)->respond([
                    'message' => 'Resolved details list.',
                    'issues' => $issues,
                    'status_code' => $this->getStatusCode()
                ]);

            }else{
                return $this->respondUnauthorized('Forbidden');
            }
    }

    public function destroy()
    {
 
        $userid = Auth::user()->id;
        DB::table('users')->where('id', '=', $userid)->update(array('deviceid' => NULL));
        Auth::logout();
        return $this->respondOk('logged out');
    }

    public function issueNotif($issueId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }

        $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [$currentId])
            ->whereNotIn('messages.userid', [$currentId])
            ->where('messages.msgStatus', 'new')->where('issues.id', $issueId)->count();

            if($count == 0){
                return $this->setStatusCode(201)->respond([
                    'message' => 'Notification count.',
                    'count' => '',
                    'className' => 'issueNotif_'.$issueId,
                    'status_code' => $this->getStatusCode()
                ]); 
            }
            else{
                return $this->setStatusCode(201)->respond([
                    'message' => 'Notification count.',
                    'count' => $count,
                    'className' => 'issueNotif_'.$issueId,
                    'status_code' => $this->getStatusCode()
                ]); 
            }               
    }

    public function storeNotif($storeId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }

        $hasDept = DB::table('stores')->where('id',$storeId)->pluck('hasDept');

        if (Auth::user()->type == 'admin') {
            $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status','new')->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->whereNotIn('messages.userid', [$currentId])->count();
        }
        else{
            if ($hasDept == 0) {
             $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status','new')->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->whereNotIn('messages.userid', [$currentId])->count();
            }
            else{
                $count = DB::table('messages')
                ->join('issues', 'messages.issueid', '=', 'issues.id')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                ->join('groups', 'groups.id', '=','department-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                ->where('user-groups.userid', Auth::user()->id)
                ->where('messages.msgStatus', 'new')
                ->where('issues.storeid', $storeId)
                ->where('issues.status','new')
                ->whereNotIn('messages.userid', [Auth::user()->id])->count();
            }
        }

        if($count == 0){
            return $this->setStatusCode(201)->respond([
                'message' => 'Notification count.',
                'count' => '',
                'className' => 'storeNotif_'.$storeId,
                'status_code' => $this->getStatusCode()
            ]);
        }

        else{
            return $this->setStatusCode(201)->respond([
                'message' => 'Notification count.',
                'count' => $count,
                'className' => 'storeNotif_'.$storeId,
                'status_code' => $this->getStatusCode()
            ]);
        }
    }

    public function storeNotifInProg($storeId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }

        $hasDept = DB::table('stores')->where('id',$storeId)->pluck('hasDept');

        if ($hasDept != 0) {
            $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status', 'inprogress')->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->whereNotIn('messages.userid', [$currentId])->count();
        }
        else{
            $count = DB::table('messages')
                ->join('issues', 'messages.issueid', '=', 'issues.id')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                ->join('groups', 'groups.id', '=','department-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                ->where('user-groups.userid', Auth::user()->id)
                ->where('issues.status', 'inprogress')
                ->where('messages.msgStatus', 'new')
                ->where('issues.storeid', $storeId)
                ->whereNotIn('messages.userid', [Auth::user()->id])->count();
        }

        if($count == 0){
            return $this->setStatusCode(201)->respond([
                'message' => 'Notification count.',
                'count' => '',
                'className' => 'storeNotif_'.$storeId,
                'status_code' => $this->getStatusCode()
            ]);
        }
        else{
            return $this->setStatusCode(201)->respond([
                'message' => 'Notification count.',
                'count' => $count,
                'className' => 'storeNotif_'.$storeId,
                'status_code' => $this->getStatusCode()
            ]);
        }
    }


    public function checkReply(){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
            


        if(Input::get('getLatestMsg') == ''){
            $latestMsg = 0;
        }else{
            $latestMsg = Input::get('getLatestMsg');
        }


        $newMsg = DB::table('messages')->where('issueid', Input::get('msgIssueId'))->where('id', '>', $latestMsg)->whereNotIn('userid', [$currentId])->where('msgStatus', 'new')->orderBy('created_at', 'ASC');

        if($newMsg->count() != 0){
            $getAll = $newMsg->get();
            return $this->setStatusCode(200)->respond([
                'message' => 'New message.',
                'messages' => $getAll,
                'status_code' => $this->getStatusCode()
            ]);
        
        }else{
            return $this->setStatusCode(404)->respond([
                'message' => 'not found',
                'status_code' => $this->getStatusCode()
            ]);
        }
    }

    public function uploadAPI(){
            //$NAME = $_FILES["pictures"]["name"];
            $file=Input::file('pictures');


            return $this->setStatusCode(200)->respond([
                'name' => $file->getClientOriginalName(),
                'status_code' => $this->getStatusCode()
            ]);

    }

    public function setDeviceID(){
       $query = DB::table('users')->where('email', '=', Input::get('email'));
       
       if($query->count() == 1){
            DB::table('users')->where('email', '=', Input::get('email'))->update(['deviceid' => Input::get('token')]);     
            return $this->setStatusCode(200)->respond([
                'message' => 'Device token successfully set',
                'status_code' => $this->getStatusCode()
            ]);
        }
        else
            return $this->setStatusCode(500)->respond([
                'message' => $query->count(),
                'status_code' => $this->getStatusCode()
            ]);
    }

}