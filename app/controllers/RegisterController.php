<?php

class RegisterController extends \BaseController {


    public function signup(){
        $us = '';
        $pw = '';
        $email = Input::get('email');
        $bool = DB::table('users')->where('email','=', $email)->count();
        $status = 'false';
        preg_match('/^[A-Za-z0-9+-._"]+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/', $email, $matches);
        //preg_match("/^[a-z0-9_\+-]+(\.[a-z0-9_\+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,4})$/", $email, $matches);
        //$re = "/^[A-Za-z0-9_+-.]+([\\.-]?[\\.+]?\\w+)*@\\w+([\\.-]?[\\.+]?\\w+)*(\\.\\w{2,3})+$/"; 
        ///^[A-Za-z0-9_+-.]+([\.-]?[\.+]?\w+)*@\w+([\.-]?[\.+]?\w+)*(\.\w{2,3})+$/
        //preg_match('/^[A-Za-z0-9+-._]+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $email, $matches);
        //if($bool == 0){
         if (isset($matches[0]) && $matches[0] == $email && $bool == 0 && strpos($matches[0], '"') == 0) {
                $msg = 'Congrats! Account Created. Please check your email.';
                $status = 'true';
                
                $length = 10;

                $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

                $randomString2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

                $user = new Register;
                $user->email = Input::get('email');
                $mail = explode('@', Input::get('email'));
                $user->name = $mail[0];
                $user->type = 'consumer';
                $user->password = Hash::make($randomString);
                $user->confirmation = 2;
                $user->confirmation_code = $randomString2;
                $user->forgot = 0;
                $user->forgot_code = 'SEEDEDACCOUNT';
                $user->district_manager = '0';
                $user->save();

                $us = Input::get('email');
                $pw = $randomString;


                $url = URL::to('/changepassword/'.$us);

                Mail::send('emails.template2', array('us' => Input::get('email'), 'pw' => $pw, 'url' => $url), function($message) use($us){
                    $message->from('support@skosay.com', 'Skosay - Say it anywhere!');
                    $message->to($us)->subject('Skosay Account confirmation');
                });

                $id = User::where('email', Input::get('email'))->pluck('id');

                $month = 2592000 + time();
                setcookie('fbId', $id, $month);
                setcookie('fName', $mail[0], $month);

                if(isset($_COOKIE['guestId'])){
                    setcookie('guestId', '', time()-3600);
                    unset($_COOKIE['guestId']);
                }
                //Cache::flush();
            }
        else if($bool != 0 && $matches[0] == $email){
            $msg = 'Email Address is already taken';
            $status = 'false';
        }
        else{
                $msg = 'Please put a valid email';
                $status = 'false';
            }

        if ($status == 'true') {
            $fmsg = "<div><center><font color='green'>".$msg."</font></center></div>";
        }else{
            $fmsg = "<div><center><font color='red'>".$msg."</font></center></div>";
        }

        $responseArray = array(
            'status' => $status,
            'msg' => $fmsg
            
        );

        return $responseArray;
    }

    public function register(){
        return View::make('history.signup');
    }

    public function confirmAccount($id){
        DB::table('users')->where('confirmation_code', $id)->update(array('confirmation' => '1'));
        $email = User::where('confirmation_code', $id)->pluck('email');
        //return View::make('history.login2')->with('email', User::where('confirmation_code', $id)->pluck('email'));
        return Redirect::to('login2')->with('email', $email);
        //return Redirect::route('history.login2', array('email' => $email));
        // return Redirect::to('login2')->with('email', User::where('confirmation_code', $id)->pluck('email'));
    }

    public function store(){
        if(filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL) && strcasecmp(Input::get('password'), Input::get('confirm-password')) == 0 && strlen(Input::get('password')) >= 7 && strlen(Input::get('confirm-password')) >= 7)
        {
            $bool = DB::table('users')->where('email', Input::get('email'))->count();
            if($bool == 0){
                function generateRandomString($length = 20){
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++){
                        $randomString .= $characters[rand(0, strlen($characters) - 1)];
                    }
                    return $randomString;
                }

                    function detectDuplicate(){
                        $randVar = generateRandomString();
                        $bool = DB::table('messages')->where('userid', '=', $randVar)->count();
                        if($bool == 0){
                            return $randVar;
                        }
                        else{
                            detectDuplicate();
                        }
                }

                $input = Input::all();
                $name = $input['name'];
                $confirmCode = detectDuplicate();
                $email = $input['email'];
                $url = URL::to('/').'/confirmAccount/'.$confirmCode;
                $data = ['url' => $url];

                $user = new Register;
                $user->email = $email;
                $user->name = $name;
                $user->type = 'consumer';
                $user->password = Hash::make($input['password']);
                $user->confirmation = 0;
                $user->confirmation_code = $confirmCode;
                $user->save();

                //oauth_client
                $user= new ClientRegister;
                $user->id = $email;
                $user->secret = '12345';
                //$user->secret = str_random(5);
                $user->name = $name;
                $user->save();


                Mail::send('emails.template', $data, function($message) use($email){
                    $message->from('support@skosay.com', 'Skosay - Say it anywhere!');
                    $message->to($email)->subject('Skosay account confirmation');
                });
                
                
                $responseArray = array(
                    'msg' => '<div class="item" style="background-color: darkseagreen; border-color: green;"><center><h4><font color="green">Congrats! Account Created!<br/>Check your email to confirm account creation!</font></h4></center></div>',
                    'status' => 'true'
                );
            }else{
                $responseArray = array(
                    'msg' => '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Email is already taken</font></h4></center></div>',
                    'status' => 'false'
                );
            }

            return $responseArray;
        }else{

            $bool = DB::table('users')->where('email', Input::get('email'))->count();
            $msg = '';
            if(!filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL)){
                $msg = 'Please put a valid email';
            }
            elseif($bool != 0){
                $msg = 'Email is already taken';
            }
            elseif(strlen(Input::get('password')) < 7 && strlen(Input::get('confirm-password')) < 7 ){
                if(Input::get('password') == "" || Input::get('confirm-password') == ""){
                            if($msg == ''){
                                $msg = 'All fields should be filled out';
                            }
                            else{
                                $msg .= '<br/>All fields should be filled out';
                            }
                    }
                elseif($msg == ''){
                    $msg = 'Password must be at least 7 characters';
                }
                else{
                    $msg .= '<br/>Password must be at least 7 characters';            
                }
            }
            elseif(strcasecmp(Input::get('password'), Input::get('confirm-password')) != 0){
                if($msg == ''){
                    $msg = 'Password and confirm password do not match.';
                }
                else{
                    $msg .= '<br/>Password and confirm password do not match.';
                }
            }
            $fmsg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">'.$msg.'</font></h4></center></div>';
            return array(
                'status' => 'false',
                'msg' => $fmsg
            );
        }
    }

    public function checkPassword(){
        
        $msg = '';
        $input = Input::all();

       $user = User::where('id', Auth::user()->id)->pluck('password');
        if (strlen(Input::get('password')) < 7 || strlen(Input::get('confirm-password')) < 7 ){
                    $msg = 'Password must be at least 7 characters';
            }elseif(Input::get('password') !== Input::get('confirm-password')){
                    $msg = 'Password and confirm password do not match.';
        }elseif (!Hash::check($input['oldpassword'], Auth::user()->password)){
                    $msg = 'Your old password was incorrect.';
        }else{
            User::where('id', Auth::user()->id)->update(array('password' => Hash::make(Input::get('password'))));
            $responseArray = array(
            'msg' => '<div class="item" style="background-color: darkseagreen; border-color: green;"><center><h4><font color="green">Your Password have been changed!</font></h4></center></div>',
                    'status' => 'true'
            );
            return $responseArray;
        }
            $fmsg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">'.$msg.'</font></h4></center></div>';

            return array(
                'status' => 'false',
                'msg' => $fmsg
        );
    }

    public function checkEmail(){
        $msg = '';
        $bool = DB::table('users')->where('email', Input::get('emailField'))->count();
        if(filter_var(Input::get('emailField'), FILTER_VALIDATE_EMAIL)){
            if($bool != 0){
                $msg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Email is already in use</font></h4></center></div>';
                $status = 'false';
            }else{
                $status = 'true';
            }
        }else{
            $msg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Please put a valid email</font></h4></center></div>';
            $status = 'false';
        }
        return array(
            'msg' => $msg,
            'status' => $status
        );
    }

    public function confirmForgot(){
        return View::make('history.confirmForgotPass');
    }

    public function forgotPass(){
        return View::make('history.forgotPassword');
    }

    public function checkEmailForm(){
        $status = 'false';
        $msg = '';

        $bool = User::where('email', Input::get('email'))->count();
        if($bool == 0){
            $msg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Email does not exist</font></h4></center></div>';
            if(Input::get('email') == ""){
                $msg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Email address field should be field out.</font></h4></center></div>';
            }
        }else{
            $status = 'true';
        }

        return array(
            'msg' => $msg,
            'status' => $status
        );
    }

    public function sendConfirmFp(){
            $email = Input::get('trigger');
            $checkMail = User::where('email', $email);
        if(filter_var($email, FILTER_VALIDATE_EMAIL) && $checkMail->count() != 0){

            function generateRandomString($length = 20){
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++){
                        $randomString .= $characters[rand(0, strlen($characters) - 1)];
                    }
                    return $randomString;
                }

                function detectDuplicate(){
                    $randVar = generateRandomString();
                    $bool = DB::table('messages')->where('userid', '=', $randVar)->count();
                    if($bool == 0){
                        return $randVar;
                    }
                    else{
                        detectDuplicate();
                    }
                }


            $fcode = detectDuplicate(); 
            DB::table('users')->where('email', $email)->update(array('forgot_code' => $fcode));
            

            $data = ['url' => URL::to('/').'/forgotPass/'.$checkMail->pluck('id').'/'.$fcode];
            Mail::send('emails.forgotpass', $data, function($message) use($email){
                $message->from('info@skosay.solutionsresource.com', 'Skosay - Say it anywhere!');
                $message->to($email)->subject('Skosay forgot password');
            });
            $msg = '<div class="item" style="background-color: darkseagreen; border-color: green;"><center><h4><font color="green">Confirmation has been sent to your email.</font></h4></center></div>';
        }else{
            $msg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Email does not exist</font></h4></center></div>';
        }

        return $msg;
    }

    public function forgotPassForm($id,$fcode){
        $user = User::where('id', $id);
        $usercode = User::where('forgot_code', $fcode);

        if($usercode->count()== 0){
            return $msg = 'Invalid confirmation link';
        }
        else{
            return View::make('history.forgotPassword')->with('id' ,$id);
        }
    }
    public function newPassword(){
        $status = 'false';
        $msg = 'Password successfully changed.';
        if(strlen(Input::get('pass1')) < 7  && strlen(Input::get('pass2')) < 7){
            $msg = 'Password must be at least 7 characters';
                if(Input::get('pass1') == "" && Input::get('pass2') == "" || Input::get('pass1') == "" || Input::get('pass2') == ""){
                    $msg = 'All fields should be filled out.';
                }
        }else if(Input::get('pass1') !== Input::get('pass2')){
            $msg = 'Password and confirm password do not match.';
        }
        else{
                $status = 'true';
                User::where('id', Input::get('hiddenId'))->update(array('forgot_code' => 'none'));
                User::where('id', Input::get('hiddenId'))->update(array('password' => Hash::make(Input::get('pass1'))));
            }
            if($msg == "Password successfully changed.")
                $fmsg = '<div class="item" style="background-color: darkseagreen; border-color: green;"><center><h4><font color="green">'.$msg.'</font></h4></center></div>';
            else
                $fmsg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">'.$msg.'</font></h4></center></div>';

        return array(
            'msg' => $fmsg,
            'status' => $status
        );
    }

    public function changepass(){
        if(Auth::check() && Auth::user()->type == 'consumer'){
            return View::make('history.changepass');
        }
        else if(Auth::check() && Auth::user()->type == 'admin'){
            return Redirect::to('administrator');
        }
        else if(Auth::check() && Auth::user()->type == 'manager'){
            return Redirect::to('administrator');
        }
        else{
            return View::make('history.firstlog');
        }
    }   

    public function changepass2($id){

         $confirmation = User::where('email', $id)->pluck('confirmation');

        if(Auth::check() && Auth::user()->type == 'consumer'){
            return View::make('location');
        }
        else if(Auth::check() && Auth::user()->type == 'admin'){
            return Redirect::to('administrator');
        }
        else if(Auth::check() && Auth::user()->type == 'manager'){
            return Redirect::to('administrator');
        }
        else if($confirmation != 2){
            return Redirect::to('/');
        }
        else{
            return View::make('history.firstlog')->with('id',$id);
        }
    }


    public function checkPassword2(){
        
        $msg = '';
        $input = Input::all();

       $password = User::where('email', Input::get('idnum'))->pluck('password');
       $confirmation = User::where('email', Input::get('idnum'))->pluck('confirmation');



       if ($confirmation == 2) {
           if (strlen(Input::get('password')) < 7 || strlen(Input::get('confirm-password')) < 7 ){
                    $msg = 'Password must be at least 7 characters';
            }elseif(Input::get('password') !== Input::get('confirm-password')){
                    $msg = 'Password and confirm password do not match.';
            }elseif (! Input::get('oldpassword') == $password){
                        $msg = 'Your old password was incorrect.';
            }else{
                User::where('email', Input::get('idnum'))->update(array('password' => Hash::make(Input::get('password')), 'confirmation' => 1));
                $responseArray = array(
                'msg' => '<div class="item" style="background-color: darkseagreen; border-color: green;"><center><h4><font color="green">Your Password have been changed!</font></h4></center></div>',
                        'status' => 'true'
                );
                return $responseArray;
            }
                $fmsg = '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">'.$msg.'</font></h4></center></div>';

                return array(
                    'status' => 'false',
                    'msg' => $fmsg
            );
       }else{

       }
       
    }

}
?>