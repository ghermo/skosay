<?php

class LocationController extends \BaseController {

	public function index(){
		$store = Store::all();
        if(Auth::check() || isset($_COOKIE['fbId']) || isset($_COOKIE['guestId'])){
            return View::make('index',['store'=>$store]);
        }
        else{
            return Redirect::to('/location');
        }
	}

    public function listLocation(){
        if(Auth::check() || isset($_COOKIE['fbId']) || isset($_COOKIE['guestId'])){
            return View::make('index')->with('logged',1);
        }
        else{
            return View::make('index')->with('logged',0);
        }
    }

    public function logOut(){
        Auth::logout();
            return Redirect::to('location');
    }


    public function feedbackLogin(){
            if((Auth::check()) && (Auth::user()->type == 'admin' || Auth::user()->type == 'manager')) {
                    Auth::logout();
            }
        return View::make('history.login2')->with('inputAll', Input::all());
    }

//    public function feedbackInit(){
//        if(isset($_COOKIE['inputAll'])){
//            $inputAll = unserialize($_COOKIE['inputAll']);
//            // ALGO FOR NEW STORES START
//            $storeCount = DB::table('stores')->where('id', Input::get('pId'))->orWhere('storename', str_replace('_', ' ', $inputAll['pName']))->count();
//            if($storeCount == 0){
//                DB::table('stores')->insert(
//                    array(
//                        'id' => $inputAll['pId'],
//                        'storename' => str_replace('_', ' ', $inputAll['pName'])
//                    )
//                );
//            }
//            // END
//            return View::make('location.feedbackQ1')->with('inputAll', $inputAll);
//        }else if(Session::get('inputAll') != null){
//              // ALGO FOR NEW STORES START
//            $storeCount = DB::table('stores')->where('id', Input::get('pId'))->orWhere('storename', str_replace('_', ' ', Session::get('inputAll')['pName']))->count();
//            if($storeCount == 0){
//                DB::table('stores')->insert(
//                    array(
//                        'id' => Session::get('inputAll')['pId'],
//                        'storename' => str_replace('_', ' ', Session::get('inputAll')['pName'])
//                    )
//                );
//            }
//            // END
//            return View::make('location.feedbackQ1')->with('inputAll', Session::get('inputAll'));
//        }else
//            return Redirect::to('location');
//    }


    public function feedback2(){
        $deptData = Department::where('id', Input::get('dId'))->first();

        $questions = Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
            ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
            ->join('departments', 'departments.qGroupId', '=', 'qgroups.id')->where('departments.id', Input::get('dId'))->orderBy('questions.orderId', 'DESC')->get();

        $storeData = Store::where('id', Input::get('dStoreid'))->first();
        //return $storeData;
        return View::make('location.feedback2')->with('questions', $questions)->with('storeData', $storeData)->with('deptData', $deptData);
        
    }

    public function feedback3($did,$sid){

        $deptData = Department::where('id', $did)->first();
        $storeid = Department::where('id', $did)->pluck('storeid');

        $questions = Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
            ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
            ->join('departments', 'departments.qGroupId', '=', 'qgroups.id')
            ->where('departments.id', $did)->orderBy('questions.orderId', 'DESC')->get();


        $storeData = Store::where('id', $sid)->first();
        $storeid2 = Store::where('id', $sid)->pluck('id');



        if(Auth::check() || isset($_COOKIE['fbId']) || isset($_COOKIE['guestId'])){
            $log = 1;
        }
        else{
            $log = 0;
        }

        if($storeid == $storeid2){

            return View::make('location.feedback2')->with('questions', $questions)->with('storeData', $storeData)->with('deptData', $deptData)->with('logged',$log);

        }else{
            return Redirect::to('/error');
        }
        
        //return $deptData;
        
    }


    public function show2($id){

        if(Auth::check()){
            $authId = Auth::user()->id;
        }
        else if(isset($_COOKIE['fbId'])){
            $authId = $_COOKIE['fbId'];
        }
        else if(isset($_COOKIE['guestId'])){
            $authId = $_COOKIE['guestId'];
        }else{
            $authId = 0;
        }

        $storeData = Store::where('id', $id)->first();
            if ($storeData == NULL || $storeData == '') {
                return Redirect::to('location');
            }else{
            $questions = Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
                ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
                ->join('stores', 'stores.qGroupId', '=', 'qgroups.id')->where('stores.id', $id)->orderBy('questions.orderId', 'DESC')->get();
            }

            $deptCount = DB::table('departments')->where('storeid', $id)->count();
            if ($deptCount != 0) {
                $departments = Department::where('storeid', $id)->get();
                $deptCounter = 1;
                return View::make('location.index')->with('departments', $departments)->with('storeData', $storeData)->with('deptCounter', $deptCounter)->with('logged', $authId)->with('id', $id);            

            }else{

                return View::make('location.feedback')->with('questions', $questions)->with('storeData', $storeData)->with('logged', $authId)->with('id', $id);
            }

        /*if(Auth::check()){
            $currentId = Auth::user()->id;

            $storeData = Store::where('id', $id)->first();
            if ($storeData == NULL || $storeData == '') {
                return Redirect::to('location');
            }else{
            $questions = Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
                ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
                ->join('stores', 'stores.qGroupId', '=', 'qgroups.id')->where('stores.id', $id)->orderBy('questions.orderId', 'DESC')->get();
            }

            $deptCount = DB::table('departments')->where('storeid', $id)->count();
            if ($deptCount != 0) {
                $departments = Department::where('storeid', $id)->get();
                $deptCounter = 1;
                return View::make('location.index')->with('departments', $departments)->with('storeData', $storeData)->with('deptCounter', $deptCounter);            

            }else{

                return View::make('location.feedback')->with('questions', $questions)->with('storeData', $storeData);
            }
        }
        else if(isset($_COOKIE['fbId']) || isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['fbId'];

            $storeData = Store::where('id', $id)->first();
            if ($storeData == NULL || $storeData == '') {
                return Redirect::to('location');
            }else{
            $questions = Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
                ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
                ->join('stores', 'stores.qGroupId', '=', 'qgroups.id')->where('stores.id', $id)->orderBy('questions.orderId', 'DESC')->get();
            }

            $deptCount = DB::table('departments')->where('storeid', $id)->count();
            if ($deptCount != 0) {
                $deptCounter = 1;
                $departments = Department::where('storeid', $id)->get();
            
                return View::make('location.index')->with('departments', $departments)->with('storeData', $storeData)->with('deptCounter', $deptCounter);           

            }else{

                return View::make('location.feedback')->with('questions', $questions)->with('storeData', $storeData);
            }
        }
        else{

            return View::make('history.login2')->with('id', $id);
        }*/

    }

 

    public function show(){
        $storeCount = DB::table('stores')->where('id', Input::get('pId'))->count();
            if($storeCount == 0){
                DB::table('stores')->insert(
                    array(
                        'id' => Input::get('pId'),
                        'storename' => str_replace('_', ' ', Input::get('pName')),
                        'address' => Input::get('pAddress'),
                        'photo' => Input::get('iconsrc'),
                        'qGroupId' => '1'
                    )
                );
            }

        $storeData = Store::where('id', Input::get('pId'))->first();
        if ($storeData == NULL || $storeData == '') {
                $questions =  Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
                ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
                ->join('stores', 'stores.qGroupId', '=', 'qgroups.id')->where('stores.id', 1)->orderBy('questions.orderId', 'DESC')->get();
        }else{
            $questions = Question::join('question-groups', 'questions.id', '=', 'question-groups.qid')
                ->join('qgroups', 'qgroups.id', '=', 'question-groups.qgroupid')
                ->join('stores', 'stores.qGroupId', '=', 'qgroups.id')->where('stores.id', Input::get('pId'))->orderBy('questions.orderId', 'DESC')->get();
        }


            $deptCount = DB::table('departments')->where('storeid', Input::get('pId'))->count();
                if ($deptCount != 0) {
                    $departments = Department::where('storeid', Input::get('pId'))->get();
            
                     return View::make('location.index')->with('departments', $departments)->with('storeData', $storeData);            

                }else{

                return View::make('location.feedback')->with('questions', $questions)->with('storeData', $storeData);
                }



    }

    public function saveFeedback2(){
        return Redirect::to('/location');
    }

    private function genString($filename){
        $rand = rand(0, 10000);
        $randvar = $rand.$filename;
        $bool = DB::table('messages')->where('photo', '=', 'uploads/'.$randvar)->count();
        if($bool === 0)
            return $randvar;
        else
            return $this->genString($filename);

    }

    public function saveFeedback(){
        $message = '';
        for($i = 1; $i <= Input::get('numOfQuestions'); $i++){
            $message .= Input::all()['input_'.$i].';';
        }


        //return Input::get('newfile');
        $fphoto = '';
        if(Input::get('origFileName') != ''){
                $file = base64_decode(substr(Input::get('newfile'), strpos(Input::get('newfile'), ",")+1));
                //$file = base64_decode(Input::get('newfile'));
                //$file = Input::get('newfile');
                $orFile = Input::get('origFileName'); 
                $filename = $this->genString($orFile);
                $path = 'uploads/' . $filename;
                $fphoto = $path;
                $img=Image::make($file)->save(public_path().'/' .$path);
        }
         // return $justPngData=substr(Input::get('newfile'), strpos(Input::get('newfile'), ",")+1);
        //return base64_decode(Input::get('newfile'));       
         if(trim(Input::get('comment')) != ''){
            $message .= strip_tags(trim(Input::get('comment')));
        }

        $date = new DateTime();

        if(Auth::check()){
            $authId = Auth::user()->id;
        }else if(isset($_COOKIE['fbId'])){
            $authId = $_COOKIE['fbId'];
        }else if(isset($_COOKIE['guestId'])){
            $authId = $_COOKIE['guestId'];
        }


        $issue = new Issue;
        $issue->userid =  $authId;
        $issue->storeid = Input::get('storeId');
        $issue->isDept = Input::get('deptId');
        $issue->created_at = $date->format('Y-m-d H:i:s');
        $issue->updated_at = $date->format('Y-m-d H:i:s');
        $issue->status = 'new';
        $issue->save();

        $messageSave = new Message;
        $messageSave->userid = $authId;
        $messageSave->issueid = $issue->id;
        $messageSave->message = $message;
        $messageSave->msgType = 'feedBack';
        $messageSave->photo = $fphoto;
        $messageSave->msgStatus = 'new';
        $issue->created_at = $date->format('Y-m-d H:i:s');
        $issue->updated_at = $date->format('Y-m-d H:i:s');
        $messageSave->save();

        $fakeData= array('id'=> Input::get('storeId'), 'updated_at' => $date->format('Y-m-d H:i:s'));
        DB::table('stores')->where('id', Input::get('storeId'))->update($fakeData);

        $dName = DB::table('departments')->where('id', Input::get('deptId'))->pluck('name');

        pushNotify(Input::get('storeId'), Input::get('deptId'));

        if (Input::get('deptId') == 0) {
            $receivers = DB::table('users')
                        ->join('user-groups', 'user-groups.userid', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'user-groups.groupid')
                        ->join('store-groups', 'store-groups.groupid', '=', 'groups.id')
                        ->join('stores', 'stores.id', '=', 'store-groups.storeid')
                        ->where('stores.id',Input::get('storeId'))
                        ->select('users.id','users.mobile','users.name')
                        ->get();
            $rCount = DB::table('users')
                        ->join('user-groups', 'user-groups.userid', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'user-groups.groupid')
                        ->join('store-groups', 'store-groups.groupid', '=', 'groups.id')
                        ->join('stores', 'stores.id', '=', 'store-groups.storeid')
                        ->where('stores.id',Input::get('storeId'))
                        ->select('users.id','users.mobile','users.name')
                        ->count();

            $mesCount = DB::table('issues')->where('storeid',Input::get('storeId'))
                    ->where('status','new')->groupBy('storeid')->count();

            if (($mesCount % 5 == 0) && ($rCount != 0)) {
                foreach($receivers as $receiver) {
                    if ($receiver->mobile != NULL) {
                        Twilio::message( $receiver->mobile, "Skosay Notify:\nHi ".$receiver->name.'. You have '.$mesCount.' unread messages for '.$storename.".\nClick the link below\nhttp://app.skosay.com/historypage");
                            $us = User::find($receiver->id);
                            $us->notiFlag = 1;
                            $us->save();
                    }
                }
            }

        }else{
            $receivers = DB::table('users')
                        ->join('user-groups', 'user-groups.userid', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'user-groups.groupid')
                        ->join('department-groups', 'department-groups.groupid', '=', 'groups.id')
                        ->join('departments', 'departments.id', '=', 'department-groups.deptid')
                        ->where('departments.id',Input::get('deptId'))
                        ->select('users.id','users.mobile','users.name')
                        ->get();
            $rCount = DB::table('users')
                        ->join('user-groups', 'user-groups.userid', '=', 'users.id')
                        ->join('groups', 'groups.id', '=', 'user-groups.groupid')
                        ->join('department-groups', 'department-groups.groupid', '=', 'groups.id')
                        ->join('departments', 'departments.id', '=', 'department-groups.deptid')
                        ->where('departments.id',Input::get('deptId'))
                        ->select('users.id','users.mobile','users.name')
                        ->count();
            $mesCount = DB::table('issues')->where('status','new')->where('isDept', Input::get('deptId'))->groupBy('storeid')->count();
                    
            if ($mesCount % 5 == 0) {
                if ($rCount != 0) {
                    foreach($receivers as $receiver) {
                        if ($receiver->mobile != NULL) {
                            Twilio::message( $receiver->mobile, "Skosay Notify:\nHi ".$receiver->name.'. You have '.$mesCount.' unread messages for '.$dName.".\nClick the link below\nhttp://app.skosay.com/historypage");
                            $us = User::find($receiver->id);
                            $us->notiFlag = 1;
                            $us->save();
                        }
                    }
                }
                
            }

        }

    /*        
        return View::make('location.feedbackConfirm')->with('storeCust', Customer::where('storename', Store::where('id', Input::get('storeId'))->pluck('storename'))->count());
      */  
    }

    public function upload($file){
        $file=Input::file('file');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $img=Image::make($file);
        $path = public_path('uploads/' . $filename);
        $img->resize(300, null, function ($constraint){$constraint->aspectRatio();});
        $img->save($path);
    }

     public function feedbacklast(){
        return View::make('location.feedbackQConfirm');
    }

    public function fbFeedback(){
        $application = array(
            'appId' => '1532542913646124',
            'secret' => 'd214d47325b954a152549ae50731990a'
        );
        //Session::get('fbInput');
        $permissions = 'email';
//      $url_app = value(URL::previous());
        $url_app = value(URL::action('LocationController@fbFeedback'));
        FacebookConnect::getFacebook($application);
        $month = 2592000 + time();
        setcookie('fName', FacebookConnect::getUser($permissions,$url_app)["user_profile"]["first_name"].' '.FacebookConnect::getUser($permissions,$url_app)["user_profile"]["last_name"], $month);
        setcookie('fbId', FacebookConnect::getUser($permissions,$url_app), $month);
        return Redirect::to('location');
    }
    /*Authored by: Niko*/
    public function notifBadge(){
        if(Auth::check()){
            if(Auth::user()->type == 'manager'){
                $count = Message::join('issues', 'issues.id', '=', 'messages.issueid')
                            ->join('stores', 'stores.id', '=', 'issues.storeid')
                            ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                            ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                            ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                            ->where('user-groups.userid', Auth::user()->id)
                            ->where('messages.msgStatus', 'new')
                            ->whereNotIn('messages.userid', [Auth::user()->id])->count();
            }
            else if(Auth::user()->type == 'admin'){
                $count = Message::where('msgStatus', 'new')->whereNotIn('userid', [Auth::user()->id])->count();
            }
            else{
                $count = DB::table('messages')
                        ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [Auth::user()->id])->whereNotIn('messages.userid', [Auth::user()->id])
                        ->where('messages.msgStatus', 'new')->where('messages.msgType', 'reply')->count();
            }

            return $count;
        }else{
            $currentId = $_COOKIE['fbId']; 
            $count = DB::table('messages')
                        ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [$currentId])->whereNotIn('messages.userid', [$currentId])
                        ->where('messages.msgStatus', 'new')->where('messages.msgType', 'reply')->count();
                        return $count;
        }
            
    }

    public function initLogin(){
        if(Auth::check() && Auth::user()->type == 'consumer'){
            return Redirect::to('location');
        }
        else if(isset($_COOKIE['guestId']) || isset($_COOKIE['fbId'])){
            return Redirect::to('location');
        }
        else if(Auth::check() && Auth::user()->type == 'admin'){
            return Redirect::to('administrator');
        }

        return Redirect::to('/location');
    }
}
