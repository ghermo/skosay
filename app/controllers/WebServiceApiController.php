<?php

class WebServiceApiController extends \ApiController {

	public function webLogin()
    {
        $status = 'FAILED';
        if(Auth::attempt(array('email' => Input::get('username'), 'password' => Input::get('password'))) && Auth::user()->type == 'admin'){
                $status = 'SUCCESS';           
        
        return $this->setStatusCode(200)->respond([
            'userid'     => Auth::user()->id,
            'name'       => Auth::user()->name,
            'status_code'=> $this->getStatusCode(),
            'token'      => Authorizer::issueAccessToken()
                               
        ]);


        }
        else{
        	return $this->respondUnauthorized('Invalid user type.');               
        }
        
    }


public function create(){
		$userData = '';
	    $msg = 'User Created.';
	    $status = '';
	    if (strlen(Input::get('name')) == '') {
	    	$msg = 'Name Field is Required';
	    	$status = 'FAILED';
	    }
	    elseif(strlen(Input::get('email')) == '' && strlen(Input::get('password')) == ''){
	    	$msg = 'Email and Password fields are required';
	    	$status = 'FAILED';
	    }elseif(strlen(Input::get('password')) < 7){
	        if($msg == ''){
	            $msg = 'Password must be at least 7 characters';
	            $status = 'FAILED';
	        }
	        else{
	            $msg .= ' and password must be<br/> at least 7 characters';
	            $status = 'FAILED';
	        }
	    }elseif(!filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL)){
	    	if($msg == ''){
	        	$msg = 'Invalid email';
	        	$status = 'FAILED';
	    	}
	        else{
	            $msg .= ' and Invalid email';
	            $status = 'FAILED';
	        }
	    }
	    elseif(User::where('email', Input::get('email'))->count() == 1){
	        $msg = 'Email is already taken';
	        $status = 'FAILED';
	    }
	    elseif(strlen(Input::get('mobile')) == ''){
	    	$msg = 'Mobile Number is required';
	    	$status = 'FAILED';
	    }
	    elseif(strlen(Input::get('actType')) == ''){
	    	$msg = 'User Type is required';
	    	$status = 'FAILED';
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    
	    if($status == 'SUCCESS'){
	    	
	    	if(Input::get('actType') == 'manager'){
		        $dataArray = [
							    ['email' => Input::get('email'),
					             'password' => Hash::make(Input::get('password')),
					             'name' => Input::get('name'),
					             'type' => Input::get('actType'),
					             'confirmation' => '1',
					             'mobile' =>  Input::get('mobile'),
					             'district_manager' => Input::get('checkvalue')
							    ]
							 ];
							 
				$user= new ClientRegister;
                $user->id = Input::get('email');
                $user->secret = '12345';
                $user->name = Input::get('name');
                $user->save();
				$ids = [];

				foreach($dataArray as $data){
						$ids[] = DB::table('users')->insertGetId($data);
				}
					UserGroup::insert(array(
							array(
									'userid' => $ids[0],
									'groupid' => Input::get('getGroup')
								)
						));
			}else{
				$dataArray = [
							    ['email' => Input::get('email'),
					             'password' => Hash::make(Input::get('password')),
					             'name' => Input::get('name'),
					             'type' => Input::get('actType'),
					             'confirmation' => '1',
					             'mobile' =>  Input::get('mobile'),
					             'district_manager' => Input::get('checkvalue')

							    ]
	
							 ];

				$user= new ClientRegister;
                $user->id = Input::get('email');
                $user->secret = '12345';
                $user->name = Input::get('name');
                $user->save();
				$ids = [];

				foreach($dataArray as $data){
						$ids[] = DB::table('users')->insertGetId($data);
				}
			}


			$ucfInput = ucfirst(Input::get('actType'));

			if(Input::get('actType') == 'manager' && Input::get('checkvalue') == '1'){
					$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.'District Manager'.'</td><td></td>';
			}elseif(Input::get('actType') == 'manager' && Input::get('checkvalue') != '1'){
				$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.$ucfInput.'</td><td></td>';
			}elseif(Input::get('actType') == 'admin' && Input::get('checkvalue') != '1'){
				$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.'Administrator'.'</td><td></td>';
			}else{
				$userData = '<tr bgcolor="#E2E2E2"><td>'.$ids[0].'</td><td>'.Input::get('name').'</td><td>'.Input::get('email').'</td><td>'.Input::get('mobile').'</td><td>'.$ucfInput.'</td><td></td>';
			}
		return $this->setStatusCode(201)->respond([
            'message' => $msg,
            'status_code' => $this->getStatusCode()
        ]);
	    }

	    else{
			return $this->setStatusCode(500)->respond([
				'message' => $msg,
            	'status_code' => $this->getStatusCode()
        	]);
		}
	    
	    
	}


	public function edit(){

		$msg = 'Successfully Changed.';
	    $status = 'FAILED';
	    
	    $count = count(DB::table('users')->where('email', Input::get('emailHidden_'.Input::get('userIdEdit')))->first());
	    $id = Input::get('userIdEdit');
		$email = DB::table('users')->find($id);
		$oldEmail = $email->email;

	    if(Input::get('emailHidden_'.Input::get('userIdEdit')) == ''){
		    $msg = 'Email cannot be empty';
	    }
		else if(!filter_var(Input::get('emailHidden_'.Input::get('userIdEdit')), FILTER_VALIDATE_EMAIL)){
			$msg = 'Invalid email';
		}
		else if($count != 0 && Input::get('emailHidden_'.Input::get('userIdEdit')) != $oldEmail)
		{
			$msg = 'Email Address Already Taken';
		}
		else
		{
			$status = 'SUCCESS';
		}

	    if($status == 'SUCCESS'){
	    	$email = DB::table('users')->where('id', Input::get('userIdEdit'))->pluck('email');
	    	DB::table('oauth_clients')->where('id', $email)->update( array(
	            'id' => Input::get('emailHidden_'.Input::get('userIdEdit')),
	            'name' => Input::get('emailHidden_'.Input::get('userIdEdit')),
	  
	        ));

	        User::where('id', Input::get('userIdEdit'))->update( array(
	            'email' => Input::get('emailHidden_'.Input::get('userIdEdit')),
	            'type' => Input::get('typeHidden_'.Input::get('userIdEdit')),
	            'name' => Input::get('nameHidden_'.Input::get('userIdEdit')),
	            'mobile' => Input::get('mobileHidden_'.Input::get('userIdEdit')),
	            'district_manager' => Input::get('districtHidden_'.Input::get('userIdEdit'))
	        ));
	        return $this->setStatusCode(201)->respond([
        	    'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
	    }
	    else{
	    	return $this->setStatusCode(500)->respond([
				'message' => $msg,
            	'status_code' => $this->getStatusCode()
        	]);
	    }

	    
	}

	public function delete(){
		
		$email = DB::table('users')->where('id', Input::get('userId'))->pluck('email');
		DB::table('oauth_clients')->where('id', $email)->delete();
		User::where('id', Input::get('userId'))->delete();


		if(Input::get('userId') == ''){
			return $this->setStatusCode(500)->respond([
        		'message' => 'userId is required.',
        		'status_code' => $this->getStatusCode()
        	]);
        }
        else{
        	return $this->setStatusCode(201)->respond([
        		'message' => 'User Deleted.',
        		'status_code' => $this->getStatusCode()
        	]);
        }
	}




	public function addDepartment(){
		$msg = '';
		$status = 'FAILED';

		$departments = Department::all();
		$count = 0;

		foreach ($departments as $d) {
		 	if ($d->name == Input::get('deptname') && $d->storeid == Input::get('storeAdd')) {
		 		$count =+ 1;
		 	}
		 } 
		if(Input::get('deptname') == ''){
			$msg = 'deptname is required.';
				return $this->setStatusCode(500)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
		}
		elseif(Input::get('storeAdd') == ''){
			$msg = 'storeAdd is required.';
				return $this->setStatusCode(500)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
		}
		elseif(Input::get('questionAdd') == ''){
			$msg = 'questionAdd is required.';
				return $this->setStatusCode(500)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
		}
		else{
		if ($count == 0) {
			
			Department::insert(array(
	        'name' => Input::get('deptname'),
	        'storeid' => Input::get('storeAdd'),
	        'qGroupId' => Input::get('questionAdd')
		    ));

		   	DB::table('stores')->where('id', Input::get('storeAdd'))->update(array('hasDept' => 1));
		    $msg = 'Successfully Added.';
		    $status = 'SUCCESS';

		    return $this->setStatusCode(201)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
		}else{
			$msg = "Duplicates are not allowed";

			return $this->setStatusCode(500)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
		}

		}
	}

	public function editDepartment(){
		$msg = 'Successfully Changed.';
	    $status = 'FAILED';

	    if(Input::get('hiddenDepartmentName_'.Input::get('deptId')) == ''){
	        $msg = 'Store name cannot be empty';
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {
	        Department::where('id', Input::get('deptId'))->update(array(
	            'name' => Input::get('hiddenDepartmentName_'.Input::get('deptId'))
	        ));

			return $this->setStatusCode(201)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
	   
	    }
	    else{
	    	return $this->setStatusCode(500)->respond([
				'message' => $msg,
        	    'status_code' => $this->getStatusCode()
        	]);
	    }

	    
	}

	public function deleteDepartment(){
		$getstoreid = DB::table('departments')->where('id', Input::get('deptId'))->pluck('storeid');
		$count = DB::table('departments')->where('storeid', $getstoreid)->count();

		if($count <= 1){

			$storeid = DB::table('departments')->where('id', Input::get('deptId'))->pluck('storeid');
			DB::table('stores')->where('id', $storeid)->update(array('hasDept' => 0));
			DepartmentGroup::where('deptid',Input::get('deptId'))->delete();
			Department::where('id', Input::get('deptId'))->delete();



		}
		else{
			DepartmentGroup::where('deptid',Input::get('deptId'))->delete();
			Department::where('id', Input::get('deptId'))->delete();
		}
		if(Input::get('deptId') == ''){
			return $this->setStatusCode(500)->respond([
				'message' => 'deptId is required.',
        	    'status_code' => $this->getStatusCode()
        	]);
		}
		else{
			return $this->setStatusCode(201)->respond([
				'message' => 'Successfully Deleted.',
        	    'status_code' => $this->getStatusCode()
        	]);
        }

	}

	public function signup(){
        $us = '';
        $pw = '';
        $email = Input::get('email');
        $bool = DB::table('users')->where('email','=', $email)->count();
        $status = 'false';
        preg_match('/^[A-Za-z0-9+-._"]+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/', $email, $matches);
        //preg_match("/^[a-z0-9_\+-]+(\.[a-z0-9_\+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,4})$/", $email, $matches);
        //$re = "/^[A-Za-z0-9_+-.]+([\\.-]?[\\.+]?\\w+)*@\\w+([\\.-]?[\\.+]?\\w+)*(\\.\\w{2,3})+$/"; 
        ///^[A-Za-z0-9_+-.]+([\.-]?[\.+]?\w+)*@\w+([\.-]?[\.+]?\w+)*(\.\w{2,3})+$/
        //preg_match('/^[A-Za-z0-9+-._]+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', $email, $matches);
        //if($bool == 0){
         if (isset($matches[0]) && $matches[0] == $email && $bool == 0 && strpos($matches[0], '"') == 0) {
                $msg = 'Account created.';
                $status = 'true';
                
                $length = 10;

                $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

                $randomString2 = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

                $user = new Register;
                $user->email = Input::get('email');
                $mail = explode('@', Input::get('email'));
                $user->name = $mail[0];
                $user->type = 'consumer';
                $user->password = Hash::make($randomString);
                $user->confirmation = 2;
                $user->confirmation_code = $randomString2;
                $user->forgot = 0;
                $user->forgot_code = 'SEEDEDACCOUNT';
                $user->district_manager = '0';
                $user->save();

                $us = Input::get('email');
                $pw = $randomString;


                $url = URL::to('/changepassword/'.$us);

                Mail::send('emails.template2', array('us' => Input::get('email'), 'pw' => $pw, 'url' => $url), function($message) use($us){
                    $message->from('support@skosay.com', 'Skosay - Say it anywhere!');
                    $message->to($us)->subject('Skosay Account confirmation');
                });

                $id = User::where('email', Input::get('email'))->pluck('id');

                $month = 2592000 + time();
                setcookie('fbId', $id, $month);
                setcookie('fName', $mail[0], $month);

                if(isset($_COOKIE['guestId'])){
                    setcookie('guestId', '', time()-3600);
                    unset($_COOKIE['guestId']);
                }
                //Cache::flush();
            }
        else if($bool != 0 && $matches[0] == $email){
            $msg = 'Email Address is already taken';
            $status = 'false';
        }
        else{
                $msg = 'Please put a valid email';
                $status = 'false';
            }

        if ($status == 'true') {
            return $this->setStatusCode(201)->respond([
				'message' => $msg,
				'status' => $status,
        	    'status_code' => $this->getStatusCode()
        	]);
        }else{
            return $this->setStatusCode(500)->respond([
				'message' => $msg,
				'status' => $status,
        	    'status_code' => $this->getStatusCode()
        	]);
        } 
    }


    public function addStore(){
		$status = 'FAILED';
	    $msg = '';
	    $string = Input::all();

	    $rules = array(
		    'storeid'  => 'alpha_num',		    
		);

		$validation = Validator::make($string, $rules);
 
	   	if (Input::get('storename') == ' ' || Input::get('storename') == '' || trim(Input::get('storename')) == ''){
	    	$msg = 'Please input store name';
	   	}
	    elseif(Input::get('storeadd') == ' ' || Input::get('storeadd') == '' || trim(Input::get('storeadd')) == ''){
	        $msg = 'Please input address';
	    }
	    elseif(Input::get('storeid') == ' ' || Input::get('storeid') == '' || trim(Input::get('storeid')) == ''){
	        if($msg == ''){
	            $msg = 'Please input store id';
	        }
	        else{
	            $msg .= ' and store id';
	        }
	    }elseif(Store::where('id', Input::get('storeid'))->count() == 1){
	        $msg = 'Store id already exist';
	    }
	    elseif ($validation->fails()){
		    $msg = 'Store id should not contain special characters';
		}else{
	    	$status = 'SUCCESS';
		}

	    if($status == 'SUCCESS'){
	        Store::insert(array(
	            'id' => Input::get('storeid'),
	            'storename' => Input::get('storename'),
	            'address' => Input::get('storeadd')
	        ));
	        $msg = 'Successfully Added.';
	        return $this->setStatusCode(201)->respond([
				'message' => $msg,
        		'status_code' => $this->getStatusCode()
       		]);
	    }
	    else{
	    	return $this->setStatusCode(500)->respond([
				'msg' => $msg,
        		'status_code' => $this->getStatusCode()
       		]);	
	    }
	}

	public function editStore(){
		$msg = '';
	    $status = 'FAILED';

	    if(Input::get('hiddenStorename_'.Input::get('storeId')) == ''){
	        $msg = 'Store name cannot be empty';
	    }
	    else if(Input::get('hiddenAddress_'.Input::get('storeId')) == ''){
	        if($msg == ''){
	            $msg = 'Address cannot be empty';
	        }
	        else{
	            $msg = 'Store name and address cannot be empty';
	        }
	    }
	    else{
	        $status = 'SUCCESS';
	    }

	    if($status == 'SUCCESS')
	    {
	        Store::where('id', Input::get('storeId'))->update(array(
	            'storename' => Input::get('hiddenStorename_'.Input::get('storeId')),
	            'address' => Input::get('hiddenAddress_'.Input::get('storeId'))
	        ));
	        return $this->setStatusCode(200)->respond([
				'message' => 'Successfully Changed.',
        		'status_code' => $this->getStatusCode()
       		]);	
	    }
	    else{
	    	return $this->setStatusCode(500)->respond([
				'msg' => 'Failed.',
        		'status_code' => $this->getStatusCode()
       		]);	
	    }

	    
	}

	public function deleteStore(){
		if(Input::get('storeId') == ''){
				return $this->setStatusCode(500)->respond([
				'msg' => 'storeId is required.',
        		'status_code' => $this->getStatusCode()
       		]);
       	}
       	else{
       		
       		Store::where('id', Input::get('storeId'))->delete();
			return $this->setStatusCode(200)->respond([
				'message' => 'Successfully Deleted.',
        		'status_code' => $this->getStatusCode()
       		]);
       	}
	}
}
?>