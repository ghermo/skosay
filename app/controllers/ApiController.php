<?php
class ApiController extends \BaseController {

protected $statusCode=200;

public function respond($data, $headers = [])
{
	return Response::json($data, $this->getStatusCode(), $headers);
}

public function getStatusCode()
{
	return $this->statusCode;
}

public function setStatusCode($statusCode)
{
	$this->statusCode = $statusCode;
	return $this;
}

protected function respondOk($message) 
{
	return $this->setStatusCode(200)->respond([
			'message' => $message,
			'status_code' => $this->getStatusCode()
		]);
}

protected function respondCreated($message) 
{
	return $this->setStatusCode(201)->respond([
			'message' => $message,
			'status_code' => $this->getStatusCode()
		]);
}

protected function respondNoContent($message) 
{
	return $this->setStatusCode(204)->respond([
			'message' => $message,
			'status_code' => $this->getStatusCode()
		]);
}

protected function respondUnauthorized($message) 
{
	return $this->setStatusCode(401)->respond([
			'message' => $message,
			'status_code' => $this->getStatusCode()
		]);
}

protected function respondForbidden($message) 
{
	return $this->setStatusCode(403)->respond([
			'message' => $message,
			'status_code' => $this->getStatusCode()
		]);
}

public function respondNotFound($message)
{
	return $this->setStatusCode(404)->respond([
		'message' => $message,
		'status_code' => $this->getStatusCode()
	]);
}

protected function respondUnprocessableEntity($message) 
{
	return $this->setStatusCode(422)->respond([
			'message' => $message,
			'status_code' => $this->getStatusCode()
		]);
}

public function respondInternalError($message)
{
	return $this->setStatusCode(500)->respond([
		'message' => $message,
		'status_code' => $this->getStatusCode()
	]);
}

public function respondBadGateway($message)
{
	return $this->setStatusCode(502)->respond([
		'message' => $message,
		'status_code' => $this->getStatusCode()
	]);
}

}