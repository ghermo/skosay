<?php

class ManagerController extends \BaseController {

	public function message($id,$fid){
        //dd(StoreGroup::where('storeid', $id)->count());
        $messages = DB::table('messages')->where('issueid',$fid)->get();
        $deptid = DB::table('table')->select('id')->where('storeid', $id);
            if(StoreGroup::where('storeid', $id)->count() != 0 && Auth::user()->type == 'manager'){
                DB::table('messages')->where('issueid', $fid)->whereNotIn('userid', [Auth::user()->id])->update(array('msgStatus' => 'read'));
            }
            else if(DepartmentGroup::where('deptid', $id)->count() == 0 && Auth::user()->type == 'manager'){
                DB::table('messages')->where('issueid', $fid)->whereNotIn('userid', [Auth::user()->id])->update(array('msgStatus' => 'read'));
        }
        return View::make('history.message',['messages'=>$messages]);
	}

	public function store(){
        if(Auth::check())
        {
            
            $date = new DateTime();
            $timeStamp = $date->format('Y-m-d H:i:s');
            $dateTime = date("m/d/Y h:i:s", strtotime($timeStamp));
            $type = DB::table('users')->where('id', Auth::user()->id)->first();
            $type = $type->type;
            if($type == 'admin')
            {
                $all = Input::all();

                $message = new Messages;
                
                $message->message = strip_tags(Input::get('reply'));
                $message->issueid = Input::get('issueid');
                $message->userid = Auth::user()->id;
                $message->msgType = 'reply';
                $message->created_at = $timeStamp;
                $message->updated_at = $timeStamp;
               
                $message->save();

                return Redirect::back();
            }
        }
        Redirect::to('administrator');
	}

    public function change($id){
          $all = Issue::find($id);
          $stat = $all->status;

          if($stat =='new'){
            $all->status = 'inprogress';
            $all->save();

            return Redirect::to('/new');
          }
          else if ($stat == 'inprogress'){
            $all->status = 'resolved';
            $all->save();

            return Redirect::to('/inprogress');
          }

    }

    public function historyPage(){
        if(Auth::check()){
            if(Auth::user()->type == 'manager'){
                    $stores = DB::table('stores')
                        ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                        ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                        ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                        ->join('issues', 'issues.storeid', '=', 'stores.id')
                        ->where('user-groups.userid', Auth::user()->id)->where('issues.status', 'new')->groupBy('stores.id')->orderBy('issues.updated_at','DESC')->get();

                    $storeDept = DB::table('stores')
                        ->join('issues', 'issues.storeid', '=','stores.id')
                        ->join('departments', 'departments.id', '=', 'issues.isDept')
                        ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                        ->join('groups', 'groups.id', '=','department-groups.groupid')
                        ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                        ->where('user-groups.userid', Auth::user()->id)
                        ->where('issues.status','new')
                        ->groupBy('stores.id')
                        ->orderBy('issues.updated_at','DESC')
                        ->get();

                   

                    return View::make('manager/historypage')->with('stores', $stores)->with('departments', $storeDept);



            }
            else if(Auth::user()->type == 'admin'){
                $stores = DB::table('stores')
                    ->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', 'new')->groupBy('stores.id')->orderBy('issues.updated_at','DESC')->get();

                return View::make('manager/historypage')->with('stores', $stores);
            }
            else{
                return Redirect::to('administrator');
            }
        }
        else{
            return Redirect::to('administrator');
        }
    }

    public function storeNotif($storeId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }

        $hasDept = DB::table('stores')->where('id',$storeId)->pluck('hasDept');

        if (Auth::user()->type == 'admin') {
            $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status','new')->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->whereNotIn('messages.userid', [$currentId])->count();
        }
        else{
            if ($hasDept == 0) {
             $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status','new')->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->whereNotIn('messages.userid', [$currentId])->count();
            }
            else{
                $count = DB::table('messages')
                ->join('issues', 'messages.issueid', '=', 'issues.id')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->where('user-groups.userid', Auth::user()->id)
                ->where('messages.msgStatus', 'new')
                ->where('issues.storeid', $storeId)
                ->where('issues.status','new')
                ->whereNotIn('messages.userid', [Auth::user()->id])->count();
            }
        }

        if($count == 0){
            $response = array(
                'count' => '<span class="badge badge-assertive"></span>',
                'className' => 'storeNotif_'.$storeId
            );
        }
        else{
            $response = array(
                'count' => '<div class="badge-chat"><span class="badge-text">'.$count.'</span></div>',
                'className' => 'storeNotif_'.$storeId
            );
        }

        return $response;
    }

    public function storeNotifInProg($storeId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }


        $hasDept = DB::table('stores')->where('id',$storeId)->pluck('hasDept');

        if ($hasDept != 0) {
            $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status', 'inprogress')->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->whereNotIn('messages.userid', [$currentId])->count();
        }
        else{
            $count = DB::table('messages')
                ->join('issues', 'messages.issueid', '=', 'issues.id')
                ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('issues.status', 'inprogress')
                ->where('messages.msgStatus', 'new')
                ->where('issues.storeid', $storeId)
                ->whereNotIn('messages.userid', [Auth::user()->id])->count();
        }

        if($count == 0){
            $response = array(
                'count' => '<span class="badge badge-assertive"></span>',
                'className' => 'storeNotif_'.$storeId
            );
        }
        else{
            $response = array(
                'count' => '<div class="badge-chat"><span class="badge-text">'.$count.'</span></div>',
                'className' => 'getMessage_'.$storeId
            );
        }

        return $response;
    }

    public function historyDetails($id){
        if(Auth::check())
        {
            if(Auth::user()->type == 'admin'){
                $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','new')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();


                return View::make('manager/historydetails')->with('issues', $issues);
            }
            else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('issues.status','=','new')
                    ->where('issues.storeid','=',$id)
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->select('issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','new')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }

                //return $issues;
                return View::make('manager/historydetails')->with('issues', $issues);
            }
        }
    }

    public function issueNotif($issueId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }

        $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [$currentId])
            ->whereNotIn('messages.userid', [$currentId])
            ->where('messages.msgStatus', 'new')->where('issues.id', $issueId)->count();

            if($count == 0){
                $response = array(
                    'count' => '<span class="badge badge-assertive"></span>',
                    'className' => 'storeNotif_'.$issueId
                );
            }
            else{
                $response = array(
                    'count' => '<div class="badge-chat"><span class="badge-text">'.$count.'</span></div>',
                    'className' => 'issueNotif_'.$issueId
                );
            }

        return $response;
    }

    public function inProgress(){
        if(Auth::user()->type == 'admin'){
            $stores = DB::table('stores')->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', 'inprogress')->groupBy('stores.id')->orderBy('stores.updated_at','DESC')->get();

            return View::make('manager/historyinprog')->with('stores', $stores);
        }
        else if(Auth::user()->type == 'manager'){
            $stores = DB::table('stores')
                ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                ->join('issues', 'issues.storeid', '=', 'stores.id')->where('user-groups.userid', Auth::user()->id)->where('issues.status', 'inprogress')->groupBy('stores.id')->orderBy('issues.updated_at', 'DESC')->get();

            $storeDept = DB::table('stores')
                        ->join('issues', 'issues.storeid', '=','stores.id')
                        ->join('departments', 'departments.id', '=', 'issues.isDept')
                        ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                        ->join('groups', 'groups.id', '=','department-groups.groupid')
                        ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                        ->where('user-groups.userid', Auth::user()->id)
                        ->where('issues.status','inprogress')
                        ->groupBy('stores.id')
                        ->orderBy('issues.updated_at','DESC')
                        ->get();

            
            return View::make('manager/historyinprog')->with('stores', $stores)->with('departments', $storeDept);
        }
        else{
            return Redirect::to('administrator');
        }

        //return View::make('manager/historyinprog')->with('stores', $stores);
    }

    public function inProgressDetails($id){
        if(Auth::user()->type == 'admin'){
            $issues = DB::table('issues')
                ->join('stores', 'stores.id', '=', 'issues.storeid')->where('stores.id', $id)->where('issues.status','=','inprogress')->select('issues.isDept','issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at')->orderBy('issues.updated_at', 'DESC')->get();

                return View::make('manager/historydetails_inprog')->with('issues', $issues);
            }
            else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->select('issues.isDept','issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('issues.status','=','inprogress')
                    ->where('stores.id','=',$id)
                    ->groupBy('issues.id')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->select('issues.isDept','issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','inprogress')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }

                //return $issues;
                return View::make('manager/historydetails_inprog')->with('issues', $issues);

            }
            else{
                return Redirect::to('administrator');
            }
        }

    public function resolved(){
        if(Auth::user()->type == 'admin'){
            $stores = Issue::groupBy('storeid')->where('status','=','resolved')->orderBy('updated_at','DESC')->get();

            return View::make('manager/historyres')->with('stores', $stores);
        }
        else if(Auth::user()->type == 'manager'){
            $stores = DB::table('stores')
                ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                ->join('issues', 'issues.storeid', '=', 'stores.id')->where('user-groups.userid', Auth::user()->id)->where('issues.status', 'resolved')->groupBy('stores.id')->orderBy('issues.updated_at', 'DESC')->get();

            $storeDept = DB::table('stores')
                        ->join('issues', 'issues.storeid', '=','stores.id')
                        ->join('departments', 'departments.id', '=', 'issues.isDept')
                        ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                        ->join('groups', 'groups.id', '=','department-groups.groupid')
                        ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                        ->where('user-groups.userid', Auth::user()->id)
                        ->where('issues.status','resolved')
                        ->groupBy('stores.id')
                        ->orderBy('issues.updated_at','DESC')
                        ->get();

            return View::make('manager/historyres')->with('stores', $stores)->with('departments', $storeDept);           
        }
        else{
            return Redirect::to('administrator');
        }

        //return View::make('manager/historyres')->with('stores', $stores);
    }

    public function resolvedDetails($id){
        if(Auth::user()->type == 'admin'){
            $issues = DB::table('issues')
                ->join('stores', 'stores.id', '=', 'issues.storeid')->where('stores.id', $id)->where('issues.status','=','resolved')->select('issues.storeid', 'issues.isDept', 'issues.id', 'issues.userid', 'issues.updated_at')->orderBy('issues.updated_at', 'DESC')->get();
            return View::make('manager/historydetails_resolved')->with('issues', $issues);
        }
        else if(Auth::user()->type == 'manager'){

                $hasDept = DB::table('stores')->where('id',$id)->pluck('hasDept');

                if($hasDept == 1) {
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                    ->join('groups', 'groups.id', '=','department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->select('issues.isDept','issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('issues.status','=','resolved')
                    ->where('stores.id','=',$id)
                    ->groupBy('issues.id')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }
                else{
                    $issues = DB::table('issues')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->select('issues.isDept','issues.storeid', 'issues.id', 'issues.userid', 'issues.updated_at','issues.isDept')
                    ->where('stores.id', $id)
                    ->where('issues.status','=','resolved')
                    ->orderBy('issues.updated_at', 'DESC')
                    ->get();

                }

                //return $issues;
                return View::make('manager/historydetails_resolved')->with('issues', $issues);

            }
            else{
                return Redirect::to('administrator');
            }
    }
}
