<?php

class SessionsController extends \BaseController {

	public function create()
	{
        if(Auth::check())
        {
            if(Auth::user()->type == 'admin' || Auth::user()->type == 'manager')
            {	
                return Redirect::to('historypage'); 
            }
            else if(Auth::user()->type == 'consumer') 
            {
                return Redirect::to('location');
            }
        }
        else
        {
    		return View::make('manager.login');
        }
        return View::make('manager.login');
    }

    public function store()
    {
        $userData = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        if(Auth::attempt($userData))
        {
            if(Auth::user()->type == 'admin' || Auth::user()->type == 'manager')
            {
                $response = array(
                    'msg' => '',
                    'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="true"/>'
                );
            }
            else
            {
                Auth::logout();
                $response = array(
                    'msg' => '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Invalid Credentials</font></h4></center></div>',
                    'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
                );
            }
        }
        else
        {
            $response = array(
                'msg' => '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Invalid Credentials</font></h4></center></div>',
                'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
            );
        }
        return $response;
    }

	public function destroy()
    {
		Auth::logout();
        return Redirect::to('administrator');
	}


}
