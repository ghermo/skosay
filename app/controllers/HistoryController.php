<?php

class HistoryController extends \BaseController {

    public function message($id,$fid){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
         else{
            $currentId = $_COOKIE['fbId'];
         }
            
        $messages = Message::where('issueid', $fid)->get();
        Message::where('issueid', $fid)->whereNotIn('userid', [$currentId])->update(array('msgStatus' => 'read'));
        return View::make('history.message',['messages'=>$messages,'trueid'=>$id]);
    }

    public function issues($id){
        if(Auth::check()){
            if(Auth::user()->type == 'admin'){
                $issue = DB::table('issues')->where('storeid',$id)->orderBy('id', 'DESC')->get();
            }else{
                $issue = DB::table('issues')->where('storeid',$id)->where('userid', Auth::user()->id)->where('status', '!=', 'archive')->orderBy('updated_at', 'DESC')->get();
            }
        }else if(isset($_COOKIE['fbId'])){
            $issue = DB::table('issues')->where('storeid',$id)->where('userid', $_COOKIE['fbId'])->orderBy('id', 'DESC')->get();
        }else if(isset($_COOKIE['guestId'])){
            $issue = DB::table('issues')->where('storeid',$id)->where('userid', $_COOKIE['guestId'])->orderBy('id', 'DESC')->get();
        }else{
            return Redirect::to('message/login');   
        }
        return View::make('history.issues',['issue'=>$issue,'storenum'=>$id])->with('issueid', $id);     
    
    }

    public function index(){
        if(Auth::check()){
            if(Auth::user()->type == 'admin' || Auth::user()->type == 'manager'){
                Auth::logout();
                return Redirect::to('/');
            }else{
                $store = DB::table('stores')
                    ->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', '!=', 'archive')->join('messages', 'messages.issueid', '=', 'issues.id')->where('issues.userid', Auth::user()->id)->groupBy('stores.id')->orderBy('issues.updated_at', 'DESC')->get();
            }
        }else if(isset($_COOKIE['fbId'])){
            $store = DB::table('stores')
                    ->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', '!=', 'archive')->join('messages', 'messages.issueid', '=', 'issues.id')->where('issues.userid', $_COOKIE['fbId'])->groupBy('stores.id')->orderBy('issues.updated_at', 'DESC')->get();
        }else if(isset($_COOKIE['guestId'])){
            $store = DB::table('stores')
                ->join('issues', 'issues.storeid', '=', 'stores.id')->where('issues.status', '!=', 'archive')->where('issues.userid', $_COOKIE['guestId'])->groupBy('stores.id')->get();
        }else{
            return Redirect::to('/');
        }

        return View::make('history.index',['store'=>$store]);

    }

    public function reply(){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
        else{
            return Redirect::to('/message/login');
        }

        $replyString = strip_tags(trim(Input::get('reply')));
        $date = new DateTime();
        $timeStamp = $date->format('Y-m-d H:i:s');
        $dateTime = date("m/d/Y H:i:s", strtotime($timeStamp));
        $sID = DB::table('issues')->where('id', Input::get('issueId'))->pluck('storeid');
        $issueStatus = DB::table('issues')->where('id', Input::get('issueId'))->pluck('status');
        $deptID = DB::table('issues')->where('id', Input::get('issueId'))->pluck('isDept');
        if($issueStatus == 'archive'){
            //
        }
        else{
            pushNotify($sID, $deptID);    
        if(Input::file('qwe') != null){
            if(Input::file('qwe')->getSize()<2097152){
                $quality = 60;  
                $file=Input::file('qwe');
                $rand = rand(0, 1000);
                $orFile = $file->getClientOriginalName(); 
                $filename = $rand.$orFile;
                $img=Image::make($file);
                $path = public_path('uploads/' . $filename);
                $img->resize(300, null, function ($constraint){$constraint->aspectRatio();});
                $img->save($path, $quality);
                $fphoto = 'uploads/'.$filename;
            }
        }else{
            $fphoto = '';
        }
            
        DB::table('messages')->insert(
            array(
            'userid' => $currentId,
            'issueid' => Input::get('issueId'),
            'photo' => $fphoto,
            'message' => $replyString,
            'msgType' => 'reply',
            'msgStatus' => 'new',
            'created_at' => $timeStamp,
            'updated_at' => $timeStamp
        ));

            $issueid = Input::get('issueId');
            $fakeData= array('id'=> $issueid, 'updated_at' => $timeStamp);
            DB::table('issues')->where('id', $issueid)->update($fakeData);


        if(Auth::check() && Auth::user()->type == 'admin'){
            $userPhoto = '<img src="../../images/manager.png" style="width:100%; max-width:80px;">';
        }else if(Auth::check() && Auth::user()->type == 'manager'){
            $userPhoto = '<img src="../../images/manager.png" style="width:100%; max-width:80px;">';
        }else{
            $userPhoto = '<img src="../../images/user.png" style="width:100%; max-width:80px;">';
        }

        $sphoto = '<img src="../../'.$fphoto.'" style="width:100%;">';

        if (Input::file('qwe') != null) {
            $response = array(
                'htmlElem' => '<div style="padding:6px;"><div style="width:20%; float:right;margin-left: 18px;margin-top: 6px;"></div><div class="bubble"><p>'.$sphoto.'</p><p>'.$replyString.'</p><div class="pointer"></div><div class="pointerBorder"></div></div></div><div class="messageDate">'.$dateTime.'</div>'
            );
            
        }else{
            $response = array(
                'htmlElem' => '<div style="padding:6px;"><div style="width:20%; float:right;margin-left: 18px;margin-top: 6px;"></div><div class="bubble"><p>'.$replyString.'</p><div class="pointer"></div><div class="pointerBorder"></div></div></div><div class="messageDate">'.$dateTime.'</div>'
            );
        }
        return $response;
        }
    }
    
    public function reply1($id){   
            $all = Issue::find($id);
            $stat = Input::get('statusChange');
            $all->status = $stat;
            $all->save();
            if($stat == 'inprogress')
                $response = array('htmlElem' => '2');
            else if($stat == 'resolved')
                $response = array('htmlElem' => '3');
            else if($stat == 'archive')
                $response = array('htmlElem' => '1');
            else
                $response = array('htmlElem' => '1'); 
            
            if(!(Input::get('reply1') == '' || Input::get('reply1') == ' '))
            {     
                if(Auth::check()){
                    $currentId = Auth::user()->id;
                }else if(isset($_COOKIE['fbId'])){
                    $currentId = $_COOKIE['fbId'];
                }else if(isset($_COOKIE['guestId'])){
                    $currentId = $_COOKIE['guestId'];
                }else{
                    return Redirect::to('/message/login');
                }

                $replyString = strip_tags(trim(Input::get('reply1')));
                $date = new DateTime();
                $timeStamp = $date->format('Y-m-d H:i:s');
                $dateTime = date("m/d/Y H:i:s", strtotime($timeStamp));
                $sID = DB::table('issues')->where('id', Input::get('issueId1'))->pluck('storeid');
                $sname = DB::table('stores')->where('id',$sID)->pluck('storename');
                DB::table('messages')->insert(
                    array(
                    'userid' => $currentId,
                    'issueid' => Input::get('issueId1'),
                    'message' => $replyString,
                    'msgType' => 'reply',
                    'msgStatus' => 'new',
                    'created_at' => $timeStamp,
                    'updated_at' => $timeStamp
                ));  
            }
            return $response; 
        }    

       

    public function checkReply(){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }
            
        if(Input::get('getLatestMsg') == ''){
            $latestMsg = 0;
        }else{
            $latestMsg = Input::get('getLatestMsg');
        }

        $newMsg = DB::table('messages')->where('issueid', Input::get('msgIssueId'))->where('id', '>', $latestMsg)->whereNotIn('userid', [$currentId])->where('msgStatus', 'new')->orderBy('created_at', 'ASC');

        if($newMsg->count() != 0){
            $newMsgs = $newMsg->get();
            $response = array();
            for($i=0; $i< count($newMsgs); $i++){
                $replyType = DB::table('users')->where('id', $newMsgs[$i]->userid)->pluck('type');
                $dateTime = date("m/d/Y H:i:s", strtotime($newMsgs[$i]->created_at));
                $name = DB::table('users')->where('id', $newMsgs[$i]->userid)->pluck('name');

                $sphoto = '<img src="../../'.$newMsgs[$i]->photo.'" style="width:100%;">';
                if($replyType == 'admin'){
                    $userPhoto = '<img src="../../images/manager.png" style="width:100%; max-width:80px;">';
                }elseif($replyType == 'manager'){
                    $userPhoto = '<img src="../../images/manager.png" style="width:100%; max-width:80px;">';
                }else{
                    $userPhoto = '<img src="../../images/user.png" style="width:100%; max-width:80px;">';
                }

                if ($newMsgs[$i]->photo == NULL) {
                    $response[$i] = '<div style="padding:6px;"><div style="width:20%; float:left;margin-right: 18px;margin-top: 6px;"></div><div class="bubble-left"><p>'.$newMsgs[$i]->message.'</p><div class="pointer"></div><div class="pointerBorder"></div></div></div><div class="messageDate">'.$dateTime.' by '. $name .'</div>';
                    DB::table('messages')->where('id', $newMsgs[$i]->id)->update(array('msgStatus' => 'read'));
                }else{
                    $response[$i] = '<div style="padding:6px;"><div style="width:20%; float:left;margin-right: 18px;margin-top: 6px;"></div><div class="bubble-left"><p>'.$sphoto.'</p><p>'.$newMsgs[$i]->message.'</p><div class="pointer"></div><div class="pointerBorder"></div></div></div><div class="messageDate">'.$dateTime.' by '. $name .'</div>';
                    DB::table('messages')->where('id', $newMsgs[$i]->id)->update(array('msgStatus' => 'read'));
                }
            }
            return $response;
        }
    }

//----------CHANGE PASSWORD

    public function changepass(){
        return View::make('history/changepass');
    }

//-----------------START login2
    public function login2(){
        $email = Input::get('email');
        if(Auth::check()){
            return Redirect::to('message');
        }else if(isset($_COOKIE['guestId']) || isset($_COOKIE['fbId'])){
            return Redirect::to('location');
        }else{
            return View::make('history/login2')->with('email',$email);
        }
    }

    public function doLogin2(){
        $userData = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

            if(!Auth::attempt($userData)){
                    $response = array(
                    'msg' => '<div><center><h4><font color="red">Invalid Credentials</font></h4></center></div>',
                    'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
                    );    
            }else{
                if(Auth::user()->type == 'admin' || Auth::user()->type == 'manager'){
                    Auth::logout();
                    $response = array(
                        'msg' => '<div><center><h4><font color="red">Invalid Credentials</font></h4></center></div>',
                        'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
                    );
                }
                else if(Auth::user()->confirmation == 0){
                    $response = array(
                        'msg' => '<div><center><h4><font color="red">Please confirm your account first</font></h4></center></div>',
                        'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
                    );
                    Auth::logout();
                }else{
                    if(isset($_COOKIE['fbId'])){
                        setcookie('fbId', '', time()-3600);
                        setcookie('fName', '', time()-3600);
                        unset($_COOKIE['fbId']);
                        unset($_COOKIE['fName']);
                    }

                    if(isset($_COOKIE['guestId'])){
                        setcookie('guestId', '', time()-3600);
                        unset($_COOKIE['guestId']);
                    }

                    $response = array(
                        'msg' => '',
                        'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="true"/>'
                    );
                }
            }
        return $response;
    }

//----------------END login2

    public function filterLogout(){
        Auth::logout();
        setcookie('fbId', '', time()-3600);
        setcookie('fName', '', time()-3600);
        unset($_COOKIE['fbId']);
        unset($_COOKIE['fName']);

        setcookie('guestId', '', time()-3600);
        unset($_COOKIE['guestId']);
        return Redirect::to('/');
    }

    public function getMessage($storeId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }

        $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [$currentId])
            ->whereNotIn('messages.userid', [$currentId])
            ->where('messages.msgStatus', 'new')->where('issues.storeid', $storeId)->count();

        if($count == 0){
            $response = array(
                'count' => '<span class="badge badge-assertive"></span>',
                'className' => 'storeNotif_'.$storeId
            );
        }else{
            $response = array(
                'count' => '<div class="badge-chat"><span class="badge-text">'.$count.'</span></div>',
                'className' => 'getMessage_'.$storeId
            );
        }
        return $response;
    }

    public function issueNotif($issueId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }

        $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [Auth::user()->id])
            ->whereNotIn('messages.userid', [Auth::user()->id])   
             ->where('messages.msgStatus', 'new')->where('issues.id', $issueId)->count();

       if($count == 0){
            $response = array(
                'count' => '<span class="badge badge-assertive"></span>',
                'className' => 'issueNotif_'.$issueId
            );
        }else{
            $response = array(
                    'count' => '<div class="badge-chat"><span class="badge-text">'.$count.'</span></div>',
                    'className' => 'getIssue_'.$issueId
                );
            }
            return $response;
        }
  
    public function getMessageResolved($issueId){
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }

        $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [$currentId])
            ->whereNotIn('messages.userid', [$currentId])
            ->where('messages.msgStatus', 'new')->where('issues.id', $issueId)->count();

        if($count == 0){
            $response = array(
                    'count' => '<span class="badge badge-assertive"></span>',
                    'className' => 'storeNotif_'.$issueId
                );
        }else{
            $response = array(
                    'count' => '<div class="badge-chat"><span class="badge-text">'.$count.'</span></div>',
                    'className' => 'getMessage_'.$issueId
                );
        }
        return $response;
    }

    public function loginGuest(){
        function generateRandomString($length = 10){
            $characters = '0123456789';
            $randomString = '';
            for ($i = 0; $i < $length-5; $i++) {
                    $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        }

        function detectDuplicate(){
            $randVar = generateRandomString();
            $bool = DB::table('issues')->where('userid', '=', $randVar)->count();
                if($bool == 0){
                    return $randVar;
                }
                else{
                    detectDuplicate();
                }
        }
            $randVar = detectDuplicate();
            $month = 604800 + time();
            setcookie('guestId', $randVar, $month);
            Session::put('inputAll', Input::all());
        return Redirect::to('feedback');
    }

    public function loginFb(){
        if((Auth::check()) && (Auth::user()->type != 'admin')){
            Auth::logout();
        }

            $month = 2592000 + time();
//        setcookie('inputAll', serialize(Input::all()), $month);
            $application = array(
                'appId' => '1532542913646124',
                'secret' => 'd214d47325b954a152549ae50731990a'
        );
        $permissions = 'email';
        $url_app = value(URL::action('HistoryController@loginFb'));
        FacebookConnect::getFacebook($application);

        $noemail = 0;
        if (!isset(FacebookConnect::getUser($permissions,$url_app)["user_profile"]["email"])) {
            $email2 = FacebookConnect::getUser($permissions,$url_app)["user_profile"]["first_name"].''.FacebookConnect::getUser($permissions,$url_app)["user_profile"]["last_name"].'@facebook.com';
            $noemail = 1;
        }else{
            $email2 = FacebookConnect::getUser($permissions,$url_app)["user_profile"]["email"];
        }

        
        $email = str_replace("%40", "@", $email2);
        $name = str_replace("+", " ",FacebookConnect::getUser($permissions,$url_app)["user_profile"]["first_name"].' '.FacebookConnect::getUser($permissions,$url_app)["user_profile"]["last_name"]);
        
        $fbid = FacebookConnect::getUser($permissions,$url_app)["user_profile"]["id"];

        $bool = DB::table('users')->where('email', $email)->count();

        if ($bool == 0) {

            function generateRandomString($length = 20){
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++){
                        $randomString .= $characters[rand(0, strlen($characters) - 1)];
                    }
                    return $randomString;
                }
                    function detectDuplicate(){
                        $randVar = generateRandomString();
                        $bool = DB::table('messages')->where('userid', '=', $randVar)->count();
                        if($bool == 0){
                            return $randVar;
                        }
                        else{
                            detectDuplicate();
                        }
                }

            $confirmCode = detectDuplicate();

            $length = 10;

            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

            $user = new Register;
            $user->fbid = $fbid;
            $user->email = $email;
            $user->name = $name;
            $user->type = 'consumer';
            $user->password = Hash::make($randomString);
            $user->confirmation = 2;
            $user->confirmation_code = $confirmCode;
            $user->save();


            $url = URL::to('/changepassword/'.$email);

            if ($noemail == 0) {
                Mail::send('emails.template2', array('us' => $email, 'pw' => $randomString, 'url' => $url), function($message) use($email){
                $message->from('support@skosay.com', 'Skosay - Say it anywhere!');
                $message->to($email)->subject('Skosay Account confirmation');
                });
            }
        }

        $usId = DB::table('users')->where('fbid', '=', $fbid)->pluck('id');        

        setcookie('fbId', $usId, $month);
        setcookie('fName', FacebookConnect::getUser($permissions,$url_app)["user_profile"]["first_name"].' '.FacebookConnect::getUser($permissions,$url_app)["user_profile"]["last_name"], $month);
        setcookie('fEmail', $email, $month);

        $try = Input::get('fbQR');

        if ($try == "" || $try == NULL){
            return Redirect::to('location');
        }
        else{
            return Redirect::to('feedback/'.$try);            
        }
    }

    public function loginMessage(){
        return View::make('history.loginMessage');
    }

    public function loginMessageGuest(){
        if((Auth::check()) && (Auth::user()->type != 'admin')) {
                Auth::logout();
        }

        function generateRandomString($length = 10) {
            $characters = '0123456789';
            $randomString = '';
            for ($i = 0; $i < $length-5; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        }

        function detectDuplicate(){
            $randVar = generateRandomString();
            $bool = DB::table('messages')->where('userid', '=', $randVar)->count();
            if($bool == 0){
                return $randVar;
            }
            else{
                detectDuplicate();
            }
        }
            $randVar = detectDuplicate();
            $month = 2592000 + time();
            setcookie('guestId', $randVar, $month);

        return Redirect::to('message');
    }

    public function loginMessageFb(){
        if((Auth::check()) && (Auth::user()->type != 'admin')) {
                Auth::logout();
        }

            $month = 2592000 + time();
        $application = array(
            'appId' => '1532542913646124',
            'secret' => 'd214d47325b954a152549ae50731990a'
        );
        $permissions = 'email';
        $url_app = value(URL::action('HistoryController@fbLogin'));
        FacebookConnect::getFacebook($application);
        setcookie('fbId', FacebookConnect::getUser($permissions,$url_app)["user_profile"]["id"], $month);
        return Redirect::to('message');
    }

    public function fbLogin(){
        $application = array(
            'appId' => '1532542913646124',
            'secret' => 'd214d47325b954a152549ae50731990a'
        );
        $permissions = 'email';
        $url_app = value(URL::action('LocationController@listLocation'));
        FacebookConnect::getFacebook($application);
        if(!isset($_COOKIE['fbId'])){
            $month = 2592000 + time();
            setcookie('fbId', FacebookConnect::getUser($permissions,$url_app)["user_profile"]["id"], $month);
        }
        return Redirect::to('message');
    }

    public function doLogin3(){
        $userData = array(
            'email' => Input::get('emailG'),
            'password' => Input::get('password')
        );

            if(!Auth::attempt($userData)) {
                $response = array(
                    'msg' => '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Invalid Credentials1</font></h4></center></div>',
                    'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
            );
            }
            else{
                if(Auth::user()->type == 'admin' || Auth::user()->type == 'manager'){
                    Auth::logout();
                    $response = array(
                        'msg' => '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Invalid Credentials2</font></h4></center></div>',
                        'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
                    );
                }
            else if(Auth::user()->confirmation == 0){
                $response = array(
                    'msg' => '<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Please confirm your account first</font></h4></center></div>',
                    'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="false"/>'
                );
                Auth::logout();
            }else{
                if(isset($_COOKIE['fbId'])){
                    setcookie('fbId', '', time()-3600);
                    setcookie('fName', '', time()-3600);
                    unset($_COOKIE['fbId']);
                    unset($_COOKIE['fName']);
                }

                $response = array(
                    'msg' => '',
                    'hidden' => '<input type="hidden" name="testHidden" id="testHidden" value="true"/>'
                );
            }
        }
        return $response;
    }
}

