<?php

use Ehatid\Mailers\FeedbackMailer;
use Ehatid\Forms\ApplyForm;

class FeedbackController extends BaseController {

	protected $feedbackMailer;
	protected $applyForm;

	function __construct(FeedbackMailer $feedbackMailer, ApplyForm $applyForm)
	{
		$this->feedbackMailer = $feedbackMailer;
		$this->applyForm = $applyForm;
	}

	public function index() {
		return View::make('feedback.index');
	}

	public function mail()
	{
		$input = Input::all();

		$data = array(
            'input'=> $input
        );

        //dd($input);

        $this->applyForm->validate($input);
        
		$this->feedbackMailer->resendDetailsTo($input['email'], $data);
		return View::make('hello');
	}

}
