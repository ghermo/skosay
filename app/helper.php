<?php

function pushNotify($storeid, $deptid){
    if($deptid == '')
        $deptid = 0;
    if ($deptid == 0) {
       $receivers = DB::table('users')
        	->join('user-groups', 'user-groups.userid', '=', 'users.id')
                            ->join('groups', 'groups.id', '=', 'user-groups.groupid')
                            ->join('store-groups', 'store-groups.groupid', '=', 'groups.id')
                            ->join('stores', 'stores.id', '=', 'store-groups.storeid')
                            ->where('stores.id',$storeid)
                            ->lists('deviceid');
                                
    }else{
    	$receivers = DB::table('users')
                            ->join('user-groups', 'user-groups.userid', '=', 'users.id')
                            ->join('groups', 'groups.id', '=', 'user-groups.groupid')
                            ->join('department-groups', 'department-groups.groupid', '=', 'groups.id')
                            ->join('departments', 'departments.id', '=', 'department-groups.deptid')
                            ->where('departments.id', $deptid)
                            ->lists('deviceid');
    }

    $device_token = $receivers;

    $url = 'https://push.ionic.io/api/v1/push';

    $data = array(
        'tokens' => $device_token,
        'notification' => array('alert' => 'You have a new message'),     
    );
                 
    $content = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
    curl_setopt($ch, CURLOPT_USERPWD, "010e289bec4e52b1a225e248ed09ca55282c5e377e48fff1" . ":" ); 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'X-Ionic-Application-Id: 61b45343'
    ));
    $result = curl_exec($ch);
    curl_close($ch);
}