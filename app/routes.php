<?php
/*Authored by: Niko*/
/*@START*/
Route::get('fb', 'HistoryController@fb');
Route::get('fbLogin', 'HistoryController@fbLogin');
/*@END*/

// SEARCH ROUTES
Route::get('search', 'SearchController@init');
Route::post('searchThis','SearchController@search');

// LOCATION ROUTES
Route::get('location/logout', 'LocationController@logOut');

// MESSAGE ROUTES
Route::get('message', 'HistoryController@index');
Route::get('/issues/{id}','HistoryController@issues');
Route::get('logoutUser', 'HistoryController@filterLogout');
Route::get('feedback/logoutUser', 'HistoryController@filterLogout');
Route::get('/issues/{id}/{fid}','HistoryController@message');

// MANAGER APP ROUTES
Route::get('/changestatus/{id}', 'ManagerController@change');
Route::get('administrator','SessionsController@create');
Route::get('logout','SessionsController@destroy');
Route::get('/message/{id}/{fid}','ManagerController@message');
Route::post('/message/{id}/{fid}','ManagerController@store');

Route::get('historypage', 'ManagerController@historyPage');
Route::get('historydetails/{id}', 'ManagerController@historyDetails');
Route::get('historydetails_inprog/{id}', 'ManagerController@inProgressDetails');
Route::get('historydetails_resolved/{id}', 'ManagerController@resolvedDetails');

Route::get('analytics', function(){
     
        return View::make('manager/analytics2'); 
});

Route::get('/new', function()
{
    return Redirect::to('historypage');
});
Route::get('/inprogress', 'ManagerController@inProgress');
Route::get('/resolved', 'ManagerController@resolved');
Route::post('session/store', 'SessionsController@store');


// LOCATION ROUTES
Route::get('/', 'LocationController@initLogin');
Route::get('location', 'LocationController@listLocation');
Route::get('location2','LocationController@initLogin');
//Route::get('feedback', 'LocationController@feedbackInit');
Route::post('feedback', 'LocationController@show');
Route::post('feedback2', 'LocationController@feedback2');
Route::post('saveFeedback', 'LocationController@saveFeedback');
Route::get('saveFeedback', 'LocationController@saveFeedback2');

Route::get('success', function() {
    return View::make('location.feedbackConfirm')
        ->with('storeCust', Customer::where('storename', Store::where('id', Route::input('storeId'))->pluck('storename'))->count()); 
}); 

//Route::post('feedbackLogin', 'LocationController@feedbackLogin');
Route::get('fbFeedback', 'LocationController@fbFeedback');
Route::get('feedbackLast', 'LocationController@feedbacklast');
Route::get('thanksGuest', function(){
    if(isset($_COOKIE['guestId'])){
        return View::make('location.feedbackQConfirm');
    }else{
        return Redirect::to('/');
    }
});


// QR Code Trick
Route::get('feedback/{id}', 'LocationController@show2');

//Straight to Department Feedback
Route::get('feedback3/{did}/{sid}', 'LocationController@feedback3');



//Route::get('thanksUser', function($storeCust){
//    dd($storeCust);
//  if(Auth::check() || isset($_COOKIE['fbId'])){
//      return View::make('location.feedbackConfirm');
//  }else
//      return Redirect::to('/');
//});

//Change Password by STEPH :))


Route::get('changepassword/{id}', 'RegisterController@changepass2');
Route::get('changepassword', 'RegisterController@changepass');
Route::post('checkPass', 'RegisterController@checkPassword');
Route::post('checkPass2', 'RegisterController@checkPassword2');

// AJAX ROUTES
Route::post('getMessage_{id}', 'HistoryController@getMessage');
Route::post('getMessageResolved_{id}', 'HistoryController@getMessageResolved');
Route::post('sendReply', 'HistoryController@reply');
Route::post('sendReply1/{id}', 'HistoryController@reply1');
Route::post('checkReply', 'HistoryController@checkReply');
Route::post('issueNotif_{id}', 'ManagerController@issueNotif');
Route::post('storeNotif_{id}', 'ManagerController@storeNotif');
Route::post('storeNotifInProg_{id}', 'ManagerController@storeNotifInProg');
Route::get('getcount','LocationController@notifBadge');

//New login Routes
Route::get('login2', 'HistoryController@login2');
Route::post('login2', 'HistoryController@doLogin2');
Route::post('login3', 'HistoryController@doLogin3');
Route::get('loginFb', 'HistoryController@loginFb');
Route::post('loginGuest', 'HistoryController@loginGuest');
Route::get('loginGuest', 'HistoryController@loginMessageGuest');
Route::get('loginMessage', 'HistoryController@loginMessage');

//SIGN UP ROUTES
Route::get('register', 'RegisterController@register');
Route::post('register_action', 'RegisterController@store');
Route::post('checkEmail', 'RegisterController@checkEmail');
Route::post('checkPassword', 'RegisterController@checkPassword');
Route::post('signup', 'RegisterController@signup');

// FORGOT PASSWORD
Route::get('forgotPass', 'RegisterController@forgotPass');
Route::get('confirmForgot', 'RegisterController@confirmForgot');
// AJAX FOR FORGOT PASSWORD
Route::post('checkEmailForm', 'RegisterController@checkEmailForm');
Route::post('sendConfirmFp', 'RegisterController@sendConfirmFp');
Route::get('forgotPass/{id}/{fcode}', 'RegisterController@forgotPassForm');
Route::post('newPassword', 'RegisterController@newPassword');

// MAIL CONFIRM ROUTES
Route::get('confirmAccount/{id}', 'RegisterController@confirmAccount');
Route::get('mConfirmAccount/{id}', 'RegisterApiController@mConfirmAccount');




//THIS FUNCTION IS FOR ROUTE PROTECTION - IT REDIRECTS THE SYSTEM WHEN THE ROUTE/METHOD IS NOT FOUND AND/OR DOESN'T EXIST - Jan Sarmiento
//App::missing(function()
//{
    //return Response::view('errors.missing', array(), 404);
  //  return Redirect::to('/');
//});


//WEB SERVICE ROUTES

Route::get('webService', function()
{
    if(Auth::check()){
        if(Auth::user()->type == 'admin'){
            return Redirect::to('/webDash');
        }else if(Auth::user()->type == 'manager'){
           return Redirect::to('/administrator'); 
        }
        else{
           return Redirect::to('/'); 
        }
    }else if(isset($_COOKIE['fbId'])){
        return Redirect::to('/');   
    }else if(isset($_COOKIE['guestId'])){
        return Redirect::to('/');   
    }else{
        return View::make('webservice.login');
    }
    

});

Route::post('/webLogin', 'UsersController@webLogin');
Route::get('/webDash', function(){
    return View::make('webservice.userpage');
})->before('auth');
Route::get('/webStores', function(){
    return View::make('webservice.stores');
})->before('auth');

/* ROSEY */
Route::get('/webStoreGroups', function(){
    return View::make('webservice.storegroups');
})->before('auth');
/* */

Route::get('/webQuestions', function(){
    return View::make('webservice.questions');
})->before('auth');

Route::get('/webDept', function(){
    return View::make('webservice.department.department');
})->before('auth');

Route::get('/webReports', function(){
    return View::make('webservice.reports');
})->before('auth');

Route::get('/webCustomers', function(){
    return View::make('webservice.customer');
})->before('auth');


Route::get('webLogout', function(){Auth::logout(); return Redirect::to('/webService');});
Route::post('webCreateUser', 'ServiceController@create');

Route::post('webDeleteUser', 'ServiceController@delete');

Route::post('/webEditUser', 'ServiceController@edit');

Route::post('/webAddStore', 'ServiceController@addStore');

Route::post('/webDeleteStore', 'ServiceController@deleteStore');

Route::post('/webEditStore', 'ServiceController@editStore');

Route::get('/webGroup/{id}', function($id){
    return View::make('webservice.group')->with('gId', $id)->with('groupDetails', Group::where('id', $id)->first());
})->before('auth');
Route::post('/webEditGroup', 'ServiceController@editGroup');

Route::post('/webDeleteGroup', 'ServiceController@deleteGroup');

Route::post('/webAddGroup', 'ServiceController@addGroup');

Route::post('/webAddDetail', 'ServiceController@addDetail');

Route::post('/webDeleteDetail', 'ServiceController@deleteDetail');

Route::post('/webAddUserGroup', 'ServiceController@addUserGroup');

Route::post('/webEditUserGroup', 'ServiceController@editUserGroup');

Route::post('/webDeleteUserGroup', 'ServiceController@deleteUserGroup');

Route::post('/webDeleteQuestion', 'ServiceController@deleteQuestion');

Route::post('/webEditQuestion', 'ServiceController@editQuestion');

Route::post('/webAddQuestion', 'ServiceController@addQuestion');

Route::post('/webDeleteQuestionGroup', 'ServiceController@deleteQuestionGroup');

Route::post('/webDeleteQuestionGroupDetail', 'ServiceController@deleteQuestionGroupDetail');

Route::post('/webEditQuestionGroup', 'ServiceController@editQuestionGroup');

Route::post('/webEditQuestionGroupDetail', 'ServiceController@editQuestionGroupDetail');

Route::post('/webAddQuestionGroupDetail', 'ServiceController@addQuestionGroupDetail');

Route::get('/webQGroup/{id}', function($id){
    return View::make('webservice.question.assign-question')->with('qgId', $id)->with('qgroupDetails', Qgroup::where('id', $id)->first());
})->before('auth');

Route::post('/webDeleteQStoreGroup', 'ServiceController@deleteQStoreGroup');

Route::post('/webAddQStoreGroup', 'ServiceController@addQStoreGroup');

Route::post('/webAddQuestionGroup', 'ServiceController@addQuestionGroup');

Route::post('/webAddUserGroupConnection', 'ServiceController@addUserGroupConnection');

Route::post('/webAddDepartment', 'ServiceController@addDepartment');

Route::post('/webDeleteDepartment', 'ServiceController@deleteDepartment');

Route::post('/webDeleteDGroup', 'ServiceController@deleteDGroup');

Route::post('/webAddDGroup', 'ServiceController@addDepartmentGroup');

Route::post('/webAddGroup2', 'ServiceController@addGroup2');

Route::post('/webEditGroup2', 'ServiceController@editGroup2');

Route::post('/webAddQuestion2', 'ServiceController@addQuestion2');

Route::post('/webEditQuestion2', 'ServiceController@editQuestion2');

Route::post('/webAddDept2', 'ServiceController@addDept2');

Route::post('/webEditDept2', 'ServiceController@editDept2');

Route::post('/webAddUser2', 'ServiceController@addUser2');

Route::post('/webEditUser2', 'ServiceController@editUser2');

Route::get('activities/{id}', function($id)
{
    return View::make('webservice.activities')->with('id', $id);
});

Route::get('excel', 'ExcelController@expexcel');





Route::get('tutorial', function(){
    if(Auth::check()){
        if(Auth::user()->type == 'admin'){
            return Redirect::to('/');
        }else if(Auth::user()->type == 'manager'){
            return Redirect::to('/');
        }else{
            return View::make('tutorial.index');    
        }
    }else if(isset($_COOKIE['fbId'])){
        return View::make('tutorial.index');    
    }else{
        return Redirect::to('/');
    }
});
Route::get('terms', function(){
    if(Auth::check()){
       if(Auth::user()->type == 'admin'){
            return Redirect::to('/');
        }else if(Auth::user()->type == 'manager'){
            return Redirect::to('/');
        }else{
           return View::make('term.terms');   
        }
    }else if(isset($_COOKIE['fbId'])){
        return View::make('term.terms');     
    }else{
        return Redirect::to('/');
    }
        
});

Route::get('privacypolicy', function(){
    if(Auth::check()){
       if(Auth::user()->type == 'admin'){
            return Redirect::to('/');
        }else if(Auth::user()->type == 'manager'){
            return Redirect::to('/');
        }else{
           return View::make('term.policy');   
        }
    }else if(isset($_COOKIE['fbId'])){
        return View::make('term.policy');     
    }else{
        return Redirect::to('/'); 
    }
});

//*** OAuth
Route::get('oauth/authorize', ['before' => 'check-authorization-params|auth', function() {
    // display a form where the user can authorize the client to access it's data
    View::make('oauth/authorization-form', Authorizer::getAuthCodeRequestParams());
}]);

Route::post('oauth/authorize', ['before' => 'csrf|check-authorization-params|auth', function() {

    $params['user_id'] = Auth::user()->id;

    $redirectUri = '';

    // if the user has allowed the client to access its data, redirect back to the client with an auth code
    if (Input::get('approve') !== null) {
        $redirectUri = Authorizer::issueAuthCode('user', $params['user_id'], $params);
    }

    // if the user has denied the client to access its data, redirect back to the client with an error message
    if (Input::get('deny') !== null) {
        $redirectUri = Authorizer::authCodeRequestDeniedRedirectUri();
    }

    return Redirect::to($redirectUri);
}]);

//web service search routes
Route::get('/searchUser', 'SearchController@searchUser');

Route::get('/searchAssign', 'SearchController@searchAssign');

Route::get('/searchDept', 'SearchController@searchDept');

Route::get('/searchDeptGrp', 'SearchController@searchDeptGrp');

Route::get('/searchStores', 'SearchController@searchStores');

Route::get('/searchGroups', 'SearchController@searchGroups');

Route::get('/searchQuestions', 'SearchController@searchQuestions');

Route::get('/searchQGroups', 'SearchController@searchQGroups');

Route::get('/searchQGroupsToStores', 'SearchController@searchQGroupsToStores');

Route::get('/searchReports', 'SearchController@searchReports');


//OAuth endpoints 
Route::group(array('prefix' => 'api/v1', 'before' => 'oauth'), function()
{
    //Manager
    Route::post('/changestatus/{id}', 'ManagerApiController@change');
    Route::get('/message/{id}/{fid}','ManagerApiController@message');
    Route::post('/message/{id}/{fid}','ManagerApiController@store');

    Route::get('/new', 'ManagerApiController@newMessage');
    Route::get('/inprogress', 'ManagerApiController@inProgress');
    Route::get('/resolved', 'ManagerApiController@resolved');

    Route::get('historydetails/{id}', 'ManagerApiController@historyDetails');
    Route::get('historydetails_inprog/{id}', 'ManagerApiController@inProgressDetails');
    Route::get('historydetails_resolved/{id}', 'ManagerApiController@resolvedDetails');
    Route::get('issuelist/{id}', 'ManagerApiController@issuelist');

    //ajax
    Route::post('issueNotif_{id}', 'ManagerApiController@issueNotif');
    Route::post('storeNotif_{id}', 'ManagerApiController@storeNotif');
    Route::post('storeNotifInProg_{id}', 'ManagerApiController@storeNotifInProg');
    Route::post('checkreply', 'ManagerApiController@checkReply');
    
    //all
    Route::get('locations', 'ManagerApiController@viewAll');
    Route::get('locationsDetails/{id}', 'ManagerApiController@viewAllHistoryDetails');

    //WebService
    Route::post('webServiceCreateUser', 'WebServiceApiController@create');
    Route::post('webServiceEditUser', 'WebServiceApiController@edit');
    Route::post('webServiceDeleteUser', 'WebServiceApiController@delete');

    Route::post('webServiceAddStore', 'WebServiceApiController@addStore');
    Route::post('webServiceEditStore', 'WebServiceApiController@editStore');
    Route::post('webServiceDeleteStore', 'WebServiceApiController@deleteStore');

    Route::post('webServiceAddDepartment', 'WebServiceApiController@addDepartment');
    Route::post('webServiceEditDepartment', 'WebServiceApiController@editDepartment');
    Route::post('webServiceDeleteDepartment', 'WebServiceApiController@deleteDepartment');

});

Route::group(['prefix' => 'api/v1'], function()
{
    //ManagerLogin
    Route::post('managerLogin', 'LoginApiController@managerLogin');
    Route::get('logout','ManagerApiController@destroy');
    
    //Consumer
    Route::post('register', 'RegisterApiController@Register');
    Route::post('login', 'LoginApiController@Login');    

    //WebService Login
    Route::post('webServiceLogin', 'WebServiceApiController@webLogin');
    Route::post('webServiceSignup', 'WebServiceApiController@signup');

    //Customers/Industry/Tiers
    Route::post('addIndustry', 'CustomersApiController@addIndustry');
    Route::post('editIndustry', 'CustomersApiController@editIndustry');
    Route::post('deleteIndustry', 'CustomersApiController@deleteIndustry');

    Route::post('registerCustomer', 'CustomersApiController@addCustomer');
    
    Route::post('webhook', 'ManagerApiController@setDeviceID');
});

Route::get('d6ejnof7dqa2t0xou2msd6ejnof7dqa2t0xou2ms', function(){

        return View::make('cron.index');     
   
});


//Acorn Line
Route::get('acorn', function()
{
    return Redirect::to('feedback/dfd9ccf8672596b4f97ad6c1c9c513db190c6968');
});

Route::get('error', function()
{
    return View::make('error');
});



/*Route::get('/download', array('as' => 'do', function()
{
    $val = "C:\Users\Josh\Desktop\_ve2\skosay-consumer\public\uploads";
    echo "fsds";
    $filepath = $val . "//523bg-chitoge.png";
    echo $filepath;
    return Response::download($filepath);
    //response needs C:\Users\Josh\Desktop\ve2\skosay-consumer\public\uploads//523bg-chitoge.png
}));


  */

Route::get('/download',function(){
    $val = "/uploads/";
    //$val = rtrim($val);
    $filename = $_GET['photo'];
    $filepath = substr(public_path(),0,strlen(public_path())) . $val . substr($filename,8,strlen($filename));
        //echo $filepath;
    return Response::download($filepath);

});

Route::post('uploadAPI', 'ManagerApiController@uploadAPI');



//{{HTML::linkRoute('do','Download',array('photo' => $photo) , array('class' => 'button button-calm'))}}-->
//<a href="{{$fphoto}}" class="button button-calm" download="skosay{{$message->id}}" target="_blank"> 
                                 