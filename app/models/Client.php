<?php

class Client extends Eloquent {
        protected $guarded = array();
        protected $table = 'oauth_clients'; // table name
        public $timestamps = 'false' ; // to disable default timestamp fields

        // model function to store form data to database
        public static function saveFormData($data)
        {
            DB::table('oauth_clients')->insert($data);
        }

}

?>