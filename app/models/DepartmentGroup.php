<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class DepartmentGroup extends Eloquent implements UserInterface, RemindableInterface {

	public $timestamps = false;

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'department-groups';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */


}
