@extends('layouts.default2')
@section('content')
<?php
    $ctr = App::environment();
    $domain =  DB::table('domain')->where('appEnv',$ctr)->first();
?>
<script>
    function submitForm(){
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';
        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData){
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
                $('#errorMsg').empty().append(userData['msg']);
                setTimeout(function(){ $('#errorMsg').empty(); },3000);
            }
        })
    }

    function checkLogin(){
        if(document.getElementById('testHidden').value == 'true'){
            document.getElementById('redirectForm').submit();
        }else{
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0)
                submitForm();
        }
    });

    function validateInput(){
        if(document.getElementById('email').value.length != 0 && document.getElementById('password').value.length != 0 )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
    <p align="center" style="margin-top:3em;margin-bottom: 20px;">
    @if($ctr == 'skosay')
        <img src="images/skopic-manager.png" style="width:230px;">
    @else
        <img src="{{$domain->logo}}" style="width:230px;">
    @endif
    <br>
    </p>
<div class="list">
		<!-- if there are login errors, show them here -->
    <form method="POST" action="session/store" id="ajaxForm">
			{{ $errors->first('email') }}
			{{ $errors->first('password') }}
        <div class="card" style="margin-top: -0.5em;">
            <label class="item item-input">
                {{ Form::email('email', Input::old('email'), array('onkeyup' => 'validateInput()', 'id' => 'email', 'placeholder' => 'Email', 'autocapitalize' => 'off')) }}
            </label>
        </div>
        <div class="card" style="margin-top: -0.5em;">
            <label class="item item-input">
                {{ Form::password('password', array('onkeyup' => 'validateInput()', 'id' => 'password', 'placeholder' => 'Password'))  }}
            </label>
        </div>

        @if(Session::has('msg'))
            <p><i><center><font color="red">{{{ Session::get('msg') }}}</font></center></i></p>
        @endif
	{{ Form::close() }}
        <div class="list list-inset" style="margin-top: -1em;background-color:{{$domain->headColor}};">
            {{ Form::submit('Sign in', array('onclick' => 'submitForm()', 'disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized ')) }}
            <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Signing in. Please wait <span class="ion-refreshing"></span></button>
        </div>

        <form id="redirectForm" method="GET" action=""></form>
            <input type="hidden" name="testHidden" id="testHidden" value="none"/>
            <div style="margin-top: 2em;background-color:{{$domain->headColor}};" id="errorMsg" class="list list-inset"></div>
@stop