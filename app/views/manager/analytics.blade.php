@extends('layouts.default')
@section('content')

{{HTML::style('css/analytics.css')}}
<!-- jQuery -->
{{HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js')}}
<!-- {{HTML::script('http://code.jquery.com/jquery.min.js')}} -->
<!--  highcharts plugin -->
{{HTML::script('http://code.highcharts.com/highcharts.js')}}
<!-- SmartMenus jQuery plugin -->
{{HTML::script('js/jquery.smartmenus.js')}}
{{HTML::script('js/responsivemobilemenu.js')}}

<!-- Horizontal Nav plugin -->
{{HTML::script('js/jquery.horizontalNav.js')}}
{{HTML::script('js/date.format.js')}}

<!-- SmartMenus core CSS (required) -->
{{HTML::style('css/sm-core-css.css')}}
<!-- "sm-blue" menu theme (optional, you can use your own CSS, too) -->
{{HTML::style('css/sm-simple/sm-simple.css')}}
{{HTML::style('css/font-awesome.css')}}
{{HTML::style('css/responsivemobilemenu.css')}}
<!-- horizontal navigation bar that controls days, months, time view for skosay analytics -->
{{HTML::style('css/analytics_horizontalNav.css')}}

<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<br>
<br>
<?php $apage = 2;
$fbdetails=DB::select('SELECT * FROM fb_by_hour ORDER BY hour ASC;');
$fb_percentage= DB::select('select * from fb_per');
$category_all=DB::select('SELECT name FROM departments WHERE storeid="5553a7e5753a7184010273fc4d2b8249ee13c588"');
$qname=DB::select('SELECT * FROM question_name');
$qp=DB::select('SELECT * FROM wm_groceryp');

//echo $fbdetails;
?>
<!-- {{ HTML::image('images/skosay_gray.png' , 'Skosay Gray', array('class' => 'logo'))  }} -->
<script type="text/javascript">
//When document is ready...
var fbd={{json_encode($fbdetails)}}; //convert to JSON
var fbp={{json_encode($fb_percentage)}}; //convert to JSON
//alert(fbd[0]);

var d = new Date();
var n = d.getTime();

function store_in_array(arr)
{
	processed_json=new Array();
	 // Populate series
    for (i = 0; i < arr.length; i++){
        processed_json.push([new Date(arr[i].hour).getTime(), parseInt(arr[i].fb_num)]);
    }
	//alert(processed_json[0]);
    return processed_json;
} 

function calc_percentage(value)
{
	var p=(value * 100)/fbp[0].total;
	return p;
}
$(document).ready(function() {
  // Call horizontalNav on the navigations wrapping element
	$("#time_view").horizontalNav({ 
		responsive : true,
	    responsiveDelay : 100,
	    tableDisplay : true,
	    minimumItems : 0
	});

	Highcharts.setOptions({
		global: {
		    useUTC: false
		}
		});

	$('#response_bar').highcharts({
        chart: {
            type: 'column',
            zoomType: 'xy',
        },
        title: {
            text: 'ENGAGEMENT',
            style: {
                //color: '#FF00FF',
                fontSize: '12px'
            },
            align: 'center',
            x: 10
        },
         xAxis: [{
            type:'datetime',
            tickInterval:2*3600 * 1000
            //min:24*3600*1000,
            //max:23*3600*1000 
            /* dateTimeLabelFormats : {
                hour: '%I %p',
                minute: '%I:%M %p'
            },
 */
               /*  style: {
                    color: '#89A54E'
                }, 

            }  */
         
        }],   

        tooltip: {
        	formatter: function() {
                return  Highcharts.dateFormat('%e - %b - %Y',this.x)+'<br>'+Highcharts.dateFormat('%H:%M', this.x)+'</br>'+'<br>'+"No. of Feedback: "+'</br>'+'<b>'+this.y+'</b>';
            },
            shared: true
        },
        
        yAxis: [{ // Primary yAxis
            labels: {
                //format: '{value}�C',
                style: {
                    color: '#89A54E'
                }
            },
            min: 0,
            //max:4,
            tickInterval: 10

            /* title: {
                text: 'Temperature',
                style: {
                    color: '#89A54E'
                }
            } */
        }],
        /*  plotOptions:{
            series:{
                pointStart:Date.parse(fbd[0].hour),//UTC(2015,0,1),
                //pointInterval: 2*3600 * 1000,
                pointEnd: 23*3600 * 1000
            }
        }, */ 
        series: [{
            name: 'Feedback',
            data:store_in_array(fbd)  //[2,6,10,18,14,20,13,15,16,5,7,22],
            //pointStart: Date.UTC(2010, 0, 1),
            //pointInterval: 3600 * 1000 // one day
        }]
    });

	$('#pie_left').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'ENGAGEMENT RATE',
            	style: {
                    //color: '#FF00FF',
                    fontSize: '11px'
                }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        
        plotOptions: 
            {
            pie: 
           	{
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: 
                {
                    //enabled: false,
                    distance:-8,
                    //<b>{point.name}</b>: 
                    format: '{point.percentage:.1f} %',
                    style: 
                    {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
        		showInLegend: true,
        		legend: 
            	{
        			layout: 'vertical',
                    itemStyle: {
                       fontSize: '3px'
                       //color: '#A0A0A0'
                    },
                    itemHoverStyle: {
                       color: '#FFF'
                    },
                    itemHiddenStyle: {
                       color: '#444'
                    }

              },
            }
        },
        series: [{
            type: 'pie',
            //name: 'Browser share',
            data: [
                ['No',calc_percentage(fbp[0].no_total)],
                {
                    name: 'Yes',
                    y: calc_percentage(fbp[0].yes_total),
                    color: "grey",
                    sliced: true,
                    selected: true
                }
            ]
        }]
    });

	$('#pie_right').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'AVG. FEEDBACK TIME',
                 	style: {
                         //color: '#FF00FF',
                         fontSize: '11px'
                     }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: 
           {
            	allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: 
                {
                    //enabled: false,
                    distance:-1,
                    //<b>{point.name}</b>: 
                    format: '{point.percentage:.1f} %',
                    style: 
                    {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
        		showInLegend: true,
        		legend: 
            	{
        			layout: 'vertical',
        			itemWidth: 80,
                    itemStyle: {
                       fontSize: '3px'
                       //color: '#A0A0A0'
                    },
                    itemHoverStyle: {
                       color: '#FFF'
                    },
                    itemHiddenStyle: {
                       color: '#444'
                    }

                },
            }
        },
        series: [{
            type: 'pie',
            //name: 'Browser share',
            data: [
                ['Minutes',20],
                ['Hours', 40],
                {
                    name: 'Days',
                    y: 30,
                    sliced: true,
                    selected: true
                },
                ['Weeks', 10]
            ]
        }]
    });
});
</script>

<!--<div id="wrapper">
		<ul id="main-menu" class="sm sm-simple collapsed">
		<li id="category">
			<a href="#">Category</a>
		    <ul>
		      <li><a href="#">Item 2-1</a></li>
		      <li><a href="#">Item 2-2</a></li>
		      <li><a href="#">Item 2-3</a></li>
		    </ul>
		 </li>
		</ul>
</div>

<div id="box">
	<p>	
		please be patient mobile analytics is coming soon.	
	</p>
</div>-->

<div class='menu-style-toggled'>
	<div class='menu-style-inner'>
	<div class='rmm' data-menu-style='minimal'>
	<ul>
	<!-- <li><a href='#home'>Home</a></li> -->
	 @foreach ($category_all as $category)
      <li><a href="#">{{{$category->name}}}</a></li>
    @endforeach
	</ul>
	</div>
	</div>
</div><!-- menu-style-toggled -->

<div id="navbar" style="margin-top: 2em;`">
<tab id="time_view" class="horizontal-nav" style="margin-top: 2em;">
		  <ul>
		    <li><a href="#">Today</a></li>
		    <li><a href="#">Days</a></li>
		    <li><a href="#">Weeks</a></li>
		    <li><a href="#">Months</a></li>
		  </ul>
</tab>
</div>

<div id="response_bar"></div>
<div id="response_boxes">
<div id="row">
 @foreach ($qp as $qper)
	<table id="box1">
	  <tr>
	    <th>{{{$qper->question}}}</th>
	  </tr>
	  <tr>
	  <td>
	  <li style="text-align: left;" >{{{round(($qper->yes_total/$qper->total)*100,0).'% yes'}}}</li>
	   &nbsp;
	   <li style="text-align: left;" >{{{round(($qper->no_total/$qper->total)*100,0).'% no'}}}</li>
  	  </td>
  	  </tr>
	</table>
 @endforeach
	
	<!-- <table id="box2">
	<tr>
	    <th>Question 2</th>
	  </tr>
	  <tr>
	  <td>
	   <li style="text-align: left;" >40% yes</li>
	   &nbsp;
	   <li style="text-align: left;" >60% no</li>
  	  </td>
  	  </tr>
	</table>
	
	<table id="box3">
	<tr>
	    <th>Question 3</th>
	 </tr>
	  <tr>
	  <td>
	   <li style="text-align: left;" >60% yes</li>
	   &nbsp;
	   <li style="text-align: left;" >40% no</li>
  	  </td>
  	  </tr>
	</table>-->
</div>
</div>

<div id="response_pies">
<div id="pie_left"></div>
<div id="pie_right"></div>
</div>



@stop
@section('footer')

<?php include(app_path().'/views/layouts/footer2.blade.php'); ?>

@stop
