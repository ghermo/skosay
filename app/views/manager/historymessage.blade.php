
@extends('layouts.default')

@section('content')
<div class="item item-divider">
	
	
</div>
<div class="list">

	<?php
		$id=Session::get('mess_id');
		$message=Messages::where('issueid','=', $fid)->get();
	?>
	@foreach ($messages as $message)
		<div class="list card">

        	<div class="item item-avatar">
          	{{ HTML::image('images/user.png') }}
          	<h2>{{$email}}</h2>
          	<p>{{$message->created_at}}</p>
        	</div>

        	<div class="item item-body">
        		<p>
            	{{$message->message}}
          		</p>
        	</div>

    	</div>
	@endforeach
	
</div>


@stop


@section('reply')

<div class="item item-input-inset" style="width:100%;background: rgb(53, 53, 53);">
	<label class="item-input-wrapper">
	<input type="text" placeholder="Reply">
	</label>
	<button class="button button-small button-positive">
	Submit
	</button>
</div>
  



@stop