@extends('layouts.default')
<?php 
    $apage = 1;
    if (!isset($departments)) {
        $departments = 0;
    }
?>
@section('content')
<script>
    setInterval(function(){
        var forms = document.getElementsByTagName('FORM');
        for (var i=0; i<forms.length; i++){
            var url = forms[i].id;
            $.ajax({
                type : 'POST',
                url : url,
                data : $('#'+url).serialize(),
                success : function(data){
                    $('.'+data['className']).empty().append(data['count']);;
                }
            })
        }
    },3000);
</script>
<div class="button-bar bar-dark">
  <a class="button active" href="new" style="color:#ffffff;background-color:#cccccc;">New / Open</a>
  <a class="button" href="inprogress" style="color:#00B9F2;background-color:#ffffff;">In Process</a>
  <a class="button" href="resolved" style="color:#00B9F2;background-color:#ffffff;">Resolved</a>
</div>
<div class="list">
    @if(count($stores) == 0 && count($departments) == 0)
        <div style="margin-top: 10em;">
            <center>
                <img src="../images/messages.png" style="width: 50px;"><br>
                <h4>No feedback found</h4>
                <h5>Tap <a href="#" onClick="window.location.reload()">here</a> to refresh page!</h5>
            </center>
        </div>
    @elseif(count($stores) == 0 && $departments == 0)
        <div style="margin-top: 10em;">
            <center>
                <img src="../images/messages.png" style="width: 50px;"><br>
                <h4>No feedback found</h4>
                <h5>Tap <a href="#" onClick="window.location.reload()">here</a> to refresh page!</h5>
            </center>
        </div>
    @endif
    
    @foreach($stores as $store)
        <?php
        $count = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.status','new')->where('messages.msgStatus', 'new')->where('issues.storeid', $store->storeid)->whereNotIn('messages.userid', [Auth::user()->id])->count();
        ?>
        <a href="historydetails/{{$store->storeid}}" class="item item-thumbnail-left" style="min-height:65px;">
            <img src="{{ $store->photo }}" style="width:50px;"/>
            <h3 style="width:80%; white-space: normal; text-align: left;">{{ $store->storename }}</h3>
            
            <form method="POST" id="storeNotif_{{$store->storeid}}" action="storeNotif_{{$store->storeid}}">
                <div class="storeNotif_{{$store->storeid}}">
                    @if($count != 0)
                    <div class="badge-chat"><span class="badge-text">{{ $count }}</span></div>
                    @endif
                </div>
            </form>
            <h4 style="width:80%; font-size:12px;vertical-align:top; text-align: left;">{{ $store->address }}</h4>
        </a>
    @endforeach

    @if(Auth::user()->type == 'manager')
        @if(isset($departments))
            @foreach($departments as $department)
                <?php 
                    $storeid = $department->storeid;
                    $store = DB::table('stores')->where('id', $storeid)->pluck('storename');
                    $storeaddress = DB::table('stores')->where('id', $storeid)->pluck('address');
                    $photo = DB::table('stores')->where('id', $storeid)->pluck('photo');
                    $count = DB::table('messages')
                    ->join('issues', 'messages.issueid', '=', 'issues.id')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                        ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                        ->join('groups', 'groups.id', '=','department-groups.groupid')
                        ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                        ->where('user-groups.userid', Auth::user()->id)
                    ->where('messages.msgStatus', 'new')
                    ->where('issues.storeid', $department->storeid)
                    ->where('issues.status','new')
                    ->whereNotIn('messages.userid', [Auth::user()->id])->count();
                ?>

                <a href="historydetails/{{$department->storeid}}" class="item item-thumbnail-left" style="min-height:65px;">
                <img src="{{ $photo }}" style="width:50px;"/>
                <p></p>
                <h3 style="width:80%; white-space: normal; text-align: left;">{{ $store }}</h3>
                
                <form method="POST" id="storeNotif_{{$department->storeid}}" action="storeNotif_{{$department->storeid}}">
                    <div class="storeNotif_{{$department->storeid}}">
                        @if($count != 0)
                        <div class="badge-chat"><span class="badge-text">{{ $count }}</span></div>
                        @endif
                    </div>
                </form>
                <h4 style="width:80%; font-size:12px;vertical-align:top; text-align: left;">{{ $storeaddress }}</h4>
                </a>
               
            @endforeach
        @endif
    @endif

</div>
@stop

@section('footer')

<?php include(app_path().'/views/layouts/footer2.blade.php'); ?>

@stop
