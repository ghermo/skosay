@extends('layouts.default')
@section('content')
<?php
    $chat = 1;
    $issueId = "";
    $page = 2;
    $latestId = '';
    $msgIssue = '';
    $issueType = '';
?>
<script>
    window.onload = function(){
        if(document.getElementById('replyX').value.length == 0)
            document.getElementById('submitButton').disabled = true;
        else
            document.getElementById('submitButton').disabled = false;
    };

    function validate(){
        if(document.getElementById('replyX').value.length == 0)
            document.getElementById('submitButton').disabled = true;
        else
            document.getElementById('submitButton').disabled = false;
    }

    function submitButton(){
        document.getElementById('submitButton').disabled = true;
        $('#reply').val($('#replyX').val());
        var ajaxUrl = $('#submitForm').attr('action');
            $.ajax({
            type : 'POST',
            url : ajaxUrl,
            data : $('#submitForm').serialize(),
            success : function(data){
                $('#replyMessage').append(data['htmlElem']);
                $('#replyX').val('');
                validate();
                $(document).scrollTop($(document).height());
//                window.location.href="#goTodiv";
            }
        })
    }

    setInterval(function(){
        var url = $('#checkReply').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#checkReply').serialize(),
            success : function(data){
                for (var i=0; i<data.length; i++)
                {
                    $('#replyMessage').append(data[i]);
                    $(document).scrollTop($(document).height());
//                    window.location.href="#goTodiv";
                }
            }
        })
    },3000);

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('replyX').value.length != 0)
                submitButton();
        }
    });
</script>

<style>
    .bubble{
        float:right;
        width: 62%;
        height:100%;
        position: relative;
        min-height: 80px;
        padding: 10px 20px;
        background: #73D3E8;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
    }

    .bubble:after{
        content: '';
        position: absolute;
        border-style: solid;
        border-width: 8px 0 8px 14px;
        border-color: transparent #73D3E8;
        display: block;
        width: 0;
        z-index: 1;
        right: -14px;
        top: 35px;
    }

    .bubble-left{
        float:left;
        width: 62%;
        height:100%;
        position: relative;
        min-height: 80px;
        padding: 20px 20px;
        background: #e5e5e5;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
    }

    .bubble-left:after{
        content: '';
        position: absolute;
        border-style: solid;
        border-width: 8px 14px 8px 0;
        border-color: transparent #e5e5e5;
        display: block;
        width: 0;
        z-index: 1;
        left: -14px;
        top: 35px;
    }
</style>
@foreach($messages as $message)
<?php
    $issueid = $message->issueid;

    $issue = DB::table('issues')->where('id', '=', $issueid)->get();

?>
<div style="background-color: #e5e5e5;">
    @foreach($issue as $issue)
        <?php
        $issid = $issue->storeid;
            $storeid = DB::table('stores')->where('id', '=', $issid)->pluck('storename');
        ?>
    @endforeach
</div>

    @endforeach
<div class="header" style="background-color: #e5e5e5;color:gray;  overflow: hidden;">
    <a class="ion-ios7-arrow-back backBtnMes" style="text-align:left;color:#00B9F2;height:10%;width:80%;" href="/historydetails/{{$issid}}"></a>
    {{ $storeid }}
</div>
<br>
<div id="replyMessage">
    @foreach($messages as $message)
    <?php
        $issueType = DB::table('issues')->where('id', $message->issueid)->pluck('status');
        $issueId = $message->issueid;
        $issuespage = 1;
        $userid = $message->userid;
        $photo = $message->photo;
        $email = DB::table('users')->where('id',$userid)->pluck('email');
        $type = DB::table('users')->where('id',$userid)->pluck('type');
        $name = DB::table('users')->where('id',$userid)->pluck('name');
        if(Auth::check()){
            $authId = Auth::user()->id;
        }
        else if(isset($_COOKIE['guestId'])){
            $authId = $_COOKIE['guestId'];
        }
        else if (isset($_COOKIE['fbId'])){
            $authId = $_COOKIE['fbId'];
        }
    ?>


    @if($userid != $authId)
    <div style="padding:6px;">
        <div style="width:20%; float:left;margin-right: 18px;margin-top: 6px;">
            @if($type == 'admin')
                {{ HTML::image('images/manager.png' , 'Manager', array('style' => 'width:100%; max-width:80px;')) }}
            @elseif($type == 'manager')
                {{ HTML::image('images/manager.png' , 'Manager', array('style' => 'width:100%; max-width:80px;')) }}
            @else
                {{ HTML::image('images/user.png' , 'User', array('style' => 'width:100%; max-width:80px;')) }}
            @endif
        </div>
        <div class="bubble-left">
            <p>
                <?php if(!empty($photo)){ ?>
                    <img src="../../{{$photo}}" style="width:100%;" />
                    <br>
                <?php } ?>
                    @if($message->msgType == "feedBack")
                        <?php $msg = explode(';', $message->message); ?>
                    @if(isset($msg[3]) && $msg[3] != '')
                        {{ $msg[3] }}

                @else
                    Hi, can you help me with this?
                @endif
                @elseif($message->msgType == 'reply')
                    {{ $message->message }}
                @endif
            </p>
            <div class="pointer"></div>
            <div class="pointerBorder"></div>
        </div>
    </div>
    <div class="messageDate">{{ date("m/d/Y h:i:s", strtotime($message->created_at)) }} by {{ $name }}</div>
    <?php $latestId = $message->id;?>
    <?php $msgIssue = $message->issueid;?>
    @else
    <div style="padding:6px;">
        <div style="width:20%; float:right;margin-left: 18px;margin-top: 6px;">
            @if($type == 'admin')
                <img src="../../images/manager.png" style="width:100%; max-width:80px;">
            @elseif($type == 'manager')
                <img src="../../images/manager.png" style="width:100%; max-width:80px;">
            @else
                <img src="../../images/user.png" style="width:100%; max-width:80px;">
            @endif
        </div>
        <div class="bubble">
            <p>
                <?php if(!empty($photo)){ ?>
                    <img src="../../{{$photo}}" style="width:100%;" />
                    <br>
                <?php } ?>
                @if($message->msgType == "feedBack")
                    <?php $msg = explode(';', $message->message); ?>
                @if(isset($msg[3]) && $msg[3] != '')
                    {{ $msg[3] }}
                @else
                    Hi, can you help me with this?
                @endif
                @elseif($message->msgType == 'reply')
                    {{ $message->message }}
                @endif
            </p>
            <div class="pointer"></div>
            <div class="pointerBorder"></div>
        </div>
    </div>
    <div class="messageDate">{{$message->created_at}}</div>
    @endif
    @endforeach
    <form id="checkReply" method="POST" action="/checkReply">
        <input type="hidden" id='getLatestMsg' name='getLatestMsg' value="{{$latestId}}"/>
        <input type="hidden" id='getLatestMsg' name='msgIssueId' value="{{$msgIssue}}"/>
    </form>
</div>

<div id="goTodiv"></div>
<br>
<br>
<br>
@stop
@section('reply')
<form method="POST" id='submitForm' action="/sendReply" style="width:100%; display: none;">
    <input type="hidden" name="issueId" id="issueId" value="{{{ $issueId }}}"/>
    <input type="hidden" name="reply" id="reply" value=""/>
</form>
<div class="item item-input-inset" style="width:100%;background: rgb(53, 53, 53);">
    <label class="item-input-wrapper">
        <input type="text" onkeyup="validate()" placeholder="Write your reply" id="replyX" name="replyX" maxlength="200">
    </label>
    <button id="submitButton" type="button" onclick="submitButton()" class="button button-small button-positive" disabled>Send</button>
        @if(Auth::check())
            @if(Auth::user()->type == 'admin' && $issueType != 'resolved')
                <a style="float: center;" class="button button-calm" id='changeStatus' href="/changestatus/{{$msgIssue}}">Change Status</a>
            @elseif(Auth::user()->type == 'manager' && $issueType != 'resolved')
                <a style="float: center;" class="button button-calm" id='changeStatus' href="/changestatus/{{$msgIssue}}">Change Status</a>
            @endif
        @endif
    </div>
@stop