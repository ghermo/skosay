@extends('layouts.default')
<?php $apage = 1; ?>
@section('content')
<script>
    setInterval(function(){
        var forms = document.getElementsByTagName('FORM');
        for (var i=0; i<forms.length; i++){
            var url = forms[i].id;
            $.ajax({
                type : 'POST',
                url : '/'+url,
                data : $('#'+url).serialize(),
                success : function(data){
                    $('.'+data['className']).empty().append(data['count']);;
                }
            })
        }
    },3000);
</script>
<?php $bkBtn=5;$ctr=0; ?>
<div class="button-bar bar-dark">
  <a class="button" href="../historypage" style="color:#00B9F2;background-color:#ffffff;">New / Open</a>
  <a class="button" href="../inprogress" style="color:#00B9F2;background-color:#ffffff;">In Process</a>
  <a class="button " href="../resolved" style="color:#ffffff;background-color:#cccccc;">Resolved</a>
</div>
<div class="list">
	@foreach($issues as $issues)
	 	<?php
            $deptName = DB::table('departments')->where('id',$issues->isDept)->pluck('name');
	 		$user_id = $issues->userid;
	 		$storename = DB::table('stores')->where('id',$issues->storeid)->pluck('storename');
			$address = DB::table('stores')->where('id',$issues->storeid)->pluck('address');
	 		$email = DB::table('users')->where('id',$user_id)->pluck('email');
            $count = DB::table('messages')->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.id', $issues->id)->where('messages.msgStatus', 'new')->whereNotIn('messages.userid', [Auth::user()->id])->count();
            $text = DB::table('messages')
            ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.id', $issues->id)->where('messages.msgType', 'feedback')->pluck('message');

	        $msg = explode(';', $text);
			
			if(isset($msg[count($msg)-1]) && $msg[count($msg)-1] != ''){
	            $msgNum = strlen($msg[count($msg)-1]);
	            $msgShort = substr($msg[count($msg)-1], 0, 40);
	            if ($msgNum > 40) {
	                $msgShort = $msgShort . "...";
	            }
	        }else{
	            $msgShort = 'No message left by the customer.';
	        } 
        ?>
        <?php if ($ctr == 0){ ?>
        <div class="list" style="text-align: center; margin:0;" >
		    <a class="item" href="#" style="padding:8px; background: #DFDFDF;">
		        <h2 style="margin:0;">{{{ $storename }}}</h2>
		        <p>{{$address}}</p>
		    </a>
		</div>
        <?php } ?>
        <a href="/message/{{$issues->storeid}}/{{$issues->id}}#goTodiv" class="item item-thumbnail-left" style="min-height:65px;">
   		<img src="../images/PostOffice.png" style="width:50px;"/>
        <h3 style="text-align: left; width: 80%;" >
        Chat {{ $issues->id }} : {{ $msgShort }}
        <p style="width:80%; font-size:12px;vertical-align:top; text-align: left;margin:0;">{{ date("m/d/Y h:i:s", strtotime($issues->updated_at)) }}</p>
        @if($issues->isDept != 0)
          <p style="width:80%; text-align:left;margin:0;font-size:12px;vertical-align:top;"> {{ $deptName }} </p>
        @endif
        </h3>
            <form method="POST" id="issueNotif_{{$issues->storeid}}" action="issueNotif_{{$issues->storeid}}">
                <div class="issueNotif_{{$issues->storeid}}">
                    @if($count == 0)
                    <span class="badge badge-assertive"></span>
                    @else
                	<div class="badge-chat"><span class="badge-text">{{ $count }}</span></div>
                    @endif
                </div>
            </form>
		</a>
		<?php $ctr++; ?>
	@endforeach
	</div>
<br>
@stop


@section('footer')

<?php include(app_path().'/views/layouts/footer2.blade.php'); ?>

@stop