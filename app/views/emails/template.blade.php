<html>
    <head></head>
    <body>
        <center>
            <h4>Thanks for using Skosay!</h4><br/>
            Please click the link below to confirm your account and start Skosay-ing!<br/>
            {{ $url }}
        </center>
    </body>
</html>