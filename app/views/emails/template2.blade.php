<html>
    <head></head>
    <body>
        <center>
        	<a href="www.skosay.com"><img src="http://app.skosay.com/images/skopicdark.png"></a>
            <h4>Thanks for using Skosay!</h4>
        </center>
            Welcome to Skosay, the service that allows you to say it anywhere. <br><br> 
			Our goal is to connect what matters with who matters and you are an important part in the process.  We are excited you are here!<br><br>
			By using your email, you can now get feedback when you comment on a business/location by using www.skosay.com on your mobile device.  These businesses want to engage with you.  Your account username is <strong> {{ $us }} </strong> and your temporary  password is
			<strong> {{ $pw }} </strong>.<br><br>
			Click <a href="{{ $url }}">here</a> if you would like to update your profile.<br><br>
            
			Thanks for saying it with Skosay.<br><br>
			-The Skosay Team<br>
			Contact us at info@skosay.com

        
    </body>
</html>