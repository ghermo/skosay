@extends('layouts.default')
@section('content')
<script>
    setInterval(function(){
        var forms = document.getElementsByTagName('FORM');
        for (var i=0; i<forms.length; i++)
        {
            var url = forms[i].id;
            $.ajax({
                type : 'POST',
                url : url,
                data : $('#'+url).serialize(),
                success : function(data){
                    $('.'+data['className']).empty().append(data['count']);;
                }
            })
        }
    },10000);
</script>

<?php $page = 2; ?>
<div class="list">
    @if(count($store) != 0)
        @foreach ($store as $store)
        <a href="/issues/{{ $store->storeid }}"  class="item item-thumbnail-left" href="#" style="min-height:70px; padding-left:65px;text-align:left;">
            <?php
            if(Auth::check()){
                $currentId = Auth::user()->id;
            }
            else if(isset($_COOKIE['guestId'])){
                $currentId = $_COOKIE['guestId'];
            }
            else{
                $currentId = $_COOKIE['fbId'];
            }
            $photo = DB::table('stores')->where('id',$store->storeid)->pluck('photo');
            $storename = DB::table('stores')->where('id',$store->storeid)->pluck('storename');
            $address = DB::table('stores')->where('id',$store->storeid)->pluck('address');

            $count = DB::table('messages')
                ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [$currentId])->whereNotIn('messages.userid', [$currentId])
                        ->where('messages.msgStatus', 'new')->where('issues.storeid', $store->storeid)->count();

            $store2 = DB::table('stores')
                    ->join('issues', 'issues.storeid', '=', 'stores.id')->join('messages', 'messages.issueid', '=', 'issues.id')->select(array(DB::raw('stores.storename as sName, stores.address as address')))->where('stores.id', $store->storeid)->groupBy('sName')->orderBy('issues.updated_at', 'DESC')->get();

            ?>
            {{ HTML::image($photo , $storename, array('style' => 'width:50px;')) }}
            @foreach($store2 as $store2)
            <h3 style="width:80%; white-space: normal;">{{ $storename }}</h3>
            <h4 style="width:80%; font-size:12px;vertical-align:top; ">{{ $address }}</h4>
            @endforeach
           <form action="getMessage_{{$store->storeid}}" method="POST" id="getMessage_{{$store->storeid}}">
                <div class="getMessage_{{$store->storeid}}">
                    @if($count == 0)
                    <span class="badge badge-chat"></span>
                    @else
                    <div class="badge-chat"><span class="badge-text">{{ $count }}</span></div>
                    @endif
                </div>
            </form>
        </a>
        @endforeach
    @else
        <div style="margin-top: 10em;">
            <center>
                <img src="../images/messages.png" style="width: 50px;"><br>
                <h4>No messages or feedback found.</h4>
                <h5>Tap <a href="/location">here</a> to leave a feedback.</h5>
            </center>
        </div>
    @endif
</div>
@stop

@section('footer')
<?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop