<!doctype html>
<html lang="en">
<head>
    {{HTML::style('css/css.css');}}
    {{HTML::style('css/ionic.css');}}

    {{HTML::script('js/jquery-1.11.0.min.js');}}
    {{HTML::script('js/ionic.bundle.js');}}

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title>Forgot Password</title>
    <?php $url = URL::to('/'); ?>
    <script>
        function sendForm()
        {
            var form = $('#fpForm');
            $.ajax({
                type : 'POST',
                url : form.attr('action'),
                data : form.serialize(),
                success : function(data){
                    if(data['status'] == 'true'){
                        $('#errorMsg').empty().append(data['msg']);
                        window.location = '../../?successForgot=1';
                    }
                    else
                        $('#errorMsg').empty().append(data['msg']);
                }
            });

            setTimeout(function(){
                $('#errorMsg').empty();
            }, 6000);
        }
        
        function buttonControl()
        {
            if(document.getElementById('pass1').value.length != 0 && document.getElementById('pass2').value.length != 0)
                document.getElementById('sendButton').disabled = false;
            else
                document.getElementById('sendButton').disabled = true;
        }
    </script>
</head>

<body style="background-color:#00b9f2">
    <p align="center" style="margin-top: 2em;">
        {{ HTML::image('images/skopic.png' , 'Skosay', array('style' => 'width:230px; ')) }}
        <br><br>
    </p>
    <p align="center">
        <font color="white" style="text-align:center;">{{ User::where('id', $id)->pluck('email'); }}</font>
    </p>

    <form action="/newPassword" method="POST" id="fpForm">
        <div class="card">
            <label class="item item-input">
                <input type="password" onkeyup="buttonControl()" id="pass1" name="pass1" placeholder="Put your new password. (Minimum Character 7)">
            </label>
        </div>

        <div class="card">
            <label class="item item-input">
                <input type="password"  onkeyup="buttonControl()" id="pass2" name="pass2" placeholder="Confirm new password">
            </label>
        </div>
        <input type="hidden" value="{{ $id }}" name="hiddenId">
    </form>
    <div class="list list-inset" style="margin-top: -1em; background-color:#00b9f2">
        <button id="sendButton" class="button button-block button-energized" onclick="sendForm();">Send</button>
        <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Logging in. Please wait <span class="ion-refreshing"></span></button>
    </div>
    <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2"></div>

    <br>
</body>
</html>