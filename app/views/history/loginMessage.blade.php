<!DOCTYPE html>
<html>
<head>
    <title>Skosay</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

    </script>

    {{HTML::style('css/css.css');}}
    {{HTML::style('css/ionic.css');}}


    {{HTML::script('js/jquery-1.11.0.min.js');}}
    {{HTML::script('js/ionic.bundle.js');}}
    {{HTML::script('js/javascript.js');}}

</head>

<body style="background-color:#1f8fe2">
<script>
    window.onload = function()
    {
        $('#password').val('');
    };

    function submitForm()
    {
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
                $('#errorMsg').empty().append(userData['msg']).show();

                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function loginFb()
    {
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            document.getElementById('redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0)
                submitForm();
        }
    });

    function validateInput()
    {
        if(document.getElementById('email').value.length != 0 && document.getElementById('password').value.length != 0 )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
<?php $page = 1;?>
    @if(Auth::check())
        <br>
        <br>
        <br>
        <p align="center">
            <img src="images/skopic1.png">
        </p>
        <form id="redirectForm" method="GET" action="message">
            <center>
            <button class="button button-stable" style="margin-top: 3em; border: 2px solid #a1a1a1; padding: 5px 30px; background: #dddddd; border-color:#1f8fe2; border-radius: 25px; color:#1f8fe2;">
                Continue as {{ Auth::user()->email }}
            </button>
            </center>
        </form>
    @else
        <form method="GET" action="login2" id="ajaxForm">
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
        </p>
        <br>
        <br>
        <br>
        <p align="center">
            <img src="images/skopic1.png">
        </p>
        <div class="card" style="margin-top: 4em;">
            <label class="item item-input">
                {{ Form::text('email', Input::old('email'), array('onkeyup' => 'validateInput()','id' => 'email','placeholder' => 'E-mail', 'value' => Input::old('email'))) }}
            </label>
            <label class="item item-input">
                {{ Form::password('password', array('onkeyup' => 'validateInput()', 'placeholder' => 'Password', 'id' => 'password'))}}
            </label>
        </div>

        {{ Form::close() }}

    @endif
    <div class="list list-inset" style="margin-top: -1em; background-color:#1f8fe2">
        {{ Form::submit('Sign In',  array('disabled' => 'true','id' => 'submitButton', 'class' => 'button button-block button-energized ','type' => 'button','onclick' => 'submitForm()')) }}
        <button id="falseButton" class="button button-block button-energized"  disabled style="display: none;">Logging in. Please wait <span class="ion-refreshing"></span></button>
        <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>
    </div>
    <form id="redirectForm" method="GET" action="message"></form>
    <input type="hidden" name="testHidden" id="testHidden" value="none"/>
    <div id="errorMsg" class="list list-inset" style="background-color:#1f8fe2">
    </div>
    <center>
        @if(isset($_COOKIE['fbId']))
        <form action="message" method="GET">
            <button id="loginFbBtn" style="padding:0px 0px; border: none; background: #1f8fe2">
                Continue with {{ $_COOKIE['fbId'] }}
            </button>
        </form>
        @else
            <form action="loginFb" method="GET" id="loginFb">
                <button id="loginFbBtn" style="padding:0px 0px; border: none; background: #1f8fe2"><img src="images/btnfb.png"></img></button>
            </form>
        @endif
    </center>
     <br><br>
     <div class="tabs-striped tabs-background-dark tabs-color-assertive" >
        <div class="tabs" style="position:fixed;">
            <?php $page = 2; ?>
           <?php include(app_path().'/views/layouts/footer.blade.php'); ?>
        </div>
    </div>
</body>
</html>

