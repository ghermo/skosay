<!doctype html>
<html lang="en">
    <head>
        {{HTML::style('css/css.css');}}
        {{HTML::style('css/ionic.css');}}

        {{HTML::script('js/jquery-1.11.0.min.js');}}
        {{HTML::script('js/ionic.bundle.js');}}

        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
        <title>Forgot Password</title>
        <script>
            function checkEmail()
            {
                var forgotForm = $('#forgotForm');
                $.ajax({
                    type : 'POST',
                    url : forgotForm.attr('action'),
                    data : forgotForm.serialize(),
                    success : function(data)
                    {
                        if(data['status'] == 'true')
                        {
                            document.getElementById('sendButton').disabled = false;
                            $('#errorMsg').empty();
                            document.getElementById('emailSucc').style.display = '';
                            document.getElementById('emailFail').style.display = 'none';
                            emptyDiv();
                            sendConfirmation();
                        }
                        else if(data['status'] == 'false')
                        {
                            $('#errorMsg').empty().append(data['msg']);
                            document.getElementById('emailSucc').style.display = 'none';
                            document.getElementById('emailFail').style.display = '';
                            emptyDiv();
                        }
                    }
                });
            }

            function sendConfirmation()
            {
                document.getElementById('sendButton').disabled = true;
                document.getElementById('sendButton').style.display = 'none';
                document.getElementById('falseButton').style.display = '';

                var conf = $('#sendConfirmFp');
                $.ajax({
                    type : 'POST',
                    url : conf.attr('action'),
                    data : conf.serialize(),
                    success : function(data)
                    {
                        document.getElementById('sendButton').disabled = false;
                        document.getElementById('sendButton').style.display = '';
                        document.getElementById('falseButton').style.display = 'none';
                        $('#errorMsg').empty().append(data);
                        emptyDiv();
                    }
                });
            }

            function emptyDiv()
            {
                setTimeout(function(){$('#errorMsg').empty();},10000);
            }
        </script>
    </head>

    <body style="background-color:#00b9f2">
        <p align="center" style="margin-top: 2em;">
        <img src="images/skopic.png" style="width:230px;"><br><br>
        </p>
        <p align="center">
            <font color="white" style="text-align:center;">Forgot your password? No worries.</font>
        </p>

        <form action="sendConfirmFp" method="POST" id="sendConfirmFp" name="sendConfirmFp">
            <input type="hidden" value="" id="trigger" name="trigger">
        </form>

        <form action="checkEmailForm" method="POST" id="forgotForm">
            <div class="card" style="margin-top: 1em;">
                <label class="item item-input">
                    <input type="text" value="CONTROL_INPUT" style="display: none;">
                    <input type="email" id="email" name="email" placeholder="What is your email?" autocapitalize="off" onchange=" $('#trigger').val(document.getElementById('email').value);" onkeyup="$('#trigger').val(document.getElementById('email').value);">
                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="emailSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="emailFail"></i>
                </label>
            </div>
        </form>

        <div class="list list-inset" style="background-color:#00b9f2">
            <button class="button button-block button-energized" onclick="checkEmail();" id="sendButton">Send confirmation</button>
            <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Sending confirmation. Please wait <span class="ion-refreshing"></span></button>
        </div>

        <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
        </div>

    </body>
</html>