@extends('layouts.default')
@section('content')
<script>
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    function submitForm()
    {
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
                $('#errorMsg').empty().append(userData['msg']).show();

                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            document.getElementById('redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0)
                submitForm();
        }
    });

    function validateInput()
    {
        if(document.getElementById('email').value.length != 0 && document.getElementById('password').value.length != 0 )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
<?php $page = 2; ?>
<!-- if there are login errors, show them here -->
    <form method="POST" action="login" id="ajaxForm">
    <p>
        {{ $errors->first('email') }}
        {{ $errors->first('password') }}
    </p>
    <div class="card" style="margin-top: 4em;">
        <label class="item item-input">
            {{ Form::text('email', Input::old('email'), array('onkeyup' => 'validateInput()','id' => 'email','placeholder' => 'E-mail', 'value' => Input::old('email'))) }}
        </label>
        <label class="item item-input">
            {{ Form::password('password', array('onkeyup' => 'validateInput()', 'placeholder' => 'Password', 'id' => 'password'))}}
        </label>
        @if(Session::has('msg'))
            <p><i><center><font color="red">{{{ Session::get('msg') }}}</font></center></i></p>
        @endif
    </div>
    {{ Form::close() }}
    <div class="list list-inset" style="margin-top: -1em;">
        {{ Form::submit('Login',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-positive','type' => 'button','onclick' => 'submitForm()')) }}
        <button id="falseButton" class="button button-block button-positive" disabled style="display: none;">Logging in. Please wait <span class="ion-refreshing"></span></button>
    </div>
    <form id="redirectForm" method="GET" action=""></form>
    <input type="hidden" name="testHidden" id="testHidden" value="none"/>
    <div id="errorMsg" class="list list-inset">
    </div>
@stop

@section('footer')
<?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop