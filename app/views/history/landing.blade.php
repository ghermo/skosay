@extends('layouts.default2')
@section('content')
<script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

</script>

<script>
	mixpanel.track("Landed on simplified Sign In screen.");
	
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    

    function submitForm()
    {
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData)
            {
                var msg = userData['msg'];
                $('#errorMsg').empty().append(msg).show();
                $('#redirectForm').submit();
            }
        })
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            //return false;
            //$('#submitButton').submit();
            submitForm();
        }
    });

    function loginFb()
    {
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').val(userData['status']);
                checkLogin();
//                $('#errorMsg').empty().append(userData['msg']).show();
//                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            $('#redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

   

    function validateInput()
    {
        if(document.getElementById('email').value.length != 0)
        	if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.getElementById('email').value)))  
				document.getElementById('submitButton').disabled = true;  
			else
				document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
<?php 
$email = Session::get('email');
if (isset($_GET['successForgot'])) {
    $success = $_GET['successForgot'];
?>
    <script>
    setTimeout(function(){
    $('#errorMsg').empty();
    }, 6000);
    </script>
<?php } ?>
<!-- if there are login errors, show them here -->
            <form method="POST" action="signup" id="ajaxForm">
        <!--
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
        </p>
        -->

        <p align="center" style="margin-top:2em;">
            @if(isset($email))
                {{ HTML::image('images/skopic.png' , 'Skosay Logo', array('style' => 'width:230px;')) }}
            @else
                {{ HTML::image('images/skopic.png' , 'Skosay Logo', array('style' => 'width:230px;')) }}
            @endif
        </p>
        @if(isset($success))
        <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
            <div class="item" style="background-color: darkseagreen; border-color: green;">
                <center>
                    <h4>
                        <font color="green">Congratulations! Password has been changed.</font>
                    </h4>
                </center>
            </div>
        </div>
        @else
        <div id="errorMsg" style="background-color:#00b9f2"></div>
        @endif
        
        <center>
            <h3 style="color:white;">Your feedback matters.</h3>
            <span style="font-size: 13px; color: white;">To get started, all we need is your email address.</span>
        </center> 
        
        <div class="card" style="margin-top: 12px';">

            @if(isset($id))
                <input type='hidden' value="{{$id}}" name="sid" id="sid">
            @endif

            <label class="item item-input">
               @if(isset($email))
                    {{ Form::email('email', $email, array('onkeyup' => 'validateInput()','id' => 'email', 'value' => $email, 'autocapitalize' => 'off')) }}
               @else
                    {{ Form::email('email', Input::old('email'), array('onkeyup' => 'validateInput()','id' => 'email', 'placeholder' => 'Your email address', 'autocapitalize' => 'off')) }}
                @endif
            </label>
        </div>

        {{ Form::close() }}
         
        <div class="list list-inset" style="margin-top: -1em; background-color:#00b9f2">
            {{ Form::submit('Go',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized','type' => 'button','onclick' => 'submitForm()')) }}
            <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Signing in. Please wait <span class="ion-refreshing"></span></button>
<!--            <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>-->
            <center>
                <span style="font-size: 11px; color: white;">
                    * we hate spam, too. we never share your email with anyone.
                </span>
            </center>
        </div>
            <form id="redirectForm" method="GET" action="/">
                <input type="hidden" name="email" id="emailG" value="none"/>
                <input type="hidden" name="password" id="password" value="none"/>
                <input type="hidden" name="testHidden" id="testHidden" value="none"/>
            </form>
    
    

    <br>
    <div class="list list-inset" style="margin-bottom:-4px; margin-top: -1em; background-color:#00b9f2">
        @if(isset($id))
            <form action="../loginFb" method="GET" id="loginFb">
            <input type="hidden" id="fbQR" value="{{$id}}" name="fbQR">
        @else
            <form action="loginFb" method="GET" id="loginFb">
        @endif
            <center>
                <span style="font-size: 13px; color: white;">
                    or connect using your Facebook account.
                </span>
            </center>
            <button id="loginFbBtn" class="button button-block button-royal">
                @if(isset($email))
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @else
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @endif
            </button>
        </form>
        <br>
    </div>
 	<center style="margin-top:0px;">
    <p style="color:white;font-size:13px;">
        Already have an account? <a class="text-link" href="/login2" style="color:rgb(0, 71, 255);">Sign in here.</a>
    </p>
	 </center>
	 <br><br>

@stop

