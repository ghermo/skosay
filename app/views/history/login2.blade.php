@extends('layouts.default2')
@section('content')
<script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

</script>

<script>
	mixpanel.track("Landed on sign in screen.");
	
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    function submitForm()
    {
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
                $('#errorMsg').empty().append(userData['msg']).show();

                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function loginFb()
    {
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
//                $('#errorMsg').empty().append(userData['msg']).show();
//                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            document.getElementById('redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0)
                submitForm();
        }
    });

    function validateInput()
    {
        if(document.getElementById('email').value.length != 0 && document.getElementById('password').value.length != 0 )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
<?php 
$email = Session::get('email');
if (isset($_GET['successForgot'])) {
    $success = $_GET['successForgot'];
?>
    <script>
    setTimeout(function(){
    $('#errorMsg').empty();
    }, 6000);
    </script>
<?php } ?>
<!-- if there are login errors, show them here -->
        @if(isset($id))
            <form method="POST" action="../login2" id="ajaxForm">
        @else
            <form method="POST" action="login2" id="ajaxForm">
        @endif
        <!--
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
        </p>
        -->

        <p align="center" style="margin-top:2em;">
            @if(isset($email))
                {{ HTML::image('images/skopic.png' , 'Skosay Logo', array('style' => 'width:230px;')) }}

                
            @else
                {{ HTML::image('images/skopic.png' , 'Skosay Logo', array('style' => 'width:230px;')) }}
                 
            @endif
        </p>
        @if(isset($success))
        <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
            <div class="item" style="background-color: darkseagreen; border-color: green;">
                <center>
                    <h4>
                        <font color="green">Congratulations! Password has been changed.</font>
                    </h4>
                </center>
            </div>
        </div>
        @else
        <div id="errorMsg" style="background-color:#00b9f2"></div>
        @endif
        
        <center><a class="text-link" href="/">First time here? Let's get you signed up <span class="ion-chevron-right" style="font-size: 14px; color: white;"></span></a></center> 
        
        <div class="card" style="margin-top: 12px';">

            @if(isset($id))
                <input type='hidden' value="{{$id}}" name="sid" id="sid">
            @endif

            <label class="item item-input">
               @if(isset($email))
                    {{ Form::email('email', $email, array('onkeyup' => 'validateInput()','id' => 'email', 'value' => $email, 'autocapitalize' => 'off')) }}
               @else
                    {{ Form::email('email', Input::old('email'), array('onkeyup' => 'validateInput()','id' => 'email', 'placeholder' => 'Your email', 'autocapitalize' => 'off')) }}
                @endif
            </label>
        </div>
        <div class="card "style="margin-top: 1em;">
            <label class="item item-input">
                {{ Form::password('password', array('onkeyup' => 'validateInput()', 'placeholder' => 'Your password', 'id' => 'password', 'autocapitalize' => 'off'))}}
            </label>

        </div>
        {{ Form::close() }}
         
        <div class="list list-inset" style="margin-top: -1em; background-color:#00b9f2">
            {{ Form::submit('Sign in',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized','type' => 'button','onclick' => 'submitForm()')) }}
            <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Signing in. Please wait <span class="ion-refreshing"></span></button>
<!--            <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>-->
            <center><a class="text-link" href="/confirmForgot">Forgot Password?</a></center>
        </div>
        @if(isset($id))
            <form id="redirectForm" method="GET" action="{{$id}}">
            </form>
        @else
            <form id="redirectForm" method="GET" action="location">
            </form>
        @endif
    
    <input type="hidden" name="testHidden" id="testHidden" value="none"/>

    <br>
    <div class="list list-inset" style="margin-bottom:-20px; margin-top: -1em; background-color:#00b9f2">
        @if(isset($id))
            <form action="../loginFb" method="GET" id="loginFb">
            <input type="hidden" id="fbQR" value="{{$id}}" name="fbQR">
        @else
            <form action="loginFb" method="GET" id="loginFb">
        @endif
            
            <button id="loginFbBtn" class="button button-block button-royal">
                @if(isset($email))
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @else
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @endif
            </button>
        </form>
        <br>
    </div>
 	<center style="margin-top:0px;">
     <a href="http://www.skosay.com/?view_full_site=true"><button id="redirectFullSiteBtn" class="button button-light button-clear" style="color:white;font-size:12px;">View Desktop Site</button></a>
	 </center>
	 <br><br>

@stop

