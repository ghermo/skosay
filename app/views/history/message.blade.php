@extends('layouts.default')
@section('content')

<style>
.modalDialog {
    /*position: fixed;*/
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}
.modalDialog > div {
    width: 96%;
    position: relative;
    margin: 10% auto;
    padding: 5px 5px 13px 7px;
    border-radius: 10px;
    background: #fff;
    background: -moz-linear-gradient(#fff, #999);
    background: -webkit-linear-gradient(#fff, #999);
    background: -o-linear-gradient(#fff, #999);
}


// modal design for change-status
#modalDialog1 {
    position:  relative;
    font-family: Arial, Helvetica, sans-serif;
}

#close1 {
    -webkit-transition: opacity .25s ease-in;
    -moz-transition: opacity .25s ease-in;
    transition: opacity .25s ease-in;
    z-index: 100000;
    top: 0px;
    position: fixed;
    width: 100%;
    background:  rgba(0,0,0,0.8);
    position: center; 
    padding: 10%;
    height: 100%;
    display: none;
}

.close1 {
    cursor: pointer;
    background: #606061;
    color: #FFFFFF;
    line-height: 25px;
    position: absolute;
    right: 10px;
    text-align: center;
    top: 10px;
    width: 24px;
    text-decoration: none;

    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}
.close1:hover {
    cursor: pointer;
    background: #00d9ff;
}
</style>
<script>

    $(document).ready(function(){

        $("#close1").click(function(){
            document.getElementById('close1').style.display = 'none';
        });
        //toggle between hide and show
        $("#hideModalDialog").click(function(){
            document.getElementById('close1').style.display = 'block';
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function checkImg(){
        if(document.getElementById('blah').src.length != 0  )
            document.getElementById('blah').className = 'showThis';
        else
            document.getElementById('blah').className = 'hideThis';
    }

    $("#qwe").change(function(){
        readURL(this);
    });

    var control = document.getElementById("qwe");
    control.addEventListener("change", function(event) {

    // When the control has changed, there are new files

    var i = 0,
        files = control.files,
        len = files.length;
    
    for (; i < len; i++) {
        var div = document.getElementById('path');
            var str = files[i].name;
            var res = str.slice(0,14);
            div.innerHTML = res;

            // if(this.files[0].size < 2097152 )
            //      document.getElementById('fsize').style.display = '';
            // else
            //     document.getElementById('fsize').style.display = 'none';

            //this.files[0].size gets the size of your file.
            // alert(this.files[0].size);

        var s = this.files[0].size;

        if(s > 2097152){
            alert('2MB is the maximum file size');
        }
            
    }

}, false);
</script>

<?php
if (Auth::check()) {
    if (Auth::user()->type == 'admin') {
        foreach($messages as $message){
            $issueid = $message->issueid;
            $issueStat = DB::table('issues')->where('id', '=', $issueid)->pluck('status');
        }
        if ($issueStat == 'inprogress') {
            $bkBtn = 7;
        }else if ($issueStat == 'resolved') {
            $bkBtn = 8;
        }else {
            $bkBtn = 6;
        }
    }else if(Auth::user()->type == 'manager'){
        foreach($messages as $message){
            $issueid = $message->issueid;
            $issueStat = DB::table('issues')->where('id', '=', $issueid)->pluck('status');
        }
        if ($issueStat == 'inprogress') {
            $bkBtn = 7;
        }else if ($issueStat == 'resolved') {
            $bkBtn = 8;
        }else {
            $bkBtn = 6;
        }
    }else{
        $bkBtn = 2;
    }
}else{
    $bkBtn = 2;
}

$chat = 1;
$issueId = "";
$page = 2;
$latestId = '';
$msgIssue = '';
$locCtr = 0;
?>
<script>
/*var pNameDisplay, pAddress;*/

    window.onload = function()
    {
        if(document.getElementById('replyX').value.length == 0)
            document.getElementById('submitButton').disabled = true;
        else
            document.getElementById('submitButton').disabled = false;
    };

    function validate()
    {
        $('#reply').val($('#replyX').val());
        if(document.getElementById('replyX').value.length == 0)
            document.getElementById('submitButton').disabled = true;
        else
            document.getElementById('submitButton').disabled = false;
    }

    function submitButton()
    {
    
        mixpanel.track("Interacted with manager");
        document.getElementById('submitButton').disabled = true;
        $('#reply').val($('#replyX').val());
        var form = document.querySelector('#submitForm');
        var formdata = new FormData(form)
        var ajaxUrl = $('#submitForm').attr('action');
        $.ajax({
            type : 'POST',
            url : ajaxUrl,
            cache: false,
            contentType: false,
            processData: false,
            data : formdata,
            success : function(data){
                
                $('#replyMessage').append(data['htmlElem']);
                $('#replyX').val('');
                $('#qwe').val('');
                $('#blah').attr("src", '');
                $('#path').html('File:&nbspNo file chosen');
                validate();
                $(document).scrollTop($(document).height());
//                window.location.href="#goTodiv";
                
            }
        })
    
    }

    setInterval(function(){
        var url = $('#checkReply').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#checkReply').serialize(),
            success : function(data){
                for (var i=0; i<data.length; i++)
                {
                    $('#replyMessage').append(data[i]);

                    $(document).scrollTop($(document).height());
//                    window.location.href="#goTodiv";

                }
            }
        })
    },3000);

    function submitButtonStatus(status)
    {
        mixpanel.track("Interacted with manager");
        document.getElementById('statusChange').value = status;  
        $('#reply1').val($('#replyX').val());
        var ajaxUrl = $('#submitForm1').attr('action');
        $.ajax({
            type : 'POST',
            url : ajaxUrl,
            data : $('#submitForm1').serialize(),
            success : function(data){
                //$('#replyMessage').append(data['htmlElem']);
                $('#replyX').val('');
                validate();
                $(document).scrollTop($(document).height());
//                window.location.href="#goTodiv";
                var res = data['htmlElem'];
                if (res == 2) {
                    window.location.href = "/inprogress";
                }else if(res == 3){
                    window.location.href = "/resolved";
                }else{
                    window.location.href = "/historypage";
                }
                
            }

        })
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('replyX').value.length != 0)
                submitButton();
        }
    });
</script>

@foreach($messages as $message)
    <?php
    $issueid = $message->issueid;
    $issue = DB::table('issues')->where('id', '=', $issueid)->get();
    $deptId = DB::table('issues')->where('id', '=', $issueid)->pluck('isDept');
    $deptName = DB::table('departments')->where('id', '=', $deptId)->pluck('name');
    ?>
    @foreach($issue as $issue)
        <?php
        $issid = $issue->storeid;
        $storeName = DB::table('stores')->where('id', '=', $issid)->pluck('storename');
        $storeAddress = DB::table('stores')->where('id', '=', $issid)->pluck('address');
        $storeName = substr($storeName, 0, 35);
        ?>
    @endforeach
@endforeach

<?php

$issueStatus = DB::table('issues')->where('id', $issueid)->pluck('status');

if($issueStatus == 'archive'){
    ?>
<div style="margin-top: 10em;">
<center>
<div class="center-justified" style="margin-top:1em; width: 500px;/* padding: 0px; */margin-left: 24px;margin-right: 24px;">
    <h3 style="margin:0;text-align: center;">
    <font color="#696969">
        This issue is already archived.
    </font>
    </h3>
    
    
</div>
</center>

    <div style="margin:24px;margin-top:1em;">
    <a href="/location" class="button button-block button-positive" style="background-color:#CF8344;border-radius: 4px;">
        Go Back
    </a>
    </div>
</div>
<?php
}

else{

?>
    <div class="list" style="text-align: center; margin:0;" >
        <a class="item" href="#" style="padding:8px; background: #DFDFDF;">
            <center><h2 style="margin:0; width: 80%">{{{ $storeName }}}</h2></center>
            <p style="margin:0;">{{{ $storeAddress }}}</p>
            @if(isset($deptName))
            <p>{{{ $deptName }}}</p>
            @endif
        </a>
    </div>
    <br>
    
    <div id="replyMessage">
    @foreach($messages as $message)
        <?php
            $issueType = DB::table('issues')->where('id', $message->issueid)->pluck('status');
            $issueId = $message->issueid;
            $issuespage = 1;
            $userid = $message->userid;
            $photo = $message->photo;
            $email = DB::table('users')->where('id',$userid)->pluck('email');
            $type = DB::table('users')->where('id',$userid)->pluck('type');
            $name = DB::table('users')->where('id',$userid)->pluck('name');
            $id = DB::table('users')->where('id',$userid)->pluck('id');
            $dispFirstName = explode(' ', $name); 
            $dispEmailB4At = explode('@', $email);
            
            if (sizeof($dispEmailB4At) > 0 && sizeof($dispFirstName) == 0){
                $dispFirstName = array_fill(0, 1, (string)$dispEmailB4At[0]);
            }
            else
            {
                $dispFirstName = explode(' ', $name); 
            }
            
            if(Auth::check()){
                $authId = Auth::user()->id;
            }
            else if(isset($_COOKIE['fbId'])){
                $authId = $_COOKIE['fbId'];
            }
            else if(isset($_COOKIE['guestId'])){
                $authId = $_COOKIE['guestId'];
                $guestName = 'Guest';
            }

            $urlPhoto = URL::to($photo);

        ?>
        @if($userid != $authId)
            <div style="padding:6px;">
                <div style="width:20%; float:left;margin-right: 18px;margin-top: 6px;">
                    <?php /**
                    @if($type == 'admin')
                    {{ HTML::image('images/manager.png' , 'Manager', array('style' => 'width:100%; max-width:80px;')) }}
                    @elseif($type == 'manager')
                    {{ HTML::image('images/manager.png' , 'Manager', array('style' => 'width:100%; max-width:80px;')) }}
                    @else
                    {{ HTML::image('images/user.png' , 'User', array('style' => 'width:100%; max-width:80px;')) }}
                    @endif
                    */?>
                </div>
                <div class="bubble-left" style="padding:6px;">
                    <p>
                        <?php $fphoto =  URL::to($photo); ?>
                        <?php if(!empty($photo)){ ?>
                            <a href="#openModal{{$message->id}}">
                                {{ HTML::image($photo , 'User', array('style' => 'width:100%;')) }}


                            </a>
                            <div id="openModal{{$message->id}}" class="modalDialog">
                            <div>
                                
                                    {{ HTML::image($photo,'',array('style' => 'width:100%; max-height: 400px;')) }}
                                    <a href="/download?photo={{$photo}}" class="button button-calm"> 
                                    Download
                                    </a>
                                    <a href="#close" class="button button-stable">
                                  Cancel
                                </a>
                            </div>
                        </div>
                        <br>
                        <?php } ?>
                        @if($message->msgType == "feedBack")
                          <?php $msg = explode(';', $message->message); ?>
                          @if(isset($msg[5]) && $msg[5] != '')
                              {{ $msg[5] }}
                          @elseif(isset($msg[4]) && $msg[4] != '')
                              {{ $msg[4] }}
                          @elseif(isset($msg[3]) && $msg[3] != '')
                              {{ $msg[3] }}
                          @elseif(isset($msg[2]) && $msg[2] != '')
                              {{ $msg[2] }}
                          @elseif(isset($msg[1]) && $msg[1] != '')
                              {{ $msg[1] }}
                          @else
                              No message left by customer.
                          @endif
                        @elseif($message->msgType == 'reply')
                          {{ $message->message }}
                        @endif
                    </p>
                    <div class="pointer"></div>
                    <div class="pointerBorder"></div>
                </div>
            </div>
            <div class="messageDate">{{ date("m/d/Y H:i:s", strtotime($message->created_at)) }} by
                @if($userid == $id)
                   {{ $dispFirstName[0] }}
                @else
                    Guest
                @endif
            </div>
        <?php
            if(isset($authId))
            {
                $msgIssue = $message->issueid;
                $latestId = $message->id;
            }
        ?>
        @else
            <div style="padding:6px;">
                <div style="width:20%; float:right;margin-left: 18px;margin-top: 6px;">
                    <?php /**
                    @if($type == 'admin')
                    {{ HTML::image('images/manager.png' , 'Manager', array('style' => 'width:100%; max-width:80px;')) }}
                    @elseif($type == 'manager')
                    {{ HTML::image('images/manager.png' , 'Manager', array('style' => 'width:100%; max-width:80px;')) }}
                    @else
                    {{ HTML::image('images/user.png' , 'User', array('style' => 'width:100%; max-width:80px;')) }}
                    @endif
                    */?>
                </div>
                <div class="bubble" style="padding:6px;">
                    <p>
                        <?php $fphoto =  URL::to($photo); ?>
                        <?php if(!empty($photo)){ ?>
                        <a href="#openModal{{$message->id}}">
                            {{ HTML::image($photo,'',array('style' => 'width:100%;')) }}
                        </a>
                        


                        <div id="openModal{{$message->id}}" class="modalDialog">
                            <div>
                                {{ HTML::image($photo,'',array('style' => 'width:100%; max-height: 460px;')) }}
                                <a href="/download?photo={{$photo}}" class="button button-calm">
                                    Download
                                </a>
                                <a href="#close" class="button button-stable">
                                    Cancel
                                </a>
                            </div>
                        </div>

                        <br>
                        <?php } ?>
                        @if($message->msgType == "feedBack")
                            <?php $msg = explode(';', $message->message); ?>
                            @if(isset($msg[5]) && $msg[5] != '')
                              {{ $msg[5] }}
                            @elseif(isset($msg[4]) && $msg[4] != '')
                              {{ $msg[4] }}
                            @elseif(isset($msg[3]) && $msg[3] != '')
                              {{ $msg[3] }}
                            @elseif(isset($msg[2]) && $msg[2] != '')
                              {{ $msg[2] }}
                            @elseif(isset($msg[1]) && $msg[1] != '')
                              {{ $msg[1] }}
                            @else
                                No message left by customer.
                            @endif
                        @elseif($message->msgType == 'reply')
                              {{ $message->message }}
                        @endif
                    </p>
                    <div class="pointer"></div>
                    <div class="pointerBorder"></div>
                </div>
            </div>
            <div class="messageDate">{{ date("m/d/Y H:i:s", strtotime($message->created_at)) }} by
                @if(Auth::check())
                   {{ $dispFirstName[0] }}
                @elseif(isset($_COOKIE['fbId']))
                   {{ $_COOKIE['fName']; }}
                @else
                    {{ $guestName; }}
                @endif
            </div>
        <?php
            if(isset($authId))
            {
                $msgIssue = $message->issueid;
                $latestId = $message->id;
            }
        ?>
        @endif
    @endforeach
        <form id="checkReply" method="POST" action="/checkReply">
            <input type="hidden" id='getLatestMsg' name='getLatestMsg' value="{{$latestId}}"/>
            <input type="hidden" id='msgIssueId' name='msgIssueId' value="{{$msgIssue}}"/>
        </form>
    </div>


<div id="goTodiv"></div>
<br>
<br>
<br>
<br>
<br>
@stop

@section('reply')

<div id="modalDialog1">
    <div id="close1">
        <a class="close1">X</a>
        @if($issueType != 'inprogress')
            <button style=" margin: 0px 0px 10px 4px; top: 6px; width : 100%; top: 40%; bottom: 40%;" class="button button-calm" type="button" onclick="submitButtonStatus('inprogress')">
                In Process
            </button>                         
        @endif
        <button style="margin: 0px 0px 10px 4px; top: 6px;width : 100%;top: 40%; bottom: 40%;" class="button button-calm" type="button" onclick="submitButtonStatus('resolved')">
            Resolved
        </button>
        <button style=" margin: 0px 4px 10px 4px; top: 6px; width : 100%;top: 40%; bottom: 40%;" class="button button-calm" type="button" onclick="submitButtonStatus('archive')">
            Archive
        </button> 

    </div>
</div>

<div class="bar bar-footer" style="padding: 0px;position:fixed;">
    <div class="bar bar-footer" style="padding: 0px;bottom: 38px;">
    
    <form method="POST" id='submitForm1' action="/sendReply1/{{$msgIssue}}" style="width:100%; display: none;">
        <input type="hidden" name="issueId1" id="issueId1" value="{{{ $issueId }}}"/>
        <input type="hidden" name="reply1" id="reply1" value=""/>
        <input type="hidden" name="statusChange" id="statusChange" value=""/>
    </form>

    <div class="item item-input-inset" style="width:120%;background:rgb(53, 53, 53);border:none;">
        <label class="item-stacked-label">
            <textarea onkeyup="validate()" cols="100" rows="2" name="replyX" id="replyX" style="width:100%;" 
                placeholder=" Write your message. Maximum 200 characters." maxlength="200"></textarea>
        </label>
    </div>
       
    </div>


    <div class="item item-input-inset" style="width:110%;background:rgb(53, 53, 53); float:right;">
        <p class="truncate" style="margin: 10px 4px 0 0px; font-size: 10px; max-width: 30px;">
            <img id="blah" src="" alt="your image" style="height:24px;width:24px;border:1px;" onchange="checkImg()"/>
            <span id="fsize" style="display: none;"><font color="red">File exceeds the 2MB limit</font></span> 
        </p>
        <div>
            <form method="POST" id='submitForm' action="/sendReply" style="width:100%;" enctype="multipart/form-data">
            <script> function sel_file() { $("input[name=qwe]").trigger('click'); } </script> 
            <input type="file" name="qwe" id="qwe" style="opacity:0;width:39px;" onchange="readURL(this);"/> 
            <a class="button button-small button-positive icon ion-camera" href="javascript:sel_file();" 
                style="position:absolute;left:40px;top:6px;height:34px;vertical-align:top;"></a>
            
            <input type="hidden" name="issueId" id="issueId" value="{{{ $issueId }}}"/>
            <input type="hidden" name="reply" id="reply" value=""/>
            </form>
            @if(Auth::check())
            @if(Auth::user()->type == 'admin' && $issueType != 'resolved')
                <a href = "#" style="margin: 0px 0px 0 4px; position: absolute; right: 124px; top: 6px;" id="hideModalDialog"  class="button button-calm" >
                    Change Status
                </a>
            @elseif(Auth::user()->type == 'manager' && $issueType != 'resolved')
                <a href = "#" style= "margin: 0px 0px 0 4px; position: absolute; right: 124px; top: 6px;" id="hideModalDialog" class="button button-calm" >
                    Change Status
                </a>
            @elseif((Auth::user()->type == 'admin' || Auth::user()->type == 'manager') && $issueType == 'resolved')
                <button style="margin: 0px 0px 0 4px; position: absolute; right: 124px; top: 6px; width:100px;" id="changeStatus" type="button" onclick="submitButtonStatus('archive')" class="button button-calm" >
                    Archive
                </button>
            @endif
            @endif

            <input type="hidden" name="issueStatus" id="issueStatus" value= <?php $issueStatus?>/> 

            <button id="submitButton" type="button" onclick="submitButton()" class="button button-positive" disabled style="position:absolute; right:9px; top:6px; width:110px;">
                Send
            </button> 
            
        </div>
    </div>
</div>
    <?php }?>
@stop