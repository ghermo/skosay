@extends('layouts.default2')
@section('content')
<script>

    function submitForm()
    {
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';
		mixpanel.track("New user signed up");
		
        var finalForm = $('#signUpForm');
        var url = finalForm.attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : finalForm.serialize(),
            success : function(userData)
            {
                $('#errorMsg').empty().append(userData['msg']);
                if(userData['status'] == 'false')
                {
                    document.getElementById('submitButton').style.display = '';
                    document.getElementById('falseButton').style.display = 'none';
                    validateInput();
                }
                else
                {
                    document.getElementById('submitButton').style.display = '';
                    document.getElementById('falseButton').style.display = 'none';
                    validateInput();
                    setTimeout(function(){$('#redirect').submit();}, 5000);
                }
                window.location.href="#errorMsg";
                clearDiv();
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            document.getElementById('redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

     $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0)
                submitForm();
        }
    });

    function validateInput()
    {
//        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//        if(re.test(document.getElementById('email').value))
//        {
//            if(document.getElementById('email').value != '0' && document.getElementById('password').value != '0' && document.getElementById('confirm-password').value != '0')
//                document.getElementById('submitButton').disabled = false;
//            else
//                document.getElementById('submitButton').disabled = true;
//        }
//        else
//            document.getElementById('submitButton').disabled = true;
    }

    function clearDiv()
    {
        setTimeout(function(){ $('#errorMsg').empty(); }, 5000);
    }

    function validateEmail()
    {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(document.getElementById('emailField').value))
        {
            var form = $('#checkEmail');
            $.ajax({
                type: 'POST',
                url : form.attr('action'),
                data: form.serialize(),
                success : function(data)
                {
                    if(data['status'] == 'false')
                    {
                        $('#errorMsg').empty().append(data['msg']);
                        document.getElementById('emailFail').style.display = '';
                        document.getElementById('emailSucc').style.display = 'none';
                        $('#email').val('0');
                    }
                    else if(data['status'] == 'true')
                    {
                        $('#email').val(document.getElementById('emailField').value);
                        $('#errorMsg').empty().append('<center><font color="white">We will not share your email to a 3rd party. Promise!</font></center>');
                        document.getElementById('emailFail').style.display = 'none';
                        document.getElementById('emailSucc').style.display = '';
                    }
                    validateInput();
                    clearDiv();
                }
            });
        }
        else
        {
            $('#errorMsg').empty().append('<div class="item" style="background-color: #faebcc; border-color: red;"><center><h4><font color="red">Please input a valid email</font></h4></center></div>');
            document.getElementById('emailFail').style.display = '';
            document.getElementById('emailSucc').style.display = 'none';
//            document.getElementById('submitButton').disabled = true;
        }
        clearDiv();
    }

    function validatePass()
    {
        $('#password').val(document.getElementById('passwordField').value);
        buttonControl();
    }

    function validatePassCon()
    {
        $('#confirm-password').val(document.getElementById('confirm-passwordField').value);
        buttonControl();
    }

    function validEmail()
    {
        $('#email').val(document.getElementById('emailField').value);
        buttonControl();
    }
    function validName()
    {
        $('#name').val(document.getElementById('nameField').value);
        buttonControl();
    }

    function buttonControl()
    {
        if(document.getElementById('password').value.length != 0 && document.getElementById('confirm-password').value.length != 0 && document.getElementById('email').value.length != 0)
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }

</script>
    <form id="signUpForm" method="POST" action="register_action">
        <input type="hidden" value="" id="name" name="name">
        <input type="hidden" value="" id="email" name="email">
        <input type="hidden" value="" id="password" name="password">
        <input type="hidden" value="" id="confirm-password" name="confirm-password">
    </form>


        <p align="center" style="margin-top:2em;">

                <img src="images/skopic.png" style="width:230px;"><br



        </p>
        
        <center><a class="text-link" href="/">Already have an account? Sign in here <span class="ion-chevron-right" style="font-size: 14px; color: white;"></span></a></center>

		<div class="card" style="margin-top: 1em;">
            <label class="item item-input">
                {{ Form::text('nameField', Input::old('email'), array('onkeyup' => 'validName();', 'id' => 'nameField','placeholder' => 'What is your name?', 'value' => Input::old('email'))) }}
            </label>
        </div>

        <form action="checkEmail" method="POST" id="checkEmail" name="checkEmail">
            <div class="card" style="margin-top: 1em;">
                <label class="item item-input">

                    {{ Form::email('emailField', Input::old('email'), array('onkeyup' => 'validEmail(); validateEmail();', 'id' => 'emailField','placeholder' => 'What is your email?', 'value' => Input::old('email'), 'autocapitalize' => 'off')) }}

                    

                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="emailSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="emailFail"></i>
                </label>
            </div>
        </form>

        <form action="checkPassword" method="POST" id="checkPassword" name="checkPassword">
            <div class="card "style="margin-top: 1em;">
                <label class="item item-input">
                    {{ Form::password('passwordField', array('onkeyup' => 'validatePass();', 'placeholder' => 'What is your password?', 'id' => 'passwordField', 'autocapitalize' => 'off'))}}
                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="passSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="passFail"></i>
                </label>
            </div>
            <div class="card "style="margin-top: 1em;">
                <label class="item item-input">
                    {{ Form::password('confirm-passwordField', array('onkeyup' => 'validatePassCon();', 'placeholder' => 'Confirm your password', 'id' => 'confirm-passwordField', 'autocapitalize' => 'off'))}}
                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="cpassSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="cpassFail"></i>
                </label>
            </div>
        </form>

     <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
         <center><font color="white">We will not share your email to a 3rd party. Promise!</font></center>
     </div>

     <br>
    <div class="list list-inset" style="margin-top: -1em; background-color:#00b9f2">
        {{ Form::submit('Sign me up',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized','type' => 'button', 'onclick' => 'submitForm()')) }}
        <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Creating your account. Please wait <span class="ion-refreshing"></span></button>
    </div>

    <!--<br>
    <br>
    <br>
    <center>
        <form action="loginGuest" method="POST" id="loginGuest">
            <font color="white" >Not ready to sign up? <br> No Worries...</font> <br>
            <button id="loginGuestBtn" class="button button-stable" style="margin-top:1em" >
                Try It Out Here
            </button>
        </form>
    </center>-->
    <form id="redirect" action="/" method="GET"></form>

@stop