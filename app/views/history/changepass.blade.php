@extends('layouts.default2')
@section('content')
<script>
    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('passwordField').value.length != 0 || document.getElementById('confirm-passwordField').value.length != 0)
                submitForm();
        }
    });
    
    function submitForm()
    {
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var finalForm = $('#signUpForm');
        var url = finalForm.attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : finalForm.serialize(),
            success : function(userData)
            {

                $('#errorMsg').empty().append(userData['msg']);
                if(userData['status'] == 'false')
                {
                    document.getElementById('submitButton').style.display = '';
                    document.getElementById('falseButton').style.display = 'none';
                    
                }
                else
                {
                    document.getElementById('submitButton').style.display = '';
                    document.getElementById('falseButton').style.display = 'none';
                    
                    setTimeout(function(){$('#redirect').submit();}, 5000);
                }

                setTimeout(function(){
                    $('#errorMsg').empty();
                },8000);
            }
        })
    }
    
    function validateOldPass()
    {
        $('#oldpassword').val(document.getElementById('oldpasswordField').value);
        buttonControl();
    }

    function validatePass()
    {
        $('#password').val(document.getElementById('passwordField').value);
        buttonControl();
    }

    function validatePassCon()
    {
        $('#confirm-password').val(document.getElementById('confirm-passwordField').value);
        buttonControl();
    }

    

    function buttonControl()
    {
        if(document.getElementById('oldpassword').value.length != 0 && document.getElementById('password').value.length != 0 && document.getElementById('confirm-password').value.length != 0)
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }

</script>

    <form id="signUpForm" method="POST" action="checkPass">
        <input type="hidden" value="" id="oldpassword" name="oldpassword">
        <input type="hidden" value="" id="password" name="password">
        <input type="hidden" value="" id="confirm-password" name="confirm-password">
    </form>

        <p align="center" style="margin-top:2em;">
            <img src="images/skopic.png" style="width:230px;"><br>
        </p>
        <p align="center">
            <font color="white" style="text-align:center;">Let's change your password.</font>
        </p>
      
        <form action="changepassword" method="POST" id="changepassword" name="changepassword">
            <div class="card "style="margin-top: 1em;">
                <label class="item item-input">
                    {{ Form::password('oldpasswordField', array('onkeyup' => 'validateOldPass();', 'placeholder' => 'What is your old password?', 'id' => 'oldpasswordField'))}}
                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="passSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="passFail"></i>
                </label>
            </div>
            <div class="card "style="margin-top: 1em;">
                <label class="item item-input">
                    {{ Form::password('passwordField', array('onkeyup' => 'validatePass();', 'placeholder' => 'What is your new password?', 'id' => 'passwordField'))}}
                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="passSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="passFail"></i>
                </label>
            </div>
            <div class="card "style="margin-top: 1em;">
                <label class="item item-input">
                    {{ Form::password('confirm-passwordField', array('onkeyup' => 'validatePassCon();', 'placeholder' => 'Confirm your new password.', 'id' => 'confirm-passwordField'))}}
                    <i class="ion-ios7-checkmark" style="color : green; font-size: 23px; margin-right: 0.4em; display: none;" id="cpassSucc"></i>
                    <i class="ion-ios7-close" style="color : firebrick; font-size: 23px; margin-right: 0.4em; display: none;" id="cpassFail"></i>
                </label>
            </div>
        </form>

        <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
         
     </div>

     <br>
    <div class="list list-inset" style="margin-top: -1em; background-color:#00b9f2">
        {{ Form::submit('Change Password',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized','type' => 'button', 'onclick' => 'submitForm()')) }}
        <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Changing your password. Please wait <span class="ion-refreshing"></span></button>
    </div>

    <form id="redirect" action="/" method="GET"></form>


@stop
