@extends('layouts.default')

@section('content')
<script>
    setInterval(function(){
        var forms = document.getElementsByTagName('FORM');
        for (var i=0; i<forms.length; i++)
        {
            var url = forms[i].id;
            $.ajax({
                type : 'POST',
                url : '/'+url,
                data : $('#'+url).serialize(),
                success : function(data){
                    $('.'+data['className']).empty().append(data['count']);;
                }
            })
        }
    },3000);
</script>
<?php
	$page = 2;
    $bkBtn = 1;
	$backBtn = 1;
	$ctr = 0;
?>
@foreach ($issue as $issue)
    <?php
        if(Auth::check()){
            $currentId = Auth::user()->id;
        }
        else if(isset($_COOKIE['fbId'])){
            $currentId = $_COOKIE['fbId'];
        }
        else if(isset($_COOKIE['guestId'])){
            $currentId = $_COOKIE['guestId'];
        }

        $photo = DB::table('stores')->where('id',$issue->storeid)->pluck('photo');
        $storename = DB::table('stores')->where('id',$issue->storeid)->pluck('storename');
        $address = DB::table('stores')->where('id',$issue->storeid)->pluck('address');
        $count = DB::table('messages')
        ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.id', $issue->id)->where('messages.msgStatus', 'new')->whereNotIn('messages.userid', [$currentId])->count();

        $text = DB::table('messages')->where('issueid',$issue->id)->where('msgType','feedback')->pluck('message');
        $msg = explode(';', $text);

        $deptName = DB::table('departments')->where('id',$issue->isDept)->pluck('name');


        if(isset($msg[count($msg)-1]) && $msg[count($msg)-1] != ''){
            $msgNum = strlen($msg[count($msg)-1]);
            $msgShort = substr($msg[count($msg)-1], 0, 40);
            if ($msgNum > 40) {
                $msgShort = $msgShort . "...";
            }

        }else{
            $msgShort = 'No message left by the customer.';
        }
        $ctr++;

    ?>
	<?php if ($ctr == 1){ ?>
	<div class="list" style="text-align: center; margin:0;" >
        <a class="item" href="#" style="padding:8px; background: #DFDFDF;">
            <h2 style="margin:0;">{{{ $storename }}}</h2>
            <p>{{$address}}</p>
        </a>
	</div>
	<?php } ?>
<div class="list">
	<a href="/issues/{{$issue->storeid}}/{{$issue->id}}#goTodiv" class="item item-thumbnail-left" style="text-align:top;padding-left:65px;text-align:left;">
        {{ HTML::image('images/PostOffice.png' , 'issues', array('style' => 'width:50px;')) }}

      <span style="width:80%; text-align:left;">Chat {{$issue->id}} : {{ $msgShort }}</span>
      <p style="width:80%; text-align:left;margin:0;">on {{ date("m/d/Y H:i:s", strtotime($issue->updated_at)) }}</p>
      @if($issue->isDept != 0)
      <p style="width:80%; text-align:left;margin:0;"> {{ $deptName }} </p>
      @endif
        <form action="getMessageResolved_{{$issue->id}}" method="POST" id="getMessageResolved_{{$issue->id}}">
                <div class="getMessage_{{$issue->id}}">
                    @if($count == 0)
                    <span class="badge badge-chat"></span>
                    @else
                    <div class="badge-chat"><span class="badge-text">{{ $count }}</span></div>
                    @endif
                </div>
            </form>
    </a>
	@endforeach
</div>

@stop

@section('footer')

<?php include(app_path().'/views/layouts/footer.blade.php'); ?>

@stop