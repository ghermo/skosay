@extends('layouts.default')

@section('content')


<div class="container" style="max-width:445px; margin-top: 56px;">



		<!-- if there are login errors, show them here -->
	{{ Form::open(['route'=> 'session.store']) }}
		<p>
			{{ $errors->first('email') }}
			{{ $errors->first('password') }}
		</p>
		<div class="list list-inset">
		<p class="item item-input">

			{{ Form::email('email', Input::old('email'), array('placeholder' => 'E-mail')) }}
		</p>

		<p class="item item-input">

			{{ Form::password('password', array('placeholder' => 'Password'))}}
		</p>

		<p>{{ Form::submit('Login',  array('class' => 'button button-block button-positive')) }}</p>
	</div>
	{{ Form::close() }}
</div>

@stop
