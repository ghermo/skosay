@extends('layouts.default')
@section('content')

<?php 
  $page = 3; 
  $ctr = App::environment();
  $domain =  DB::table('domain')->where('appEnv',$ctr)->first();
?>

<script>
    function validateInput(){
         if(document.getElementById('gmap_keyword').value.length != 0  )
                document.getElementById('button1').disabled = false;
            else
                document.getElementById('button1').disabled = true;
    }

    function doSearch(e, input){
       

        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
          if(document.getElementById('gmap_keyword').value.length != 0 )  
          {
             
             document.getElementById('loadDiv').style.display = '';
             document.getElementById('errorDiv').style.display = 'none';
             findPlaces('search');
             return false;
          }
          else
            
            document.getElementById('loadDiv').style.display = 'none';
            document.getElementById('errorDiv').style.display = '';
            
            return false;
        }

        
    }
    // initialization
    google.maps.event.addDomListener(window, 'load', initialize);

</script>

@if($domain->appEnv == 'skosay')
<div id="gmap_canvas"></div>
    <div class="list list-inset" style="border:1px solid #0066CC;">
        <div class="item item-input">
        <input type="hidden" id="lat" name="lat" value="36.2756996155" />
        <input type="hidden" id="lng" name="lng" value="-94.2969970703" />
        <input type="text" placeholder="Search by name, location, zip code" id="gmap_keyword" name="gmap_keyword" onkeyup="validateInput(); doSearch(event, this); " ><!-- doSearch(event, this); -->
        <button id="button1" name="button1" class="button button-icon icon ion-search" onclick="findPlaces('search'); return false;" disabled></button>
        </div>
    </div>
    <div style="border:2px; vertical-align:top; text-align: left;font-size:10px;color:gray;">&nbsp;&nbsp;&nbsp;&nbsp;Example: Green Leaf Grill Little Rock</div>
<div class="list" id="demoDay"></div>
<div class="list" id="places"></div>
<div class="list" id="errorDiv" style="margin-top: 4.5em; display: none;">
    
        <h4>Sorry! Nothing was found!</h4>
        <h5><font style="margin: 0 auto; text-align: justify; width: 5em;">Following are the reasons:<br/><br>
            1. Store is not registered to google. <br/>
            2. Store can not be reached from your<br> location.<br/>
            3. Internet Connection Problem<br/>
            4. Turn on GPS and Google Services<br/></font></h5>
        <img src="images/google.png"><br/><br/>
        <br/>
    
</div>

<!-- End of Main Server -->

@else
<form id="search" method="POST" action="/searchThis">
    <div class="list list-inset" style="border:1px solid #0066CC;">
        <div class="item item-input">
          <input type="hidden" value="{{$ctr}}" id="env" name="env">
          <input type="text" placeholder="Search by name, location, zip code" id="name" name="name" onkeyup="validateInput();" ><!-- doSearch(event, this); -->
          <button id="button1" name="button1" class="button button-icon icon ion-search"></button>
        
        </div>
        </form>
    </div>
    <br>
@if (isset($results))
  @if ($results == 'none')
<div class="list" id="errorDiv" style="margin-top: 4.5em;">
    
        <h4>Sorry! Nothing was found!</h4>
        <h5><font style="margin: 0 auto; text-align: justify; width: 5em;">Following are the reasons:<br/><br>
            1. Store is not registered to google. <br/>
            2. Store can not be reached from your<br> location.<br/>
            3. Internet Connection Problem<br/>
            4. Turn on GPS and Google Services<br/></font></h5>
        <img src="images/google.png"><br/><br/>
        <br/>
    
</div>
@else
<div class="list" id="demoDay">
  
    @foreach ($results as $store)
      <form method="POST" action="feedback" id="{{ $store->storename }}">
        <input type="hidden" name="question" value="1" />
        <input type="hidden" name="iconsrc" value="{{ $store->photo }}" />
        <input type="hidden" name="pId" value="{{ $store->id }}" />
        <input type="hidden" name="pType" value="Walmart'" />
        <input type="hidden" name="pAddress" value="{{ $store->address }}" />
        <input type="hidden" name="pName" value="{{ $store->storename }}" />
        <button type="submit" class="item item-thumbnail-left" style="text-align:left; padding-left:25px; width: 100%;">    
            <img src="{{ $store->photo }}" style="width:50px;">
            <img src="images/right.PNG" style="height:30px;vertical-align:center;" align="right">
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font style="font-size:15px;vertical-align:top;">{{ $store->storename }}</font>
            <br />
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font color="grey" style="font-size:12px;vertical-align:top;">
                {{ $store->address }}</font>
        </button>
    </form>
    @endforeach
  @endif
  @endif
</div>

@endif
<div id="loadDiv" style="margin-top: 7em; display: none;">
    Searching..<Br/>
    <span class="ion-loading-c" style="font-size: 40px; margin: 1em; color: #ADADAD"></span>
</div>

@stop

@section('footer')
<?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop