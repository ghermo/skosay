<?php $messages = DB::table('messages')->where('userid', $id)->orderBy('created_at', 'DESC')->paginate(5); ?>
            
            <table class="table" style="overflow: hidden;" >
                <tr>
                    <th>Issue ID</th>
                    <th>Sent to</th>
                    <th>Questions</th>
                    <th>Answers</th>
                    <th>Type</th>
                    <th>Date Sent</th>
                </tr>
                <tbody id="mainTable">
                
                @foreach($messages as $message)
                    <?php 
                    $issueid = $message->issueid;
                    $storeId = DB::table('issues')->where('id', $issueid)->pluck('storeid');
                    $storeName = DB::table('stores')->where('id', $storeId)->pluck('storename');
                    $qGroupId = DB::table('stores')->where('id', $storeId)->pluck('qGroupId');
                    $qgets = DB::table('question-groups')->where('qgroupid', $qGroupId)->get();
                    foreach($qgets as $qget){
                        $x[] = $qget->id;
                    }
                    $xcount0 = DB::table('question-groups')->where('qgroupid', $qGroupId)->count();
                    $xcount = $xcount0-1;
                    $end = end($x);
                    $questions = DB::table('questions')->whereBetween('id', array($x[0], $end))->orderBy('orderId','ASC')->get();                          
                    $fullmsg = $message->message; 
                    $created = $message->created_at;
                    $msgtype = DB::table('messages')->where('userid', $id)->pluck('msgType');
                ?>
                <tr>
                    <td>
                        {{ $issueid }}
                    </td>

                    <td>
                        {{ $storeName }}

                    </td>

                    <td>
                        @foreach($questions as $question)
                        {{ $question->question }}<br><br>
                        @endforeach
                    </td>

                    <td>
                        {{ $fullmsg }}
                    </td>
                    <td>
                        {{ $msgtype }}
                    </td>
                    <td>
                        {{ $created }}
                    </td>
                    
                </tr>

                @endforeach

                </tbody>
            </table>
