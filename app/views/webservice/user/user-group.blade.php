<script>
    

    function deleteStore2(data)
        {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteGroup',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                },
                error : function(){

                }
            });
        }

        function initEdit2(data)
            {
                $('#editGroupname_'+data).show();
               
                $('#gname_'+data).hide();
                
                $('#editButtons1_'+data).show();
                $('#actionButtons1_'+data).hide();
            }


        function submitEdit2(data)
        {
            $('#hiddenGroupname_'+data).val($('#gnameEdit_'+data).val());
           
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditGroup',
                data : $('#editForm1_'+data).serialize(),    
                success : function(group){
                    if(group['status'] == 'FAILED')
                        alert(group['msg']);
                    else if(group['status'] == 'SUCCESS')
                    {
                        $('#editGroupname_'+data).hide();
                        
                        $('#gname_'+data).show();
                        

                        $('#editButtons1_'+data).hide();
                        $('#actionButtons1_'+data).show();

                        $('#gname_'+data).empty().append($('#hiddenGroupname_'+data).val());
                       
                    }
                }
            })
        }


        function cancelEdit2(data)
        {
            $('#editGroupname_'+data).hide();
           
            $('#gname_'+data).show();
         

            $('#editButtons1_'+data).hide();
            $('#actionButtons1_'+data).show();
        }

        function validateInput1()
        {
            if(document.getElementById('gname').value.length != 0)
                document.getElementById('submitBtn1').disabled= false;
            else
                document.getElementById('submitBtn1').disabled= true;
        }
</script>

<div class="col-md-8" >
    <table style="padding: .2em;" class="table">
        <tr>
            <th>User Group ID</th>
            <th>User Group name</th>
            <th style="width: 15%;">Action</th>
        </tr>
        <tbody id="mainTable1">
          
            <?php $group = UserGroup::join('groups', 'groups.id', '=', 'user-groups.groupid')->groupBy('groupname')->orderBy('groups.id', 'ASC')->get();?>
         
        @foreach($group as $group)
        <tr>
            <td>{{ $group->id }}</td>
            <td>
                
                    <p id="gname_{{$group->id}}"> {{ $group->groupname }} </p>
                    <div class="input-group" style="margin-top: 10px; display: none;" id="editGroupname_{{$group->id}}">
                        <span class="input-group-addon">G</span>
                        <input type="text" id="gnameEdit_{{$group->id}}" name="gnameEdit_{{$group->id}}" class="form-control" value="{{$group->groupname}}">
                    </div>
                
            </td>
            
            <td>
                            <div id="actionButtons2_{{ $group->id }}">
                                <a href="#" style="color : #27AE60" onclick="initEdit2('{{ $group->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span>
                                </a>
                                <form id="actionForm2_{{ $group->id }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" onclick="deleteStore1('actionForm2_{{ $group->id }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <input type="hidden" id="groupId" name="groupId" value="{{ $group->id }}">

                                </form>
                            </div>

                            <div id="editButtons2_{{ $group->id }}" style="display: none;">
                                <a href="#" style="color : #3498DB" onclick="submitEdit2('{{ $group->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" onclick="cancelEdit2('{{ $group->id }}')">
                                    <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="editForm1_{{$group->id}}" action="">
                                    <input type="hidden" value="XXX" name="hiddenGroupname_{{$group->id}}" id="hiddenGroupname_{{$group->id}}">
                                    <input type="hidden" value="{{$group->id}}" name="groupid" id="groupid">
                                </form>
                            </div>
                        </td>
        </tr>
        @endforeach
    </tbody>
    </table>
</div>
<div class="col-md-4">
    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
        <h3 style="margin-top: 0px; text-align: center;">Create user group</h3>
        <form method="POST" id="usergroupForm">
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Group name</span>
            <input type="text" id="gname" name="gname" class="form-control" placeholder="Enter group name here" onkeyup="validateInput1()">
        </div>
    </form>
        <button onclick="submitForm3()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="submitBtn1" name="submitBtn1" disabled>
            Create new group
        </button><label style="margin-left: 30px; color: indianred;" id="errorMsgg"></label>
    </div>
</div>
