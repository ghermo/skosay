@extends('webservice.layouts.default')

@section('header')


    <script>

     function getgroup(sel) {
       // alert(sel.value);
       var val = sel.value;
       $('#getGroup').val(val);


    }



        function submitForm()
        {
            var submitForm = $('#createUser');

            document.getElementById('btnSubmit').style.display = 'none';
            document.getElementById('btnFalse').style.display = '';
            $.ajax({
                type : 'POST',
                url : submitForm.attr('action'),
                data : submitForm.serialize(),
                success : function(data){
                    if(data['status'] == 'SUCCESS')
                    {
                        $('#mainTable').append(data['user']);
                        $('#errorMsg').empty().append('User Created Successfully');
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';
                        document.getElementById('managerGroup').style.display = 'none';                            
                        $("#createUser")[0].reset();
                        location.reload();
                    }
                    else
                        $('#errorMsg').empty().append(data['msg']);
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';

                    setTimeout(function(){ $('#errorMsg').empty(); },4000);
                }
            });
        }

        function submitForm3()
        {
            $.ajax({
                type : 'POST',
                url : '/webAddUserGroup',
                data : $('#usergroupForm').serialize(),
                success : function(data){
                    if(data['status'] == 'SUCCESS')
                    {
                        $('#mainTable1').append(data['msg']);
                        $('#errorMsgg').empty().append('Group has been added.');
                    }
                    else
                        $('#errorMsgg').empty().append(data['msg']);
                }
            })
        }

        function deleteUser(user)
        {
            var r = confirm("Are you sure you want to delete this user?");
            if (r == true) 
            {
    
                var form = $('#'+user);
                $.ajax({
                    type : 'POST',
                    url : '/webDeleteUser',
                    data : form.serialize(),
                    success : function(){
                        form.closest('tr').hide('slow');
                    },
                    error : function(){

                    }
                });
            }
            else
            {
                
            }
        }

        function initEdit(data)
        {

            var t = $('#type_'+data).val();
            
//            alert($('#'+data).attr('id'));
            $('#actionButtons_'+data).hide();
            $('#editButtons_'+data).show();
            if(t == 'manager')
            {
                $('#emailDiv_'+data).show();
                $('#nameDiv_'+data).show();
                $('#districtDiv_'+data).show();
                $('#mobileDiv_'+data).show();

                $('#userName_'+data).hide();
                $('#userEmail_'+data).hide();
                $('#userDistrict_'+data).hide();
                $('userMobile_'+data).hide();
            }
            else
                $('#emailDiv_'+data).show();
                $('#nameDiv_'+data).show();
                $('#mobileDiv_'+data).show();

                $('#userName_'+data).hide();
                $('#userEmail_'+data).hide();
                $('#userMobile_'+data).hide();
        }

        function editUser(user)
        {
            var form = $(user);
//            alert(form.attr('action'));
            $.ajax({
                type : 'POST',
                url : form.attr('action'),
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                },
                error : function(){

                }
            });
        }

        function cancelEdit(data)
        {

            $('#actionButtons_'+data).show();
            $('#editButtons_'+data).hide();

            $('#emailDiv_'+data).hide();
            $('#nameDiv_'+data).hide();
            $('#mobileDiv_'+data).hide();
            $('#typeDiv_'+data).hide();
            $('#districtDiv_'+data).hide();

            $('#nameEdit_'+data).val($('#samplename_'+data).val());
            $('#emailEdit_'+data).val($('#sampleemail_'+data).val());
            $('#mobileEdit_'+data).val($('#samplemobile_'+data).val());
            $('#typeEdit_'+data).val($('#typesample_'+data).val());
            $('#districtEdit_'+data).val($('#dman_'+data).val());
            

            $('#userName_'+data).show();
            $('#userEmail_'+data).show();
            $('#userMobile_'+data).show();
            $('#userType_'+data).show();
            $('#userDistrict_'+data).show();

            location.reload();

        }

        function saveEdit(data)
        {
            $('#nameHidden_'+data).val($('#nameEdit_'+data).val());
            $('#typeHidden_'+data).val($('#typeEdit_'+data).val());
            $('#emailHidden_'+data).val($('#emailEdit_'+data).val());
            $('#mobileHidden_'+data).val($('#mobileEdit_'+data).val());

            $('#checkval_'+data).empty().val($('#typeEdit_'+data).find("option:selected").text());

            var t = $('#checkval_'+data).val();
            
            if(t == 'District Manager')
            {
                $('#dman1_'+data).val('1');
            }
            else
                $('#dman1_'+data).val('0');

            $('#districtHidden_'+data).val($('#dman1_'+data).val());

            $.ajax({
                type : 'POST',
                url : '/webEditUser',
                data : $('#editForm_'+data).serialize(),
                success : function(data){
                    if(data['status'] == 'FAILED')
                        alert(data['msg']);
                    else if(data['status'] == 'SUCCESS')
                    {
                        var actType, typeHidden = $('#typeHidden_'+data['id']), dHidden = $('#checkval_'+data['id']) ;
                        if(typeHidden.val() == 'consumer')
                            actType = 'Consumer';
                        else if(typeHidden.val() == 'manager' && dHidden.val() != 'District Manager')
                            actType = 'Manager';
                        else if(typeHidden.val() == 'admin')
                            actType = 'Administrator';
                        else if(typeHidden.val() == 'manager' && dHidden.val() == 'District Manager')
                            actType = 'District Manager'


                        

                        $('#userName_'+data['id']).empty().append($('#nameHidden_'+data['id']).val());
                        $('#userEmail_'+data['id']).empty().append($('#emailHidden_'+data['id']).val());
                        $('#userMobile_'+data['id']).empty().append($('#mobileHidden_'+data['id']).val());

                        $('#userType_'+data['id']).empty().append(actType);
                       

                        cancelEdit(data['id']);
                    }
                }
            });
        }

        
    </script>
@stop

@section('content')
<div class="bs-example" style="margin-top: 60px; margin-right: 10px;">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#sectionA">Users</a></li>
        <li><a data-toggle="tab" href="#sectionB" >User Group Connections</a></li>
        <!-- <li><a data-toggle="tab" href="#sectionC" >User Management</a></li> -->
    </ul>
</div>







<br>

<div class="tab-content">
    <div id="sectionA" class="tab-pane fade in active">
        <div class="col-md-8">
            {{ Form::open(array('url'=>'/searchUser', 
            'method' => 'GET', 
            'id' => 'search')) }}
        

                <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
                <button type="submit" class="btn btn-primary" style="display: none;"></button>
            
            {{ Form::close() }}
             <?php 
             Paginator::setPageName('users');
             $users = DB::table('users')->whereNotIn('id', [Auth::user()->id])->orderBy('id', 'ASC')->paginate(5); ?>
            <table class="table" style="overflow: hidden;" >
                <tr>
                    <th>ID</th>
                    <th style="width: 20%;">Name</th>
                    <th  style="width: 25%;">Email</th>
                    <th style ="width: 15%;">Mobile</th>
                    <th style="width: 20%;">Type</th>
                    
                    <th  style="width: 30%;">Action</th>
                </tr>
                <tbody id="mainTable">
                   
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>
                        @if($user->name != null)
                        
                            <p id="userName_{{$user->id}}"> {{ $user->name }} </p>
                            <div class="input-group" id="nameDiv_{{ $user->id }}" style="display: none;">
                                
                                <input value="{{$user->name}}" type="text" id="nameEdit_{{$user->id}}" name="nameEdit_{{$user->id}}" class="form-control" placeholder="Name" maxlength="255">
                            </div>
                        
                        @else
                            <p id="userName_{{$user->id}}"> <i>None</i> </p>
                            <div class="input-group" id="nameDiv_{{ $user->id }}" style="display: none;">
                                
                                <input value="{{$user->name}}" type="text" id="nameEdit_{{$user->id}}" name="nameEdit_{{$user->id}}" class="form-control" placeholder="Name" maxlength="255">
                            </div>
                        @endif
                        <input type="hidden" id="samplename_{{$user->id}}" name="samplename_{{$user->id}}" value="{{$user->name}}">
                    </td>
                    <td>
                        <p id="userEmail_{{$user->id}}"> {{ $user->email }} </p>

                        <div class="input-group" id="emailDiv_{{ $user->id }}" style="display: none;">
                            
                            <input value='{{ $user->email }}' type="email" id="emailEdit_{{$user->id}}" name="emailEdit_{{$user->id}}" class="form-control" placeholder="Email" maxlength="255">
                        </div>
                        <input type="hidden" id="email2_{{$user->id}}" name="email2_{{$user->id}}" value='{{$user->email}}'>
                        <input type="hidden" id="sampleemail_{{$user->id}}" name="sampleemail_{{$user->id}}" value='{{$user->email}}'>
                    </td>

                    <td>
                        @if($user->mobile != null)
                        
                            <p id="userMobile_{{$user->id}}"> {{ $user->mobile }} </p>
                            <div class="input-group" id="mobileDiv_{{ $user->id }}" style="display: none;">
                                
                                <input value="{{$user->mobile}}" type="text" id="mobileEdit_{{$user->id}}" name="mobileEdit_{{$user->id}}" class="form-control" placeholder="Mobile" maxlength="20">
                            </div>
                        
                        @else
                            <p id="userMobile_{{$user->id}}"> <i>None</i> </p>
                            <div class="input-group" id="mobileDiv_{{ $user->id }}" style="display: none;">
                                
                                <input value="{{$user->mobile}}" type="text" id="mobileEdit_{{$user->id}}" name="mobileEdit_{{$user->id}}" class="form-control" placeholder="Mobile" maxlength="20">
                            </div>
                        @endif
                        <input type="hidden" id="samplemobile_{{$user->id}}" name="samplemobile_{{$user->id}}" value="{{$user->mobile}}">
                    </td>

                    <td>
                        <p id="userType_{{$user->id}}">
                            @if($user->type == 'admin')
                                Administrator
                            @elseif($user->type == 'consumer')
                                Consumer
                            @elseif($user->type == 'manager' && $user->district_manager == '0')
                                Manager
                            @elseif($user->type == 'manager' && $user->district_manager == '1')
                                District Manager
                            @endif
                        </p>
                        <input type="hidden" id="type_{{$user->id}}" name="type_{{$user->id}}" value="{{$user->type}}" >
                        <div class="input-group" id="typeDiv_{{ $user->id }}" style="display: none;">
                            
                            <label>
                                <select id="typeEdit_{{$user->id}}" name="typeEdit_{{$user->id}}" class="form-control" >
                                    <option value="admin" <?php if($user->type == 'admin'){echo 'selected';} ?>>Administrator</option>
                                    <option value="manager" <?php if($user->type == 'manager'){echo 'selected';} ?>>Manager</option>
                                    <option value="consumer" <?php if($user->type == 'consumer'){echo 'selected';} ?>>Consumer</option>
                                    <option value="manager" <?php if($user->type == 'manager'){echo 'selected';} ?>>District Manager</option>
                                </select>
                            </label>
                            <input type="hidden" name="checkval_{{ $user->id }}" id="checkval_{{ $user->id }}" value="0">
                            <input type="hidden" id="dman1_{{$user->id}}" name="dman1_{{$user->id}}" value="0">  
                        </div>
                        <input type="hidden" id="typesample_{{$user->id}}" name="typesample_{{$user->id}}" value="{{$user->type}}">
                    </td>
                    
                    <td>
                        <form method="POST" id="editForm_{{$user->id}}">
                            <input type="hidden" name="emailHidden_{{$user->id}}" id="emailHidden_{{$user->id}}" value="NONE">
                            <input type="hidden" name="typeHidden_{{$user->id}}" id="typeHidden_{{$user->id}}" value="NONE">
                            <input type="hidden" name="nameHidden_{{$user->id}}" id="nameHidden_{{$user->id}}" value="NONE">
                            <input type="hidden" name="userIdEdit" id="userIdEdit" value="{{ $user->id }}">
                            <input type="hidden" name="districtHidden_{{$user->id}}" id="districtHidden_{{$user->id}}" value="NONE">
                            <input type="hidden" name="mobileHidden_{{$user->id}}" id="mobileHidden_{{$user->id}}" value="NONE">
                        </form>
                        <div id="actionButtons_{{ $user->id }}">
                            <a  href="#" title="Edit User" style="color : #27AE60" onclick="initEdit('{{ $user->id }}')">
                                <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span>
                            </a>
                            <form id="actionForm_{{ $user->id }}" method="POST" action="" style="display: inline;">
                                <a  href="#" style="color : #C0392B" title="Delete User" onclick="deleteUser('actionForm_{{ $user->id }}')">
                                    <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <input type="hidden" id="userId" name="userId" value="{{ $user->id }}">
                            </form>
                        </div>
                        <div id="editButtons_{{ $user->id }}" style="display: none;">
                            <a href="#" style="color : #2980B9" title="Save Changes" onclick="saveEdit('{{ $user->id }}')">
                                <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                            </a>
                            <a href="#" style="color : #C0392B" title="Cancel" onclick="cancelEdit('{{$user->id}}')">
                                <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                            </a>
                        </div>
                    </td>
                </tr>

                @endforeach

                </tbody>
            </table>
           <center><?php Paginator::setPageName('users');?>{{ $users->links(); }}</center> 
        </div>
        <div class="col-md-4" style="background-color: #BDC3C7; border-radius: 10px; padding: 20px;">
            <h3 style="margin-top: 0px; text-align: center;">Add User</h3>
                <form method="POST" action="/webCreateUser" id="createUser">
                    <div class="input-group">
                        <span class="input-group-addon">N</span>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">E</span>
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">P</span>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" maxlength="255">
                    </div>
                    <div class="input-group"  style="margin-top: 10px;">
                        <span class="input-group-addon">M</span>
                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Mobile Number" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">Account Type</span>
                        <select id="actType" name="actType" class="form-control" onchange="gettype(); getval(this); ">
                            <option value="admin">Administrator</option>
                            <option value="manager">Manager</option>
                            <option value="consumer">Consumer</option>
                            <option value="manager">District Manager</option>
                        </select>
                    </div>
                     <div class="input-group" style="margin-top: 10px; display: none;" id="managerGroup" name="managerGroup">
                        <span class="input-group-addon">Group</span>
                        
                                <select id="groupAdd" name="groupAdd" class="form-control" onchange="getgroup(this);">
                                    
                                    @foreach(Group::all() as $g)

                                    <option value="{{$g->id}}" >{{$g->groupname}}</option>

                                    @endforeach
                                </select>
                           
                            
                    </div>
                    <input type="hidden" id="getGroup" name="getGroup" value="1"/>
                    
                    <div class="input-group" style="margin-top: 10px; display:none;" id="set" name="set" >
                        <!-- <span class="input-group-addon">District Manager</span> -->
                        <input type="checkbox" id="district" name="district" value="1" style=" margin-left: 1em;" >
                        <label for="district" style=" margin-left: 1em;"><nobr>Set as District Manager</nobr></label>
                    </div>
                    <input type="hidden" id="checkvalue" name="checkvalue" value="0" >
                    
                    
                </form>
            <button onclick="submitForm()" id="btnSubmit" name="btnSubmit" type="button" class="btn btn-primary" style="margin-top: 20px;">
                Create new account
            </button>
            <button onclick="submitForm()" id="btnFalse" name="btnFalse" type="button" class="btn btn-primary" disabled style="display: none; margin-top: 20px; margin-bottom: 30px;">
                Create new account
            </button>
            <br><label style="width:200px; margin-top: 20px; color: indianred;" id="errorMsg"></label>
        </div>

    </div>
    <div id="sectionB" class="tab-pane fade" style="margin-top: 10px;">
        @include('webservice.user.assign-user')

    </div>
    <!-- <div id="sectionC" class="tab-pane fade">
<!---->
<!--    </div>-->
</div> 
@stop
<script>


//     document.addEventListener('DOMContentLoaded', function () {
//       document.querySelector('#district').addEventListener('change', changeHandler);
// });

//     function changeHandler(){
//    //Do Something...maybe another function showAlert(), for instance
//    if(district.checked){
//       //do something
//       $('#checkvalue').val($('#district').val());
//    }
//    else{
//       //do something else
//       $('#checkvalue').val('0');
//    }
// }
    function gettype()
    {

        
            var type = ($('#actType').find("option:selected").text());
            if(type == "District Manager")
            {
                $('#checkvalue').val('1');
            }
            else
                $('#checkvalue').val('0');
       

    }

    
    
    function getval(sel) {
       // alert(sel.value);
        

        var val = sel.value;
       
       if(val == 'manager')
       {
            // $('#set').hide();
            // document.getElementById('set').style.display = 'none';
            document.getElementById('managerGroup').style.display = '';
            

       }
       else
            // $('#set').show();
            // document.getElementById('set').style.display = '';
            document.getElementById('managerGroup').style.display = 'none';


    }
   

</script>

