<html>
    <head>
        <title>Skosay</title>
        {{HTML::style('css/bootstrap.min.css');}}

        {{HTML::script('js/jquery-1.11.0.min.js');}}
        {{HTML::script('js/bootstrap.min.js');}}

        <script>
            $(function () {
                $('#myTab a:first').tab('show')
            });
            $(function() {
                //for bootstrap 3 use 'shown.bs.tab' instead of 'shown' in the next line
                $('a[data-toggle="tab"]').on('click', function (e) {
                    //save the latest tab; use cookies if you like 'em better:
                    localStorage.setItem('lastTab', $(e.target).attr('href'));
                });

                //go to the latest tab, if it exists:
                var lastTab = localStorage.getItem('lastTab');

                if (lastTab) {
                    $('a[href="'+lastTab+'"]').click();
                }
            });
        </script>
        @yield('header')
    </head>
    <body style="margin: 10px;">
        @if(Auth::check())
            @if(Auth::user()->type == 'admin')
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <ul class="nav navbar-nav">
                        <li style="margin-right: -100px;"><img src="../images/banner.png" style="margin: 10px; width: 45%;"></li>
                        <li class="#active"><a href="/webDash" class=""><span class="glyphicon glyphicon-user" style="color: #27AE60; font-size: 18px; margin-right: 5px;"></span>User Accounts</a></li>

                        <li><a href="/webDept" class=""><span class="glyphicon glyphicon-pushpin" style="color: #9B31CF; font-size: 18px; margin-right: 5px;"></span>Departments</a></li>
                        <li><a href="/webStores"><span class="glyphicon glyphicon-shopping-cart" style="color: #C0392B; font-size: 18px; margin-right: 5px;"></span>Stores</a></li>
                        
                        <li><a href="/webStoreGroups"><span class="glyphicon glyphicon-shopping-cart" style="color: #C0392B; font-size: 18px; margin-right: 5px;"></span>Groups</a></li>

                        <li><a href="/webQuestions"><span class="glyphicon glyphicon-question-sign" style="color: #3366FF; font-size: 18px; margin-right: 5px;"></span>Questions</a></li>
                        <li><a href="/webReports"><span class="glyphicon glyphicon-pencil" style="color: #3366FF; font-size: 18px; margin-right: 5px;"></span>Reports</a></li>
                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li> -->
                    </ul>
                    <a href="/webLogout" title="Logout" style="float: right;"><button class="btn btn-danger" style="margin: 10px;"><span class="glyphicon glyphicon-off" style="margin-bottom: 3px; "></span></button></a>
                </nav>
            @endif
        @endif

        @yield('content')
    </body>
</html>
