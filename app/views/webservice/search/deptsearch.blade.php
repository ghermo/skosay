@extends('webservice.layouts.default')

@section('header')
    <script>

        function validateName()
        {
            var str = document.getElementById('deptname').value;
            var str2 = str.trim();


            if(str2.length == 0)
                document.getElementById('btnSubmit').disabled = true;
            else
                document.getElementById('btnSubmit').disabled = false;
        }

        function deleteDepartment(data)
        {
            var r = confirm("Are you sure you want to delete this department?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteDepartment',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                    location.reload();
                },
                error : function(){

                }
            });
            }
            else
            {
                
            }
        }

        function initEdit(data)
        {
            $('#editStorename_'+data).show();
            $('#editAddress_'+data).show();
            $('#pstorename_'+data).hide();
            $('#paddress_'+data).hide();

            $('#editButtons_'+data).show();
            $('#actionButtons_'+data).hide();
        }

        function submitEdit(data)
        {
            $('#hiddenStorename_'+data).val($('#storenameEdit_'+data).val());
            $('#hiddenAddress_'+data).val($('#addressEdit_'+data).val());
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditStore',
                data : $('#editForm_'+data).serialize(),
                success : function(store){
                    if(store['status'] == 'FAILED')
                        alert(store['msg']);
                    else if(store['status'] == 'SUCCESS')
                    {
                        $('#editStorename_'+data).hide();
                        $('#editAddress_'+data).hide();
                        $('#pstorename_'+data).show();
                        $('#paddress_'+data).show();

                        $('#editButtons_'+data).hide();
                        $('#actionButtons_'+data).show();

                        $('#pstorename_'+data).empty().append($('#hiddenStorename_'+data).val());
                        $('#paddress_'+data).empty().append($('#hiddenAddress_'+data).val());
                    }
                }
            })
        }

        function cancelEdit(data)
        {
            $('#editStorename_'+data).hide();
            $('#editAddress_'+data).hide();
            $('#pstorename_'+data).show();
            $('#paddress_'+data).show();

            $('#storenameEdit_'+data).val($('#sname_'+data).val());
            $('#addressEdit_'+data).val($('#sadd_'+data).val());

            $('#editButtons_'+data).hide();
            $('#actionButtons_'+data).show();

            location.reload();
        }   
        
        function submitForm(){
            var yo =  document.getElementById('deptname').value;


            if (yo != "" && yo != " ") {

            var form = $('#deptForm');
            // document.getElementById('btnFalse').disabled = true;
                    
            $.ajax({
            type : 'POST',
            url : '/webAddDepartment',
            data : form.serialize(),
            success : function(data){
            if(data['status'] == 'SUCCESS'){
                //$('#mainTable1').append(data['msg']);
                $('#errorMsgg').empty().append('Department has been added.');
                //document.getElementById('submitBtn').disabled = false;
                //$("#groupForm")[0].reset();
                location.reload();
            }
            else
                $('#errorMsgg').empty().append(data['msg']);
                location.reload();
                               // document.getElementById('submitBtn').disabled = false;
            }})

            }else
                $('#errorMsgg').empty().append('Department Name is Required');
        }

        
    </script>
@stop

@section('content')
<?php $group = Group::all();?>
<div class="bs-example" style="margin-top: 60px; margin-right: 10px;"><br>
        &nbsp&nbsp&nbsp&nbsp<a href="webDept"><button class="btn btn-primary">Back</button></a>&nbsp&nbsp&nbsp<b><font size="4">Search Results for "{{$placeholder}}"</b><br><br>
   
<div class="col-md-8" >

                {{ Form::open(array('url'=>'/searchDept', 
                'method' => 'GET', 
                'id' => 'search')) }}
                    <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
                    <button type="submit" class="btn btn-primary" style="display: none;"></button>
                {{ Form::close() }}

                <table style="padding: .2em;" class="table">
                <tr>
                    <th>ID</th>
                    <th>Department</th>
                    <th>For Store:</th>
                    <th>Action</th>
                </tr>
                <tbody id="mainTable">
                    <?php
                        $departments = DB::table('departments')->orderBy('id', 'DESC')->where('name', 'LIKE', '%'.$tags.'%')->orWhere('id', 'LIKE', '%'.$tags.'%')->paginate(5);
                        $count = DB::table('departments')->orderBy('id', 'DESC')->where('name', 'LIKE', '%'.$tags.'%')->orWhere('id', 'LIKE', '%'.$tags.'%')->count();
                    
                        if($count == null){
                            echo "<b>No Results Found.</b><br><br>";
                        }
                    ?>
                @foreach($departments as $dept)
                    <?php $storename = Store::where('id', $dept->storeid)->pluck('storename'); ?>

                    <tr>
                        <td><p>{{$dept->id}}</p></td>
                        <td>
                            <p id="pdepartmentname_{{$dept->id}}"> {{ $dept->name }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editdepartment_{{$dept->id}}">
                                <span class="input-group-addon">N</span>
                                <input type="text" id="departmentEdit_{{$dept->id}}" name="departmentEdit_{{$dept->id}}" class="form-control" value="{{$dept->name}}" maxlength="255">
                                <input type="hidden" id="departmentname_{{$dept->id}}" name="departmentname_{{$dept->id}}" value="{{$dept->name}}" >
                            </div>
                        </td>
                        <td>
                            @if($storename == NULL)
                            <p id="paddress_{{$dept->id}}"> Not yet Saved </p>
                            @else
                            <p id="paddress_{{$dept->id}}"> {{ $storename }} </p>
                            @endif
                        </td>
                        <td>
                            <div id="actionButtons_{{ $dept->id }}">
                                
                                <form id="actionForm_{{ $dept->id }}"  method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete Department" onclick="deleteDepartment('actionForm_{{ $dept->id }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <input type="hidden" id="deptId" name="deptId" value="{{ $dept->id }}" >
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <center>{{ $departments->appends(array('search' => $tags))->links(); }}
</center>
            </div>
        <div class="col-md-4">
            <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
                <h3 style="margin-top: 0px; text-align: center;">Add Department</h3>
                <form method="POST" id="deptForm" onkeypress="return event.keyCode != 13;">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon" style="padding-right: 20px;
                    padding-left: 18px;">Name</span>
                        <input type="text" onkeyup="validateName()" id="deptname" name="deptname" class="form-control" placeholder="Enter Department Name Here" maxlength="255" required onkeypress="return stopEnter()">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">Store</span>
                        <select id="storeAdd" name="storeAdd" class="form-control">
                                    
                                    @foreach(Store::all() as $s)

                                    <option value="{{$s->id}}" >{{$s->storename}}</option>

                                    @endforeach
                        </select>
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">Question Group</span>
                        <select id="questionAdd" name="questionAdd" class="form-control">
                                    
                                    @foreach(Qgroup::all() as $g)

                                    <option value="{{$g->id}}" >{{$g->groupName}}</option>

                                    @endforeach
                        </select>
                    </div>
                    
                </form>
                <button onclick="submitForm()" disabled type="button" class="btn btn-primary" style="margin-top: 20px;" id="btnSubmit" name="btnSubmit">
                        Add Department
                    </button>
                <br><label style="margin-top: 20px; color: indianred;" id="errorMsgg"></label>
            </div>
        </div>
    </div>
    <div id="sectionB" class="tab-pane fade">
       @include('webservice.department.department-group')
    </div>
    <!-- <div id="sectionC" class="tab-pane fade">
        @include('webservice.stores.question-to-stores')
    </div> -->
</div>

@stop