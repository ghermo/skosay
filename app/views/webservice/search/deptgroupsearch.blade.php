@extends('webservice.layouts.default')

@section('header')

<script type="text/javascript">
  
// $('#groupName_'+data).hide();

        function deleteDGroups(data)
        {
            var r = confirm("Are you sure you want to delete this department-group?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteDGroup',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                    //location.reload();
                },
                error : function(){

                }
            });
            }
            else{}
        }

        function submitFormDepartmentGroup()
        {
            $.ajax({
                type : 'POST',
                url : '/webAddDGroup',
                data : $('#deptgroupForm').serialize(),
                success : function(data){
                    if(data['status'] == 'SUCCESS')
                    {
                        $('#mainTableB').append(data['ddata']);
                        $('#errorMsg').empty().append('Connection has been added.');
                        //location.reload();
                    }
                    else
                        $('#errorMsg').empty().append('Duplication is not allowed');
                }
            })
        }


</script>
@stop

@section('content')
<?php $group = Group::all();?>
<div class="bs-example" style="margin-top: 60px; margin-right: 10px;"><br>
        &nbsp&nbsp&nbsp&nbsp<a href="webDept"><button class="btn btn-primary">Back</button></a>&nbsp&nbsp&nbsp<b><font size="4">Search Results for "{{$placeholder}}"</b><br><br>
   
<div class="col-md-8" >
    {{ Form::open(array('url'=>'/searchQGroups', 
    'method' => 'GET', 
    'id' => 'search')) }}

        <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
        <button type="submit" class="btn btn-primary" style="display: none;"></button>
    {{ Form::close() }}
    <table class="table" style="overflow: hidden;" >
        <tr>
            <th style="width: 15%">Id</th>
            <th style="width: 35%">Group Name</th>
            <th style="width: 25%">Department Name</th>
            <th style="width: 25%">Action</th>
        </tr>
        <tbody id="mainTableB">
            <?php 
            Paginator::setPageName('departmentgroups');
            $dgroups = DepartmentGroup::orderBy('department-groups.id', 'DESC')->join('departments', 'departments.id', '=', 'department-groups.deptid')->join('groups', 'groups.id', '=', 'department-groups.groupid')->where('departments.name', 'LIKE', '%'.$tags.'%')->orWhere('groupname', 'LIKE', '%'.$tags.'%')->orWhere('department-groups.id', 'LIKE', '%'.$tags[0].'%')->paginate(5); 
            $count = DepartmentGroup::orderBy('department-groups.id', 'DESC')->join('departments', 'departments.id', '=', 'department-groups.deptid')->join('groups', 'groups.id', '=', 'department-groups.groupid')->where('departments.name', 'LIKE', '%'.$tags.'%')->orWhere('groupname', 'LIKE', '%'.$tags.'%')->orWhere('department-groups.id', 'LIKE', '%'.$tags[0].'%')->count(); 

            if($count == null){
                    echo "<b>No Results Found.</b><br><br>";

            }
            ?>
            @foreach($dgroups as $dg)
            <tr>
            <td>
                <p id="dgID_{{$dg->id}}"> {{ $dg->id }} </p>
            </td>
            <td>
                <p id="dgName_{{$dg->id}}"> 
                    <?php $groupName = Group::where('id',$dg->groupid)->pluck('groupname');?>
                    {{ $groupName }}
                </p>
            </td>        
            <td>
                <p id="ddName_{{$dg->id}}" name="ddName_{{$dg->id}}">
                    <?php $deptName = Department::where('id',$dg->deptid)->pluck('name');?>
                    {{ $deptName }}
                </p>         
            </td>
            <td >
                <div id="actionButtons3_{{ $dg->id }}">
                    <form id="actionForm3_{{ $dg->id }}" method="POST" action="" style="display: inline;">
                        <a href="#" style="color : #C0392B" title="Delete" onclick="deleteDGroups('actionForm3_{{ $dg->id }}')">
                            <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                        </a>
                        <input type="hidden" id="dGroupid" name="dGroupid" value="{{ $dg->id }}">
                    </form>
                </div>

            </td>

            </tr> 
            @endforeach
        </tbody>
    </table>
    <center>{{ $dgroups->appends(array('search' => $tags))->links(); }}</center>
</div>

<div class="col-md-4">
    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
        <h3 style="margin-top: 0px; text-align: center;">Create department group connection</h3>
        <form method="POST" id="deptgroupForm">
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Group name</span>
            <select id="groupid" name="groupid" class="form-control" >
                 @foreach(Group::all() as $g)
                    <option value="{{$g->id}}" >{{$g->groupname}} </option>
                @endforeach
            </select>
        </div>
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Department Name</span>
            <select id="deptid" name="deptid" class="form-control" >
                <?php $deptgroups = DB::table('departments')->get(); ?>
                 @foreach($deptgroups as $dg)
                    <option value="{{$dg->id}}" >{{$dg->name}} </option>
                @endforeach
            </select>
        </div>
    </form>
        <button onclick="submitFormDepartmentGroup()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="submitBtn1" name="submitBtn1">
            Connect
        </button><br><label style="width: 200px; margin-top: 10px; color: indianred;" id="errorMsg"></label>
    </div>
</div>
@stop