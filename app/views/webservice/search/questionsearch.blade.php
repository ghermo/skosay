@extends('webservice.layouts.default')

@section('header')
    <script>

  
           
     function getval(sel) 
    {
       // alert(sel.value);
        

       
        var v = document.getElementById('qNum').value;
        
        if(v == 1)
        { //I'm supposing the "Other" option value is 0.
            if(document.getElementById('qname1').value.length != 0)
                document.getElementById('btnSubmit').disabled = false;
            else
                document.getElementById('btnSubmit').disabled = true;

          
            document.getElementById('qDiv2').style.display = 'none';
            document.getElementById('qDiv3').style.display = 'none';
            document.getElementById('qDiv4').style.display = 'none';
            document.getElementById('qDiv5').style.display = 'none';
          

            
        }   

        else if(v == 2)
        { //I'm supposing the "Other" option value is 0.

            document.getElementById('qDiv1').style.display = '';
            document.getElementById('qDiv2').style.display = '';
            document.getElementById('qDiv3').style.display = 'none';
            document.getElementById('qDiv4').style.display = 'none';
            document.getElementById('qDiv5').style.display = 'none';
         
            if(document.getElementById('qname1').value.length != 0 && document.getElementById('qname2').value.length != 0)
                document.getElementById('btnSubmit').disabled = false;
            else
                document.getElementById('btnSubmit').disabled = true;
        }

        else if(v == 3)
        { //I'm supposing the "Other" option value is 0.
            document.getElementById('qDiv1').style.display = '';
            document.getElementById('qDiv2').style.display = '';
            document.getElementById('qDiv3').style.display = '';
            document.getElementById('qDiv4').style.display = 'none';
            document.getElementById('qDiv5').style.display = 'none';
  
            
            if(document.getElementById('qname1').value.length != 0 && document.getElementById('qname2').value.length != 0 && document.getElementById('qname3').value.length != 0)
                document.getElementById('btnSubmit').disabled = false;
            else
                document.getElementById('btnSubmit').disabled = true;
        }
        else if(v == 4)
        { //I'm supposing the "Other" option value is 0.
            document.getElementById('qDiv1').style.display =  '';
            document.getElementById('qDiv2').style.display = '';
            document.getElementById('qDiv3').style.display = '';
            document.getElementById('qDiv4').style.display = '';
            document.getElementById('qDiv5').style.display = 'none';


            if(document.getElementById('qname1').value.length != 0 && document.getElementById('qname2').value.length != 0 && document.getElementById('qname3').value.length != 0 && document.getElementById('qname4').value.length != 0)
                document.getElementById('btnSubmit').disabled = false;
            else
                document.getElementById('btnSubmit').disabled = true;
        }
        else if(v == 5)
        { //I'm supposing the "Other" option value is 0.
            document.getElementById('qDiv1').style.display = '';
            document.getElementById('qDiv2').style.display = '';
            document.getElementById('qDiv3').style.display = '';
            document.getElementById('qDiv4').style.display = '';
            document.getElementById('qDiv5').style.display = '';

            
            if(document.getElementById('qname1').value.length != 0 && document.getElementById('qname2').value.length != 0 && document.getElementById('qname3').value.length != 0 && document.getElementById('qname4').value.length != 0 && document.getElementById('qname5').value.length != 0)
                document.getElementById('btnSubmit').disabled = false;
            else
                document.getElementById('btnSubmit').disabled = true;
        }

       


    }
        

      
       

        function deleteQuestion4(data)
        {
            var r = confirm("Are you sure you want to delete this question?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteQuestion',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                },
                error : function(){

                }
            });
            }
            else
            {}
        }

        function initEdit4(data)
        {
            $('#editquestionOrder_'+data).show();
            $('#editQuestion_'+data).show();
            
            $('#questionOrder_'+data).hide();
            $('#question_'+data).hide();
         

            $('#editButtons4_'+data).show();
            $('#actionButtons4_'+data).hide();
        }

        function submitEdit4(data)
        {
            $('#hiddenOrder_'+data).val($('#questionOrderEdit_'+data).val());
            $('#hiddenQuestion_'+data).val($('#questionEdit_'+data).val());
           
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditQuestion',
                data : $('#editForm4_'+data).serialize(),
                success : function(question){
                    if(question['status'] == 'FAILED')
                        alert(question['msg']);
                    else if(question['status'] == 'SUCCESS')
                    {
                        $('#editquestionOrder_'+data).hide();
                        $('#editQuestion_'+data).hide();
                       
                        $('#questionOrder_'+data).show();
                        $('#question_'+data).show();
                        

                        $('#editButtons4_'+data).hide();
                        $('#actionButtons4_'+data).show();

                        $('#questionOrder_'+data).empty().append($('#hiddenOrder_'+data).val());
                        $('#question_'+data).empty().append($('#hiddenQuestion_'+data).val());
                        
                    }
                }
            })
        }

        function cancelEdit4(data)
        {
           

            $('#editquestionOrder_'+data).hide();
            $('#editQuestion_'+data).hide();
            
            $('#questionOrder_'+data).show();
            $('#question_'+data).show();

            $('#questionOrderEdit_'+data).val($('#o_'+data).val());
            $('#questionEdit_'+data).val($('#q_'+data).val());
           

            $('#editButtons4_'+data).hide();
            $('#actionButtons4_'+data).show();

            location.reload();
        }   



            

        
    </script>
@stop

@section('content')

<div class="bs-example" style="margin-top: 60px; margin-right: 10px;"><br>
        &nbsp&nbsp&nbsp&nbsp<a href="webQuestions"><button class="btn btn-primary">Back</button></a>&nbsp&nbsp&nbsp<b><font size="4">Search Results for "{{$placeholder}}"</b><br><br>
   
<div class="col-md-8" >
                {{ Form::open(array('url'=>'/searchQuestions', 
                'method' => 'GET', 
                'id' => 'search')) }}

                    <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
                    <button type="submit" class="btn btn-primary"style="display: none;"></button>

                {{ Form::close() }}
            <table style="padding: .2em;" class="table">
                <tr>
                    <th>Question ID</th>
                    <th >Order ID</th>
                    <th>Question</th>
                
                    <th style="width: 15%;">Action</th>
                </tr>
                <tbody id="mainTable">
                    <?php $questions = Question::select(array('question-groups.qgroupid as qgroupid', 'questions.orderId as orderId', 'questions.question as question', 'questions.id as id'))->join('question-groups', 'question-groups.qid', '=', 'questions.id')->get();?>

                    <?php 
                    $q = DB::table('questions')->select(array('question-groups.qgroupid as qgroupid', 'questions.orderId as orderId', 'questions.question as question', 'questions.id as id'))->join('question-groups', 'question-groups.qid', '=', 'questions.id')->where('questions.question', 'LIKE', '%'.$tags.'%')->orWhere('questions.id', 'LIKE', '%'.$tags.'%')->paginate(5); 
                    $count = DB::table('questions')->select(array('question-groups.qgroupid as qgroupid', 'questions.orderId as orderId', 'questions.question as question', 'questions.id as id'))->join('question-groups', 'question-groups.qid', '=', 'questions.id')->where('questions.question', 'LIKE', '%'.$tags.'%')->orWhere('questions.id', 'LIKE', '%'.$tags.'%')->count(); 

                    if($count == null){
                    echo "<b>No Results Found.</b><br><br>";

                }

                    ?>
                   
                @foreach($q as $question)

                    <tr>
                        <td><p>{{$question->id}}</p></td>
                        <td>
                            <p id="questionOrder_{{$question->id}}"> {{ $question->orderId }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editquestionOrder_{{$question->id}}">
                                <span class="input-group-addon">O</span>
                                <input type="text" id="questionOrderEdit_{{$question->id}}" name="questionOrderEdit_{{$question->id}}" class="form-control" value="{{$question->orderId}}" maxlength="1">
                                <input type="hidden" id="o_{{$question->id}}" name="q_{{$question->id}}" value="{{$question->orderId}}" >  
                            </div>
                        </td>
                        <td>
                            <p id="question_{{$question->id}}"> {{ $question->question }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editQuestion_{{$question->id}}">
                                <span class="input-group-addon">Q</span>
                                <input type="text" id="questionEdit_{{$question->id}}" name="questionEdit_{{$question->id}}" class="form-control" value="{{$question->question}}" maxlength="255">
                                <input type="hidden" id="q_{{$question->id}}" name="q_{{$question->id}}" value="{{$question->question}}" >
                            </div>
                        </td>
                     
                        <td>
                            <div id="actionButtons4_{{ $question->id }}">
                                <a href="#" style="color : #27AE60" title="Edit Question" onclick="initEdit4('{{ $question->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span>
                                </a>
                                <form id="actionForm4_{{ $question->id }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete Question" onclick="deleteQuestion4('actionForm4_{{ $question->id }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>


                                    <input type="hidden" id="qId" name="qId" value="{{ $question->id }}">
                                </form>
                            </div>

                            <div id="editButtons4_{{ $question->id }}" style="display: none;">
                                <a href="#" style="color : #3498DB" title="Save Changes" onclick="submitEdit4('{{ $question->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" title="Cancel" onclick="cancelEdit4('{{ $question->id }}')">
                                    <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="editForm4_{{$question->id}}" action="">
                                    <input type="hidden" value="X" name="hiddenOrder_{{$question->id}}" id="hiddenOrder_{{$question->id}}">
                                    <input type="hidden" value="XX" name="hiddenQuestion_{{$question->id}}" id="hiddenQuestion_{{$question->id}}">
                                    <input type="hidden" value="XXX" name="hiddenQuestionGroup_{{$question->id}}" id="hiddenQuestionGroup_{{$question->id}}">
                                    <input type="hidden" value="{{$question->id}}" name="qid" id="qid">
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table> <center>{{ $q->appends(array('search' => $tags))->links(); }}</center>
        </div>
        <div class="col-md-4">
            <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
                <h3 style="margin-top: 0px; text-align: center;">Add a Question</h3>
                <form method="POST" id="questionForm">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon" style="padding-right: 20px;
                    padding-left: 20px;">Group</span>
                        
                                <select id="groupAdd" name="groupAdd" class="form-control" >
                                    @foreach(Qgroup::all() as $g)
                                    <option value="{{$g->id}}" >{{$g->groupName}}</option>

                                    @endforeach
                                </select>
                           
                            
                    </div>

                    <input type="hidden" id="getGroup" name="getGroup" value="1"/>

                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon" style="padding-right: 20px;
                    padding-left: 10px;">Number</span>
                        <select id="qNum" name="qNum" class="form-control" onchange="getval(this); ">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                       
                    <div class="input-group" style="margin-top: 10px;">
                        
                        <input type="hidden" id="orderId1" name="orderId1" class="form-control" value="1">
                    </div>
                    <div class="input-group" style="margin-top: 10px; " id="qDiv1" name="qDiv1">
                        <span class="input-group-addon">Question</span>
                        <input type="text" id="qname1" name="qname1" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        
                        <input type="hidden" id="orderId2" name="orderId2" class="form-control" value="2">
                    </div>
                    <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv2" name="qDiv2">
                        <span class="input-group-addon">Question</span>
                        <input type="text" id="qname2" name="qname2" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        
                        <input type="hidden" id="orderId3" name="orderId3" class="form-control" value="3">
                    </div>
                    <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv3" name="qDiv3">
                        <span class="input-group-addon">Question</span>
                        <input type="text" id="qname3" name="qname3" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        
                        <input type="hidden" id="orderId4" name="orderId4" class="form-control" value="4">
                    </div>
                    <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv4" name="qDiv4">
                        <span class="input-group-addon">Question</span>
                        <input type="text" id="qname4" name="qname4" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
                    </div>
                    <div class="input-group" style="margin-top: 10px;"> 
                        <input type="hidden" id="orderId5" name="orderId5" class="form-control" value="5">
                    </div>
                    <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv5" name="qDiv5">
                        <span class="input-group-addon">Question</span>
                        <input type="text" id="qname5" name="qname5" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
                    </div>
                </form>
                
                
                <button onclick="submitForm()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="btnSubmit" name="btnSubmit" disabled>
                    Add Question/s
                </button>
                <button onclick="submitForm()" type="button" class="btn btn-primary" style="display: none; margin-top: 10px;" id="btnFalse" name="btnFalse" disabled>
                    Add Question/s
                </button>
                <br><label style="margin-top: 20px; color: indianred;" id="qErrMsg"></label>

            </div>
        </div>
    </div>
    <div id="sectionB" class="tab-pane fade">
        @include('webservice.question.question-group')
    </div>
    <div id="sectionC" class="tab-pane fade">
        @include('webservice.stores.question-to-stores')
    </div>
</div>
<script>
    $("#groupAdd").change(function(){
  // alert($('#groupAdd').val());
        $('#getGroup').val($('#groupAdd').val());
    });

    

     function submitForm()
            {
                document.getElementById('btnSubmit').style.display = 'none';
                document.getElementById('btnFalse').style.display = '';
                $.ajax({
                    type : 'POST',
                    url : '/webAddQuestion',
                    data : $('#questionForm').serialize(),
                    success : function(data){
                       
                    if(data['status'] == 'SUCCESS')
                    {
                        
                            $('#mainTable').append(data['msg']);
                            $('#qErrMsg').empty().append('Question has been added.');
                            document.getElementById('btnSubmit').style.display = '';
                            document.getElementById('btnFalse').style.display = 'none';
                            $("#questionForm")[0].reset();
                            location.reload();
                    }
                    
                    else
                        $('#qErrMsg').empty().append(data['msg']);
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';
                        location.reload();
                        
                    }
                })
            }

    
    


</script>
@stop