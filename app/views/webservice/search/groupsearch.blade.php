@extends('webservice.layouts.default')

@section('header')

<script>

    function deleteStore1(data, groupid)
        {   
            
            var val = $('#ss_'+groupid).val();
        
            if(val >= 1)
            {
                alert("Cannot delete!");

            }

            else
            {
                var r = confirm("Are you sure you want to delete this store group?");
                if (r == true) 
                {
                    var form = $('#'+data);
                    $.ajax({
                        type : 'POST',
                        url : '/webDeleteGroup',
                        data : form.serialize(),
                        success : function(){
                            form.closest('tr').hide('slow');
                        },
                        error : function(){

                        }
                    });
                }
            }
            
            
        }

        function initEdit1(data)
            {
                $('#editGroupname_'+data).show();
               
                $('#gname_'+data).hide();
                
                $('#editButtons1_'+data).show();
                $('#actionButtons1_'+data).hide();
            }


        function submitEdit1(data)
        {
            $('#hiddenGroupname_'+data).val($('#gnameEdit_'+data).val());
           
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditGroup',
                data : $('#editForm1_'+data).serialize(),    
                success : function(group){
                    if(group['status'] == 'FAILED')
                        alert(group['msg']);
                    else if(group['status'] == 'SUCCESS')
                    {
                        $('#editGroupname_'+data).hide();
                        
                        $('#gname_'+data).show();
                        

                        $('#editButtons1_'+data).hide();
                        $('#actionButtons1_'+data).show();

                        $('#gname_'+data).empty().append($('#hiddenGroupname_'+data).val());
                       
                    }
                }
            })
        }


        function cancelEdit1(data)
        {
            $('#editGroupname_'+data).hide();
           
            $('#gname_'+data).show();

            $('#gnameEdit_'+data).val($('#gname1_'+data).val());
         

            $('#editButtons1_'+data).hide();
            $('#actionButtons1_'+data).show();
        }

        function validateInputStore()
        {
            if(document.getElementById('nameStore').value.length != 0)
                document.getElementById('submitBtnStore').disabled= false;
            else
                document.getElementById('submitBtnStore').disabled= true;
        }

    function submitForm1()
                {
                    var form = $('#groupForm');
                    document.getElementById('submitBtnStore').disabled = true;
                    
                    $.ajax({
                        type : 'POST',
                        url : '/webAddGroup',
                        data : form.serialize(),
                        success : function(data){
                            if(data['status'] == 'SUCCESS')
                            {
                                $('#mainTable1').append(data['msg']);
                                $('#errorMsgg').empty().append('Group has been added.');
                                document.getElementById('submitBtnStore').disabled = false;
                                
                               $("#groupForm")[0].reset();

                               location.reload();
                            }
                            else
                                $('#errorMsgg').empty().append(data['msg']);
                                document.getElementById('submitBtnStore').disabled = false;
                            setTimeout(function(){ $('#errorMsgg').empty(); },4000);
                        }
                    })
                }
        
</script>
@stop

@section('content')

<div class="bs-example" style="margin-top: 60px; margin-right: 10px;"><br>
        &nbsp&nbsp&nbsp&nbsp<a href="webStoreGroups"><button class="btn btn-primary">Back</button></a>&nbsp&nbsp&nbsp<b><font size="4">Search Results for "{{$placeholder}}"</b><br><br>
   
<div class="col-md-8" > 
    {{ Form::open(array('url'=>'/searchGroups', 
    'method' => 'GET', 
    'id' => 'search')) }}

        <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
        <button type="submit" class="btn btn-primary"style="display: none;"></button>
    {{ Form::close() }}
    <table style="padding: .2em;" class="table">
        <tr>
            <th>Store Group ID</th>
            <th style="width: 35%">Group name</th>
            <th style="width: 15%;">Stores in group</th>
            <th style="width: 15%;">Action</th>
        </tr>
        <tbody id="mainTable1">
        <?php 
        $Groupall = DB::table('groups')->orWhere('groupname', 'LIKE', '%'.$tags.'%')->orWhere('id', 'LIKE', '%'.$tags.'%')->paginate(5); 
        $count = DB::table('groups')->orWhere('groupname', 'LIKE', '%'.$tags.'%')->orWhere('id', 'LIKE', '%'.$tags.'%')->count(); 

        if($count == null){
            echo "<b>No Results Found.</b><br><br>";
        }
        ?>
        @foreach($Groupall as $group)
        <tr>
            <td>{{ $group->id }}</td>
            <td>
                <a href="webGroup/{{$group->id}}" style="text-decoration: none;">
                    <p id="gname_{{$group->id}}"> {{ $group->groupname }} </p></a>
                    <div class="input-group" style="margin-top: 10px; display: none;" id="editGroupname_{{$group->id}}">
                        <span class="input-group-addon">G</span>
                        <input type="text" id="gnameEdit_{{$group->id}}" name="gnameEdit_{{$group->id}}" class="form-control" value="{{$group->groupname}}" maxlength="255">
                        <input type="hidden" id="gname1_{{$group->id}}" name="gname1_{{$group->id}}" value="{{$group->groupname}}">
                    </div>
                
            </td>
            <td><p> {{ $x = StoreGroup::where('groupid', $group->id)->count() }} </p>
                  
            </td>
            <td>
                            <div id="actionButtons1_{{ $group->id }}">
                                <a href="#" style="color : #27AE60" title="Edit" onclick="initEdit1('{{ $group->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span>
                                </a>
                               
                                <form id="actionForm1_{{ $group->id }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete" onclick="deleteStore1('actionForm1_{{ $group->id }}', '{{ $group->id }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <input type="hidden" id="groupId" name="groupId" value="{{ $group->id }}">
                                    
                                </form>
                         <input type="hidden" id="ss_{{$group->id}}" name="ss_{{$group->id}}" value="{{ $x }}">
                                
                            </div>

                            <div id="editButtons1_{{ $group->id }}" style="display: none;">
                                <a href="#" style="color : #3498DB" title="Save Changes" onclick="submitEdit1('{{ $group->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" title="Cancel" onclick="cancelEdit1('{{ $group->id }}')">
                                    <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="editForm1_{{$group->id}}" action="">
                                    <input type="hidden" value="XXX" name="hiddenGroupname_{{$group->id}}" id="hiddenGroupname_{{$group->id}}">
                                    <input type="hidden" value="{{$group->id}}" name="groupid" id="groupid">
                                </form>
                            </div>
                        </td>
        </tr>
        @endforeach
    </tbody>
    </table>
    <center> {{ $Groupall->appends(array('search' => $tags))->links(); }}</center>
</div>
<div class="col-md-4">

    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
        <h3 style="margin-top: 0px; text-align: center;">Create a Group</h3>
        <form method="POST" id="groupForm" onkeypress="return event.keyCode != 13;">
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Group name</span>
            <input type="text" id="nameStore" name="nameStore" class="form-control" placeholder="Enter group name here" onkeyup="validateInputStore()" maxlength="255">
        </div>
    </form>
        <button onclick="submitForm1()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="submitBtnStore" name="submitBtnStore" disabled>
            Create new group
        </button>
        <button onclick="submitForm1()" type="button" class="btn btn-primary" style="display: none; margin-top: 10px;" id="btnFalseStore" name="btnFalseStore" disabled>
            Create new group
        </button>
        <br><label style="margin-top: 20px; color: indianred;" id="errorMsgg"></label>
    </div>
</div>
@stop