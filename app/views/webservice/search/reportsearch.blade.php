@extends('webservice.layouts.default')

@section('header')

@stop

@section('content')

<div class="bs-example" style="margin-top: 60px; margin-right: 10px;">
</div>
<div class="tab-content">
    <div id="sectionA" class="tab-pane fade in active" style="margin-top: 15px;">
        <center>
        	&nbsp&nbsp&nbsp&nbsp<br><a href="webReports"><button class="btn btn-primary">Back</button></a>&nbsp&nbsp&nbsp<b><font size="4">Search Results for "{{$placeholder}}"</b><br><br>
            {{ Form::open(array('url'=>'/searchReports', 
            'method' => 'GET', 
            'id' => 'search')) }}
                <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
                <button type="submit" class="btn btn-primary" style="display: none;"></button>
            {{ Form::close() }}

             <?php 
             $issues = DB::table('issues')->join('users', 'users.id', '=', 'issues.userid')->groupBy('issues.userid')->orderBy('issues.created_at', 'DESC')->where('users.email', 'LIKE', '%'.$tags.'%')->orWhere('users.name', 'LIKE', '%'.$tags.'%')->orWhere('issues.userid', 'LIKE', '%'.$tags.'%')->paginate(5); 
             $count = DB::table('issues')->join('users', 'users.id', '=', 'issues.userid')->groupBy('issues.userid')->orderBy('issues.created_at', 'DESC')->where('users.email', 'LIKE', '%'.$tags.'%')->orWhere('users.name', 'LIKE', '%'.$tags.'%')->orWhere('issues.userid', 'LIKE', '%'.$tags.'%')->count(); 

             if($count == null){
                    echo "<b>No Results Found.</b><br><br>";

             }
             ?>
            <table class="table" style="overflow: hidden;" >
                <tr>
                    <th style="width: 10%;">ID</th>
                    <th style="width: 25%;">Name</th>
                    <th style="width: 20%;">Account type</th>
                    <th style="width: 25%;">Email</th>
                    <th style="width: 20%;">Last Activity</th>
                </tr>
                <tbody id="mainTable">
                @foreach($issues as $issue)
                <tr>
                    <td>
                        {{ $issue->userid }}
                    </td>
                    <td>
                        @if(DB::table('users')->where('id', $issue->userid)->pluck('name') == "")               
                        <a href="/activities/{{ $issue->userid }}" id="userName_{{DB::table('users')->where('id', $issue->userid)->pluck('id')}}"> 
                            <?php  
                                echo "Guest";           
                            ?>
                        </a>  
                        @else
                        <a href="/activities/{{DB::table('users')->where('id', $issue->userid)->pluck('id')}}" id="userName_{{DB::table('users')->where('id', $issue->userid)->pluck('id')}}"> 
                            <?php  
                                echo DB::table('users')->where('id', $issue->userid)->pluck('name')

                            ?>
                        </a> 
                        @endif
                    </td>
                    <td>
                        <?php 

                            if(DB::table('users')->where('id', $issue->userid)->pluck('fbid') == 0) 
                            {
                                if(DB::table('users')->where('id', $issue->userid)->pluck('name') == "") 
                                {
                                    $type = "Guest";
                                }
                                else
                                {
                                    $type = "Standard";
                                }                              
                            } 
                              else 
                            {
                                $type = "Facebook";
                            }


                        ?>
                        {{ $type }}
                    </td>

                    <td>
                        <?php 
                        if(DB::table('users')->where('id', $issue->userid)->pluck('name') == ""){
                            echo "none";
                        }
                        else{
                            echo DB::table('users')->where('id', $issue->userid)->pluck('email');
                        }

                        ?>
                          
                    </td>
                    
                    <td>
                    <?php $latest = DB::table('messages')->where('userid', $issue->userid)->select('updated_at')->orderBy('updated_at', 'DESC')->get();?>
                    {{ $latest[0]->updated_at }}
                    
                    </td>
                    
                </tr>

                @endforeach

                </tbody>
            </table>
           <center>{{ $issues->appends(array('search' => $tags))->links(); }}</center> 
        </center>
</div> 
@stop

