@extends('webservice.layouts.default')

@section('header')
<script type="text/javascript">
  
// $('#groupName_'+data).hide();

    function initEdit3(data)
        {
//            alert($('#'+data).attr('id')); 

            $('#groupName_'+data).hide();
            $('#userGroupDiv_'+data).show();

            $('#gs_'+data).val($('#userGroupEdit_'+data).val());

            $('#actionButtons3_'+data).hide();
            $('#editButtons3_'+data).show();

        }

 

        function submitEdit3(data)
        {
            $('#sample_'+data).empty().val($('#userGroupEdit_'+data).find("option:selected").text());
            
            $('#groupHidden_'+data).val($('#userGroupEdit_'+data).val());
            // $('#hiddenAddress_'+data).val($('#addressEdit_'+data).val());
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditUserGroup',
                data : $('#editForm3_'+data).serialize(),
                success : function(){
                   
                        $('#userGroupDiv_'+data).hide();
                       
                        $('#groupName_'+data).show();
                      

                        $('#editButtons3_'+data).hide();
                        $('#actionButtons3_'+data).show();

                        $('#groupName_'+data).empty().append($('#sample_'+data).val());
                    
                }
            })


        }

        function deleteQuestion3(data)
        {
            var r = confirm("Are you sure you want to delete this user group?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteUserGroup',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                },
                error : function(){

                }
            });
            }
            else{}
        }

        function cancelEdit3(data)
        {
            $('#actionButtons3_'+data).show();
            $('#editButtons3_'+data).hide();

            $('#groupName_'+data).show();
            $('#userGroupDiv_'+data).hide();

            $('userGroupEdit_'+data).val($('#gs_'+data).val());

            $('#actionButtons3_'+data).show();
            $('#editButtons3_'+data).hide();

            location.reload();
        }

        function submitFormConnection()
        {
            $.ajax({
                type : 'POST',
                url : '/webAddUserGroupConnection',
                data : $('#usergroupForm').serialize(),
                success : function(data){
                    if(data['status'] == 'SUCCESS')
                    {
                        $('#errorMsgg').empty().append('Connection has been added.');
                    }
                    else
                        $('#errorMsgg').empty().append('Duplication is not allowed');
                }
            })
            location.reload();
        }


</script>
@stop

@section('content')


<div class="bs-example" style="margin-top: 60px; margin-right: 10px;"><br>
        &nbsp&nbsp&nbsp&nbsp<a href="webDash"><button class="btn btn-primary">Back</button></a>&nbsp&nbsp&nbsp<b><font size="4">Search Results for "{{$placeholder}}"</b><br><br>
   
<div class="col-md-8" >
        {{ Form::open(array('url'=>'/searchAssign', 
        'method' => 'GET', 
        'id' => 'search')) }}
            <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
            <button type="submit" class="btn btn-primary" style="display: none;"></button>
        {{ Form::close() }}
             
            <table class="table" style="overflow: hidden;" >
                <tr>
                    <th style="width: 15%">Name</th>
                    <th style="width: 25%">Email</th>
                    <th style="width: 15%">Type</th>
                    <th style="width: 25%" >User Group</th>
                    <th style="width: 25%">Action</th>
                </tr>
                <tbody id="mainTable">
                    <?php $user = User::where('type', 'manager')->get();?>
                    <?php 

                        $user1 = User::select(array('users.id as id', 'users.email as email', 'users.name as name', 'users.type as type'))->groupBy('users.id')->where('users.type','manager')->paginate(8);
                        $user2 =DB::table('users')
                        ->join('user-groups', 'user-groups.userid', '=','users.id')
                        ->join('groups','groups.id','=','user-groups.groupid')
                        ->select(array('users.id as id', 'users.email as email', 'users.name as name', 'users.type as type','groups.groupname as gname','groups.id as gId','user-groups.id as ugId'))
                        ->where('users.name', 'LIKE', '%'.$tags.'%')->orWhere('users.email', 'LIKE', '%'.$tags.'%')->orWhere('users.id', 'LIKE', '%'.$tags.'%')->orWhere('users.type', 'LIKE', '%'.$tags.'%')->orWhere('groups.groupname', 'LIKE', '%'.$tags.'%')
                        ->paginate(8);

                        $count =DB::table('users')
                        ->join('user-groups', 'user-groups.userid', '=','users.id')
                        ->join('groups','groups.id','=','user-groups.groupid')
                        ->select(array('users.id as id', 'users.email as email', 'users.name as name', 'users.type as type','groups.groupname as gname','groups.id as gId','user-groups.id as ugId'))
                        ->where('users.name', 'LIKE', '%'.$tags.'%')->orWhere('users.email', 'LIKE', '%'.$tags.'%')->orWhere('users.id', 'LIKE', '%'.$tags.'%')->orWhere('users.type', 'LIKE', '%'.$tags.'%')->orWhere('groups.groupname', 'LIKE', '%'.$tags.'%')
                        ->count();

                        if($count == null){
                            echo "<b>No Results Found.</b><br><br>";

                        }
                    ?>

                    <?php $questions = Question::select(array('question-groups.qgroupid as qgroupid', 'questions.orderId as orderId', 'questions.question as question', 'questions.id as id'))->join('question-groups', 'question-groups.qid', '=', 'questions.id')->paginate(5);?>

                @foreach($user2 as $user)
                <tr>
                    <td>
                        <p id="userName_{{$user->id}}"> {{ $user->name }} </p>
                        
                       
                    </td>
                    <td>
                        <p id="userEmail_{{$user->id}}"> {{ $user->email }} </p>
                        <input type="hidden" id="useremail" name="useremail" value="{{$user->email}}"/>
                    </td>
                    <td>
                        <p id="userType_{{$user->id}}">
                            @if($user->type == 'admin')
                                Administrator
                            @elseif($user->type == 'consumer')
                                Consumer
                            @elseif($user->type == 'manager')
                                Manager
                            @endif
                        </p>
                        <input type="hidden" id="usertype" name="usertype" value="{{$user->type}}"/>
                    </td>
                    
                    <td>

                        
                        

                        <p id="groupName_{{$user->id}}" name="groupName_{{$user->id}}"> 
                                {{ $user->gname }}   
                        </p>
                        <div class="input-group" style="margin-top: 10px; display: none;" id="userGroupDiv_{{$user->id}}">

                                <span class="input-group-addon">UG</span>
                                <select type="text" id="userGroupEdit_{{$user->id}}" name="userGroupEdit_{{$user->id}}" class="form-control" >
                                    @foreach(Group::all() as $g)
                                    @if($g->groupname == $user->gname)
                                    <option value="{{$g->id}}" selected="selected">{{$g->groupname}}</option>
                                    @else
                                    <option value="{{$g->id}}">{{$g->groupname}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                <input type="hidden" id="sample_{{$user->id}}" name="sample_{{$user->id}}" value="xxx">
                                <input type="hidden" id="gs_{{$user->id}}" name="gs_{{$user->id}}"   >
                        </div>
                                
                    </td>
                    <td >
                                    <!-- ///////// -->

                            <div id="actionButtons3_{{ $user->id }}">
                                <a href="#" style="color : #27AE60" title="Edit" onclick="initEdit3('{{ $user->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span>
                                </a>
                                
                                    <input type="hidden" id="groupId" name="groupId" value="{{ $user->gId }}">
                                <form id="actionForm3_{{ $user->ugId }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete" onclick="deleteQuestion3('actionForm3_{{ $user->ugId }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <input type="hidden" id="userId" name="userId" value="{{ $user->ugId }}">
                                </form>
                            </div>

                            <div id="editButtons3_{{ $user->id }}" style="display: none;">
                                <a href="#" style="color : #3498DB" title="Save Changes" onclick="submitEdit3('{{ $user->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" title="Cancel" onclick="cancelEdit3('{{$user->id}}')">
                                <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="editForm3_{{$user->id}}" action="">
                                    <input type="hidden" value="XXX" name="groupHidden_{{$user->id}}" id="groupHidden_{{$user->id}}">
                                    
                                    <input type="hidden" value="{{$user->id}}" name="userid" id="userid">
                                </form>
                                
                            </div>


                        </div>
                        
                    </td>

                </tr> 
                @endforeach
                </tbody>
            </table>
            <center>{{ $user2->appends(array('search' => $tags))->links(); }}</center>
        </div>
        


   <div class="col-md-4">
    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
        <h3 style="margin-top: 0px; text-align: center;">Create user group connection</h3>
        <form method="POST" id="usergroupForm">
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Group name</span>
            <select id="groupid" name="groupid" class="form-control" >
                 @foreach(Group::all() as $g)
                    <option value="{{$g->id}}" >{{$g->groupname}} </option>
                @endforeach
            </select>
        </div>
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Manager Name</span>
            <select id="userid" name="userid" class="form-control" >
                <?php $us = DB::table('users')->where('type','manager')->get(); ?>
                 @foreach($us as $u)
                    <option value="{{$u->id}}" >{{$u->name}} </option>
                @endforeach
            </select>
        </div>
    </form>
        <button onclick="submitFormConnection()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="submitBtn1" name="submitBtn1">
            Connect
        </button><br><label style="margin-top: 20px; width: 200px; color: indianred;" id="errorMsgg"></label>
    </div>
</div>
@stop