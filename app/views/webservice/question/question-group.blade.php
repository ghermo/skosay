
    <script>
      
        function ideleteQuestion5(data,qgroupid)
        {
          
            var qval = $('#ss1_'+qgroupid).val();
          
            if(qval >= 1)
            {
                alert("Cannot delete!");

            }

            else
            {
                var r = confirm("Are you sure you want to delete this store group?");
                if (r == true) 
                {
                    var form = $('#'+data);
                    $.ajax({
                        type : 'POST',
                        url : '/webDeleteQuestionGroup',
                        data : form.serialize(),
                        success : function(){
                            form.closest('tr').hide('slow');
                        },
                        error : function(){

                        }
                    });
                }
            }
            
            
        }

        function initEdit5(data)
        {
            
            $('#editqGroup1_'+data).show();
           
            $('#questionGroup_'+data).hide();

            $('#qgroup2_'+data).val($('#qGroupEdit1_'+data).val());

            $('#editButtons5_'+data).show();
            $('#actionButtons5_'+data).hide();
        }

        function submitEdit5(data)
        {
            // alert(data);
            
            $('#qHidden1_'+data).val($('#qGroupEdit1_'+data).val());
           // alert($('#hiddenQuestionGroup_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditQuestionGroup',
                data : $('#QeditForm5_'+data).serialize(),
                success : function(question1){
                   
                        
                        $('#editqGroup1_'+data).hide();
                        
                        $('#questionGroup_'+data).show();

                        $('#editButtons5_'+data).hide();
                        $('#actionButtons5_'+data).show();

                        
                        $('#questionGroup_'+data).empty().append($('#qHidden1_'+data).val());
                             // alert($('#hiddenQuestionGroup_'+data).val());
                    
                }
            })
        }

        function cancelEdit5(data)
        {
           
            $('#editqGroup1_'+data).hide();
           
            $('#questionGroup_'+data).show();

            $('#qGroupEdit1_'+data).val($('#qgroup2_'+data).val());

            $('#editButtons5_'+data).hide();
            $('#actionButtons5_'+data).show();
        }   
       
        function validateInputgroup()
        {
             if(document.getElementById('qgname').value.length != 0)
                document.getElementById('submitBtnQGroup1').disabled= false;
            else
                document.getElementById('submitBtnQGroup1').disabled= true;
        }
      

            

        
    </script>

   
<div class="col-md-8">
        {{ Form::open(array('url'=>'/searchQGroups', 
        'method' => 'GET', 
        'id' => 'search')) }}        
            <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
            <button type="submit" class="btn btn-primary"style="display: none;"></button>
    </form>
            <table style="padding: .2em;" class="table">
                <tr>
                    <th style="width: 15%">Question Group ID</th>
    
                    
                    <th style="width: 25%">Question Groupname</th>
                    <th style="width: 15%;">Number of Questions</th>
                    
                    <th style="width: 15%;">Action</th>
                </tr>
                <tbody id="mainTableQGroup1">
                     

                     <?php 
                     Paginator::setPageName('questiongroups');
                     $qGroup = Qgroup::paginate(5); 
                     ?>
                    
                   
                @foreach($qGroup as $question)

                    <tr>
                        <td><p style="margin-left:1em;">{{$question->id}}</p></td>
                    
                        
                        <td>
                            <a href="webQGroup/{{$question->id}}" style="text-decoration: none;">
                            <p id="questionGroup_{{$question->id}}"> {{ $question->groupName }} </p></a>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editqGroup1_{{$question->id}}" name ="editqGroup1_{{$question->id}}">
                                <span class="input-group-addon">QG</span>
                                <input type="text" value="{{$question->groupName}}" class="form-control" id="qGroupEdit1_{{$question->id}}" name="qGroupEdit1_{{$question->id}}">

                               <input type="hidden" value="{{$question->groupName}}" id="qgroup2_{{$question->id}}" name="qgroup2_{{$question->id}}">
                                
                            </div>
                        </td>
                        <td align="center">
                            <p>{{ $xsample = QuestionGroup::where('qgroupid', $question->id)->count() }}</p>                           
                        </td>
                        <td>
                            <div id="actionButtons5_{{ $question->id }}">
                                <a href="#" style="color : #27AE60" title="Edit" onclick="initEdit5('{{ $question->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-edit"></span>
                                </a>
                                <form id="iactionForm5_{{ $question->id }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete" onclick="ideleteQuestion5('iactionForm5_{{ $question->id }}','{{ $question->id }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>

                                    
                                    <input type="hidden" id="qId" name="qId" value="{{ $question->id }}">
                                   
                                </form>
                                 <input type="hidden" value="{{ $xsample }}" id="ss1_{{ $question->id }}" name="ss1_{{ $question->id }}" >
                            </div>
                            <div id="editButtons5_{{ $question->id }}" style="display: none;">
                                <a href="#" style="color : #3498DB" title="Save Changes" onclick="submitEdit5('{{ $question->id }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" title="Cancel" onclick="cancelEdit5('{{ $question->id }}')">
                                    <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="QeditForm5_{{$question->id}}" action="">
                                    <input type="hidden" name="qHidden1_{{ $question->id }}" id="qHidden1_{{$question->id}}">
                                <input type="hidden" value="{{$question->id}}" name="qid" id="qid">
                                </form>
                            </div>
                            
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
           <center> <?php  Paginator::setPageName('questiongroups'); ?>{{ $qGroup->links() }}</center>
     
</div>

<div class="col-md-4">
    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
        <h3 style="margin-top: 0px; text-align: center;">Create a question group</h3>
        <form method="POST" id="groupForm1" name="groupForm1" onkeypress="return event.keyCode != 13;">
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Group name</span>
            <input type="text" id="qgname" name="qgname" class="form-control" placeholder="Enter group name here" onkeyup="validateInputgroup()" maxlength="255">
        </div>
        </form>
        <button onclick="submitFormGroup1()" type="submit" class="btn btn-primary" style=" margin-top: 10px;" id="submitBtnQGroup1" name="submitBtnQGroup1" disabled>
            Create new group
        </button>
        <button onclick="submitFormGroup1()" type="submit" class="btn btn-primary" style="display: none; margin-top: 10px;" id="btnFalseQGroup1" name="btnFalseQGroup1" disabled>
            Create new group
        </button>
        <br><label style="margin-top: 20px; width: 200px; color: indianred;" id="errorMsgg1"></label>
        
    </div>
    
</div>

<script>


    function submitFormGroup1()
    {
        var form = $('#groupForm1');
                    document.getElementById('submitBtnQGroup1').disabled = true;
                    
                    $.ajax({
                        type : 'POST',
                        url : '/webAddQuestionGroup',
                        data : form.serialize(),
                        success : function(data){
                            if(data['status'] == 'SUCCESS')
                            {
                                $('#mainTableQGroup1').append(data['msg']);
                                $('#errorMsgg1').empty().append('Group has been added.');
                                document.getElementById('submitBtnQGroup1').disabled = false;
                                $("#groupForm1")[0].reset();
                                location.reload();

                            }
                            else
                                $('#errorMsgg1').empty().append(data['msg']);
                                document.getElementById('submitBtnQGroup1').disabled = false;
                        }
                    })
    }

    

</script>



