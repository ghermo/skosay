
@extends('webservice.layouts.default')
    <script>
           
   

        function QdeleteQuestion5(data)
        {
            var r = confirm("Are you sure you want to delete this question from question group?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteQuestionGroupDetail',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                    location.reload();
                },
                error : function(){

                }
            });
            }
            else{}
        }

        function QinitEdit5(data)
        {
            
            $('#editqGroup_'+data).show();
           
            $('#questionGroup_'+data).hide();

            $('#qgroup1_'+data).val($('#qGroupEdit_'+data).val());

            $('#QeditButtons5_'+data).show();
            $('#QactionButtons5_'+data).hide();
        }

        function QsubmitEdit5(data)
        {
            // alert(data);
            $('#sample_'+data).empty().val($('#qGroupEdit_'+data).find("option:selected").text());
            $('#qHidden_'+data).val($('#qGroupEdit_'+data).val());
           // alert($('#hiddenQuestionGroup_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditQuestionGroupDetail',
                data : $('#QeditForm5_'+data).serialize(),
                success : function(question1){
                   
                        
                        $('#editqGroup_'+data).hide();
                        
                        $('#questionGroup_'+data).show();

                        $('#QeditButtons5_'+data).hide();
                        $('#QactionButtons5_'+data).show();

                        
                        $('#questionGroup_'+data).empty().append($('#sample_'+data).val());
                             // alert($('#hiddenQuestionGroup_'+data).val());
                    
                }
            })
        }

        function QcancelEdit5(data)
        {
           
            $('#editqGroup_'+data).hide();
           
            $('#questionGroup_'+data).show();

            $('#qGroupEdit_'+data).val($('#qgroup1_'+data).val());

            $('#QeditButtons5_'+data).hide();
            $('#QactionButtons5_'+data).show();
        }   
       

      function submitFormDetail()
      {
         var form = $('#detailForm');
                    document.getElementById('submitBtnDetail').disabled = true;
                    
                    $.ajax({
                        type : 'POST',
                        url : '/webAddQuestionGroupDetail',
                        data : form.serialize(),
                        success : function(data){
                            if(data['status'] == 'SUCCESS')
                            {
                                $('#mainTableDetail').append(data['msg']);
                                $('#errorDetail').empty().append('Question has been added.');
                                document.getElementById('submitBtnDetail').disabled = false;
                                
                                location.reload();
                            }
                            else
                                $('#errorDetail').empty().append(data['msg']);
                                document.getElementById('submitBtnDetail').disabled = false;
                        }
                    })
      }

        
    </script>

@section('header')


@stop

@section('content')
<div style="margin-top: 5em;">
<div style="margin-top: 5em;">
<div class="col-md-8" style="background-color: #2C3E50; color: #ECF0F1; text-align: center; border-radius: 10px;">
    <br>
    <h3>Questions under {{ $qgroupDetails->groupName }} group</h3>
    <br>
            <table style="padding: .2em;" class="table">
                <thead style="color: #ECF0F1">
                <tr>
                    <th style="width: 15%">Question ID</th>
    
                    <th style="width: 25%">Question</th>
                  
                    
                    <th style="width: 15%;">Action</th>
                </tr>
            </thead>
                <tbody style="color: #BDC3C7;" id="mainTableDetail">
                     <?php 
                     $questions = QuestionGroup::select(array('question-groups.qgroupid as qgroupid1', 'questions.question as question1', 'question-groups.id as id1','question-groups.qid as qid' ,'questions.orderId as orderid','questions.id as id','qgroups.groupName as groupname1'))->join('questions', 'questions.id', '=', 'question-groups.qid')->join('qgroups','qgroupid','=','qgroups.id')->where('qgroups.id',$qgroupDetails->id)->paginate(5);?>
                    @if($questions->count() == 0)
                        <tr id="noData"><td colspan="4"><center><i>No data available.</i></center></td></tr>
                   @else
                    @foreach($questions as $question)

                    <tr>
                        <td>{{$question->id}}</td>
                    
                        <td>
                            <p id="question_{{$question->id1}}"> {{ $question->question1 }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editQuestion_{{$question->id1}}">
                                <span class="input-group-addon">Q</span>
                                <input type="text" id="questionEdit_{{$question->id1}}" name="questionEdit_{{$question->id1}}" class="form-control" value="{{$question->question1}}">
                            </div>
                        </td>
                        
                        <td>
                            <div id="QactionButtons5_{{ $question->id1 }}">
                                
                                <form id="QactionForm5_{{ $question->id1 }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete" onclick="QdeleteQuestion5('QactionForm5_{{ $question->id1 }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <?php 
                                        $countx = DB::table('question-groups')->where('qid', $question->id)->pluck('qgroupid');
                                        $countz = DB::table('question-groups')->where('qgroupid', '=', $countx)->count();
                                    
                                        $erasedOrderId = DB::table('questions')->where('id', $question->qid)->pluck('orderId');
                                    ?>
                                    <input type="hidden" id="qId" name="qId" value="{{ $question->qid }}">
                                    <input type="hidden" id="count" name="count" value="{{ $countz }}">
                                    <input type="hidden" id="erasedOrderId" name="erasedOrderId" value="{{ $erasedOrderId }}">
                                    <input type="hidden" id="groupid" name="groupid" value="{{ $countx }}">
                                    
                                </form>
                            </div>
                            <div id="QeditButtons5_{{ $question->id1 }}" style="display: none;">
                                <a href="#" style="color : #3498DB" title="Save Changes" onclick="QsubmitEdit5('{{ $question->id1 }}')">
                                    <span style="font-size: 30px; margin: -1px; margin-right: 5px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" title="Cancel" onclick="QcancelEdit5('{{ $question->id1 }}')">
                                    <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="QeditForm5_{{$question->id1}}" action="">
                                    <input type="hidden" id="sample_{{$question->id}}" name="sample_{{$question->id}}" value="xxx">
                                    <input type="hidden" name="qHidden_{{ $question->id }}" id="qHidden_{{$question->id}}">
                                <input type="hidden" value="{{$question->id}}" name="qid" id="qid">
                                </form>
                            </div>
                            
                        </td>
                    </tr>
                @endforeach
                </tbody>
                @endif
            </table>
           <center>{{ $questions->links() }}</center>
     
</div>
</div>
</div>
<div class="col-md-4">
    <?php $qs = DB::table('questions')->get(); ?>
    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
        <h3 style="margin-top: 0px; text-align: center;">Add Question to Group</h3>
        <form method="POST" id="detailForm">
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon">Q</span>
            <select id="qgDetail" name="qgDetail" class="form-control" onchange="getVal();">
                <option value="">-- SELECT ONE --</option>
                     @foreach($qs as $qs1)
                       {{$qsample = QuestionGroup::where('qid',$qs1->id)->count();}}
                       @if($qsample==0)

                          <option value="{{ $qs1->id }}">{{ $qs1->question }}</option>
                        @endif
                    @endforeach
                       
            </select>
        </div>
        <input type="hidden" id="qval" name="qval" >
        <input type="hidden" id="qname" name="qname" >
        <input type="hidden" id="qValId" name="qValId" value="{{ $qgId}}">
    </form>
        <button onclick="submitFormDetail()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="submitBtnDetail" name="submitBtnDetail" disabled>
            Add Question
        </button>
        
        <label style="margin-left: 30px; color: indianred;" id="errorDetail"></label>
    </div>
</div>
@stop
<script>
    function getVal()
    {

        var e = document.getElementById("qgDetail");
        var strUser = e.options[e.selectedIndex].value;
        $('#qval').val(strUser);


        var type = ($('#qgDetail').find("option:selected").text());
        $('#qname').val(type);

        if($('#qname').val() == '-- SELECT ONE --')
        {
            document.getElementById('submitBtnDetail').disabled = true;

        }
        else
            document.getElementById('submitBtnDetail').disabled = false;
    }
    
</script>
