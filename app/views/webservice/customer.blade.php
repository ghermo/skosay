@extends('webservice.layouts.default')

@section('header')
<!-- Javascript section -->
<script>
$(document).ready(function(){
    document.getElementById('block').style.display = 'none';
});
/* show next form */
function nextForm($num){
    document.getElementById('form'+$num).style.display = 'block';
    document.getElementById('form'+($num-1)).style.display = 'none';
}

/* show previous form */
function prevForm($num){
    document.getElementById('form'+$num).style.display = 'block';
    document.getElementById('form'+($num+1)).style.display = 'none';
}

/* special case: skip departments; show User Accounts Form */
function nextForm2(){
    document.getElementById('form4').style.display = 'block';
    document.getElementById('form2').style.display = 'none';
}

/* special case: skip departments; show Questions Form */
function prevForm2(){
    document.getElementById('form2').style.display = 'block';
    document.getElementById('form4').style.display = 'none';
}

/* add user's input to options */
function insertOption($str, $id, $to){
    var x = document.getElementById($to);
    var option = document.createElement("option");
    option.text = $str;
    option.value = $id;
    option.id = $id;
    x.add(option);
}

/* show necessary inputs for number of questions */
function getval() {
    var v = document.getElementById('qNum').value;
    for(i=1; i<=5; i++){
        if(i<=v) document.getElementById('qDiv'+i).style.display = '';
        else document.getElementById('qDiv'+i).style.display = 'none';
    }
}

/* toggle new dept input text area */
function getvalgroup(){
    var v = document.getElementById('groupid').value;
    if(v=='new') document.getElementById('newgroupinput').style.display = 'block';
    else document.getElementById('newgroupinput').style.display = 'none';
}

/* Add Stores and Group Form */
function submitForm(){
    document.getElementById('buttonSubmit1').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddGroup2',
        data : $('#customerForm1').serialize(),
        success : function(data){
            document.getElementById('buttonSubmit1').disabled = false;

            if(data['success'] == true)
            {
                /* set values for hidden inputs */
                document.getElementById('hiddenstoreid').setAttribute("value", data['storeid']);
                document.getElementById('hiddenstoreid2').setAttribute("value", data['storeid']);
                document.getElementById('hiddengroupid2').setAttribute("value", data['groupid']);
                document.getElementById('hiddengroupid').setAttribute("value", data['groupid']);
                /* alert user of successful form submission */
                alert('Store added!');
                $("#title1").html("Edit Store");

                document.getElementById('buttonSubmit1').setAttribute('onclick', 'editForm()');
                /* show buttons */
                document.getElementById('buttonSkip1').style.display = 'block';
                document.getElementById('buttonEdit1').style.display = 'block';
                document.getElementById('buttonSubmit1').style.display = 'none';
                document.getElementById('StoreID').readOnly = true;
                /* store values */
                insertOption(data['name'], data['groupid'], 'groupid');
                insertOption(data['name'], data['groupid'], 'groupid2');
                document.getElementById('buttonSkip1').setAttribute("onclick", "nextForm(2); showEdit(2); populateEditStore('"+data['name']+"', '"+data['storename']+"', '"+data['address']+"', '"+data['latitude']+"', '"+data['longitude']+"', '"+data['hasDept']+"');");

                nextForm(2);
            }
            else{
                /* show error message */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg1group').empty().append(errmsgs["GroupName"]);
                $('#errorMsg1storeName').empty().append(errmsgs["StoreName"]);
                $('#errorMsg1storeAdd').empty().append(errmsgs["StoreAddress"]);
                $('#errorMsg1storeID').empty().append(errmsgs["StoreID"]);
                $('#errorMsg1latitude').empty().append(errmsgs["latitude"]);
                $('#errorMsg1longitude').empty().append(errmsgs["longitude"]);

                setTimeout(function(){ $('#errorMsg1group').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1storeName').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1storeAdd').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1storeID').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1latitude').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1longitude').empty(); },4000);
            }
        }
    });// ajax

}

/* Edit Store and Group */
function editForm(){
    document.getElementById('buttonSkip1').disabled = true;
    document.getElementById('buttonEdit1').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webEditGroup2',
        data : $('#customerForm1').serialize(),
        success : function(data){
            document.getElementById('buttonSkip1').disabled = false;
            document.getElementById('buttonEdit1').disabled = false;

            if(data['success'] == true)
            {
                /* alert user of successful edit */
                alert('Store edited!');
                /* edit option */
                var x = document.getElementById('groupid');
                for(var i=0; i<x.length; i++){
                    if(x.options[i].value == data['groupid']){
                        x.options[i].text = data['name'];
                    }
                }
                var x = document.getElementById('groupid2');
                for(var i=0; i<x.length; i++){
                    if(x.options[i].value == data['groupid']){
                        x.options[i].text = data['name'];
                    }
                }
                if(data['hasDept']=='no'){
                    skipDepartment = false;
                    var x = document.getElementById('editDepartment');
                    for(var i=1; i<x.length; i++){
                        x.options[i].remove();
                    }
                }
                document.getElementById('buttonSkip1').setAttribute("onclick", "nextForm(2); showEdit(2); populateEditStore('"+data['name']+"', '"+data['storename']+"', '"+data['address']+"', '"+data['latitude']+"', '"+data['longitude']+"', '"+data['hasDept']+"');");
                
                nextForm(2);
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg1group').empty().append(errmsgs["GroupName"]);
                $('#errorMsg1storeName').empty().append(errmsgs["StoreName"]);
                $('#errorMsg1storeAdd').empty().append(errmsgs["StoreAddress"]);
                $('#errorMsg1storeID').empty().append(errmsgs["StoreID"]);
                $('#errorMsg1latitude').empty().append(errmsgs["latitude"]);
                $('#errorMsg1longitude').empty().append(errmsgs["longitude"]);

                setTimeout(function(){ $('#errorMsg1group').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1storeName').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1storeAdd').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1storeID').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1latitude').empty(); },4000);
                setTimeout(function(){ $('#errorMsg1longitude').empty(); },4000);
            }
        }
    });
}

var skipQuestion = false;
function submitForm2(){
    document.getElementById('buttonSubmit2').disabled = true;
    document.getElementById('buttonAdd2').disabled = true;
    document.getElementById('buttonBack2').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddQuestion2',
        data : $('#customerForm2').serialize(),
        success : function(data){
            document.getElementById('buttonSubmit2').disabled = false;
            document.getElementById('buttonAdd2').disabled = false;
            document.getElementById('buttonBack2').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm2").reset();
                getval();
                /* alert successful form submit */
                alert('Questions added!');
                /* reset edit form */
                resetEdit(2);
                skipQuestion = true;
                /* show buttons */
                document.getElementById('buttonAdd2').style.display = 'block';
                document.getElementById('buttonBack2').style.display = 'block';
                /* store values for future edit */
                // delete this
                addEditQuestion(data['id'], data['name'], data['qnum'], data['id1'], data['id2'], data['id3'], data['id4'], data['id5'], data['q1'], data['q2'], data['q3'], data['q4'], data['q5']);
                /* store values */
                insertOption(data['name'], data['id'], 'questiongroupid');
                return true;
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg2questiongroupname').empty().append(errmsgs["questiongroupname"]);
                $('#errorMsg2question1').empty().append(errmsgs["question1"]);
                $('#errorMsg2question2').empty().append(errmsgs["question2"]);
                $('#errorMsg2question3').empty().append(errmsgs["question3"]);
                $('#errorMsg2question4').empty().append(errmsgs["question4"]);
                $('#errorMsg2question5').empty().append(errmsgs["question5"]);

                setTimeout(function(){ $('#errorMsg2questiongroupname').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question1').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question2').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question3').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question4').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question5').empty(); },4000);
            }
            return 'false';
        }
    });
}


function editForm2(){
    document.getElementById('buttonEdit2').disabled = true;
    document.getElementById('buttonCancel2').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webEditQuestion2',
        data : $('#customerForm2').serialize(),
        success : function(data){
            document.getElementById('buttonEdit2').disabled = false;
            document.getElementById('buttonCancel2').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm2").reset();
                getval();

                alert('Questions edited!');
                /* reset edit form */
                resetEdit(2);
                /* for edit */
                var x = document.getElementById('editQuestion');
                for(var i=0; i<x.length; i++){
                    if(x.options[i].id == data['id']){
                        x.options[i].text = data['name'];
                        x.options[i].value = data['id']+"`"+data['name']+"`"+data['qnum']+"`"+data['id1']+"`"+data['id2']+"`"+data['id3']+"`"+data['id4']+"`"+data['id5']+"`"+data['q1']+"`"+data['q2']+"`"+data['q3']+"`"+data['q4']+"`"+data['q5'];
                    }
                }
                /* store values */
                var x = document.getElementById('questiongroupid');
                for(var i=0; i<x.length; i++){
                    if(x.options[i].value == data['id']){
                        x.options[i].text = data['name'];
                    }
                }
                
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg2questiongroupname').empty().append(errmsgs["questiongroupname"]);
                $('#errorMsg2question1').empty().append(errmsgs["question1"]);
                $('#errorMsg2question2').empty().append(errmsgs["question2"]);
                $('#errorMsg2question3').empty().append(errmsgs["question3"]);
                $('#errorMsg2question4').empty().append(errmsgs["question4"]);
                $('#errorMsg2question5').empty().append(errmsgs["question5"]);

                setTimeout(function(){ $('#errorMsg2questiongroupname').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question1').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question2').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question3').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question4').empty(); },4000);
                setTimeout(function(){ $('#errorMsg2question5').empty(); },4000);
            }
            return 'false';
        }
    });
}

var skipDepartment = false;
function submitForm3(){
    document.getElementById('buttonSubmit3').disabled = true;
    document.getElementById('buttonAdd3').disabled = true;
    document.getElementById('buttonBack3').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddDept2',
        data : $('#customerForm3').serialize(),
        success : function(data){
            document.getElementById('buttonSubmit3').disabled = false;
            document.getElementById('buttonAdd3').disabled = false;
            document.getElementById('buttonBack3').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm3").reset();
                getvalgroup();
                document.getElementById('newgroupinput').style.display = 'block';
                /* alert user of successful form submit */
                alert('Department created!');
                /* show edit form */
                document.getElementById('edit3').style.display = 'block';
                addEditDepartment(data['groupid'], data['id'], data['name'], data['qgroupid']);
                /* store values */
                if(data['groupname']!=''){
                    insertOption(data['groupname'], data['groupid'], 'groupid');
                    insertOption(data['groupname'], data['groupid'], 'groupid2');
                }
                skipDepartment = true;
                return true;
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg3dept').empty().append(errmsgs["dept"]);
                $('#errorMsg3newgroup').empty().append(errmsgs["newgroupinput"]);

                setTimeout(function(){ $('#errorMsg3dept').empty(); },4000);
                setTimeout(function(){ $('#errorMsg3newgroup').empty(); },4000);
            }
            
            return false;
        }
    });
}


function editForm3(){
    document.getElementById('buttonCancel3').disabled = true;
    document.getElementById('buttonEdit3').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webEditDept2',
        data : $('#customerForm3').serialize(),
        success : function(data){
            document.getElementById('buttonCancel3').disabled = false;
            document.getElementById('buttonEdit3').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm3").reset();
                getvalgroup();
                document.getElementById('newgroupinput').style.display = 'block';
                /* alert user of successful edit */
                alert('Department edited!');
                /* reset edit and add form */
                $("#title3").html("Add Departments");
                /* for edit */
                var x = document.getElementById('editDepartment');
                for(var i=0; i<x.length; i++){
                    if(x.options[i].id == data['id']){
                        x.options[i].text = data['name'];
                        x.options[i].value = data['groupid']+"`"+data['id']+"`"+data['name']+"`"+data['qgroupid'];
                    }
                }
                /* store values */
                if(data['groupname']!=''){
                    insertOption(data['groupname'], data['groupid'], 'groupid');
                    insertOption(data['groupname'], data['groupid'], 'groupid2');
                }
                resetEdit(3);
                return true;
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg3dept').empty().append(errmsgs["dept"]);
                $('#errorMsg3newgroup').empty().append(errmsgs["newgroupinput"]);

                setTimeout(function(){ $('#errorMsg3dept').empty(); },4000);
                setTimeout(function(){ $('#errorMsg3newgroup').empty(); },4000);
            }
            return false;
        }
    });
}


var skipUser = false;
function submitForm4(){
    document.getElementById('buttonSubmit4').disabled = true;
    document.getElementById('buttonAdd4').disabled = true;
    document.getElementById('buttonBack4').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddUser2',
        data : $('#customerForm4').serialize(),
        success : function(data){
            document.getElementById('buttonSubmit4').disabled = false;
            document.getElementById('buttonAdd4').disabled = false;
            document.getElementById('buttonBack4').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm4").reset();
                /* alert user of successful form submit */
                alert('Account created!');
                /* show buttons */
                document.getElementById('edit4').style.display = 'block';
                /* store for future edit */
                addEditUser(data['id'], data['name'], data['email'], data['mobile'], data['groups']);
                skipUser = true;
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg4username').empty().append(errmsgs["username"]);
                $('#errorMsg4email').empty().append(errmsgs["email"]);
                $('#errorMsg4password').empty().append(errmsgs["password"]);
                $('#errorMsg4confirmpassword').empty().append(errmsgs["confirmpassword"]);
                $('#errorMsg4mobilenum').empty().append(errmsgs["mobilenum"]);
                $('#errorMsg4group').empty().append(errmsgs["groupid2"]);
            }
            setTimeout(function(){ $('#errorMsg4username').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4email').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4password').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4confirmpassword').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4mobilenum').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4group').empty(); },4000);
        }
    });
}


function editForm4(){
    document.getElementById('buttonEdit4').disabled = true;
    document.getElementById('buttonCancel4').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webEditUser2',
        data : $('#customerForm4').serialize(),
        success : function(data){
            document.getElementById('buttonEdit4').disabled = false;
            document.getElementById('buttonCancel4').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm4").reset();
                alert('Account edited!');
                $("#title4").html("Add User Accounts");
                resetEdit(4);

                /* for edit */
                var x = document.getElementById('editUser');
                for(var i=0; i<x.length; i++){
                    if(x.options[i].id == data['id']){
                        x.options[i].text = data['name'];
                        x.options[i].value = data['id']+"`"+data['name']+"`"+data['email']+"`"+data['mobile']+"`"+data['groups'];
                    }
                }
            }
            else{
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg4username').empty().append(errmsgs["username"]);
                $('#errorMsg4email').empty().append(errmsgs["email"]);
                $('#errorMsg4password').empty().append(errmsgs["password"]);
                $('#errorMsg4confirmpassword').empty().append(errmsgs["confirmpassword"]);
                $('#errorMsg4mobilenum').empty().append(errmsgs["mobilenum"]);
                $('#errorMsg4group').empty().append(errmsgs["groupid2"]);
            }
            setTimeout(function(){ $('#errorMsg4username').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4email').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4password').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4confirmpassword').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4mobilenum').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4group').empty(); },4000);
        }
    });
}


function submitNext2(){
    if(skipQuestion){
        // check if all fields are empty
        if(!$('#questiongroupname').val() && !$('#question1').val() && !$('#question2').val() && !$('#question3').val() && !$('#question4').val() && !$('#question5').val()){
           return nextForm(3);
        }
    }

    document.getElementById('buttonSubmit2').disabled = true;
    document.getElementById('buttonAdd2').disabled = true;
    document.getElementById('buttonBack2').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddQuestion2',
        data : $('#customerForm2').serialize(),
        success : function(data){

            document.getElementById('buttonSubmit2').disabled = false;
            document.getElementById('buttonAdd2').disabled = false;
            document.getElementById('buttonBack2').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm2").reset();
                getval();
                alert('Questions added!');
                resetEdit(2);
                /* for edit */
                addEditQuestion(data['id'], data['name'], data['qnum'], data['id1'], data['id2'], data['id3'], data['id4'], data['id5'], data['q1'], data['q2'], data['q3'], data['q4'], data['q5']);
                insertOption(data['name'], data['id'], 'questiongroupid');
                nextForm(3);
                skipQuestion = true;
            }
            else{
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg2questiongroupname').empty().append(errmsgs["questiongroupname"]);
                $('#errorMsg2question1').empty().append(errmsgs["question1"]);
                $('#errorMsg2question2').empty().append(errmsgs["question2"]);
                $('#errorMsg2question3').empty().append(errmsgs["question3"]);
                $('#errorMsg2question4').empty().append(errmsgs["question4"]);
                $('#errorMsg2question5').empty().append(errmsgs["question5"]);
            }
            setTimeout(function(){ $('#errorMsg2questiongroupname').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question1').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question2').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question3').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question4').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question5').empty(); },4000);
        }
    });
}

function submitNext4(){
    if(skipUser){
        // check if all fields are empty
        if(!$('#username').val() && !$('#email').val() && !$('#mobilenum').val() && !$('#groupid2').val()){
            document.getElementById('block').style.display = 'block';
            return window.location.reload();

        }
    }

    document.getElementById('buttonSubmit4').disabled = true;
    document.getElementById('buttonAdd4').disabled = true;
    document.getElementById('buttonBack4').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddUser2',
        data : $('#customerForm4').serialize(),
        success : function(data){
            document.getElementById('buttonSubmit4').disabled = false;
            document.getElementById('buttonAdd4').disabled = false;
            document.getElementById('buttonBack4').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm4").reset();
                document.getElementById('block').style.display = 'block';
                location.reload();
                alert('Account created!');
                /* show buttons */
                document.getElementById('edit4').style.display = 'block';
                skipUser = true;
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg4username').empty().append(errmsgs["username"]);
                $('#errorMsg4email').empty().append(errmsgs["email"]);
                $('#errorMsg4password').empty().append(errmsgs["password"]);
                $('#errorMsg4confirmpassword').empty().append(errmsgs["confirmpassword"]);
                $('#errorMsg4mobilenum').empty().append(errmsgs["mobilenum"]);
                $('#errorMsg4group').empty().append(errmsgs["groupid2"]);
            }
            setTimeout(function(){ $('#errorMsg4username').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4email').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4password').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4confirmpassword').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4mobilenum').empty(); },4000);
            setTimeout(function(){ $('#errorMsg4group').empty(); },4000);
        }
    });
}


function submitNext5(){
    if(skipQuestion){
        // check if all fields are empty
        if(!$('#questiongroupname').val() && !$('#question1').val() && !$('#question2').val() && !$('#question3').val() && !$('#question4').val() && !$('#question5').val()){
            return nextForm2();
        }
    }
    document.getElementById('buttonSubmit2').disabled = true;
    document.getElementById('buttonAdd2').disabled = true;
    document.getElementById('buttonBack2').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddQuestion2',
        data : $('#customerForm2').serialize(),
        success : function(data){

            document.getElementById('buttonSubmit2').disabled = false;
            document.getElementById('buttonAdd2').disabled = false;
            document.getElementById('buttonBack2').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm2").reset();
                getval();
                /* alert user of successful form submission */
                alert('Questions added!');
                /* for edit */
                resetEdit(2);
                addEditQuestion(data['id'], data['name'], data['qnum'], data['id1'], data['id2'], data['id3'], data['id4'], data['id5'], data['q1'], data['q2'], data['q3'], data['q4'], data['q5']);
                insertOption(data['name'], data['id'], 'questiongroupid');
                nextForm2();
                skipQuestion = true;
                return true;
            }
            else{
                /* show error messages */
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg2questiongroupname').empty().append(errmsgs["questiongroupname"]);
                $('#errorMsg2question1').empty().append(errmsgs["question1"]);
                $('#errorMsg2question2').empty().append(errmsgs["question2"]);
                $('#errorMsg2question3').empty().append(errmsgs["question3"]);
                $('#errorMsg2question4').empty().append(errmsgs["question4"]);
                $('#errorMsg2question5').empty().append(errmsgs["question5"]);
            }
            setTimeout(function(){ $('#errorMsg2questiongroupname').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question1').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question2').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question3').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question4').empty(); },4000);
            setTimeout(function(){ $('#errorMsg2question5').empty(); },4000);

            return 'false';
        }
    });
}

function submitNext3(){
    if(skipDepartment){
        // check if all fields are empty
        if(!$('#dept').val()){
            if($('#groupid').val() == 'new' && !$('#newgroupinput').val()){
                return nextForm(4);
            }
        }
    }

    document.getElementById('buttonSubmit3').disabled = true;
    document.getElementById('buttonAdd3').disabled = true;
    document.getElementById('buttonBack3').disabled = true;
    
    $.ajax({
        type : 'POST',
        url : '/webAddDept2',
        data : $('#customerForm3').serialize(),
        success : function(data){
            document.getElementById('buttonSubmit3').disabled = false;
            document.getElementById('buttonAdd3').disabled = false;
            document.getElementById('buttonBack3').disabled = false;

            if(data['success'] == true)
            {
                /* clear form */
                document.getElementById("customerForm3").reset();
                document.getElementById('newgroupinput').style.display = 'block';
                getvalgroup();
                /* alert user of successful form submission */
                alert('Department created!');
                addEditDepartment(data['groupid'], data['id'], data['name'], data['qgroupid']);
                /* show buttons */
                document.getElementById('edit3').style.display = 'block';
                nextForm(4);
                skipDepartment = true;
                return true;
            }
            else{
                var errmsgs = JSON.parse(data['errors']);
                $('#errorMsg3dept').empty().append(errmsgs["dept"]);
                $('#errorMsg3newgroup').empty().append(errmsgs["newgroupinput"]);
            }
            setTimeout(function(){ $('#errorMsg3dept').empty(); },4000);
            setTimeout(function(){ $('#errorMsg3newgroup').empty(); },4000);

            return false;
        }
    });
}

function hasDept(){
    var valueDept = document.getElementById('hasDept').value;
    if(valueDept == 'no'){
        document.getElementById('buttonAdd2').style.display = 'none';
        document.getElementById('buttonSubmit2').setAttribute('onclick', 'showEdit(2); showEdit(4); submitNext5();');
        document.getElementById('buttonBack4').setAttribute('onclick', 'showEdit(2); showEdit(4); prevForm2();');

        //delete all dept options
        /* for edit */
        var x = document.getElementById('editDepartment');
        for(var i=0; i<x.length; i++){
            if(i!=0){
                x.options[i].remove();
            }
        }
        //hide skip
        document.getElementById('edit3').style.display = 'none';
    }else{
        document.getElementById('buttonAdd2').style.display = 'block';
        document.getElementById('buttonSubmit2').setAttribute('onclick', 'showEdit(3); showEdit(4); submitNext2();');
        document.getElementById('buttonBack4').setAttribute('onclick', 'showEdit(3); showEdit(4); prevForm(3);');
    }
}

function showEdit($num){
    if($num == 2 && skipQuestion){
        resetEdit($num);
    }
    else if($num == 3 && skipDepartment){
        resetEdit($num);
    }
    else if($num == 4 && skipUser){
        resetEdit($num);
    }
}

function resetEdit($num){
    document.getElementById('customerForm'+$num).reset();
    document.getElementById('buttonEdit'+$num).style.display = 'none';
    document.getElementById('edit'+$num).style.display = 'block';
    document.getElementById('buttonSubmit'+$num).style.display = 'block';
    document.getElementById('null'+$num).selected = true;
    document.getElementById('buttonCancel'+$num).style.display = 'none';
    document.getElementById('buttonBack'+$num).style.display = 'block';
    document.getElementById('buttonAdd'+$num).style.display = 'block';
    if($num == 2){
        document.getElementById('title2').innerHTML='Add Questions';
        document.getElementById('buttonEditQuestion').disabled=true;
        if(document.getElementById('hasDept').value == 'no'){
            document.getElementById('buttonAdd2').style.display = 'none';
        }
        getval();
    }
    else if($num == 3){
        document.getElementById('buttonEditDepartment').disabled=true;
        document.getElementById('title3').innerHTML='Add Departments';
        document.getElementById('new').disabled = false
        document.getElementById('newgroupinput').style.display = 'block';
    }
    else if($num == 4){
        document.getElementById('buttonEditUser').disabled=true;
        document.getElementById('title4').innerHTML='Add User Accounts';
        if(document.getElementById('hasDept').value == 'no'){
            document.getElementById('buttonAdd4').style.display = 'none';
        }
    }
}


function populateEditStore($groupname, $storename, $storeadd, $latitude, $longitude, $hasDept){
    document.getElementById('GroupName').value = $groupname;
    document.getElementById('StoreName').value = $storename;
    document.getElementById('StoreAddress').value = $storeadd;
    document.getElementById('latitude').value = $latitude;
    document.getElementById('longitude').value = $longitude;
    var x = document.getElementById('hasDept');
    for(var i=0; i<x.length; i++){
        if(x.options[i].value == $hasDept){
            x.options[i].selected = true;
        }
    }
}

function addEditQuestion($id, $qgroupname, $qnum, $qidone, $qidtwo, $qidthree, $qidfour, $qidfive, $qone, $qtwo, $qthree, $qfour, $qfive){
    var x = document.getElementById('editQuestion');
    var option = document.createElement("option");
    option.text = $qgroupname;
    option.id = $id;
    option.value = $id+"`"+$qgroupname+"`"+$qnum+"`"+$qidone+"`"+$qidtwo+"`"+$qidthree+"`"+$qidfour+"`"+$qidfive+"`"+$qone+"`"+$qtwo+"`"+$qthree+"`"+$qfour+"`"+$qfive;
    x.add(option);
}

function populateEditQuestion(){
    var splitArray = document.getElementById('editQuestion').value.split('`');

    $("#title2").html("Edit Question");

    // populate data to form
    document.getElementById('hiddenqgroupid').value = splitArray[0];
    document.getElementById('questiongroupname').value = splitArray[1];
    document.getElementById('qNum').value = splitArray[2];
    // show input forms
    getval();
    document.getElementById('hiddenqid1').value = splitArray[3];
    document.getElementById('hiddenqid2').value = splitArray[4];
    document.getElementById('hiddenqid3').value = splitArray[5];
    document.getElementById('hiddenqid4').value = splitArray[6];
    document.getElementById('hiddenqid5').value = splitArray[7];
    document.getElementById('question1').value = splitArray[8];
    document.getElementById('question2').value = splitArray[9];
    document.getElementById('question3').value = splitArray[10];
    document.getElementById('question4').value = splitArray[11];
    document.getElementById('question5').value = splitArray[12];

    // hide edit form
    document.getElementById('buttonEdit2').style.display = 'block';
    document.getElementById('edit2').style.display = 'none';
    document.getElementById('buttonSubmit2').style.display = 'none';
    document.getElementById('buttonAdd2').style.display = 'none';
    document.getElementById('buttonBack2').style.display = 'none';
    document.getElementById('buttonCancel2').style.display = 'block';
}

function addEditDepartment($groupid, $deptid, $deptname, $qgroupid){
    var x = document.getElementById('editDepartment');
    var option = document.createElement("option");
    option.text = $deptname;
    option.id = $deptid;
    option.value = $groupid+"`"+$deptid+"`"+$deptname+"`"+$qgroupid;
    x.add(option);
}

function populateEditDepartment(){
    var splitArray = document.getElementById('editDepartment').value.split('`');

    $("#title3").html("Edit Department");

    // populate forms
    var x = document.getElementById('groupid');
    for(var i=0; i<x.length; i++){
        if(x.options[i].value == splitArray[0]){
            x.options[i].selected = true;
        }
    }
    document.getElementById('newgroupinput').style.display = 'none';
    document.getElementById('hiddendeptid').value = splitArray[1];
    document.getElementById('dept').value = splitArray[2];
    var y = document.getElementById('questiongroupid');
    for(var i=0; i<y.length; i++){
        if(y.options[i].value == splitArray[3]){
            y.options[i].selected = true;
        }
    }


    // hide edit form
    document.getElementById('edit3').style.display = 'none';
    document.getElementById('buttonEdit3').style.display = 'block';
    document.getElementById('buttonSubmit3').style.display = 'none';
    document.getElementById('buttonAdd3').style.display = 'none';
    document.getElementById('buttonBack3').style.display = 'none';
    document.getElementById('buttonCancel3').style.display = 'block';
}

function addEditUser($id, $name, $email, $mobilenum, $groups){
    var x = document.getElementById('editUser');
    var option = document.createElement("option");
    option.text = $name;
    option.id = $id;
    option.value = $id+"`"+$name+"`"+$email+"`"+$mobilenum;
    for(var i=0; i<$groups.length; i++){
        option.value += "`"+$groups[i];
    }
    x.add(option);
}

function populateEditUser(){
    var splitArray = document.getElementById('editUser').value.split('`');

    $("#title4").html("Edit User Accounts");

    document.getElementById('hiddenuserid').value = splitArray[0];
    document.getElementById('username').value = splitArray[1];
    document.getElementById('email').value = splitArray[2];
    document.getElementById('mobilenum').value = splitArray[3];

    var x = document.getElementById('groupid2');

    for(var i=0, j=4; i<x.length; i++){
        if(x.options[i].value == splitArray[j]){
            x.options[i].selected = true;
            j++;
        }
    }

    // hide edit form
    document.getElementById('edit4').style.display = 'none';
    document.getElementById('buttonEdit4').style.display = 'block';
    document.getElementById('buttonSubmit4').style.display = 'none';
    document.getElementById('buttonAdd4').style.display = 'none';
    document.getElementById('buttonBack4').style.display = 'none';
    document.getElementById('buttonCancel4').style.display = 'block';
}

</script>
<style type="text/css">
    .center {
        margin-left: auto;
        margin-right: auto;
        width: 70%;
    }

    .input-group-addon {
        min-width: 155px;
        text-align: left;
    }

    #block{
        width: 100%;
        height: 100%;
        position: fixed;
        z-index: 100000;
        top: 0px;
        margin-left: -10px;
        display: none;
    }
</style>
@stop

@section('content')
<div id="block"></div>
<!-- initial form -->
<div class="container center" id="form1" style="display: block; margin-top: 60px; margin-bottom: 20px;">
    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px;">
        <form id="customerForm1" onkeypress="return event.keyCode != 13;">

            <h3 id="title1" style="margin-top: 0px; text-align: center;">Add Store</h3>

            <input type="hidden" id="hiddengroupid" name="hiddengroupid">

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Group Name</span>
                <input type="text" id="GroupName" name="GroupName" class="form-control" placeholder="Enter group name here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg1group"></label></b>

            <br>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px;padding-left: 18px;">Store Name</span>
                <input type="text" id="StoreName" name="StoreName" class="form-control" placeholder="Enter store name here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg1storeName"></label></b>
            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Store Address</span>
                <input type="text" id="StoreAddress" name="StoreAddress" class="form-control" placeholder="Enter store address here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg1storeAdd"></label></b>
            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Store ID</span>
                <input type="text" id="StoreID" name="StoreID" class="form-control" placeholder="Enter store id here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg1storeID"></label></b>

            <br>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Latitude</span>
                <input type="text" id="latitude" name="latitude" class="form-control" placeholder="Enter store's latitude here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg1latitude"></label></b>
            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Longitude</span>
                <input type="text" id="longitude" name="longitude" class="form-control" placeholder="Enter store's longitude here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg1longitude"></label></b>

            <br>

            <div class="input-group" style="margin-top: 10px auto;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">With Department?</span>
                <select id="hasDept" name="hasDept" class="form-control" style="width: auto;">
                    <option value="yes">YES</option>
                    <option value="no" selected>NO</option>
                </select>
            </div>
        </form>
        <button id="buttonSubmit1" onclick="submitForm(); hasDept();" type="submit" class="btn btn-primary" style="float: right;  margin-left: 10px; margin-top: 10px;">
            Next
        </button>
        <button id="buttonEdit1" onclick="editForm(); hasDept(); showEdit(2);" type="submit" class="btn btn-primary" style="display: none; float: right;  margin-left: 10px; margin-top: 10px;">
            Save
        </button>
        <button id="buttonSkip1" onclick="nextForm(2); showEdit(2);" type="submit" class="btn btn-primary" style="display: none; float: right; margin-top: 10px;">
            Cancel
        </button>

        <br><br>
    </div>
</div>

<!-- second form -->
<div class="container center" id="form2" style="display: none; margin-top: 60px; margin-bottom: 20px;">
    <div id="edit2" style="display: none; padding: 20px; background-color: #BDC3C7; border-radius: 10px; margin-bottom: 20px;">
        <h3 style="margin-top: 0px; text-align: center;">Edit Question</h3>
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon" style="padding-right: 20px; padding-left: 10px;">Question Group</span>
            <select id="editQuestion" name="editQuestion" class="form-control" onchange="document.getElementById('buttonEditQuestion').disabled=false;">
                <option id="null2" selected disabled>-- choose question to edit --</option> 
            </select>
        </div>
        <button id="buttonEditQuestion" onclick="populateEditQuestion();  document.getElementById('buttonEditQuestion').disabled=true;" type="submit" class="btn btn-primary" style="display: block; margin-left: 10px;float: right; margin-top: 10px;" disabled>
            Edit
        </button>
        <button onclick="document.getElementById('edit2').style.display='none';" type="submit" class="btn btn-primary" style="display: block; float: right; margin-top: 10px; ">
            Cancel
        </button>

        <br><br>
    </div>

    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px;">
        <form method="POST" id="customerForm2" onkeypress="return event.keyCode != 13;">

            <h3 id="title2" style="margin-top: 0px; text-align: center;">Add Questions</h3>

            <input type="hidden" id="hiddenstoreid" name="hiddenstoreid">

            <input type="hidden" id="hiddenqgroupid" name="hiddenqgroupid">

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Question Group</span>
                <input type="text" id="questiongroupname" name="questiongroupname" class="form-control" placeholder="Enter question group name here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg2questiongroupname"></label></b>

            <br>
            
            <!-- select number of questions to add -->
            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px;
            padding-left: 10px;">Number</span>
                <select id="qNum" name="qNum" class="form-control" onchange="getval();" style="width: auto;">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>

            <input type="hidden" id="hiddenqid1" name="hiddenqid1">
            <div class="input-group" style="margin-top: 10px; " id="qDiv1" name="qDiv1">
                <span class="input-group-addon">Question 1</span>
                <input type="text" id="question1" name="question1" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg2question1"></label></b>
            
            <input type="hidden" id="hiddenqid2" name="hiddenqid2">
            <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv2" name="qDiv2">
                <span class="input-group-addon">Question 2</span>
                <input type="text" id="question2" name="question2" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg2question2"></label></b>
            
            <input type="hidden" id="hiddenqid3" name="hiddenqid3">
            <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv3" name="qDiv3">
                <span class="input-group-addon">Question 3</span>
                <input type="text" id="question3" name="question3" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg2question3"></label></b>
            
            <input type="hidden" id="hiddenqid4" name="hiddenqid4">
            <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv4" name="qDiv4">
                <span class="input-group-addon">Question 4</span>
                <input type="text" id="question4" name="question4" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg2question4"></label></b>
            
            <input type="hidden" id="hiddenqid5" name="hiddenqid5">
            <div class="input-group" style="margin-top: 10px; display: none;" id="qDiv5" name="qDiv5">
                <span class="input-group-addon">Question 5</span>
                <input type="text" id="question5" name="question5" class="form-control" placeholder="Enter Question" onkeyup="getval()" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg2question5"></label></b>

        </form>
        <button id="buttonSubmit2" onclick="showEdit(3); showEdit(4); submitNext2();" type="button" class="btn btn-primary" style="float: right; margin-left: 10px;  margin-top: 10px;">
            Next
        </button>
        <button id="buttonEdit2" onclick="editForm2()" type="submit" class="btn btn-primary" style="display: none; float: right;  margin-left: 10px; margin-top: 10px;">
            Save
        </button>
        <button id="buttonCancel2" onclick="resetEdit(2);" type="submit" class="btn btn-primary" style="display: none; float: right; margin-top: 10px; ">
            Cancel
        </button>
        <button id="buttonAdd2" type="button" onclick="submitForm2();" class="removed btn btn-primary" style="float: right; margin-left: 10px; margin-top: 10px;">
            Add
        </button>
        <button id="buttonBack2" onclick="prevForm(1);" type="button" class="btn btn-primary" style="float: right; margin-left: 10px; margin-top: 10px;">
            Back
        </button>
        
        <br><br>
    </div>
</div>


<!-- third form -->
<div class="container center" id="form3" style="display: none; margin-top: 60px; margin-bottom: 20px;">
    <div id="edit3" style="display: NOne; padding: 20px; background-color: #BDC3C7; border-radius: 10px; margin-bottom: 20px;">
        <h3 style="margin-top: 0px; text-align: center;">Edit Department</h3>
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon" style="padding-right: 20px; padding-left: 10px;">Department Name</span>
            <select id="editDepartment" name="editDepartment" class="form-control" onchange="document.getElementById('buttonEditDepartment').disabled=false;">
                <option id="null3" selected disabled>-- choose department to edit --</option> 
            </select>
        </div>
        <button id="buttonEditDepartment" onclick="populateEditDepartment(); document.getElementById('buttonEditDepartment').disabled=true;" type="submit" class="btn btn-primary" style="display: block; margin-left: 10px;float: right; margin-top: 10px;" disabled>
            Edit
        </button>
        <button onclick="document.getElementById('edit3').style.display='none';" type="submit" class="btn btn-primary" style="display: block; float: right; margin-top: 10px; ">
            Cancel
        </button>

        <br><br>
    </div>

    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px;">
        <form method="POST" id="customerForm3" onkeypress="return event.keyCode != 13;">

            <h3 id="title3" style="margin-top: 0px; text-align: center;">Add Departments</h3>

            <input type="hidden" id="hiddenstoreid2" name="hiddenstoreid2">

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon">Group Name</span>
                <select id="groupid" name="groupid" class="form-control" onchange="getvalgroup();">
                    <option id="new" value="new">-- create new group or choose from below --</option>
                </select>
            </div>

            <div class="input-group" style="margin-top: 10px; width: 100%;" id="showgroup">
                <input type="text" id="newgroupinput" name="newgroupinput" class="form-control" placeholder="Enter group name" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg3newgroup"></label></b>

            <br>

            <input type="hidden" id="hiddendeptid" name="hiddendeptid">
            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon">Department Name</span>
                <input type="text" id="dept" name="dept" class="form-control" placeholder="Enter department name" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg3dept"></label></b>

            <br>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Question Group</span>
                <select id="questiongroupid" name="questiongroupid" class="form-control">
                </select>
            </div>
         

        </form>
        <button id="buttonSubmit3" onclick="submitNext3(); showEdit(4); " type="button" class="btn btn-primary" style="float: right; margin-left: 10px;  margin-top: 10px;">
            Next
        </button>
        <button id="buttonEdit3" onclick="editForm3()" type="submit" class="btn btn-primary" style="display: none; float: right;  margin-left: 10px; margin-top: 10px;">
            Save
        </button>
        <button id="buttonCancel3" onclick="resetEdit(3);" type="submit" class="btn btn-primary" style="display: none; float: right; margin-top: 10px; ">
            Cancel
        </button>
        <button id="buttonAdd3" type="button" onclick="submitForm3();" class="btn btn-primary" style="float: right; margin-left: 10px; margin-top: 10px;">
            Add
        </button>
        <button id="buttonBack3" onclick="showEdit(2); prevForm(2);" type="button" class="btn btn-primary" style="float: right; margin-left: 10px; margin-top: 10px;">
            Back
        </button>
        <br><br>
    </div>
</div>

<!-- fourth form -->
<div class="container center" id="form4" style="display: none; margin-top: 60px; margin-bottom: 20px;">
    <div id="edit4" style="display: none; padding: 20px; background-color: #BDC3C7; border-radius: 10px; margin-bottom: 20px;">
        <h3 style="margin-top: 0px; text-align: center;">Edit User Account</h3>
        <div class="input-group" style="margin-top: 10px;">
            <span class="input-group-addon" style="padding-right: 20px; padding-left: 10px;">User</span>
            <select id="editUser" name="editUser" class="form-control" onchange="document.getElementById('buttonEditUser').disabled=false;">
                <option id="null4" selected disabled>-- choose user to edit --</option> 
            </select>
        </div>
        <button id="buttonEditUser" onclick="populateEditUser(); document.getElementById('buttonEditUser').disabled=true;" type="submit" class="btn btn-primary" style="display: block; margin-left: 10px;float: right; margin-top: 10px;" disabled>
            Edit
        </button>
        <button onclick="document.getElementById('edit4').style.display='none';" type="submit" class="btn btn-primary" style="display: block; float: right; margin-top: 10px; ">
            Cancel
        </button>

        <br><br>
    </div>


    <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px;">
        <form method="POST" id="customerForm4" onkeypress="return event.keyCode != 13;">

            <h3 id="title4" style="margin-top: 0px; text-align: center;">Add User Accounts</h3>

            <input type="hidden" id="hiddengroupid2" name="hiddengroupid2">
            <input type="hidden" id="hiddenuserid" name="hiddenuserid">

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Name</span>
                <input type="text" id="username" name="username" class="form-control" placeholder="Enter name here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg4username"></label></b>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Email</span>
                <input type="email" id="email" name="email" class="form-control" placeholder="Enter email here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg4email"></label></b>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Password</span>
                <input type="password" id="password" name="password" class="form-control" placeholder="Enter password here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg4password"></label></b>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Confirm Password</span>
                <input type="password" id="confirmpassword" name="confirmpassword" class="form-control" placeholder="Enter confirm password here" maxlength="255">
            </div>
            <b><label style="color: indianred;" id="errorMsg4confirmpassword"></label></b>

            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Mobile Number</span>
                <input type="text" id="mobilenum" name="mobilenum" class="form-control" placeholder="Enter mobile number here" maxlength="11">
            </div>
            <b><label style="color: indianred;" id="errorMsg4mobilenum"></label></b>
            
            <div class="input-group" style="margin-top: 10px;">
                <span class="input-group-addon" style="padding-right: 20px; padding-left: 18px;">Group/s<br> to manage</span>
                <select id="groupid2" name="groupid2[]" class="form-control" multiple>
                </select>
            </div>
            <p style="font-size: small; color: gray">Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.</p>
            <b><label style="color: indianred;" id="errorMsg4group"></label></b>

        </form>
        <button id="buttonSubmit4" onclick="submitNext4();" id="buttonSubmit4" type="button" class="btn btn-primary" style="float: right; margin-left: 10px;  margin-top: 10px;">
            Done
        </button>
        <button id="buttonEdit4" onclick="editForm4()" type="submit" class="btn btn-primary" style="display: none; float: right;  margin-left: 10px; margin-top: 10px;">
            Save
        </button>
        <button id="buttonCancel4" onclick="resetEdit(4);" type="submit" class="btn btn-primary" style="display: none; float: right; margin-top: 10px; ">
            Cancel
        </button>
        <button id="buttonAdd4" type="button" onclick="submitForm4();" class="removed btn btn-primary" style="float: right; margin-left: 10px; margin-top: 10px;">
            Add</button>
        <button id="buttonBack4" onclick="showEdit(2); showEdit(3); prevForm(3);" type="button" class="btn btn-primary" style="float: right; margin-left: 10px; margin-top: 10px;">
            Back
        </button>
        <br><br>
    </div>
</div>
@stop