<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>jQuery Sortable With AJAX &amp; MYSQL</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.1.custom.min.js"></script>
<link rel='stylesheet' href='css/styles.css' type='text/css' media='all' />
<script type="text/javascript">
  // When the document is ready set up our sortable with it's inherant function(s)
  $(document).ready(function() {
    $("#test-list").sortable({
      handle : '.handle',
      update : function () {
		  var order = $('#test-list').sortable('serialize');
  		$("#info").load("/sample?"+order);
      }
    });
});
</script>
</head>

<body>
<pre>
<div id="info">Waiting for update</div>
</pre>
 <?php $q = DB::table('questions')->paginate(5); ?>

<ul id="test-list">
  <li>ID | Order ID | Question</li>
  @foreach($q as $question )
  <li id="listItem_{{ $question->id }}"><img src="images/arrow.png" alt="move" width="16" height="16" class="handle" /><strong>{{$question->id}} , {{$question->orderId}} , {{$question->question}}</strong></li>
  </strong></li>
  <input type="hidden" id="qid" name="qid" value="{{ $question->id }}">
@endforeach
</ul>


<div> <center>{{ $q->links() }}</center></div>
</body>
</html>