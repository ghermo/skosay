@extends('webservice.layouts.default')

@section('header')

@stop

@section('content')
<div class="bs-example" style="margin-top: 60px; margin-right: 10px;">
</div>
<div class="tab-content">
    <div id="sectionA" class="tab-pane fade in active" style="margin-top: 15px;">
        <center>

            <?php $messages = DB::table('messages')->where('userid', $id)->where('msgType', 'feedBack')->orderBy('created_at', 'DESC')->paginate(5); ?>
            <h1 style="font-size: 20px; font-weight: bold">{{ DB::table('users')->where('id', $id)->pluck('email') }}</h1>           
            <table class="table" style="overflow: hidden;" >
                <tr>
                    <th style="width: 10%";>Issue ID</th>
                    <th style="width:20%";>Sent to</th>                    
                    <th style="width: 20%";>Answers</th>
                    <th style="width: 20%";>Message</th>
                    <th style="width: 15%";>Type</th>
                    <th style="width: 15%";>Latest Message</th>
                </tr>
                <tbody id="mainTable">
                <?php $x = 0;?>
                @foreach($messages as $message)
                <?php 
                    $issueid = $message->issueid;
                    $storeId = DB::table('issues')->where('id', $issueid)->pluck('storeid');
                    $storeName = DB::table('stores')->where('id', $storeId)->pluck('storename');
                    $qGroupId = DB::table('stores')->where('id', $storeId)->pluck('qGroupId');
                    $fullmsg = $message->message; 
                    $created = $message->created_at;
                    $msgtype = DB::table('messages')->where('userid', $id)->pluck('msgType');
                    $msg = explode(';', $fullmsg);

                    $questions = DB::table('question-groups')->where('question-groups.qgroupid', $qGroupId)->join('questions', 'questions.id', '=', 'question-groups.qid')->get();
                    
                ?>
                <tr>
                    <td>
                        {{ $issueid }}
                    </td>
                    <td>
                        {{ $storeName }}

                    </td>
                    <td>
                       @foreach($questions as $question)
                            {{ $question->question }}<br><b>{{ $msg[$question->orderId-1] }}</b><br><br>
                           
                       @endforeach
                       
                    </td>
                    <td>
                         <?php $sender1 = DB::table('users')->where('id', $message->userid)->pluck('name')?>
                         {{ max($msg) }}<br><font size=1> - {{ $sender1 }}</font><br><br>
                       <?php
                            $messages2 =  DB::table('messages')->where('msgType', 'reply')->where('issueid', $message->issueid)->get();

                       ?>
                       @foreach($messages2 as $message2)
                            <?php $sender = DB::table('users')->where('id', $message2->userid)->pluck('name')?>
                            <i>{{ $message2->message }}</i><br><font size=1> - {{ $sender }} </font><br><br>
                       @endforeach

                    </td>
                    <td>
                        {{ $msgtype }}
                    </td>
                    <td>
                        <?php $latest = DB::table('messages')->where('issueid', $message->issueid)->orderBy('updated_at', 'DESC')->get();?>
                    {{ $latest[0]->updated_at }}
                    </td>
                </tr>

                
                @endforeach
                </tbody>
            </table>
            <center>{{ $messages->links(); }}</center> 
        </center>
</div> 
@stop

