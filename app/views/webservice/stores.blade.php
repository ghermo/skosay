@extends('webservice.layouts.default')

@section('header')
    <script>
       

   
    

        function deleteStore(data)
        {
            var r = confirm("Are you sure you want to delete this store?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteStore',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                },
                error : function(){

                }
            });
            }
            else
            {
                
            }
        }

        function initEdit(data)
        {
            $('#editStorename_'+data).show();
            $('#editAddress_'+data).show();
            $('#pstorename_'+data).hide();
            $('#paddress_'+data).hide();

            $('#editButtons_'+data).show();
            $('#actionButtons_'+data).hide();
        }

        function submitEdit(data)
        {
            $('#hiddenStorename_'+data).val($('#storenameEdit_'+data).val());
            $('#hiddenAddress_'+data).val($('#addressEdit_'+data).val());
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditStore',
                data : $('#editForm_'+data).serialize(),
                success : function(store){
                    if(store['status'] == 'FAILED')
                        alert(store['msg']);
                    else if(store['status'] == 'SUCCESS')
                    {
                        $('#editStorename_'+data).hide();
                        $('#editAddress_'+data).hide();
                        $('#pstorename_'+data).show();
                        $('#paddress_'+data).show();

                        $('#editButtons_'+data).hide();
                        $('#actionButtons_'+data).show();

                        $('#pstorename_'+data).empty().append($('#hiddenStorename_'+data).val());
                        $('#paddress_'+data).empty().append($('#hiddenAddress_'+data).val());
                    }
                }
            })
        }

        function cancelEdit(data)
        {
            $('#editStorename_'+data).hide();
            $('#editAddress_'+data).hide();
            $('#pstorename_'+data).show();
            $('#paddress_'+data).show();

            $('#storenameEdit_'+data).val($('#sname_'+data).val());
            $('#addressEdit_'+data).val($('#sadd_'+data).val());

            $('#editButtons_'+data).hide();
            $('#actionButtons_'+data).show();

            location.reload();
        }   
        function submitForm1()
                {
                    var form = $('#groupForm');
                    document.getElementById('submitBtn').disabled = true;
                    
                    $.ajax({
                        type : 'POST',
                        url : '/webAddGroup',
                        data : form.serialize(),
                        success : function(data){
                            if(data['status'] == 'SUCCESS')
                            {
                                $('#mainTable1').append(data['msg']);
                                $('#errorMsgg').empty().append('Group has been added.');
                                document.getElementById('submitBtn').disabled = false;
                                $("#groupForm")[0].reset();

                            }
                            else
                                $('#errorMsgg').empty().append(data['msg']);
                                document.getElementById('submitBtn').disabled = false;
                        }
                    })
                }
           

        
    </script>
@stop

@section('content')
<?php $group = Group::all();?>
<!-- <div class="bs-example" style="margin-top: 60px; margin-right: 10px;"> -->
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#sectionA" >Stores</a></li>
        <!-- <li><a data-toggle="tab" href="#sectionB" >Store Groups</a></li> -->
  <!--       <li><a data-toggle="tab" href="#sectionC">Question to Stores</a></li>  -->
        </ul> 
    <!-- </div> -->

    <div class="tab-content" style="margin-top: 15px;">
        <div id="sectionA" class="tab-pane fade in active">
            <div class="col-md-8">
            {{ Form::open(array('url'=>'/searchStores', 
            'method' => 'GET', 
            'id' => 'search')) }}
                <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
                <button type="submit" class="btn btn-primary"style="display: none;"></button>
            {{ Form::close() }}
            <table style="padding: .2em;" class="table">
                <tr>
                    <th>ID</th>
                    <th style="width: 25%">Store name</th>
                    <th>Address</th>
                    <th style="width: 15%;">Action</th>
                </tr>
                <tbody id="mainTable">
                    <?php $s = DB::table('stores')->orderBy('id', 'ASC')->paginate(5)?>
                @foreach($s as $store)

                    <tr>
                        <td><p>{{$store->id}}</p></td>
                        <td>
                            <p id="pstorename_{{$store->id}}"> {{ $store->storename }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editStorename_{{$store->id}}">
                                <span class="input-group-addon">N</span>
                                <input type="text" id="storenameEdit_{{$store->id}}" name="storenameEdit_{{$store->id}}" class="form-control" value="{{$store->storename}}" maxlength="255">
                                <input type="hidden" id="sname_{{$store->id}}" name="sname_{{$store->id}}" value="{{$store->storename}}" >
                            </div>
                        </td>
                        <td>
                            <p id="paddress_{{$store->id}}"> {{ $store->address }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editAddress_{{$store->id}}">
                                <span class="input-group-addon">A</span>
                                <input type="text" id="addressEdit_{{$store->id}}" name="addressEdit_{{$store->id}}" class="form-control" value="{{$store->address}}" maxlength="255">
                                <input type="hidden" id="sadd_{{$store->id}}" name="sadd_{{$store->id}}" value="{{$store->address}}">
                            </div>
                        </td>
                        <td>
                            <div id="actionButtons_{{ $store->id }}">
                                <a href="#" style="color : #27AE60" title="Edit Store" onclick="initEdit('{{ $store->id }}')">
                                    <span style="font-size: 24px; margin: -1px; margin-right: 2px;" class="glyphicon glyphicon-edit"></span>
                                </a>
                                <form id="actionForm_{{ $store->id }}"  method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete Store" onclick="deleteStore('actionForm_{{ $store->id }}')">
                                        <span style="font-size: 24px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <input type="hidden" id="storeId" name="storeId" value="{{ $store->id }}" >
                                </form>
                            </div>

                            <div id="editButtons_{{ $store->id }}" style="display: none;">
                                <a href="#" style="color : #3498DB" title="Save Changes" onclick="submitEdit('{{ $store->id }}')">
                                    <span style="font-size: 24px; margin: -1px; margin-right: 2px;" class="glyphicon glyphicon-ok-sign"></span>
                                </a>
                                <a href="#" style="color : #C0392B" title="Cancel" onclick="cancelEdit('{{ $store->id }}')">
                                    <span style="font-size: 24px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                </a>
                                <form method="POST" id="editForm_{{$store->id}}" action="">
                                    <input type="hidden" value="XXX" name="hiddenStorename_{{$store->id}}" id="hiddenStorename_{{$store->id}}">
                                    <input type="hidden" value="XX" name="hiddenAddress_{{$store->id}}" id="hiddenAddress_{{$store->id}}">
                                    <input type="hidden" value="{{$store->id}}" name="storeid" id="storeid">
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table><center>{{$s->links()}}</center>
        </div>
        <div class="col-md-4">
            <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
                <h3 style="margin-top: 0px; text-align: center;">Add a store</h3>
                <form method="POST" id="storeForm" onkeypress="return event.keyCode != 13;">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon" style="padding-right: 20px;
                    padding-left: 18px;">Name</span>
                        <input type="text" id="storename" name="storename" class="form-control" placeholder="Enter store name here" maxlength="255" onkeyup="validateThis()">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">Address</span>
                        <input type="text" id="storeadd" name="storeadd" class="form-control" placeholder="Enter store address here" maxlength="255" onkeyup="validateThis()">
                    </div>
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">Store ID</span>
                        <input type="text" id="storeid" name="storeid" class="form-control" placeholder="Enter store id here" maxlength="255" onkeyup="validateThis()">
                    </div>
                </form>
                <button onclick="submitForm6();" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="btnSubmit" name="btnSubmit">
                    Add store
                </button>
                <button onclick="submitForm6();" type="button" class="btn btn-primary" style="display: none; margin-top: 10px;" id="btnFalse" name="btnFalse" disabled>
                    Add store
                </button>
                <br><label style="margin-top: 20px; width:200; color: indianred;" id="errorMsg"></label>
            </div>
        </div>
    </div>
    <div id="sectionB" class="tab-pane fade">
        @include('webservice.stores.stores-main')
    </div>
    <!-- <div id="sectionC" class="tab-pane fade">
        @include('webservice.stores.question-to-stores')
    </div> -->
</div>
<script>


 function submitForm6()
        {

            document.getElementById('btnSubmit').style.display = 'none';
            document.getElementById('btnFalse').style.display = '';

            $.ajax({
                type : 'POST',
                url : '/webAddStore',
                data : $('#storeForm').serialize(),
                success : function(data){
                    if(data['status'] == 'SUCCESS')
                    {
                        $('#mainTable').append(data['msg']);
                        $('#errorMsg').empty().append('Store has been added.');
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';
                        $("#storeForm")[0].reset();
                        location.reload();
                    }
                    else
                        $('#errorMsg').empty().append(data['msg']);
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';

                        setTimeout(function(){ $('#errorMsg').empty(); },4000);
                }
            })
        }

    



</script>
@stop