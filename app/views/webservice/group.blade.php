@extends('webservice.layouts.default')

<script>
    function submitForm2($data)
    {
       var e = document.getElementById("store");
        var strUser = e.options[e.selectedIndex].value;
        $('#selected').val(strUser);

        
        $('#groupId').val($('#groupid').val());

        document.getElementById('btnSubmit').style.display = 'none';
        document.getElementById('btnFalse').style.display = '';

         $.ajax({
                type : 'POST',
                url : '/webAddDetail',
                data : $('#groupForm').serialize(),
                success : function(data){
                   
                   
                        $('#mainTable3').append(data['msg']);
                        $('#errorMsg3').empty().append('Store has been added.');
                        location.reload();
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';
                        document.getElementById('noData').style.display = 'none';
                }
            })
        
    }

    function deleteDetail(data)
        {
            var r = confirm("Are you sure you want to delete this store?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteDetail',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                    location.reload();
                },
                error : function(){

                }
            });
            }
            else{}
        }
   
</script>
@section('header')


@stop

@section('content')
<?php $stores = Store::join('store-groups', 'stores.id', '=', 'store-groups.storeid')->where('store-groups.groupid', $groupDetails->id); ?>
<div class="tab-content" style="margin-top: 65px;">
    <div id="sectionA" class="tab-pane fade in active">
        <div class="col-md-8" style="background-color: #2C3E50; color: #ECF0F1; text-align: center; border-radius: 10px;">
            <h3>Stores under {{ $groupDetails->groupname }} group</h3>
            <table class="table">
                <thead style="color: #ECF0F1">
                    <tr>
                        <th style="width: 15%">Store Group ID</th>
                        <th style="width: 35%">Stores</th>
                        
                        <th style="width: 11%;">Action</th>
                    </tr>
                </thead>
                <tbody style="color: #BDC3C7;" id="mainTable3">
                    @if($stores->count() == 0)
                        <tr id="noData"><td colspan="3"><center><i>No data available.</i></center></td></tr>
                    @else
                        @foreach(Store::join('store-groups', 'stores.id', '=', 'store-groups.storeid')->select(array('store-groups.id as gid', 'stores.id as id', 'stores.storename as storename'))->where('store-groups.groupid', $groupDetails->id)->get() as $store)
                            <tr>
                                <td ><p id="storeID_{{$store->id}}">{{$store->gid}}</p></td>
                                <td><p id="storeName_{{$store->id}}"> {{ $store->storename }} </p></td>
                                
                                <td>
                                    <center>
                                    <form id="actionForm_{{ $store->id }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete" onclick="deleteDetail('actionForm_{{ $store->id }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>
                                    <input type="hidden" id="storeId" name="storeId" value="{{ $store->id }}">
                                    <input type="hidden" id="storeName" name="storeName" value="{{ $store->storename }}">
                                    <input type="hidden" id="storeAdd" name="storeAdd" value="{{ $store->address }}">
                                    <input type="hidden" value="{{$store->gid}}" name="groupid" id="groupid"/>
                                </form>
                                </center>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
         
         <?php $x = DB::table('stores')->get();
          ?>

           
          
            <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
                <h3 style="margin-top: 0px; text-align: center;">Add store to {{ $groupDetails->groupname }} group</h3>
                <form method="POST" id="groupForm">
                <div class="input-group" style="margin-top: 10px;">
                    <span class="input-group-addon">Stores</span>
                    <select id="store" name="store" class="form-control" onchange="getgroup();">
                    <option value="">-- SELECT STORE --</option>
                     @foreach($x as $xx)
                       {{$sample = StoreGroup::where('storeid',$xx->id)->count();}}
                        @if($sample==0)
                            <?php
                                $str = $xx->storename;
                                if (strlen($str) > 25){
                                    $str = substr($str, 0, 22) . '...';
                                }
                            ?>
                            <option value="{{ $xx->id }}">{{ $str }}</option>
                        @endif
                    @endforeach
                       
                    </select>

                    <?php
                        $type = "option:selected";
                    ?>
                       
                    <input type="hidden" id="selected" name="selected" />
                    <input type="hidden" id="selected_name" name="selected_name" />
                    <input type="hidden" value="xx" name="groupId" id="groupId"/>
                    <input type="hidden" value="{{$gId}}" name="gId" id="gId"/>
                </div>
            </form>
                <button onclick="submitForm2()" id="btnSubmit" name="btnSubmit"type="button" class="btn btn-primary" style=" margin-top: 10px;" disabled>
                    Add store
                </button>
                 <button onclick="submitForm2()" id="btnFalse" name="btnFalse"  type="button" class="btn btn-primary" style="display: none; margin-top: 10px;" disabled>
                    Add store
                </button>
                <br><label style="margin-top: 20px; width:200px; color: indianred;" id="errorMsg3"></label>
            </div>
               
        </div>
    </div>
</div>
<script >
function getgroup()
    {

        
            var type = ($('#store').find("option:selected").text());
            $('#selected_name').val(type);

            if($('#selected_name').val() == '-- SELECT STORE --')
        {
            document.getElementById('btnSubmit').disabled = true;

        }
        else
            document.getElementById('btnSubmit').disabled = false;
       

    }



</script>


@stop
