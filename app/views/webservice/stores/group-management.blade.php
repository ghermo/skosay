<div class="col-md-8" style="background-color: #34495E; border-radius: 15px; padding: 20px;">
    <div class="col-md-4">
        <h4 style="text-align: center; color: #ffffff;">Stores</h4>
        <select multiple size="26" class="form-control" data-live-search="true">
            @foreach(Store::all() as $store)
                <option value="$group->id">{{ $store->storename }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        <h4 style="text-align: center; color: #ffffff;">Groups</h4>
        <select multiple size="26" class="form-control" data-live-search="true">
            @foreach(Group::all() as $group)
            <option value="$group->id">{{ $group->groupname }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
            <button onclick="submitForm()" type="button" class="btn btn-primary" style="width: 100%; margin-top: 40px;">
                <span class="glyphicon glyphicon-plus-sign" style="margin-right: 10px;"></span>
                Add stores to group
            </button>

        <div style="overflow-y: scroll; background-color: #2C3E50; border-radius: 10px; padding: 20px; margin-top: 10px; height: 29.5em; color: white;">
            <ul style="margin: -10px; margin-left: -35px;">
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
                <li><font style="color: #E74C3C">[Store]</font> has been added to <font style="color: #3498DB">[Group]</font></li>
            </ul>
        </div>
    </div>
</div>
 