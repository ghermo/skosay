
    <script>
           
   

        function deleteQuestionQgroup(data)
        {
            var r = confirm("Are you sure you want to delete this question group from store?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteQStoreGroup',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                },
                error : function(){

                }
            });
            }
            else{}
        }

        
    </script>

   
<div class="col-md-8" >
        {{ Form::open(array('url'=>'/searchQGroupsToStores', 
        'method' => 'GET', 
        'id' => 'search')) }}        
            <input type="text" id="search" name="search" class="form-control" placeholder="Search" maxlength="255">
            <button id="srcSubmit" name="srcSubmit" type="submit" class="btn btn-primary"style="display: none;"></button>
        {{ Form::close() }}
            <table style="padding: .2em;" class="table">
                <tr>
                    <th style="width: 25%;">Stores</th>
                    <th style="width: 25%">Question Group</th>
                    <th style="width: 15%;">Action</th>
                </tr>
                <tbody id="mainTableQGroup">
                   
                     <?php 
                     Paginator::setPageName('questiongroupstore'); 
                     $ss = Qgroup::join('stores','qgroups.id','=','stores.qGroupId')->select(array('stores.storename as qstorename','qgroups.groupName as groupname', 'stores.id as id1', 'stores.qGroupId as qgroupid'))->orderBy('stores.id', 'ASC')->paginate(5);  ?>
                   
                @foreach($ss as $question)

                    <tr>
                        <td><p style="">{{$question->qstorename}}</p></td>
                    
                        <td>
                            <p id="question_{{$question->id1}}"> {{ $question->groupname }} </p>
                            <div class="input-group" style="margin-top: 10px; display: none;" id="editQuestion_{{$question->id1}}">
                                <span class="input-group-addon">Q</span>
                                <input type="text" id="questionEdit_{{$question->id1}}" name="questionEdit_{{$question->id1}}" class="form-control" value="{{$question->question1}}">
                            </div>
                            
                        </td>
                        
                        <td>
                            <div id="actionButtonsQgroup_{{ $question->id1 }}">
                                
                                <form id="actionFormQgroup_{{ $question->id1 }}" method="POST" action="" style="display: inline;">
                                    <a href="#" style="color : #C0392B" title="Delete" onclick="deleteQuestionQgroup('actionFormQgroup_{{ $question->id1 }}')">
                                        <span style="font-size: 30px; margin: -1px;" class="glyphicon glyphicon-remove-sign"></span>
                                    </a>

                                    <input type="hidden" id="sid" name="sid" value="{{ $question->id1 }}">
                                    <input type="hidden" id="qgroupid" name="qgroupid" value="{{ $question->qgroupid }}">
                                </form>
                            </div>
                            
                            
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
           <center><?php Paginator::setPageName('questiongroupstore'); ?>{{ $ss->links() }}</center>

           <?php $qstores = DB::table('stores')->get(); ?>

        
     </div>
     <div class="col-md-4">
            <div style="background-color: #BDC3C7; border-radius: 10px; padding: 20px; margin-bottom: 10px;">
                <h3 style="margin-top: 0px; text-align: center;">Add Question Group to Store</h3>
                <form method="POST" id="QGroupForm">
                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon" style="padding-right: 20px;
                    padding-left: 16px;">QG</span>
                            <select id="groupAdd1" name="groupAdd1" class="form-control" onchange="getgroupname()">
                                @foreach(Qgroup::all() as $g)
                                <option value="{{$g->id}}" >{{$g->groupName}}</option>

                                @endforeach
                            </select>
                           
                            
                    </div>

                    <input type="hidden" id="getGroup1" name="getGroup1" value="1"/>
                    <input type="hidden" id="getGroupname" name="getGroupname" value="Default Question Group"/>

                    <div class="input-group" style="margin-top: 10px;">
                        <span class="input-group-addon">Store</span>
                        <select id="storeAdd1" name="storeAdd1" class="form-control" onchange="getstorename()">
                             @foreach($qstores as $qstore)
                               {{$sample1 = Qgroup::where('id',$qstore->qGroupId)->count();}}
                               @if($sample1==0)
                                  <option value="{{ $qstore->id }}">{{ $qstore->storename }}</option>
                                @endif
                            @endforeach 
                        </select>
                    </div>
                       <input type="hidden" id="getStore" name="getStore" value="1"/>
                       <input type="hidden" id="getStorename" name="getStorename" value="WalMart"/>
                    
                </form>
                
                
                <button onclick="submitFormQgroups()" type="button" class="btn btn-primary" style=" margin-top: 10px;" id="btnSubmitQgroup" name="btnSubmitQgroup" >
                    Add 
                </button>
                <button type="button" class="btn btn-primary" style="display: none; margin-top: 10px;" id="btnFalseQgroup" name="btnFalseQgroup" disabled>
                    Add 
                </button>
             

            </div>
        </div>


<script>

 $("#groupAdd1").change(function(){
  // alert($('#groupAdd').val());
        $('#getGroup1').val($('#groupAdd1').val());
    });

 $("#storeAdd1").change(function(){
  // alert($('#groupAdd').val());
        $('#getStore').val($('#storeAdd1').val());
    });

    function submitFormQgroups()
    {
                document.getElementById('btnSubmitQgroup').style.display = 'none';
                document.getElementById('btnFalseQgroup').style.display = '';
                $.ajax({
                    type : 'POST',
                    url : '/webAddQStoreGroup',
                    data : $('#QGroupForm').serialize(),
                    success : function(data){
                       
                        
                        
                            $('#mainTableQGroup').append(data['msg']);
                            $('#errorMsg').empty().append('Store to QGroup has been added.');
                            document.getElementById('btnSubmitQgroup').style.display = '';
                            document.getElementById('btnFalseQgroup').style.display = 'none';
                            location.reload();
                        
                           
                    }
                })
    }

    function getgroupname()
    {

        
            var type = ($('#groupAdd1').find("option:selected").text());
            $('#getGroupname').val(type);


       

    }

    function getstorename()
    {

        
            var type = ($('#storeAdd1').find("option:selected").text());
            $('#getStorename').val(type);


       

    }

</script>
