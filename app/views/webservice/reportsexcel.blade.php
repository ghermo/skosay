<?php $issues = DB::table('issues')->groupBy('userid')->orderBy('created_at', 'DESC')->paginate(DB::table('issues')->count()); ?>
            <table class="table" style="overflow: hidden;" >
                <tr>
                    <th>userid</th>
                    <th>name</th>
                    <th>message id</th>
                    <th>issueid</th>
                    <th>message</th>
                    <th>msgtype</th>
                    <th>photo</th>
                    <th>msgStatus</th>  
                    <th>store_id</th>
                    <th>isDept</th>
                    <th>status</th>
                    <th>fbid</th>
                    <th>email</th>
                    <th>mobile</th>
                    <th>type</th>
                    <th>secret_id</th>
                    <th>secret_answer</th>
                    <th>confirmation</th>
                    <th>confirmation_code</th>
                    <th>forgot</th>
                    <th>forgot_code</th>
                    <th>remember_token</th>
                    <th>district_manager</th>
                    <th>notiflag</th>
                    <th>created_at</th>
                    <th>updated_at</th>

                </tr>
                <tr>
                </tr>
                <tbody id="mainTable">
                @foreach($issues as $issue)
                <tr>
                    
                     <td>
                        <?php $userid = DB::table('messages')->where('issueid', $issue->id)->pluck('userid'); echo $userid; ?>
                    </td>
                    <td>
                        <?php 
                        $blankname = DB::table('users')->where('id', $issue->userid)->pluck('name');
                        if( $blankname == "" ){
                            echo "Guest";
                        }
                        else{
                            $name = DB::table('users')->where('id', $userid)->pluck('name'); echo $name; 
                        }
                        ?>

                    </td>
                    <td>
                        <?php $id = DB::table('messages')->where('issueid', $issue->id)->pluck('id'); echo $id; ?>
                    </td>
                   
                    <td>
                        {{ $issue->id }}
                    </td>

                    <td>
                        <?php $message = DB::table('messages')->where('issueid', $issue->id)->pluck('message'); echo $message; ?>
                    </td>
                    <td>
                        <?php $msgType = DB::table('messages')->where('issueid', $issue->id)->pluck('msgType'); echo $msgType; ?>   
                    </td>
                    <td>
                        <?php $photo = DB::table('messages')->where('issueid', $issue->id)->pluck('photo'); echo $photo; ?>
                    </td>
                    <td>
                        <?php $msgStatus = DB::table('messages')->where('issueid', $issue->id)->pluck('msgStatus'); echo $msgStatus; ?>   

                    </td>
                    <td>
                        {{ $issue->storeid }}
                    </td>
                    <td>
                        {{ $issue->isDept }}
                    </td>
                    <td>
                        {{ $issue->status }}
                    </td>
                    <td>
                        <?php
                        
                            $fbid = DB::table('users')->where('id', $userid)->pluck('fbid'); echo $fbid; 
                        
                        ?>
                    </td>
                    <td>
                        <?php 
                            $email = DB::table('users')->where('id', $userid)->pluck('email'); echo $email; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $mobile = DB::table('users')->where('id', $userid)->pluck('mobile'); echo $mobile; 
                        ?>
                    </td>
                    <td>
                        <?php 
                        $blankname = DB::table('users')->where('id', $issue->userid)->pluck('name');
                        if( $blankname == "" ){
                            echo "Guest";
                        }
                        else{
                            $type = DB::table('users')->where('id', $userid)->pluck('type'); echo $type; 
                        }
                        ?>
                    </td>
                    <td>
                        <?php 
                            $secret_id = DB::table('users')->where('id', $userid)->pluck('secret_id'); echo $secret_id; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $secret_answer = DB::table('users')->where('id', $userid)->pluck('secret_answer'); echo $secret_answer; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $confirmation = DB::table('users')->where('id', $userid)->pluck('confirmation'); echo $confirmation; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $confirmation_code = DB::table('users')->where('id', $userid)->pluck('confirmation_code'); echo $confirmation_code; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $forgot = DB::table('users')->where('id', $userid)->pluck('forgot'); echo $forgot; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $forgot_code = DB::table('users')->where('id', $userid)->pluck('forgot_code'); echo $forgot_code; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $remember_token = DB::table('users')->where('id', $userid)->pluck('remember_token'); echo $remember_token; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $district_manager = DB::table('users')->where('id', $userid)->pluck('district_manager'); echo $district_manager; 
                        ?>
                    </td>
                    <td>
                        <?php 
                            $notiflag = DB::table('users')->where('id', $userid)->pluck('notiflag'); echo $notiflag; 
                        ?>
                    </td>
                    <td>
                        {{ $issue->created_at }}
                    </td>
                    <td>
                        {{ $issue->updated_at }}
                    </td>
                    
                </tr>

                @endforeach

                </tbody>
            </table>

            select * from messages m inner join issues i on i.id = m.issueid 
            inner join users u on u.id = m.userid inner join stores s on 
            s.id = i.storeid;

            /*DB::table('messages')->join('issues', 'issues.id', '=', 
            'messages.issueid')->join('users', 'users.id', '=', 'message.userid')
            ->join('stores', 'stores.id', '=', 'issues.storeid');*/