@extends('webservice.layouts.default')

@section('header')
    <script>
       
        function deleteStore(data)
        {
            var r = confirm("Are you sure you want to delete this store?");
            if (r == true) 
            {
            var form = $('#'+data);
            $.ajax({
                type : 'POST',
                url : '/webDeleteStore',
                data : form.serialize(),
                success : function(data){
//                    form.closest('tr').remove();
                    form.closest('tr').hide('slow');
                },
                error : function(){

                }
            });
            }
            else
            {
                
            }
        }

        function initEdit(data)
        {
            $('#editStorename_'+data).show();
            $('#editAddress_'+data).show();
            $('#pstorename_'+data).hide();
            $('#paddress_'+data).hide();

            $('#editButtons_'+data).show();
            $('#actionButtons_'+data).hide();
        }

        function submitEdit(data)
        {
            $('#hiddenStorename_'+data).val($('#storenameEdit_'+data).val());
            $('#hiddenAddress_'+data).val($('#addressEdit_'+data).val());
//            alert($('#hiddenStorename_'+data).val());
//            alert($('#hiddenAddress_'+data).val());
            $.ajax({
                type : 'POST',
                url : '/webEditStore',
                data : $('#editForm_'+data).serialize(),
                success : function(store){
                    if(store['status'] == 'FAILED')
                        alert(store['msg']);
                    else if(store['status'] == 'SUCCESS')
                    {
                        $('#editStorename_'+data).hide();
                        $('#editAddress_'+data).hide();
                        $('#pstorename_'+data).show();
                        $('#paddress_'+data).show();

                        $('#editButtons_'+data).hide();
                        $('#actionButtons_'+data).show();

                        $('#pstorename_'+data).empty().append($('#hiddenStorename_'+data).val());
                        $('#paddress_'+data).empty().append($('#hiddenAddress_'+data).val());
                    }
                }
            })
        }

        function cancelEdit(data)
        {
            $('#editStorename_'+data).hide();
            $('#editAddress_'+data).hide();
            $('#pstorename_'+data).show();
            $('#paddress_'+data).show();

            $('#storenameEdit_'+data).val($('#sname_'+data).val());
            $('#addressEdit_'+data).val($('#sadd_'+data).val());

            $('#editButtons_'+data).hide();
            $('#actionButtons_'+data).show();

            location.reload();
        }   
        function submitForm1()
                {
                    var form = $('#groupForm');
                    document.getElementById('submitBtn').disabled = true;
                    
                    $.ajax({
                        type : 'POST',
                        url : '/webAddGroup',
                        data : form.serialize(),
                        success : function(data){
                            if(data['status'] == 'SUCCESS')
                            {
                                $('#mainTable1').append(data['msg']);
                                $('#errorMsgg').empty().append('Group has been added.');
                                document.getElementById('submitBtn').disabled = false;
                                $("#groupForm")[0].reset();

                            }
                            else
                                $('#errorMsgg').empty().append(data['msg']);
                                document.getElementById('submitBtn').disabled = false;
                        }
                    })
                }
           

        
    </script>
@stop

@section('content')
<?php $group = Group::all();?>
<!-- <div class="bs-example" style="margin-top: 60px; margin-right: 10px;"> -->
    <ul class="nav nav-tabs">
        <!-- <li class="active"><a data-toggle="tab" href="#sectionA" >Stores</a></li> -->
        <li class = "active"><a data-toggle="tab" href="#sectionB" >Groups</a></li>
  <!--       <li><a data-toggle="tab" href="#sectionC">Question to Stores</a></li>  -->
        </ul>
   <!-- </div> -->

<div class="tab-content" style="margin-top: 15px;">
    <div id="sectionB" class="tab-pane fade in active">
        @include('webservice.stores.stores-main')
    </div>
    </div>
    <!-- <div id="sectionC" class="tab-pane fade">
        @include('webservice.stores.question-to-stores')
    </div> -->
<script>


 function submitForm6()
        {

            document.getElementById('btnSubmit').style.display = 'none';
            document.getElementById('btnFalse').style.display = '';

            $.ajax({
                type : 'POST',
                url : '/webAddStore',
                data : $('#storeForm').serialize(),
                success : function(data){
                    if(data['status'] == 'SUCCESS')
                    {
                        $('#mainTable').append(data['msg']);
                        $('#errorMsg').empty().append('Store has been added.');
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';
                        $("#storeForm")[0].reset();
                    }
                    else
                        $('#errorMsg').empty().append(data['msg']);
                        document.getElementById('btnSubmit').style.display = '';
                        document.getElementById('btnFalse').style.display = 'none';

                        setTimeout(function(){ $('#errorMsg').empty(); },4000);
                }
            })
        }

    



</script>
@stop