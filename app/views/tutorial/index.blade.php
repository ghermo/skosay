@extends('layouts.default')
@section('content')

<?php $page = 4; ?>

<style>
@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,600,300);
@charset "UTF-8";
/* Base Styles */
#cssmenu,
#cssmenu ul,
#cssmenu li,
#cssmenu a {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  font-weight: normal;
  text-decoration: none;
  line-height: 1;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  position: relative;
}
#cssmenu a {
  line-height: 1.3;
}
#cssmenu {
  text-align: left;
}
#cssmenu > ul > li:last-child {
  margin: 0;
}
#cssmenu > ul > li > a {
  font-size: 16px;
  display: block;
  color: #ffffff;
}
#cssmenu > ul > li > a > span {
  padding-left: 20px;
  color:black;
  display: block;
  border-bottom: 1px solid black;
  padding: 6px 10px;
}
#cssmenu > ul > li > a:hover {
  text-decoration: none;
}
#cssmenu > ul > li.active {
  border-bottom: 1px solid black;
}



/* Sub menu */
#cssmenu ul ul {
  padding: 5px 12px;
  display: none;
}
#cssmenu ul ul li {
  padding: 3px 0;
}
#cssmenu ul ul a {
  display: block;
  color: #595959;
  font-size: 13px;
  font-weight: bold;
}
#cssmenu ul ul a:hover {
  color: #79980d;
}

@media screen and (max-width: 395px){
    #cssmenu > ul > li > a {
      text-align: justify;
  font-size: 15px;
  display: block;
  color: #ffffff;
  }
}

</style>

<div class="list" style="text-align: center; margin:0;" >
    <a class="item" href="#" style="padding:8px; background: #DFDFDF;">
        <h2 style="margin:0;">Tutorials</h2>
    </a>
</div>
<div id='cssmenu'>
<ul>
   
   <li class='has-sub'>
    <a href='#'>
      <span>
        <i class="ion-search"> </i>
          How to Send Feedback on Near By Stores 
        
      </span>
    </a>
    <ul>
      <li>
        <a href='#'>
          <span>1. Go to app.skosay.com on your phone or tablet.</span>
        </a>
      </li>
      <li>
        <a href='#'>
          <span>2. Touch the Location Icon</span>
        </a>
      </li>
      <li>
        <a href='#'>
          <span>3. Then Select which store you want to send your feedback</span>
        </a>
      </li>
      <li>
        <a href='#'>
          <span>4. Answer the YES / No Questions</span>
        </a>
      </li>
      <li>
        <a href='#'>
          <span>5. Put your comment/feedback.</span>
        </a>
      </li>
      <li>
        <a href='#'>
          <span>6. if you want to attached an image to your feedback click the camera icon</span>
        </a>
      </li>
      <li>
        <a href='#'>
          <span>7. Then click send if your done.</span>
        </a>
      </li>
    </ul>
   </li>
   <li class='has-sub'><a href='#'><span><i class="ion-search"> </i>How to Send Feedback Stores outside your GPS</span></a>
      <ul>
        <li>
          <a href='#'>
            <span>1. Go to app.skosay.com on your phone or tablet.</span>
          </a>
        </li>
        <li>
          <a href='#'>
            <span>2. Touch the Message Icon</span>
          </a>
        </li>
        <li>
          <a href='#'>
            <span>3. Then on the search bar, Type in the Store you wanted to search</span>
          </a>
        </li>
        <li>
          <a href='#'>
            <span>4. Then Click Enter</span>
          </a>
        </li>
        <li>
          <a href='#'>
            <span>5. List of Result will display on your screen then you may now do your feedback.</span>
          </a>
        </li>
      </ul>
   </li>
   
</ul>
</div>

<script>
  ( function( $ ) {
$( document ).ready(function() {
$('#cssmenu > ul > li > a').click(function() {
  $('#cssmenu li').removeClass('active');
  $(this).closest('li').addClass('active'); 
  var checkElement = $(this).next();
  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
    $(this).closest('li').removeClass('active');
    checkElement.slideUp('normal');
  }
  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
    $('#cssmenu ul ul:visible').slideUp('normal');
    checkElement.slideDown('normal');
  }
  if($(this).closest('li').find('ul').children().length == 0) {
    return true;
  } else {
    return false; 
  }   
});
});
} )( jQuery );

</script>
@stop

@section('footer')
    <?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop