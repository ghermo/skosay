@extends('layouts.default')
<link rel="alternate" type="application/rss+xml" title="Latest snippets from Bootsnipp.com" href="http://bootsnipp.com/feed.rss">
    {{HTML::style('css/bootstrap.min.css');}}
    {{HTML::style('css/font-awesome.min.css');}}
    {{HTML::style('css/bootsnipp.css');}}
    {{HTML::style('css/ladda-themeless.min.css');}}
<style>
.cover{ 
            position:fixed; 
            top:0; 
            left:0; 
            background:rgba(0,0,0,0.6); 
            z-index:5; 
            width:100%; 
            height:100%; 
            display: block !important;
        } 
        .loginScreen { 
            background: white;
            padding: 6px;
            position: absolute;
            left: 50%;
            margin-left: -70px;
            z-index: 10;
            display: block !important;
            border-radius: 5px;

        } 
.hideThis{
    display: none !important;
}
.showThis{
    display: block;
}
</style>
<script>
    function next(num, count, ans)
    {
        var nextNum = num + 1;
        oldPage = "div_"+num;
        nextPage = "div_"+nextNum;
        inputHidden = "input_"+num;
        circleNum = "circle"+num;

        if (num == count) {
            document.getElementById(circleNum).innerHTML = "<span class='ion-ios7-checkmark-empty check-icon'></span>";
            document.getElementById(circleNum).className = "btn btn-default btn-circle";
            document.getElementById(oldPage).className = "hideThis";
            document.getElementById('last').className = "showThis";
            document.getElementById(inputHidden).value = ans;
        }
        else{
        
        document.getElementById(circleNum).innerHTML = "<span class='ion-ios7-checkmark-empty check-icon'></span>";
        document.getElementById(circleNum).className = "btn btn-default btn-circle";
        document.getElementById(oldPage).className = "hideThis";
        document.getElementById(nextPage).className = "showThis";
        document.getElementById(inputHidden).value = ans;
        }
    }
   
    function toggleSlide(data)
    {
        $(data).hide();
    }
    function setInput(data)
    {
        $(data['id']).val(data['name']);
    }

    function resize(){
        var inputfield2 = document.createElement('input');
        inputfield2.type = 'hidden';
        inputfield2.name = 'origFileName';
        inputfield2.id = 'origFileName';
        inputfield2.value = '';
        if(document.getElementById('file').value !== ''){
            var filestream = document.getElementById('blah').src;
            var img = document.createElement('img');
            img.src = filestream;
            var c = document.createElement('canvas');
            var MAX_HEIGHT = 600;
            var MAX_WIDTH = 800;
            var width = img.width;
            var height = img.height;
            if (width > height) { if (width > MAX_WIDTH) { height *= MAX_WIDTH / width; width = MAX_WIDTH; } } else { if (height > MAX_HEIGHT) { width *= MAX_HEIGHT / height; height = MAX_HEIGHT; } }
            c.width = width;
            c.height = height;
            var ctx = c.getContext('2d');
            ctx.drawImage(img, 0, 0, width, height);
            
            var inputfield = document.createElement('input');
            inputfield.type = 'hidden';
            inputfield.name = 'newfile';
            inputfield.id = 'newfile';

            inputfield.value = c.toDataURL();
            document.getElementById('feedbackForm').appendChild(inputfield);
            
            inputfield2.value = document.getElementById('file').files[0].name;
            document.getElementById('feedbackForm').appendChild(inputfield2);
            //document.getElementById('file').remove();
            $('#file').wrap('<form>').closest('form').get(0).reset();
            $('#file').unwrap();
        }

    }

    function submitForm()
    {

        resize();
        document.getElementById('cover').className = "cover";
        document.getElementById('loginScreen').className = "loginScreen";
        document.getElementById('submitButton2').style.display = '';
        document.getElementById("submitButton").style.display = 'none';
        mixpanel.track("Completed leaving feedback");
        //$('#feedbackForm').submit();

        $.ajax({
            type: 'POST',
            url: '/saveFeedback',
            data: $('#feedbackForm').serialize(),
            success: function(data){    
                window.location.href = "/success";
            },
            error: function(ts) { 
                alert(ts.responseText);
            }
        });
    }
    function validateInput()
    {

        if(document.getElementById('comment').value.length != 0  )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
    function finishLater()
    {
        mixpanel.track("Started but not completed leaving feedback");
    }
</script>

@section('content')
<?php
$feedpage = 1;
$count = count($questions);
$circle = $count + 1;

?>

<input type="hidden" id="hiddenId" value="<?php if(Session::has('msg')){echo 'true';}else{echo 'false';} ?>"/>
<div class="list" style="text-align: center; " >
    <a class="item" href="#" style="padding:8px; background: #F8F8F8  ;">
        <h2>{{ $storeData['storename'] }}</h2>
        <p style="margin:0;">{{ $storeData['address'] }}</p>
        <p>{{$deptData['name']}}</p>
    </a>
</div>
<center>
<div>
        <div class="stepwizard">
            <div class="stepwizard-row">

                <?php for ($i=1; $i <= $circle; $i++) { ?>
                    
                
                 <div class="stepwizard-step">
                     <button type="button" class="btn btn-success btn-circle" id="circle{{ $i }}"> {{ $i }}</button>

                </div>
                

                
                <?php } ?>
                </div>
            </div>

        </div>
</div>



</center>

<div id="loginScreen" style="display:none;"> 
        {{ HTML::image('images/Sending.GIF' , 'Sending...', array('style' => '')) }} 
</div>
<div id="cover" style="display:none;"> </div> 

<form id="feedbackForm" method="POST" action="/saveFeedback" enctype="multipart/form-data">
    <input type="hidden" name="storeId" id="storeId" value="{{ $storeData['id'] }}">
    <input type="hidden" name="deptId" id="deptId" value="{{ $deptData['id'] }}">
    @foreach($questions as $question)

    <?php
    $orderId = $question->orderId;
    ?>



    @if($question->orderId == 1)
    


        <div class="showThis" id="div_{{$question->orderId}}">
            <div class="card" >
            @if(strlen($question->question) < 45)
            <div id="questionBox" class="item item-text-wrap demoDayCenter" data-role="main">
            @else
            <div id="questionBox" class="item item-text-wrap" data-role="main">
            @endif
                {{ $question->question }}
            </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="button" class="button button-block button-assertive2" onclick="next({{ $orderId }}, {{ $count }}, 'NO')">No</button>
                </div>
                <div class="col">
                    <button type="button" class="button button-block button-balanced2" onclick="next({{ $orderId }}, {{ $count }}, 'YES')">Yes</button>
                </div>
            </div>
            <input type="hidden" name="input_{{$question->orderId}}" id="input_{{$question->orderId}}" value="none">
        </div>
    @else
        <div class="hideThis" id="div_{{$question->orderId}}">
            <div class="card" >
            @if(strlen($question->question) < 45)
            <div id="questionBox" class="item item-text-wrap demoDayCenter" data-role="main">
            @else
            <div id="questionBox" class="item item-text-wrap" data-role="main">
            @endif
                {{ $question->question }}
            </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="button" class="button button-block button-assertive2" onclick="next({{ $orderId }}, {{ $count }}, 'NO')">No</button>
                </div>
                <div class="col">
                    <button type="button" class="button button-block button-balanced2" onclick="next({{ $orderId }}, {{ $count }}, 'YES')">Yes</button>
                </div>
            </div>
            <input type="hidden" name="input_{{$question->orderId}}" id="input_{{$question->orderId}}" value="none">
        </div>
    @endif
    


    @endforeach

        <div class="hideThis" id="last">
            <div style="margin:12px 10px 0px 10px;">
                <div data-role="main" style="padding:8px;">
                    <center>Here's your chance to tell us what you think...</center>
                    <div class="card" style="margin:5px 10px;">
                        <div class="item item-text-wrap" style="padding:8px;">
                                <textarea placeholder="We value your opinion. You need to fill this out to submit your feedback." maxlength="255" rows="4" onkeyup="validateInput()" id="comment" name="comment" value="{{{ Input::old('comment') }}}"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
<!--    <div class="col"><button class="button button-block button-stable ion-camera"></div>-->


<style>
    .ion-ios7-close-outline:before{
        font-size: 24px;
    }
    .button .icon:before, .button.icon:before{
        font-size: 34px;
        position: absolute;
        top: 3px;
        left: 11px;
    }
.btnUpload {
   position: relative;
   overflow: hidden;
   margin: 10px;
}
.fileUpload {
    font-size:3px; 
    cursor:pointer; 
    position: relative;
    height:50px;
    top:-58;
    margin:10;
    opacity: 0;
    filter: alpha(opacity=0);
}

.truncate {
  
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

}

</style>

    <div style="width:25%; margin-left: 20px;">
        <div class="btnUpload" >
            <button class="button button-stable icon ion-camera" style="width:20px;" value="Attach or upload photo" type="button" onclick="alert('tapped');"></button>
        </div> 
        <input type="file" name="file" id="file" accept="image/*" capture="camera" class="fileUpload">
    </div> 
    <div style="width:72%;">

        <button type="button" id="submitButton" disabled="true" class="button button-block button-balanced2" onclick="submitForm()">Send</button>


        <button id="falseButton" class="hideThis button button-block button-balanced2 " disabled>Send</button>

        <button style="display:none" type="button" id="submitButton2" disabled="true" class="button button-block button-balanced2" >Sending your Feedback <span class="ion-refreshing"></span></button>


        <button style="display:none" type="button" id="submitButton2" disabled="true" class="button button-block button-balanced2" >Sending your Feedback <span class="ion-refreshing"></span></button>

    </div>

    
    
     

    </div>
    <div>

        <p class="truncate" style="width: 48%;margin:-45px 21px;position:relative;font-size:10px;">
            <img id="blah" src="" alt="your image" style="height:24px;width:24px;" onchange="checkImg()"/>
            <span id="path">File:&nbspNo file chosen</span>
            <span id="fsize" style="display: none;"><font color="red">File exceeds the 2MB limit</font></span> 
        </p>
    </div> 
    <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function checkImg(){
                
                if(document.getElementById('blah').src.length != 0  )
                document.getElementById('blah').className = 'showThis';
                else
                document.getElementById('blah').className = 'hideThis';
            }

    </script>
</div>
    <input type="hidden" name="numOfQuestions" id="numOfQuestions" value="{{ count($questions) }}">
    
        <div class="col">
            <a id="cancelButton" onclick="finishLater()" href="/location" class="button button-clear button-assertive"  style="position: absolute;right:0px;">
                <span style="color:#000;margin-right:50px;margin-left:10px;">Finish Later</span>
                <i style="color:#db0500;width:10px;top: 7px; position:absolute; right: 34px;" class="ion-ios7-close-outline"></i>
            </a>
        </div>
   
</form>
<script>
   var control = document.getElementById("file");
    control.addEventListener("change", function(event) {
    readURL(this);
    // When the control has changed, there are new files

    var i = 0,
        files = control.files,
        len = files.length;
    
    for (; i < len; i++) 
    {
        var div = document.getElementById('path');
            div.innerHTML = files[i].name;

            // if(this.files[0].size < 2097152 )
            //      document.getElementById('fsize').style.display = '';
            // else
            //     document.getElementById('fsize').style.display = 'none';

            //this.files[0].size gets the size of your file.
            // alert(this.files[0].size);
          
           
                var s = this.files[0].size;

                if(s > 2097152)
                { 
                    
                    alert('File exceeds the 2MB limit this will take time to upload');
                    //document.getElementById('falseButton').className = "button button-block button-balanced2 showThis";
                    document.getElementById('submitButtonF').disabled = true;
                    //document.getElementById('fsize').style.display = '';
                    //document.getElementById('path').style.display = 'none';
                }
                else{
                    document.getElementById('path').style.display = '';
                    //document.getElementById('fsize').style.display = 'none';
                    document.getElementById('submitButtonF').disabled = false;
                    //document.getElementById('falseButton').className = 'hideThis';

                    //validateComment();
                }          
    }

}, false);


</script>

<!--<div class="card" id="store">-->
<!--    <div class="item item-text-wrap" data-role="main">-->
<!--        Question-->
<!--        <div class="row">-->
<!--            <div class="col"><button class="button button-block button-assertive2" onclick="toggleSlide(store)">No</button></div>-->
<!--            <div class="col"><button class="button button-block button-balanced2" onclick="toggleSlide(store)">Yes</button></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
@stop
