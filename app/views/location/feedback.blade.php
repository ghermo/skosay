@extends('layouts.default')

<?php

function generateRandomString($length = 10){
  $characters = '0123456789';
  $randomString = '';
  for ($i = 0; $i < $length-5; $i++) {
    $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  return $randomString;
}

function detectDuplicate(){
  $randVar = generateRandomString();
  $bool = DB::table('issues')->where('userid', '=', $randVar)->count();
  if($bool == 0){
    return $randVar;
  }
  else{
    detectDuplicate();
  }
}

$guestRandomId = detectDuplicate();
?>

<link rel="alternate" type="application/rss+xml" title="Latest snippets from Bootsnipp.com" href="http://bootsnipp.com/feed.rss">
{{HTML::style('css/bootstrap.min.css');}}
{{HTML::style('css/font-awesome.min.css');}}
{{HTML::style('css/bootsnipp.css');}}
{{HTML::style('css/ladda-themeless.min.css');}}
<style>
    .cover{ 
        position:fixed; 
        top:0; 
        left:0; 
        background:rgba(0,0,0,0.6); 
        z-index:5; 
        width:100%; 
        height:100%; 
        display: block !important;
    } 
    .loginScreen { 
        background: white;
        padding: 6px;
        position: absolute;
        left: 50%;
        margin-left: -70px;
        z-index: 10;
        display: block !important;
        border-radius: 5px;
    } 
    .hideThis{
        display: none !important;
    }
    .showThis{
        display: block;
    }
</style>
<script>
    function next(num, count, ans)
    {
        var nextNum = num + 1;
        oldPage = "div_"+num;
        nextPage = "div_"+nextNum;
        inputHidden = "input_"+num;
        circleNum = "circle"+num;

        if (num == count) {
            document.getElementById(circleNum).innerHTML = "<span class='ion-ios7-checkmark-empty check-icon'></span>";
            document.getElementById(circleNum).className = "btn btn-default btn-circle";
            document.getElementById(oldPage).className = "hideThis";
            document.getElementById('last').className = "showThis";
            document.getElementById(inputHidden).value = ans;
        }
        else{
        
        document.getElementById(circleNum).innerHTML = "<span class='ion-ios7-checkmark-empty check-icon'></span>";
        document.getElementById(circleNum).className = "btn btn-default btn-circle";
        document.getElementById(oldPage).className = "hideThis";
        document.getElementById(nextPage).className = "showThis";
        document.getElementById(inputHidden).value = ans;
        }
    }
   
    function toggleSlide(data)
    {
        $(data).hide();
    }

    function setInput(data)
    {
        $(data['id']).val(data['name']);
    }


    function resize(){
        var inputfield2 = document.createElement('input');
        inputfield2.type = 'hidden';
        inputfield2.name = 'origFileName';
        inputfield2.id = 'origFileName';
        inputfield2.value = '';
        if(document.getElementById('file').value !== ''){
            var filestream = document.getElementById('blah').src;
            var img = document.createElement('img');
            img.src = filestream;
            var c = document.createElement('canvas');
            var MAX_HEIGHT = 600;
            var MAX_WIDTH = 800;
            var width = img.width;
            var height = img.height;
            if (width > height) { if (width > MAX_WIDTH) { height *= MAX_WIDTH / width; width = MAX_WIDTH; } } else { if (height > MAX_HEIGHT) { width *= MAX_HEIGHT / height; height = MAX_HEIGHT; } }
            c.width = width;
            c.height = height;
            var ctx = c.getContext('2d');
            ctx.drawImage(img, 0, 0, width, height);
            
            var inputfield = document.createElement('input');
            inputfield.type = 'hidden';
            inputfield.name = 'newfile';
            inputfield.id = 'newfile';

            inputfield.value = c.toDataURL();
            document.getElementById('feedbackForm').appendChild(inputfield);
            
            inputfield2.value = document.getElementById('file').files[0].name;
            document.getElementById('feedbackForm').appendChild(inputfield2);
            //document.getElementById('file').remove();
            $('#file').wrap('<form>').closest('form').get(0).reset();
            $('#file').unwrap();
        }

    }

    function submitFeedback()
    {
        resize();
        document.getElementById('cover').className = "cover";
        document.getElementById('loginScreen').className = "loginScreen";
        document.getElementById('submitButton2').style.display = '';
        document.getElementById("submitButton").style.display = 'none';
        mixpanel.track("Completed leaving feedback");
        
        $.ajax({
            type: 'POST',
            url: '/saveFeedback',
            data: $('#feedbackForm').serialize(),
            success: function(data){
                window.location.href = "/success";
            },
            error: function(ts) { 
                alert(ts.responseText);
            }
        });
        
        //$('#feedbackForm').submit();
    }

    function validateComment()
    {
        if(document.getElementById('comment').value.length != 0  )
            document.getElementById('submitButtonF').disabled = false;
        else
            document.getElementById('submitButtonF').disabled = true;
    }
    function finishLater()
    {
        mixpanel.track("Started but not completed leaving feedback");
    }

    function signUp(){
    var tab1 = $('#tab-1');
    var tab2 = $('#tab-2');
    var but1 = $('#but1');
    var but2 = $('#but2');
    tab1.removeClass('showDiv');
    tab1.addClass('hidDiv');
    tab2.removeClass('hidDiv');
    tab2.addClass('showDiv');

    but2.removeClass('activeSign');
    but2.addClass('inactiveSign');
    but1.removeClass('inactiveSign');
    but1.addClass('activeSign');

  }
    function login(){
        var tab1 = $('#tab-1');
        var tab2 = $('#tab-2');
        var but1 = $('#but1');
        var but2 = $('#but2');
        tab2.removeClass('showDiv');
        tab2.addClass('hidDiv');
        tab1.removeClass('hidDiv');
        tab1.addClass('showDiv');

        but1.removeClass('activeSign');
        but1.addClass('inactiveSign');
        but2.removeClass('inactiveSign');
        but2.addClass('activeSign');
    }

    function guestFunction(){
        var x = {{ $guestRandomId }};
        var y = "guestId=";
        var res = y.concat(x);
        var today = new Date();
        var dd = today.getDate();
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        }

        today = dd+''+yyyy;
        var f = res.concat(today);
        alert(f);
        document.cookie= f+';path=/';
    }

</script>

@section('content')
<?php

if (!isset($logged)) {
    $logged = 1;
}

if (!isset($id)) {
    $id = NULL;
}

$feedpage = 1;
$count = count($questions);
$circle = $count + 1;
?>


@if($logged == 0)
<script>
$(function() {
    window.location.href = "#modal-one";
});
</script>
@endif


<input type="hidden" id="hiddenId" value="<?php if(Session::has('msg')){echo 'true';}else{echo 'false';} ?>"/>
<div class="list" style="text-align: center; " >
    <a class="item" href="#" style="padding:8px; background: #F8F8F8  ;">
        <h2>{{ $storeData['storename'] }}</h2>
        <p>{{ $storeData['address'] }}</p>
    </a>
</div>
<center>
<div>
        <div class="stepwizard">
            <div class="stepwizard-row">

                <?php for ($i=1; $i <= $circle; $i++) { ?>
                    
                
                 <div class="stepwizard-step">
                     <button type="button" class="btn btn-success btn-circle" id="circle{{ $i }}"> {{ $i }}</button>

                </div>
                

                
                <?php } ?>
                </div>
            </div>

        </div>
</div>



</center>

<div id="loginScreen" style="display:none;"> 
    {{ HTML::image('images/Sending.GIF' , 'Sending...', array('style' => '')) }} 
</div>
<div id="cover" style="display:none;"> </div> 

<form id="feedbackForm" method="POST" action="/saveFeedback" enctype="multipart/form-data">
    <input type="hidden" name="storeId" id="storeId" value="{{ $storeData['id'] }}">
    <input type="hidden" name="deptId" id="deptId" value="0">
    @foreach($questions as $question)

    <?php
    $orderId = $question->orderId;
    ?>

    @if($question->orderId == 1)
        <div class="showThis" id="div_{{$question->orderId}}">
            <div class="card" >
            @if(strlen($question->question) < 45)
            <div id="questionBox" class="item item-text-wrap demoDayCenter" data-role="main">
            @else
            <div id="questionBox" class="item item-text-wrap" data-role="main">
            @endif
                {{ $question->question }}
            </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="button" class="button button-block button-assertive2" onclick="next({{ $orderId }}, {{ $count }}, 'NO')">No</button>
                </div>
                <div class="col">
                    <button type="button" class="button button-block button-balanced2" onclick="next({{ $orderId }}, {{ $count }}, 'YES')">Yes</button>
                </div>
            </div>
            <input type="hidden" name="input_{{$question->orderId}}" id="input_{{$question->orderId}}" value="none">
        </div>
    @else
        <div class="hideThis" id="div_{{$question->orderId}}">
            <div class="card" >
            @if(strlen($question->question) < 45)
            <div id="questionBox" class="item item-text-wrap demoDayCenter" data-role="main">
            @else
            <div id="questionBox" class="item item-text-wrap" data-role="main">
            @endif
                {{ $question->question }}
            </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="button" class="button button-block button-assertive2" onclick="next({{ $orderId }}, {{ $count }}, 'NO')">No</button>
                </div>
                <div class="col">
                    <button type="button" class="button button-block button-balanced2" onclick="next({{ $orderId }}, {{ $count }}, 'YES')">Yes</button>
                </div>
            </div>
            <input type="hidden" name="input_{{$question->orderId}}" id="input_{{$question->orderId}}" value="none">
        </div>
    @endif
    


    @endforeach

        <div class="hideThis" id="last">
            <div style="margin:12px 10px 0px 10px;">
                <div data-role="main" style="padding:8px;">
                    <center>Here's your chance to tell us what you think...</center>
                    <div class="card" style="margin:5px 10px;">
                        <div class="item item-text-wrap" style="padding:8px;">
                                <textarea placeholder="We value your opinion. You need to fill this out to submit your feedback." maxlength="255" rows="4" onkeyup="validateComment()" id="comment" name="comment" value="{{{ Input::old('comment') }}}"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
<!--    <div class="col"><button class="button button-block button-stable ion-camera"></div>-->


<style>
    .modal1:before {
        content: "";
        display: none;
        background: rgba(0, 0, 0, 0.6);
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 10;
    }
    .modal1:target:before {
        display: block;
    }
    .modal1:target .modal-dialog {
        -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
        transform: translate(0, 0);
        top: 1%;
    }
    .modal-dialog {
        background: rgba(254, 254, 254, 0.88);
        border: #333333 solid 1px;
        border-radius: 22px;
        margin-left: -160px;
        position: fixed;
        left: 50%;
        top: -100%;
        z-index: 11;
        width: 320px;
        -webkit-transform: translate(0, -500%);
        -ms-transform: translate(0, -500%);
        transform: translate(0, -500%);
        -webkit-transition: -webkit-transform 0.3s ease-out;
        -moz-transition: -moz-transform 0.3s ease-out;
        -o-transition: -o-transform 0.3s ease-out;
        transition: transform 0.3s ease-out;
    }
    .modal-body {
        padding: 0px 20px 10px 20px;
    }
    .modal-header,
    .modal-footer {
        padding: 8px 20px;
    }
    .modal-header {
        border-bottom: #818181  solid 4px;
    }
    .modal-header h2 {
        font-size: 20px;
    }
    .modal-footer {
        border-top: #eeeeee solid 1px;
        text-align: right;
    }
    /*ADDED TO STOP SCROLLING TO TOP*/
    #close {
        display: none;
    }
    .ion-ios7-close-outline:before{
        font-size: 24px;
    }
    .button .icon:before, .button.icon:before{
        font-size: 34px;
        position: absolute;
        top: 3px;
        left: 11px;
    }
    .btnUpload {
        position: relative;
        overflow: hidden;
        margin: 10px;
    }
    .fileUpload {
        font-size:3px; 
        cursor:pointer; 
        position: relative;
        height:50px;
        top:-58;
        margin:10;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .truncate {
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .showDiv{
        transition: opacity 2s linear;
        visibility: visible;
        opacity: 1;
        display: block;
    }
    .hidDiv{
        visibility: hidden;
        opacity: 0;
        display: none;
        transition: visibility 0s 2s ,opacity 2s linear;
    }
    .activeSign{
        color:#000;background: none !important;border:none; font-weight: 800;
    }
    .inactiveSign{
        color:#ffffff;background-color:#cccccc !important;border:none;
    }

    .btn-close {
        color: #000;
        font-size: 38px;
        text-decoration: none;
        position: absolute;
        left: 11px;
        top: 10px;
    }

</style>
    
    <div style="width:25%; margin-left: 20px;">
        <div class="btnUpload" >
            <button class="button button-stable icon ion-camera" style="width:20px;" value="Attach or upload photo" type="button" onclick="alert('tapped');"></button>
        </div> 
        <input type="file" name="file" id="file" accept="image/*" capture="camera" class="fileUpload"> 
    </div> 
    <div style="width:72%;">

        <button type="button" id="submitButtonF" disabled="true" class="button button-block button-balanced2" onclick="submitFeedback()">Send</button>


        <button id="falseButton" class="hideThis button button-block button-balanced2 " disabled>Send</button>

        <button style="display:none" type="button" id="submitButton2F" disabled="true" class="button button-block button-balanced2" >Sending your Feedback <span class="ion-refreshing"></span></button>


        <button style="display:none" type="button" id="submitButton2F" disabled="true" class="button button-block button-balanced2" >Sending your Feedback <span class="ion-refreshing"></span></button>

    </div>
     

    </div>
    <div>
        <p class="truncate" style="width: 48%;margin:-45px 21px;position:relative;font-size:10px;">
            <img id="blah" src="" alt="your image" style="height:24px;width:24px;" onchange="checkImg()"/>
            <span id="path">File:&nbspNo file chosen</span>
            
        </p>
        <!--<span id="fsize" style="display: none;"><font color="red">File exceeds the 2MB limit this will take time to upload</font></span> -->
    </div>

    <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            function checkImg(){
                
                if(document.getElementById('blah').src.length != 0  )
                document.getElementById('blah').className = 'showThis';
                else
                document.getElementById('blah').className = 'hideThis';
            }

    </script>
</div>
    <input type="hidden" name="numOfQuestions" id="numOfQuestions" value="{{ count($questions) }}">
    
        <div class="col">
            <a id="cancelButton" onclick="finishLater()" href="/location" class="button button-clear button-assertive"  style="position: absolute;right:0px;">
                <span style="color:#000;margin-right:50px;margin-left:10px;">Finish Later</span>
                <i style="color:#db0500;width:10px;top: 7px; position:absolute; right: 34px;" class="ion-ios7-close-outline"></i>
            </a>
        </div>
   
</form>
<script>
    var control = document.getElementById("file");
    control.addEventListener("change", function(event) {
    readURL(this);
    // When the control has changed, there are new files
    var i = 0,
        files = control.files,
        len = files.length;
    
    for (; i < len; i++) 
    {
        var div = document.getElementById('path');
            div.innerHTML = files[i].name;

            // if(this.files[0].size < 2097152 )
            //      document.getElementById('fsize').style.display = '';
            // else
            //     document.getElementById('fsize').style.display = 'none';

            //this.files[0].size gets the size of your file.
            // alert(this.files[0].size);
          
           
                var s = this.files[0].size;

                if(s > 2097152)
                { 
                    
                    alert('File exceeds the 2MB limit this will take time to upload');
                    //document.getElementById('falseButton').className = "button button-block button-balanced2 showThis";
                    document.getElementById('submitButtonF').disabled = true;
                    //document.getElementById('fsize').style.display = '';
                    //document.getElementById('path').style.display = 'none';
                }
                else{
                    document.getElementById('path').style.display = '';
                    //document.getElementById('fsize').style.display = 'none';
                    document.getElementById('submitButtonF').disabled = false;
                    //document.getElementById('falseButton').className = 'hideThis';

                    //validateComment();
                }          
    }
}, false);


</script>

<div class="modal1" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
            <center>
          {{ HTML::image('images/skopic1.png' , 'Skosay Logo', array('style' => 'width:200px;')) }}
      </center>
          <a href="#close" class="btn-close" aria-hidden="true" onclick="guestFunction()">×</a> <!--CHANGED TO "#close"-->
        </div>
        <div class="button-bar">
            <a id="but1" class="button activeSign" onclick="signUp()">SIGN UP</a>
            <a id="but2" class="button inactiveSign" onclick="login()">LOGIN</a>
          </div>
<div class="modal-body">

          


<div id="tab-1" class="hidDiv">
@if($logged == 0) 
<script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

</script>

<script>
    
    
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    function submitForm()
    {
        mixpanel.track("QR Code SI");
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
                $('#errorMsg').empty().append(userData['msg']).show();

                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function loginFb()
    {
        mixpanel.track("QR Code FB");
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
//                $('#errorMsg').empty().append(userData['msg']).show();
//                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            document.getElementById('redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0)
                submitForm();
        }
    });

    function validateInput()
    {
        if(document.getElementById('email').value.length != 0 && document.getElementById('password').value.length != 0 )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
@endif


<?php 
$email = Session::get('email');
if (isset($_GET['successForgot'])) {
    $success = $_GET['successForgot'];
?>
<?php } ?>
<!-- if there are login errors, show them here -->
        @if(isset($id))
            <form method="POST" action="../login2" id="ajaxForm">
        @else
            <form method="POST" action="login2" id="ajaxForm">
        @endif
        <!--
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
        </p>
        -->

        @if(isset($success))
        <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
            <div class="item" style="background-color: darkseagreen; border-color: green;">
                <center>
                    <h4>
                        <font color="green">Congratulations! Password has been changed.</font>
                    </h4>
                </center>
            </div>
        </div>
        @else
        <div id="errorMsg" style="background-color:#00b9f2"></div>
        @endif
        
        <center><h4 style="font-weight: 800;"> Welcome Back! </h4></center> 

                @if(isset($email))
                    {{ Form::email('email', $email, array('onkeyup' => 'validateInput()','id' => 'email', 'value' => $email, 'autocapitalize' => 'off')) }}
                @else
                    {{ Form::email('email', Input::old('email'), array('onkeyup' => 'validateInput()','id' => 'email', 'placeholder' => '   Your email', 'autocapitalize' => 'off', 'style' => 'padding: 10px;')) }}
                @endif
        <br>

                {{ Form::password('password', array('onkeyup' => 'validateInput()', 'placeholder' => 'Your password', 'id' => 'password', 'autocapitalize' => 'off', 'style' => 'padding: 10px;'))}}

        {{ Form::close() }}
        <br>
        {{ Form::submit('Sign in',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized','type' => 'button','onclick' => 'submitForm()')) }}
        <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Signing in. Please wait <span class="ion-refreshing"></span></button>
<!--            <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>-->
        <center><a class="text-link" href="/confirmForgot" style="color:black;font-size: 12px;">Forgot Password?</a></center>
        
            <form id="redirectForm" method="GET" action="/feedback/{{$id}}">
            </form>

    
    <input type="hidden" name="testHidden" id="testHidden" value="none"/>

    <br>
    
        @if(isset($id))
            <form action="../loginFb" method="GET" id="loginFb">
            <input type="hidden" id="fbQR" value="{{$id}}" name="fbQR">
        @else
            <form action="loginFb" method="GET" id="loginFb">
        @endif
            
            <button id="loginFbBtn" class="button button-block button-royal">
                @if(isset($email))
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @else
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @endif
            </button>
        </form>
     <br>



</div>
<!-- end of tab 1 -->
<div id="tab-2" class="showDiv">

<script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

</script>
@if($logged == 0)
<script>
    
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    

    function submitForm2()
    {
        mixpanel.track("QR Code SU");
        $('#errorMsg2').empty();
        document.getElementById('submitButton2').style.display = 'none';
        document.getElementById('falseButton2').style.display = '';

        var url = $('#ajaxForm2').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm2').serialize(),
            success : function(userData)
            {
                var msg = userData['msg'];
                $('#errorMsg2').empty().append(msg).show();
                $('#redirectForm2').submit();
            }
        })
    }

    $(document).keypress(function(e) {
        if(e.which == 13) {
            //return false;
            //$('#submitButton2').submit();
            submitForm2();
        }
    });

    function loginFb()
    {
        mixpanel.track("Landed on sign in screen. Login Using Popup Screen - QR Code. FB");
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').val(userData['status']);
                checkLogin();
//                $('#errorMsg2').empty().append(userData['msg']).show();
//                setTimeout(function(){ $('#errorMsg2').empty(); },4000);
            }
        })
    }

    function validateInput2()
    {
        if(document.getElementById('email2').value.length != 0)
            if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.getElementById('email2').value)))  
                document.getElementById('submitButton2').disabled = true;  
            else
                document.getElementById('submitButton2').disabled = false;
        else
            document.getElementById('submitButton2').disabled = true;
    }
</script>
@endif
<?php 
$email = Session::get('email');
if (isset($_GET['successForgot'])) {
    $success = $_GET['successForgot'];
?>
<?php } ?>
<!-- if there are login errors, show them here -->
        <form method="POST" action="../signup" id="ajaxForm2">
        
        @if(isset($success))
        <div id="errorMsg2">
            <div style="background-color: darkseagreen; border-color: green;">
                <center>
                    <h4>
                        <font color="green">Congratulations! Password has been changed.</font>
                    </h4>
                </center>
            </div>
        </div>
        @else
        <div id="errorMsg2"></div>
        @endif
        
        
        <center>
            <h4 style="color:#000;font-weight: 800;">Your feedback matters.</h4>
            <span style="font-size: 12px; color: #000;line-height:18px;">A valid email gets you started and allows you to connect directly with the business.</span>
        </center> 

            @if(isset($id))
                <input type='hidden' value="{{$id}}" name="sid" id="sid">
            @endif

               @if(isset($email))
                    {{ Form::email('email', $email, array('onkeyup' => 'validateInput()','id' => 'email2', 'value' => $email, 'autocapitalize' => 'off')) }}
               @else
                    {{ Form::email('email', Input::old('email'), array('onkeyup' => 'validateInput2()','id' => 'email2', 'placeholder' => 'Your email address', 'autocapitalize' => 'off', 'style' => 'padding: 10px;')) }}
                @endif

        {{ Form::close() }}
         
        <div>
            {{ Form::submit('Go',  array('disabled' => 'true', 'id' => 'submitButton2', 'class' => 'button button-block button-energized','type' => 'button','onclick' => 'submitForm2()')) }}
            <button id="falseButton2" class="button button-block button-energized" disabled style="display: none;">Signing in. Please waitt <span class="ion-refreshing"></span></button>
<!--            <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>-->
            <center style="margin-bottom:10px;">
                <span style="font-size: 10px; color: #000;">
                    * we hate spam, too. we never share your email with anyone.
                </span>
            </center>
        </div>
            <form id="redirectForm2" method="GET" action="/feedback/{{$id}}">
                <input type="hidden" name="email2" id="emailG" value="none"/>
                <input type="hidden" name="password2" id="password2" value="none"/>
                <input type="hidden" name="testHidden" id="testHidden" value="none"/>
            </form>
    
    <div style="margin-bottom:12px;">
        @if(isset($id))
            <form action="../loginFb" method="GET" id="loginFb">
            <input type="hidden" id="fbQR" value="{{$id}}" name="fbQR">
        @else
            <form action="loginFb" method="GET" id="loginFb">
        @endif
            <center>
                <span style="font-size: 12px; color: #000;">
                    or connect using your Facebook account.
                </span>
            </center>
            <button id="loginFbBtn" class="button button-block button-royal">
                @if(isset($email))
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @else
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @endif
            </button>
        </form>
    </div>
</div>


</div>
</div>

<!--<div class="card" id="store">-->
<!--    <div class="item item-text-wrap" data-role="main">-->
<!--        Question-->
<!--        <div class="row">-->
<!--            <div class="col"><button class="button button-block button-assertive2" onclick="toggleSlide(store)">No</button></div>-->
<!--            <div class="col"><button class="button button-block button-balanced2" onclick="toggleSlide(store)">Yes</button></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
@stop

