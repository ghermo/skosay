@extends('layouts.default')

@section('content')
<?php
$feedpage = 1;
if (isset($deptCounter)) {
    $deptC = 1;
}
?>

<?php

function generateRandomString($length = 10){
  $characters = '0123456789';
  $randomString = '';
  for ($i = 0; $i < $length-5; $i++) {
    $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  return $randomString;
}

function detectDuplicate(){
  $randVar = generateRandomString();
  $bool = DB::table('issues')->where('userid', '=', $randVar)->count();
  if($bool == 0){
    return $randVar;
  }
  else{
    detectDuplicate();
  }
}

$guestRandomId = detectDuplicate();
?>

<script>
    function guestFunction(){
        var x = {{ $guestRandomId }};
        var y = "guestId=";
        var res = y.concat(x);
         var today = new Date();
        var dd = today.getDate();
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        }

        today = dd+''+yyyy;
        var f = res.concat(today);
        document.cookie= f+';path=/';
    }
</script>


<?php
$page = 1; 

?>

@if(isset($logged))
@if($logged == 0)
<script>
$(function() {
    function show_modal(){
        window.location.href = "#modal-one";
    }
    window.setTimeout(show_modal, 2000);
});
</script>
@else
<style>
    #modal-one{
      display: none !important;
    }
</style>
@endif
@endif



<div id="gmap_canvas" style="display: none;"></div>
<div class="item item-divider" style="display: none;">
    <input type="hidden" id="lat" name="lat" value="0" />
    <input type="hidden" id="lng" name="lng" value='0' />
    <div id="button1" class="button button-block button-positive" onclick="findPlaces('location'); return false;">Search</div>
</div>

<style>
  .wrap {
    padding: 40px;
    text-align: center;
  }
  .btn {
    background: #428bca;
    border: #357ebd solid 1px;
    border-radius: 3px;
    color: #fff;
    display: inline-block;
    font-size: 14px;
    padding: 8px 15px;
    text-decoration: none;
    text-align: center;
    min-width: 60px;
    position: relative;
    transition: color .1s ease;
    /* top: 40em;*/
  }
  .btn:hover {
    background: #357ebd;
  }
  .btn.btn-big {
    font-size: 18px;
    padding: 15px 20px;
    min-width: 100px;
  }
  .btn-close {
    color: #000;
    font-size: 38px;
    text-decoration: none;
    position: absolute;
    left: 11px;
    top: 10px;
  }
  .btn-close:hover {
    color: #919191;
  }
  .modal1:before {
    content: "";
    display: none;
    background: rgba(0, 0, 0, 0.6);
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 10;
  }
  .modal1:target:before {
    display: block;
  }
  .modal1:target .modal-dialog {
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
    top: 6%;
  }
  .modal-dialog {
    background: rgba(254, 254, 254, 0.88);
    border: #333333 solid 1px;
    border-radius: 22px;
    margin-left: -160px;
    position: fixed;
    left: 50%;
    top: -100%;
    z-index: 11;
    width: 320px;
    -webkit-transform: translate(0, -500%);
    -ms-transform: translate(0, -500%);
    transform: translate(0, -500%);
    -webkit-transition: -webkit-transform 0.3s ease-out;
    -moz-transition: -moz-transform 0.3s ease-out;
    -o-transition: -o-transform 0.3s ease-out;
    transition: transform 0.3s ease-out;
  }
  .modal-body {
    padding: 0px 20px 10px 20px;
  }
  .modal-header,
  .modal-footer {
    padding: 8px 20px;
  }
  .modal-header {
    border-bottom: #818181  solid 4px;
  }
  .modal-header h2 {
    font-size: 20px;
  }
  .modal-footer {
    border-top: #eeeeee solid 1px;
    text-align: right;
  }
  /*ADDED TO STOP SCROLLING TO TOP*/
  #close {
    display: none;
  }
</style>

<div class="list" style="text-align: center; " >
    <a class="item" href="#" style="padding:8px; background: #F8F8F8  ;">
        <h2>{{ $storeData['storename'] }}</h2>
        <p>{{ $storeData['address'] }}</p>
    </a>
</div>

<h4 class="questionDept">
        Choose Category:
</h4>

<div class="list">
    @foreach ($departments as $department)
    @if(isset($deptC))
    <form method="POST" action="../feedback2">
    @else
    <form method="POST" action="feedback2">
    @endif
        <input type="hidden" name="dId" value="{{$department->id}}" />
        <input type="hidden" name="dStoreid" value="{{$department->storeid}}" />
        <input type="hidden" name="dName" value="{{$department->name}}" />
        <button class="button-full clck item item-icon-right" type="submit" style="text-align:left;">
            {{$department->name}}
            <i class="icon ion-chevron-right" style="font-size: 20px;"></i>
        </button>
    </form>
    
    @endforeach
</div>


<script>
  function signUp(){
    var tab1 = $('#tab-1');
    var tab2 = $('#tab-2');
    var but1 = $('#but1');
    var but2 = $('#but2');
    tab1.removeClass('showDiv');
    tab1.addClass('hidDiv');
    tab2.removeClass('hidDiv');
    tab2.addClass('showDiv');

    but2.removeClass('activeSign');
    but2.addClass('inactiveSign');
    but1.removeClass('inactiveSign');
    but1.addClass('activeSign');

    $(document).keypress(function(e) {
        if(e.which == 13) {
            submitForm2();
        }
    });

  }
  function login(){
    var tab1 = $('#tab-1');
    var tab2 = $('#tab-2');
    var but1 = $('#but1');
    var but2 = $('#but2');
    tab2.removeClass('showDiv');
    tab2.addClass('hidDiv');
    tab1.removeClass('hidDiv');
    tab1.addClass('showDiv');

    but1.removeClass('activeSign');
    but1.addClass('inactiveSign');
    but2.removeClass('inactiveSign');
    but2.addClass('activeSign');

    $(document).keypress(function(e) {
        if(e.which == 13) {
            if(document.getElementById('email').value.length != 0 || document.getElementById('password').value.length != 0){
                submitForm();
            }
            //return false;
        }
    });
    
  }
</script>

<style>
  .showDiv{
    transition: opacity 2s linear;
    visibility: visible;
    opacity: 1;
    display: block;
  }
  .hidDiv{
    visibility: hidden;
    opacity: 0;
    display: none;
    transition: visibility 0s 2s ,opacity 2s linear;
  }
  .activeSign{
    color:#000;background: none !important;border:none; font-weight: 800;
  }
  .inactiveSign{
    color:#ffffff;background-color:#cccccc !important;border:none;
  }
</style>


<div class="modal1" id="modal-one" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-header">
          {{ HTML::image('images/skopicorig.png' , 'Skosay Logo', array('style' => 'width:200px;')) }}
          <a href="#close" class="btn-close" aria-hidden="true" onclick="guestFunction()">×</a> <!--CHANGED TO "#close"-->
        </div>
        <div class="button-bar">
            <a id="but1" class="button activeSign" onclick="signUp()">SIGN UP</a>
            <a id="but2" class="button inactiveSign" onclick="login()">LOGIN</a>
        </div>
        <div class="modal-body">

          


<div id="tab-1" class="hidDiv">     
<script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

</script>

<script>
    
    
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    function submitForm()
    {
        mixpanel.track("Pop Up Log In");
        $('#errorMsg').empty();
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';

        var url = $('#ajaxForm').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
                $('#errorMsg').empty().append(userData['msg']).show();

                setTimeout(function(){ $('#errorMsg').empty(); },2000);
            }
        })
    }

    function loginFb()
    {
        mixpanel.track("Pop Up FB Sign Up");
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').replaceWith(userData['hidden']);
                checkLogin();
//                $('#errorMsg').empty().append(userData['msg']).show();
//                setTimeout(function(){ $('#errorMsg').empty(); },4000);
            }
        })
    }

    function checkLogin()
    {
        if(document.getElementById('testHidden').value == 'true')
        {
            document.getElementById('redirectForm').submit();
        }
        else
        {
            document.getElementById('submitButton').style.display = '';
            document.getElementById('falseButton').style.display = 'none';
        }
    }

    

    function validateInput()
    {
        if(document.getElementById('email').value.length != 0 && document.getElementById('password').value.length != 0 )
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>


<?php 
$email = Session::get('email');
if (isset($_GET['successForgot'])) {
    $success = $_GET['successForgot'];
?>
<?php } ?>
<!-- if there are login errors, show them here -->
        @if(isset($deptC))
            <form method="POST" action="../login2" id="ajaxForm">
        @else
            <form method="POST" action="login2" id="ajaxForm">
        @endif


        <!--
        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
        </p>
        -->

        @if(isset($success))
        <div id="errorMsg" class="list list-inset" style="background-color:#00b9f2">
            <div class="item" style="background-color: darkseagreen; border-color: green;">
                <center>
                    <h4>
                        <font color="green">Congratulations! Password has been changed.</font>
                    </h4>
                </center>
            </div>
        </div>
        @else
        <div id="errorMsg"></div>
        @endif
        
        <center><h4 style="font-weight: 800;"> Welcome Back! </h4></center> 

                @if(isset($email))
                    {{ Form::email('email', $email, array('onkeyup' => 'validateInput()','id' => 'email', 'value' => $email, 'autocapitalize' => 'off')) }}
                @else
                    {{ Form::email('email', Input::old('email'), array('onkeyup' => 'validateInput()','id' => 'email', 'placeholder' => 'Your email', 'autocapitalize' => 'off', 'style' => 'padding: 10px;')) }}
                @endif
        <br>

                {{ Form::password('password', array('onkeyup' => 'validateInput()', 'placeholder' => 'Your password', 'id' => 'password', 'autocapitalize' => 'off', 'style' => 'padding: 10px;'))}}

        {{ Form::close() }}
        <br>
        {{ Form::submit('Sign in',  array('disabled' => 'true', 'id' => 'submitButton', 'class' => 'button button-block button-energized','type' => 'button','onclick' => 'submitForm()')) }}
        <button id="falseButton" class="button button-block button-energized" disabled style="display: none;">Signing in. Please wait <span class="ion-refreshing"></span></button>
<!--            <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>-->
        <center><a class="text-link" href="/confirmForgot" style="color:black;font-size: 12px;">Forgot Password?</a></center>
        

        @if(isset($deptC))
            <form id="redirectForm" method="GET" action="{{$id}}">
            </form>
        @else
            <form id="redirectForm" method="GET" action="location">
            </form>
        @endif
    
    <input type="hidden" name="testHidden" id="testHidden" value="none"/>

    <br>
    
        @if(isset($deptC))
            <form action="../loginFb" method="GET" id="loginFb">
            <input type="hidden" id="fbQR" value="{{$id}}" name="fbQR">
        @else
            <form action="loginFb" method="GET" id="loginFb">
        @endif
            
            <button id="loginFbBtn" class="button button-block button-royal">
                @if(isset($email))
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @else
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @endif
            </button>
        </form>
     <br>
</div>
<!-- end of tab 1 -->


<div id="tab-2" class="showDiv">

<script>
        window.onload = function()
        {
            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

</script>

<script>
    
    window.onload = function()
    {
        $('#password').val('');
//        document.getElementById('password').value ='';
    };

    function submitForm2()
    {
        mixpanel.track("Pop Up Email Sign Up");
        $('#errorMsg2').empty();
        document.getElementById('submitButton2').style.display = 'none';
        document.getElementById('falseButton2').style.display = '';

        var url = $('#ajaxForm2').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#ajaxForm2').serialize(),
            success : function(userData)
            {
                var msg = userData['msg'];
                $('#errorMsg2').empty().append(msg).show();
                $('#redirectForm2').submit();
            }
        })
        
    }

    

    function loginFb()
    {
        mixpanel.track("Landed on sign in screen. Login Using Popup Screen - First page. Facebook");
        var url = $('#loginFb').attr('action');
        $.ajax({
            type : 'POST',
            url : url,
            data : $('#loginFb').serialize(),
            success : function(userData)
            {
                $('#testHidden').val(userData['status']);
                checkLogin();
//                $('#errorMsg2').empty().append(userData['msg']).show();
//                setTimeout(function(){ $('#errorMsg2').empty(); },4000);
            }
        })
    }

    $( document ).ready(function() {
    $('#email2').bind("input propertychange", function() { //change # to the ID of your field
            if (document.getElementById("email2").value.length != 0 && (/^[A-Za-z0-9+-._"]+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(document.getElementById('email2').value)) == true )
            { //disable submit if "no" is selected
                ///^[A-Za-z0-9_\"\+-.]+([\.-]?[\+]?[\"]?\w+)*@\w+([\.-]?[\+]?[\"]?\w+)*(\.\w{2,3})+$/
              $('#submitButton2').removeAttr('disabled');
              <?php clearstatcache(); ?>
              //(/^\w+([\.-]?[\.+]?[\."]?\w+)*@\w+([\.-]?[\.+]?[\."]?\w+)*(\.\w{2,8})+$/
        } 
        else 
        { //enable the submit button
              $('#submitButton2').attr("disabled","disabled");
        }
        });
    });

</script>
<?php 
$email = Session::get('email');
if (isset($_GET['successForgot'])) {
    $success = $_GET['successForgot'];
?>
<?php } ?>
<!-- if there are login errors, show them here -->
            @if(isset($deptC))
            <form method="POST" action="../signup" id="ajaxForm2">
            @else
            <form method="POST" action="signup" id="ajaxForm2">
            @endif
        
        @if(isset($success))
        <div id="errorMsg2">
            <div style="background-color: darkseagreen; border-color: green;">
                <center>
                    <h4>
                        <font color="green">Congratulations! Password has been changed.</font>
                    </h4>
                </center>
            </div>
        </div>
        @else
        <div id="errorMsg2"></div>
        @endif
        
        <center>
            <h4 style="color:#000;font-weight: 800;">Your feedback matters.</h4>
            <span style="font-size: 12px; color: #000;line-height:18px;">A valid email gets you started and allows you to connect directly with the business.</span>
        </center> 
        
            @if(isset($id))
                <input type='hidden' value="{{$id}}" name="sid" id="sid">
            @endif

                @if(isset($email))
                    {{ Form::email('email', $email, array('onkeyup' => 'validateInput()','id' => 'email2', 'value' => $email, 'autocapitalize' => 'off')) }}
                @else
                    {{ Form::text('email', Input::old('email'), array( 'id' => 'email2', 'placeholder' => 'Your email address', 'autocapitalize' => 'off', 'style' => 'padding: 10px;')) }}
                @endif
         
        <div>
            
             {{ Form::close() }}
             {{ Form::submit('Go',  array('disabled' => 'true', 'id' => 'submitButton2', 'class' => 'button button-block button-energized','type' => 'button','onclick' => 'submitForm2()')) }}
            <button id="falseButton2" class="button button-block button-energized" disabled style="display: none;">Signing in. Please wait <span class="ion-refreshing"></span></button>
<!--            <center><a href="#" style="text-decoration: none; color: white;">Forgot Password?</a></center>-->
            <center style="margin-bottom:10px;">
                <span style="font-size: 10px; color: #000;">
                    * we hate spam, too. we never share your email with anyone.
                </span>
            </center>
        </div>
            @if(isset($deptC))
            <form id="redirectForm2" method="GET" action="../feedback">
            @else
            <form id="redirectForm2" method="GET" action="/feedback">
            @endif
                <input type="hidden" name="email2" id="emailG" value="none"/>
                <input type="hidden" name="password2" id="password2" value="none"/>
                <input type="hidden" name="testHidden" id="testHidden" value="none"/>
            </form>
    
    <div style="margin-bottom:12px;">
        @if(isset($deptC))
            <form action="../loginFb" method="GET" id="loginFb">
            <input type="hidden" id="fbQR" value="{{$id}}" name="fbQR">
        @else
            <form action="loginFb" method="GET" id="loginFb">
        @endif
            <center>
                <span style="font-size: 12px; color: #000;">
                    or connect using your Facebook account.
                </span>
            </center>
            <button id="loginFbBtn" class="button button-block button-royal">
                @if(isset($email))
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @else
                    {{ HTML::image('images/fb.png' , 'FB Logo', array('class' => 'fbIconForm')) }}
                    Sign in with Facebook
                @endif
            </button>
        </form>
    </div>
</div>
<!-- end of tab2 -->
</div>
</div>
</div>
<!-- end of the login -->
<!-- Modal -->

<a href="#modal-one" class="btn btn-big autolik" style="display:none">Modal!</a>


@stop