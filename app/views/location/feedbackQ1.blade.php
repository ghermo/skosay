
@extends('layouts.default')
<?php
    $feedpage = 1;
    $page = 1;
?>
    <link rel="alternate" type="application/rss+xml" title="Latest snippets from Bootsnipp.com" href="http://bootsnipp.com/feed.rss">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootsnipp.css">
    <link rel="stylesheet" href="css/ladda-themeless.min.css">
<script>
    function submitYes()
    {
        document.getElementById('q1').value = 'YES';
        $('#submitForm').submit();
    }

    function submitNo()
    {
        document.getElementById('q1').value = 'NO';
        $('#submitForm').submit();
    }
</script>

@section('content')

    <input type="hidden" id="hiddenId" value="<?php if(Session::has('msg')){echo 'true';}else{echo 'false';} ?>"/>
    <div class="list" style="text-align: center; " >
        <a class="item" href="#" style="padding:8px; background: #F8F8F8  ;">
            <h2>{{{ str_replace('_',' ',$inputAll['pName']) }}}</h2>
            <p>{{{ str_replace('_',' ',$inputAll['pAddress']) }}}</p>
        </a>
    </div>
<center>
<div>
        <div class="stepwizard">
            <div class="stepwizard-row">


                 <div class="stepwizard-step">
                     <button type="button" class="btn btn-success btn-circle">1</button>

                </div>
                <div class="stepwizard-step">
                     <button type="button" class="btn btn-default btn-circle " disabled="disabled"><font color ="#cbcbcb">2</font></button>

                </div>
                <div class="stepwizard-step">
                     <button type="button" class="btn btn-default btn-circle" disabled="disabled"><font color ="#cbcbcb">3</font></button>

                 </div>
                <div class="stepwizard-step">
                     <button type="button" class="btn btn-default btn-circle" disabled="disabled"><font color ="#cbcbcb">4</font></button>

                </div>
            </div>

            <!-- <div class="stepwizard-step">
                     <button type="button" class="btn btn-default btn-circle " disabled="disabled">2</button>

            </div>
            <div class="stepwizard-step">
                     <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>

            </div>
            <div class="stepwizard-step">
                     <button type="button" class="btn btn-default btn-circle" disabled="disabled">4</button>

            </div> -->

        </div>
</div>
</center>

    <div class="card">
        <div class="item item-text-wrap" data-role="main">
            <?php
            $pName = strtolower($inputAll['pName']);
            if ($pName == 'walmart') {
            ?>
                Did you find the items you were looking for?

            <?php }else{ ?>
                Would you choose to do business with this location again?
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col"><button class="button button-block button-assertive2" onclick="submitNo()">No</button></div>
        <div class="col"><button class="button button-block button-balanced2" onclick="submitYes()">Yes</button></div>
    </div>
    <div class="row">
        <div class="col">
            <a id="cancelButton" href="/location" class="button  button-clear button-assertive " style="position: absolute; right: 28px;">
                <span style="color:#000;">Finish Later</span> 
                <i style="color:red;margin-left:8px;" class="icon ion-ios7-close-outline"></i>
            </a>
        </div>
    </div>
    <form action="feedback" method="POST" id="submitForm">
        <input type="hidden" name="q1" id='q1' value=""/>
        <input type="hidden" name="pId" value="{{ $inputAll['pId'] }}" />
        <input type="hidden" name="pName" value="{{ $inputAll['pName'] }}" />
        <input type="hidden" name="pAddress" value="{{ $inputAll['pAddress'] }}" />
        <input type="hidden" name="question" value="2" />
        <?php
            error_reporting(0);
            if(Auth::check()){
                $feedbackType = 'consumer';
            }
            else if(isset($_COOKIE['fbId'])){
                $feedbackType = 'fb';
            }
            else if(isset($_COOKIE['guestId'])){
                $feedbackType = 'guest';
            }
        ?>
        <input type="hidden" name="feedbackType" value="{{ $feedbackType }}" />
        <input type="hidden" name="iconsrc" value="{{ $inputAll['iconsrc'] }}" />
    </form>
    <?php setcookie('inputAll', '', time()-3600); 
            unset($_COOKIE['inputAll']);?>
@stop
