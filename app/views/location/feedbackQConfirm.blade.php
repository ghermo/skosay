<!DOCTYPE html>
<html>
<head>
    <title>Skosay</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    

    {{HTML::style('css/css.css');}}
    {{HTML::style('css/ionic.css');}}


    {{HTML::script('js/jquery-1.11.0.min.js');}}
    {{HTML::script('js/ionic.bundle.js');}}
    {{HTML::script('js/javascript.js');}}
    <!--    <script src="http://code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.js"></script>-->

    <!-- Needed for Cordova/PhoneGap (will be a 404 during development) -->

</head>




<body style="background-color:#1f8fe2">

<div style="margin-top: 5em; ">
	<center><h2><font color="white">Thank you <br>for saying it <br>with</font></h2></center>
	<br>
	<center><img src="images/skopic3.png" style="width: 90%; "></center>
</div>
<div style="margin-top: 7em; ">
	<center><h2><font color="white">Interested in hearing <br>back and engaging?</font></h2></center>
</div>
 <div class="row">
        <div class="col"><a href="location" class="button button-block button-assertive" >No</a></div>
        <div class="col"><a href="#" class="button button-block button-balanced" >Yes</a></div>
</div>

</body>
</html>


