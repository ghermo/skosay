
@extends('layouts.default')
<?php
$feedpage = 1;
$page = 1;
?>
<head>
    <link rel="alternate" type="application/rss+xml" title="Latest snippets from Bootsnipp.com" href="http://bootsnipp.com/feed.rss">
   <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootsnipp.css">
    <link rel="stylesheet" href="css/ladda-themeless.min.css">
</head>

<script>
    function submitYes()
    {
        document.getElementById('q3').value = 'YES';
        $('#submitForm').submit();
    }
    function submitNo()
    {
        document.getElementById('q3').value = 'NO';
        $('#submitForm').submit();
    }
    function validateInput()
    {
        if(document.getElementById('comment').value.length != 0)
            document.getElementById('submitButton').disabled = false;
        else
            document.getElementById('submitButton').disabled = true;
    }
</script>
@section('content')
<!--    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>-->
<!--    <script src="http://code.jquery.com/mobile/1.4.3/jquery.mobile-1.4.3.min.js"></script>-->
<!--    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">-->
<!--    <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>-->
<input type="hidden" id="hiddenId" value="<?php if(Session::has('msg')){echo 'true';}else{echo 'false';} ?>"/>
<div class="list" style="text-align: center;">
    <a class="item" href="#" style="padding:8px; background: #F8F8F8 ;">
        <h2>{{{ str_replace('_',' ',$inputAll['pName']) }}}</h2>
        <p>{{{ str_replace('_',' ',$inputAll['pAddress']) }}}</p>
    </a>
</div>
<center>
<div>
    <div class="stepwizard">
                <div class="stepwizard-row">
                     <div class="stepwizard-step">
                         <button type="button" class="btn btn-default btn-circle" disabled="disabled"><span class="ion-ios7-checkmark-empty check-icon"></span></button>

                    </div>
                <div class="stepwizard-step">
                         <button type="button" class="btn btn-default btn-circle" disabled="disabled"><span class="ion-ios7-checkmark-empty check-icon"></span></button>
                </div>
                <div class="stepwizard-step">
                         <button type="button" class="btn btn-default btn-circle" disabled="disabled"><span class="ion-ios7-checkmark-empty check-icon"></span></button>

                </div>
                <div class="stepwizard-step">
                         <button type="button" class="btn btn-success btn-circle">4</button>

                </div>
                </div>
    </div>
</div>
</center>
<form action="feedback" method="POST" id="submitForm" enctype="multipart/form-data">
<div style="margin:12px 10px 0px 10px;">
    <div data-role="main" style="padding:8px;">
        Here's your chance to tell us what you think...
        <div class="card" style="margin:5px 10px;">
            <div class="item item-text-wrap" style="padding:8px;">
                    <textarea placeholder="We value your opinion. You need to fill this out to submit your feedback." onkeyup="validateInput()" maxlength="255" rows="4" id="comment" name="comment" value="{{{ Input::old('comment') }}}"></textarea>
            </div>
        </div>
    </div>
</div>

<div class="row">
<!--    <div class="col"><button class="button button-block button-stable ion-camera"></div>-->
    <style>
    .ion-ios7-close-outline:before{
        font-size: 24px;
    }
    .button .icon:before, .button.icon:before{
        font-size: 34px;
        position: absolute;
        top: 3px;
        left: 11px;
    }
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>

    <div style="width:25%; margin-left: 20px;">
        <div class="fileUpload">
            <button class="button button-stable icon ion-camera" style="width:10px;" value="Attach or upload photo" type="button">
            </button>
            <input type="file" class="upload" name="file" id="file" accept="image/*" capture="camera">
        </div>
    </div>
    <div style="width:72%;"><button disabled="true" id="submitButton" class="button button-block button-balanced2" onclick="submitYes()">Send</button></div>

</div>
<div class="row">
    <div class="col"></div>
    <div class="col ">
        <a id="cancelButton" href="/location" class="button  button-clear button-assertive " style="position: absolute; right: 28px;">
            <span style="color:#000;">Finish Later</span> 
            <i style="color:red;margin-left:8px;" class="ion-ios7-close-outline"></i>
        </a>
    </div>
</div>
    <input type="hidden" name="q1" id='q1' value="{{ $inputAll['q1'] }}"/>
    <input type="hidden" name="q2" id='q2' value="{{ $inputAll['q2'] }}"/>
    <input type="hidden" name="q3" id='q3' value="{{ $inputAll['q3'] }}"/>
    <input type="hidden" name="pId" value="{{ $inputAll['pId'] }}" />
    <input type="hidden" name="pName" value="{{ $inputAll['pName'] }}" />
    <input type="hidden" name="pAddress" value="{{ $inputAll['pAddress'] }}" />
    <input type="hidden" name="question" value="done" />
    <input type="hidden" name="feedbackType" value="{{ $inputAll['feedbackType'] }}" />
    <input type="hidden" name="iconsrc" value="{{ $inputAll['iconsrc'] }}" />
</form>
@stop
