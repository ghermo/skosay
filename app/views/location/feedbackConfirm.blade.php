<!DOCTYPE html>
<html>
<head>
    <title>Skosay</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">

    {{HTML::style('css/css.css');}}
    {{HTML::style('css/ionic.css');}}

    {{HTML::script('js/jquery-1.11.0.min.js');}}
    {{HTML::script('js/ionic.bundle.js');}}
    {{HTML::script('js/javascript.js');}}
    <!--    <script src="http://code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.js"></script>-->
    <!-- Needed for Cordova/PhoneGap (will be a 404 during development) -->
</head>
<body style="background-color:#ffffff">
<!-- #00b9f2 -->
<div style="margin-top: 4.5em;">
    <center>
    <div style="width: 254px;/* padding: 0px; */margin-left: 24px;margin-right: 24px;">
        <h3 style="margin:0;text-align: left;"><font color="#696969">Thank you</font></h3>
        <h3 style="margin:0;text-align: right;"><font color="#696969">for saying it with</font></h3>
    </div>
    </center>
    <center><img src="images/skopicdark.png" style="width:230px;"></center>
</div>

<!--<p style="text-align:center;margin-top:3px;color:#474747;margin-left:50px;margin-right:50px;padding:3px;">
    @if($storeCust == 0)
        This business is not part of our network just yet, but the Skosay team will relay your valued feedback to the business.
    @else
        We will pass your feedback along and the manager may get in touch with you directly.
    @endif
</p>-->
<center>
<!--class="tip-box"--> 
<div class="center-justified" style="margin-top:1em; width: 254px;/* padding: 0px; */margin-left: 24px;margin-right: 24px;">
    <h3 style="margin:0;text-align: center;">
    <font color="#696969">
     @if($storeCust == 0)
        We value your feedback and are working to get this business added to the Skosay network
     @else
       This business values your feedback
     @endif
    </font>
    </h3>
    
    <!--<p style="color:#474747;text-align:left;font-size:19px;">Tip:</p>
    <p style="color:#474747;text-align:left;margin-top:1px;">View Messages you left in the Messages section.</p>
     {{ HTML::image('images/menu.PNG' , 'Menu', array('style' => 'width:250px;margin-top:-11px;')) }}
    <p style="color:#474747;text-align: left;margin-top:-11px;">Find more tips by tapping on the <i style="color:gray;" class="ion-navicon-round"> </i> icon and Tutorial section. </p>-->
</div>
</center>
   
 
  @if($storeCust == 0)
    <div style="margin:24px;margin-top:1em;">
    <a href="location" class="button button-block button-positive" style="background-color:#CF8344;border-radius: 4px;">
        OK
    </a>
    </div>
  @else
    <div style="margin:24px;margin-top:4em;">
    <a href="location" class="button button-block button-positive" style="background-color:#CF8344;border-radius: 4px;">
        OK
    </a>
    </div>
  @endif  




</body>
</html>



