@extends('layouts/layout')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">SKOSAY Feedback Form</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row" ng-controller="ApplicationController">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Fill up the form below.
            </div>
            {{Form::open(['url' => 'feedback'])}}
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <h2>User Details</h2>
						
									
			<div class="form-group">
            
            <p>Question:1 test ...</p>

            <label class="radio-inline">
                <input type="radio" name="q1" value="yes" checked />Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="q1" value="no" />No
            </label>

			</div>
			
			<div class="form-group">
            
            <p>Question:2 test ...</p>

            <label class="radio-inline">
                <input type="radio" name="q2" value="yes" checked />Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="q2" value="no" />No
            </label>

			</div>
		
		<div class="form-group">
            
            <p>Question:3 test ...</p>

            <label class="radio-inline">
                <input type="radio" name="q3" value="yes" checked />Yes
            </label>

            <label class="radio-inline">
                <input type="radio" name="q3" value="no" />No
            </label>


            <p class="help-block">The client's user type. This will determine what the client can perform in the system.</p>
        </div>
                        <div class="form-group {{ errors_check('username', $errors) }}">
                            <label for="username">Name</label>
                            <input class="form-control" placeholder="Name" type="text" name="username" id="username" value="{{{ Input::old('username') }}}">
                            {{ errors_for('username', $errors) }}
                        </div>
                        <div class="form-group {{ errors_check('email', $errors) }}">
                            <label for="email">Email <small>required</small></label>
                            <input class="form-control" placeholder="Email" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                            {{ errors_for('email', $errors) }}
                        </div>
                        <div class="form-actions form-group text-center">
                          <button type="submit" class="btn btn-primary">Submit Application</button>
                        </div>
                    </div>

                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>

@stop