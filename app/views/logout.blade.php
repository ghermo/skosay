@extends('layouts.default')

@section('content')

<div style="padding:16px;">
	<center>
		<img src="../images/banner.png" style="width:200px;padding-right:32px;">
	</center>
</div>
<div class="card">
  <div class="item item-text-wrap">
	<center>
		<h4 style="font-weight:bold; color:red;">You are logging out!</h4>
		<p style="font-weight:bold; color:red;">Thank you for using Skosay</p>
		<p style="font-weight:bold;"><!--Page will redirect in-->
			<span id="countdown" class="timer"></span>
		</p>
	</center>
  </div>
</div>


<script>
    var seconds = 0;
    function secondPassed() {
        var minutes = Math.round((seconds - 30)/60);
        var remainingSeconds = seconds % 60;
        if (remainingSeconds < 10) {
            remainingSeconds = "0" + remainingSeconds;
        }
        document.getElementById('countdown').innerHTML = minutes + ":" + remainingSeconds;
        if (seconds == 0) {
            clearInterval(countdownTimer);
            document.getElementById('countdown').innerHTML = "";
            window.location = "{{ $url }}";
        } else {
            seconds--;
        }
    }

    var countdownTimer = setInterval('secondPassed()', 1000);
</script>

@stop