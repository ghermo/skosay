@extends('layouts.default')
@section('content')


{{HTML::style('css/term.css')}}

<?php $page=1;?>
<div id="wrapper">
	<div>
	  <h1 id="title">PRIVACY POLICY</h1>
	</div>

	<p class="terms">
		Thank you for using SKOSAY, a service provided by Red J, Inc., an Arkansas corporation (“SKOSAY,” “Red J,” “we,” “our” or “us”). This Privacy Policy explains the agreement you make with Red J about how we collect, store, use and share information about our Users, including personally identifiable information (“Personal Information”). Capitalized terms that are not defined in this Privacy Policy have the meaning given to them in our Terms of Use (the “Terms”).
	</p>
	<br>
	<p class="terms">
		PLEASE REVIEW THIS PRIVACY POLICY CAREFULLY. BY USING THE SERVICES YOU AGREE TO BE BOUND BY EVERYTHING IN THE PRIVACY POLICY. IF YOU DO NOT AGREE, YOU SHOULD STOP USING THE SERVICES IMMEDIATELY.
	</p>
	<br>
	<br>
	<div><h4 class="fHead"> 1. Summary </h4></div>
	<p class="inline">
		We collect information from and about you when you access and use the Services, including Personal Information. We store and use this information to improve our Services, to communicate with you, to respond to requests about the Services from Users, to manage User accounts and for other uses as set out in the Terms and this Privacy Policy. In some circumstances, we share information with third parties. We have attempted to provide clear explanations of all of our information policies in this Privacy Policy so you can make meaningful decisions about whether to use the Services. If you do not agree to our information policies and practices, you cannot use the Services. If you have questions about our information policies and practices, please contact us at info@skosay.com.
	</p>
	<br>

	<div><h4 class="fHead"> 2. How We Collect and Use Information </h4></div>
	<div><h4 class="fHead"> A. Personal Information </h4></div>
	<p class="inline">
		We only collect Personal Information that you voluntarily choose to provide through your Account. We will be clear about what Personal Information we are requesting and collecting, and allow you to choose whether or not to provide that information. If you do not want us to have particular information about you, then do not publish that information on your Account or post that information on the Services. We will not collect or scrape additional Personal Information from your computer, contacts, email or other personal sources without your express permission.
		<br><br>
		We collect and store information about your location. We request that you voluntarily provide us with your location through your Account profile. We may also convert your IP address into an approximate location for our use as anonymous, generalized information (see below for more information).
		<br><br>
		Please note that some of the Personal Information we request is required to activate your Account, and some of the Personal Information we request is required to enjoy the full benefit of the Services. Accordingly, your refusal to provide certain Personal Information may prevent or limit your use and enjoyment of the Services. 
		<br><br>
		You may access, edit and delete Personal Information through your Account.  
		<br><br>
		By default, some of the information you post on or through Account may be displayed publicly on the Services, and may be collected and used by other Users or the public. If you do not want other Users or the public to have particular information, then do not publish that information on the Services.

	</p>
	<br>

	<div><h4 class="fHead"> B. Non-Personal Information </h4></div>
	<p class="inline">
		We collect and store information about how Users use the Services through tracking, tags, “cookies,” and other identification technology. This information allows us to provide Services that remember your preferences and also allows us to improve and update the Services based on aggregated usage information. The information we collect by these methods will not be stored or used by us as personally identifiable information. Disabling the use of these technologies on the Services may prevent or limit your use and enjoyment of the Services.
		<br><br>
		We also collect, store, and use anonymous, aggregate User information that we create in part from anonymized Personal Information and usage records and patterns. This information includes, but is not limited to, geographic and demographic information about Users, and search and usage patterns from Users. We only use this information in ways that protect your anonymity. 
	</p>
	<br>

	<div><h4 class="fHead"> 3. Sharing of Personal Information by SKOSAY; Third Parties </h4></div>
	<p class="inline">
		We only share information about our Users to improve the Services with better updates, better communication, and better responsiveness.
		<br><br>
		We will not share or sell Personal Information with third parties, unless the third party relationship is for the limited purpose of improving the Services provided to you and the particular third party expressly agrees to honor and follow our information, privacy, and security policies as set out in the Terms and this Privacy Policy. We will not sell or share your personal information for unrestricted third party use. 
		<br><br>
		We may at our discretion share anonymous, aggregate information about our Users with our partners and other people and businesses we do business with. We only provide this information in ways that protect your anonymity.
		<br><br>
		As noted above, certain information you make available on the Services is visible and available to other Users and the public. Sharing information from or features of the Services (on Facebook or Twitter, for example) will also increase the public visibility and availability of that information. Because of this, you understand that certain information may be indexed and visible through search engines and otherwise made widely available to the public.
		<br><br>
		SKOSAY uses third-party providers to process payments and fees on the Services. You acknowledge and agree that you will also be subject to the third-party terms and conditions associated with those third-party services if you choose to use those features. Those terms and conditions may be materially different than our policies, especially with regard to information privacy and security. You specifically acknowledge that you understand that the third-party terms and conditions will apply to your financial information to the extent that information is collected to enable and process the required payments on the Services.
		<br><br>
		We may choose to sell our company to a third party in the future. User information, including Personal Information, is typically one of the assets that is transferred to the purchasing party in these transactions. You acknowledge and agree that the purchasing or acquiring party may continue to use User information, including Personal Information, as set out in this Privacy Policy.
		<br><br>
		We will attempt to protect your information from disclosure in most instances as a general policy. However, we reserve the right to release any User information that we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process, or governmental request; (ii) enforce these Terms, including investigation of potential violations; (iii) detect, prevent, or otherwise address fraud, security, or technical issues, (iv) respond to User support requests; and (v) protect the rights, property, or safety of SKOSAY, our Users and the public.
	</p>
	<br>

	<div><h4 class="fHead"> 4. Privacy of Children </h4></div>
	<p class="inline">
		SKOSAY values the privacy of children. To protect that privacy, we take the following steps: (i) use of the Services is not allowed for children under the age of 13, and (ii) use of the Services between the ages of 13 and 18 requires a parent or legal guardian to enter into all required agreements on the user’s behalf, which will include financial responsibility. Any attempt by a child under the age of 13 to use the Services will be void. We will promptly delete all personal information associated with an Account on receipt of credible notice that the Account contains personal information about a child under age 13. If you believe that we might have any information from or about a child under 13, please contact us at info@skosay.com.
	</p>
	<br>

	<div><h4 class="fHead"> 5. Security </h4></div>
	<p class="inline">
		SKOSAY cares about the security of Accounts and Personal Information. We have taken voluntary steps to protect the security of your information under the circumstances we control. For example, information we collect and store is password protected, both at your Account level and at our administrator level. We may implement additional security features in the future.  However, we do not guarantee, represent, or warrant that we can or will prevent unauthorized disclosure of your Account and Personal Information. By using the Services, you agree that you provide your personal information and content at your own risk. You are solely responsible for maintaining the security of your Account and your password. You are solely responsible for notifying SKOSAY in writing of any unauthorized use of your Account.
	</p>
	<br>

	<div><h4 class="fHead"> 6. Communications from SKOSAY </h4></div>
	<p class="inline">
		You will automatically receive various messages from us, and you may receive communications from our partners (including advertising and marketing messages). You may change or opt out of certain communications through your Account Settings page. However, in some instances, we may need to provide you with service announcements and administrative messages. These particular communications are a necessary part of the Services. You cannot opt-out from receiving these communications.
	</p>
	<br>

	<div><h4 class="fHead"> 7. This Privacy Policy Does Not Cover Independent Actions of Third Parties and Users </h4></div>
	<p class="inline">
		SKOSAY relies on third parties for parts of the Services. You may also see information about and links to third party websites, applications, and resources on the Services that are posted by us or other Users. By using the Services, you acknowledge that we have no control over the actions of third parties that we do not own, employ, or control. This includes the independent actions of our Users on and through the Services. This Privacy Policy does not apply to those third parties, third party services, or User activities. You should carefully review the policies of any third party services you access through our Services. You agree that SKOSAY will not be liable for any consequence, loss, or damage arising (directly or indirectly) for the policies and practices of other Users and third party individuals, organizations, or companies.
	</p>
	<br>

	<div><h4 class="fHead"> 8. What Happens to Information When You Delete Your Account </h4></div>
	<p class="inline">
		If you choose to terminate your Account and end use of the Services, you acknowledge that we may not be able to fully delete the information you have posted on the Services. We maintain cached information and backup copies in the ordinary course of business. In addition, the public nature of some information on the Services means that parts of your information may have been indexed or captured by other Users or third parties.
	</p>
	<br>

	<div><h4 class="fHead"> 9. Changes to the Privacy Policy </h4></div>
	<p class="inline">
		We may revise this Privacy Policy from time to time without notice to you. The most current version will always be at the following link: www.skosay.com. If we believe in our sole discretion that a revision is material, we may also notify you by e-mail at the email address you provide for your Account. By continuing to use the Services after revisions are effective, you agree to and will be bound by the revised Privacy Policy.
	</p>
	<br>

	<p class="inline">
		We may revise this Privacy Policy from time to time without notice to you. The most current version will always be at the following link: www.skosay.com. If we believe in our sole discretion that a revision is material, we may also notify you by e-mail at the email address you provide for your Account. By continuing to use the Services after revisions are effective, you agree to and will be bound by the revised Privacy Policy.
	</p>
	<br><br><br><br>
	<p class="terms">
		If you have any questions about the Privacy Policy, please contact us at info@sksosay.com. Feedback, comments, or suggestions you provide about SKOSAY or the Services ARE entirely voluntary. We will be free to use all feedback, comments, or suggestions without obligation or compensation to you.
	</p>
	<br><br>
	<p class="terms">
		<strong>
		By registering for and/or using Services in any manner, including but not limited to visiting or browsing the SKOSAY mobile application or website, you agree to and will be bound by all of the terms and conditions contained in this Privacy Policy, and all additional terms, conditions, rules, policies, and/or procedures that may be published by SKOSAY from time to time on the Services, each of which is incorporated by reference and may be updated by SKOSAY at any time without notice to you. This Privacy Policy applies to all users of the Services without exception.
		</strong>
	</p>
	<br><br>
	<p class="terms">
		<strong>
		Effective: March 3rd, 2015
		</strong>
	</p>

</div>

<br><br><br><br>


@stop
@section('footer')
    <?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop
