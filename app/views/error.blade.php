@extends('layouts.default')
@section('content')
<br>
<br>
<h1 style='color:#00b9f2;font-weight:bold;'>404</h1>
<h2 style='color:#00b9f2;font-weight:bold;'>Page Not Found</h2>
<h4 style='color:#00b9f2;font-weight:bold;'>We're sorry the page you are <br> looking for is not available</h4>
<br><br>

<h4><a class="button button-outline" style='text-decoration:none;color:gray;' href="/">Go back to Home Page</a></h4>

@stop