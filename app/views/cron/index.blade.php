<?php

    DB::table('users')
        ->update(array('notiFlag' => 0));
	
	//Twilio::message('+639061539155', 'Skosay! Say it like it is! -GM');
	$users = DB::table('users')
            ->join('user-groups','user-groups.userid','=','users.id')
            ->join('groups','groups.id','=','user-groups.groupid')
            ->select('users.id as a','users.mobile as b','groups.id as c')
            ->where('type','manager')
            ->get();



        foreach ($users as $user) {

 
                $count2 = DB::table('messages')
                     ->join('issues', 'messages.issueid', '=', 'issues.id')
                     ->join('stores','stores.id','=','issues.storeid')
                     ->join('store-groups','store-groups.storeid','=','stores.id')
                     ->join('groups','groups.id','=','store-groups.groupid')
                     ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                    ->where('user-groups.userid', $user->a)
                    ->where('groups.id',$user->c)
                     ->where('issues.status','new')
                     ->where('messages.msgStatus', 'new')
                     ->whereNotIn('messages.userid', [$user->a])->count();
            
                $count = DB::table('messages')
                    ->join('issues', 'messages.issueid', '=', 'issues.id')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                        ->join('department-groups', 'department-groups.deptid', '=','departments.id')
                        ->join('groups', 'groups.id', '=','department-groups.groupid')
                        ->join('user-groups', 'user-groups.groupid', '=','groups.id')
                        ->where('user-groups.userid', $user->a)
                    ->where('messages.msgStatus', 'new')
                    ->where('issues.status','new')
                    ->whereNotIn('messages.userid', [$user->a])->count(); 
                
                $num = $user->b;

                $notiFlag = DB::table('users')->where('id',$user->a)->pluck('notiFlag');

                $baseURL = URL::to('/administrator');

                if ($count != 0 || $count != NULL) {
                    //echo $user->a.' = '.$count.'='.$user->c.'='.$num.'<br>';
                    if ($num != 0 && $notiFlag == 0) {
                        Twilio::message( $user->b, 'Hi Manager, You have new message click the link to check your messages. '.$baseURL);
                        DB::table('users')
                            ->where('id', $user->a)
                            ->update(array('notiFlag' => 1));
                    }
                    
                }elseif ($count2 != 0 || $count != NULL) {
                    //echo $user->a.' = '.$count2.'='.$user->c.'='.$num.'<br>';
                    if ($num != 0 && $notiFlag == 0) {
                        Twilio::message( $user->b, 'Hi Manager, You have new message click the link to check your messages. '.$baseURL);
                        DB::table('users')
                            ->where('id', $user->a)
                            ->update(array('notiFlag' => 1));
                    }
                }

                
            



        }


/*
    if ($users2 == null || $users2 == 0) {
        foreach ($users2 as $user2) {
            if ($user2->notiFlag == 0) {
                Twilio::message( $user2->mobile, "Skosay Notify:\nHi ".$user2->name.". You have unread feedbacks waiting for you.\nClick the link below\nhttp://app.skosay.com/historypage");
            }
        }
    }

    if ($users == null || $users == 0) {
        foreach ($users as $user) {
            if ($user->notiFlag == 0) {
                Twilio::message( $user->mobile, "Skosay Notify:\nHi ".$user->name.". You have unread feedbacks waiting for you.\nClick the link below\nhttp://app.skosay.com/historypage");
            }
        }
    }
*/


?> 