@extends('layouts.default')
@section('content')
<?php $page = 1; ?>
<style>
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script>
    function initialize(){
        var pyrmont = new google.maps.LatLng(lat, long);
//        var pyrmont = new google.maps.LatLng(14.5667, 121.0333);
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: pyrmont,
            zoom: 17
        });
        var request = {
            location: pyrmont,
            radius: 500,
            types: ['restaurant','store']
        };
        placesList = document.getElementById('places');
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, callback);
    }
    function callback(results, status, pagination) {
        if (status != google.maps.places.PlacesServiceStatus.OK) {
            return;
        } else {
            createMarkers(results);

            if (pagination.hasNextPage) {
                var moreButton = document.getElementById('more');

                moreButton.disabled = false;

                google.maps.event.addDomListenerOnce(moreButton, 'click',
                    function() {
                        moreButton.disabled = true;
                        pagination.nextPage();
                    });
            }
        }
    }
    function createMarkers(places) {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++)
        {
            var pName = place.name;
            pName = pName.replace(' ','_');
            placesList.innerHTML += '<a href="feedback/'+pName+'/'+place.id+'" style="text-decoration: none;"><li class="item">' + place.name+ '</li></a><input type="hidden" name="name" id="'+place.name+'" value="' + place.id+ '">';

            bounds.extend(place.geometry.location);
        }
        map.fitBounds(bounds);
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<style>
    h2 {
        font-size: 22px;
        margin: 0 0 5px 0;
    }
    ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
        height: 321px;
        width: 200px;
        overflow-y: scroll;
    }
    li {
        background-color: #f1f1f1;
        padding: 10px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: scroll;
    }
    li:nth-child(odd) {
        background-color: #fcfcfc;
    }
    #more {
        width: 100%;
        margin: 5px 0 0 0;
    }
</style>
    <div id="results" class="list" onload="location.reload()">
        <button id="more" class="button button-block button-calm">More results</button>
        <div class="list" id="places"></div>
    </div>
<div id="map-canvas" style="visibility: hidden;"></div>
@stop
@section('footer')
<a class="tab-item active" href="/location" style="color:white">
    <i class="icon ion-location"></i>
</a>
<a class="tab-item" href="/message" style="color:white">
    <img src="images/history.png" style="width: 32px; margin-top: 6px;">
</a>
<a class="tab-item" href="/search" style="color:white">
    <i class="icon ion-search"></i>
</a>

@stop