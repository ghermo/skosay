

@extends('layouts.default')

@section('content')

<div class="container" style="max-width:445px; margin-top: 56px;">
	<h2 class="form-signin-heading">Sign Up</h2>
    {{ Form::open (['route'=>'users.store']) }}
		<input type="email" class="form-control" placeholder="Email address" required="" autofocus="" name="email">
        <input type="password" class="form-control" placeholder="Password" required="" name="password">
		
        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Sign Up" style="margin-top:10px;" name="submit">
    {{ Form::close() }}
</div>

@stop