<!doctype html>
<html lang="en">
<?php 
    $ctr = App::environment();
    $domain =  DB::table('domain')->where('appEnv',$ctr)->first();
?>
<head>

	<link rel='shortcut icon' type='image/png' href='{{URL::to('/images/logo.png')}}' />
    <link rel="icon" href="{{URL::to('/images/logo.png')}}" type="image/png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::to('/images/skosay57.png')}}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::to('/images/skosay72.png')}}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::to('/images/skosay114.png')}}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::to('/images/skosay144.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{URL::to('/images/skosay57.png')}}" />

    {{HTML::style('css/css.css');}}
    {{HTML::style('css/ionic.css');}}

    {{HTML::script('js/jquery-1.11.0.min.js');}}
    {{HTML::script('js/ionic.bundle.js');}}

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title>Skosay</title>

	<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("b9cf60e9f0fdfbf5e4e13d4a45cc96e8");</script><!-- end Mixpanel -->

    <script src='//cdn.goroost.com/roostjs/63b3b65717a046ce82cb9ebf6e960036' async></script>
</head>

<body style="background-color:{{$domain->headColor}}">
	<div class="content">
    @yield('content')
	</div>
</body>
</html>