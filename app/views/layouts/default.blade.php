<!DOCTYPE html>
<html>
<head>
    <?php
        $ctr = App::environment();
        $domain =  DB::table('domain')->where('appEnv',$ctr)->first();
    ?>
    <title>{{$domain->title}}</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <!-- set a web app capable website -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">

	{{HTML::style('css/addtohomescreen.css');}}
	{{HTML::script('js/addtohomescreen.js');}}
    {{HTML::style('css/css.css');}}
    {{HTML::style('css/ionic.css');}}
    {{HTML::style('css/demo.css');}}
    {{HTML::style('css/jquery.mmenu.all.css');}}
    {{HTML::script('js/jquery-1.11.0.min.js');}}

    <link rel='shortcut icon' type='image/png' href='{{URL::to('/images/logo.png')}}' />
    <link rel="icon" href="{{URL::to('/images/logo.png')}}" type="image/png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::to('/images/skosay57.png')}}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::to('/images/skosay72.png')}}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::to('/images/skosay114.png')}}" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::to('/images/skosay144.png')}}" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{URL::to('/images/skosay57.png')}}" />

    <?php

    $connected = @fsockopen("www.google.com", 80);
                                                //website, port  (try 80 or 443)
    if ($connected){
    ?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="https://www.google.com/jsapi"></script>
    <script src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>

    <?php
    }else{
    ?>
    <script> alert("Please check your internet connection or enable your Location Services"); </script>
    <?php
    }
    ?>



    {{HTML::script('js/googlejsapi.js');}}
    {{HTML::script('js/jquery-1.11.0.min.js');}}
    {{HTML::script('js/jquery.mmenu.min.all.js');}}
    {{HTML::script('js/javascript.js');}}

    <script type="text/javascript">
        $(function() {
            $('nav#menu').mmenu();
        });
    </script>

    <script>
    (function () {
    'use strict';

    var isCommonjs = typeof module !== 'undefined' && module.exports;
    var keyboardAllowed = typeof Element !== 'undefined' && 'ALLOW_KEYBOARD_INPUT' in Element;

    var fn = (function () {
        var val;
        var valLength;

        var fnMap = [
            [
                'requestFullscreen',
                'exitFullscreen',
                'fullscreenElement',
                'fullscreenEnabled',
                'fullscreenchange',
                'fullscreenerror'
            ],
            // new WebKit
            [
                'webkitRequestFullscreen',
                'webkitExitFullscreen',
                'webkitFullscreenElement',
                'webkitFullscreenEnabled',
                'webkitfullscreenchange',
                'webkitfullscreenerror'

            ],
            // old WebKit (Safari 5.1)
            [
                'webkitRequestFullScreen',
                'webkitCancelFullScreen',
                'webkitCurrentFullScreenElement',
                'webkitCancelFullScreen',
                'webkitfullscreenchange',
                'webkitfullscreenerror'

            ],
            [
                'mozRequestFullScreen',
                'mozCancelFullScreen',
                'mozFullScreenElement',
                'mozFullScreenEnabled',
                'mozfullscreenchange',
                'mozfullscreenerror'
            ],
            [
                'msRequestFullscreen',
                'msExitFullscreen',
                'msFullscreenElement',
                'msFullscreenEnabled',
                'MSFullscreenChange',
                'MSFullscreenError'
            ]
        ];

        var i = 0;
        var l = fnMap.length;
        var ret = {};

        for (; i < l; i++) {
            val = fnMap[i];
            if (val && val[1] in document) {
                for (i = 0, valLength = val.length; i < valLength; i++) {
                    ret[fnMap[0][i]] = val[i];
                }
                return ret;
            }
        }

        return false;
    })();

    var screenfull = {
        request: function (elem) {
            var request = fn.requestFullscreen;

            elem = elem || document.documentElement;

            // Work around Safari 5.1 bug: reports support for
            // keyboard in fullscreen even though it doesn't.
            // Browser sniffing, since the alternative with
            // setTimeout is even worse.
            if (/5\.1[\.\d]* Safari/.test(navigator.userAgent)) {
                elem[request]();
            } else {
                elem[request](keyboardAllowed && Element.ALLOW_KEYBOARD_INPUT);
            }
        },
        exit: function () {
            document[fn.exitFullscreen]();
        },
        toggle: function (elem) {
            if (this.isFullscreen) {
                this.exit();
            } else {
                this.request(elem);
            }
        },
        onchange: function () {},
        onerror: function () {},
        raw: fn
    };

    if (!fn) {
        if (isCommonjs) {
            module.exports = false;
        } else {
            window.screenfull = false;
        }

        return;
    }

    Object.defineProperties(screenfull, {
        isFullscreen: {
            get: function () {
                return !!document[fn.fullscreenElement];
            }
        },
        element: {
            enumerable: true,
            get: function () {
                return document[fn.fullscreenElement];
            }
        },
        enabled: {
            enumerable: true,
            get: function () {
                // Coerce to boolean in case of old WebKit
                return !!document[fn.fullscreenEnabled];
            }
        }
    });

    document.addEventListener(fn.fullscreenchange, function (e) {
        screenfull.onchange.call(screenfull, e);
    });

    document.addEventListener(fn.fullscreenerror, function (e) {
        screenfull.onerror.call(screenfull, e);
    });

    if (isCommonjs) {
        module.exports = screenfull;
    } else {
        window.screenfull = screenfull;
    }
})();

function launchFullScreen(element) {
  if(element.requestFullScreen) {
    element.requestFullScreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullScreen) {
    element.webkitRequestFullScreen();
  }
}



        window.onload = function()
        {
            var c = document.getElementById("booty");

                // go full-screen
                if (c.requestFullscreen) {
                  c.requestFullscreen();
                }

            if(window.location.href.toString().split(window.location.host)[1] == '/login')
            {
                document.getElementById('logoutButton').style.display = 'none';
            }
        }

    </script>

    <!-- start Mixpanel -->
    <script type="text/javascript">
        (function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("b9cf60e9f0fdfbf5e4e13d4a45cc96e8");
//        Jan Sarmiento Token (For testing) : 9fa2ed6fa1093738f38a2a981f422cf3
//        Default Token (JC) : b9cf60e9f0fdfbf5e4e13d4a45cc96e8
    </script><!-- end Mixpanel -->

    <script type="text/javascript">
  $(document).ready(function() {
      $('body').css('touch-action', 'auto');
  });
</script>

</head>
<body id="booty" ng-app="Skosay" <?php if (isset($chat)) { ?> style="touch-action:none;background-color: white" <?php } ?> style="background-color:white;">

<div id="page">
<div class="header" style="background-color:{{$domain->headColor}};position:fixed;overflow: hidden; z-index:3;">

    <?php if(isset($bkBtn)){ 
        if($bkBtn == 1){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='/message'"></a>
    <?php }else if($bkBtn == 2){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='/issues/{{$issid}}'"></a>
    <?php }else if($bkBtn == 3){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='../historypage'"></a>
    <?php }else if($bkBtn == 4){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='../inprogress'" ></a>
    <?php }else if($bkBtn == 5){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='../resolved'" ></a>
    <?php }else if($bkBtn == 6){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='/historydetails/{{$issid}}'"></a>
    <?php }else if($bkBtn == 7){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='/historydetails_inprog/{{$issid}}'"></a>
    <?php } else if($bkBtn == 8){ ?>
        <a class="ion-ios7-arrow-back backBtnMes" onclick="location.href='/historydetails_resolved/{{$issid}}'"></a>
    <?php }  
    }else{ ?>
        @if($domain->appEnv != 'skosay')
        <style>
            .testclass {
              top : 10px;
              border-top: 15px double <?php echo $domain->menuColor; ?>;   
              border-bottom : 15px double <?php echo $domain->menuColor; ?>;
              height : 35px;
            }
        </style>
        @endif
        <a href="#menu" class="header-a testclass"></a>
    <?php } ?>
    <?php
        $join = 0;
        if(Auth::check())
        {
            if(Auth::user()->type == 'admin'){
                $join = DB::table('messages')->whereNotIn('userid', [Auth::user()->id])->where('msgStatus', 'new')->count();
            }
            else if(Auth::user()->type == 'manager')
            {   
                $join = DB::table('messages')
                    ->join('issues', 'issues.id', '=', 'messages.issueid')
                    ->join('stores', 'stores.id', '=', 'issues.storeid')
                    ->join('store-groups', 'store-groups.storeid', '=', 'stores.id')
                    ->join('groups', 'groups.id', '=', 'store-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('messages.msgStatus', 'new')
                    ->whereNotIn('messages.userid', [Auth::user()->id])->count();

                $joinDept = DB::table('messages')
                    ->join('issues', 'issues.id', '=', 'messages.issueid')
                    ->join('departments', 'departments.id', '=', 'issues.isDept')
                    ->join('department-groups', 'department-groups.deptid', '=', 'departments.id')
                    ->join('groups', 'groups.id', '=', 'department-groups.groupid')
                    ->join('user-groups', 'user-groups.groupid', '=', 'groups.id')
                    ->where('user-groups.userid', Auth::user()->id)
                    ->where('messages.msgStatus', 'new')
                    ->whereNotIn('messages.userid', [Auth::user()->id])->count();

                $join = $join + $joinDept;
                /*
                $us = User::find(Auth::user()->id);
                $us->notiFlag = 0;
                $us->save();*/

            }
            else if(Auth::user()->type == 'consumer'){
                $join = DB::table('messages')
                        ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', [Auth::user()
                        ->id])->whereNotIn('messages.userid', [Auth::user()->id])
                        ->where('messages.msgStatus', 'new')->where('messages.msgType', 'reply')->count();
            }
        }
        else
        {
            if(isset($_COOKIE['fbId']))
            {
                $join = DB::table('messages')
                    ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', $_COOKIE['fbId'])->whereNotIn('messages.userid', [$_COOKIE['fbId']])->where('messages.msgStatus', 'new')->where('messages.msgType', 'reply')->count();
            }
            else if(isset($_COOKIE['guestId']))
            {
                $join = DB::table('messages')
                    ->join('issues', 'messages.issueid', '=', 'issues.id')->where('issues.userid', $_COOKIE['guestId'])->whereNotIn('messages.userid', [$_COOKIE['guestId']])->where('messages.msgStatus', 'new')->where('messages.msgType', 'reply')->count();
            }
        }
    ?>
    <div id="notifDiv" style="position:absolute;right:10px;display:none;">
        @if(Auth::check())
            @if(($join != 0) && (Auth::user()->type == 'consumer'))
                <a href="/message" class="badge-header" style="color: #E8E8E8 "><span id="notifcount" class="badge-text-header">{{ $join }}</span></a>
            @elseif(($join != 0) && (Auth::user()->type == 'admin' || Auth::user()->type == 'manager'))
                <a class="badge-header" style="color: #E8E8E8 "><span id="notifcount" class="badge-text-header">{{ $join }}</span></a>
            @endif
        @else
            @if($join != 0 && isset($_COOKIE['fbId']))
                <a class="badge-header" style="color: #E8E8E8 "><span id="notifcount" class="badge-text-header">{{ $join }}</span></a>
            @endif
        @endif
    </div>
        {{ HTML::image($domain->logo , 'Skosay', array('style' => 'width:135px;margin-top:0px;')) }}

</div>
</div>

<div class="content" style="padding-top:40px;">
    @yield('content')
</div>

<?php if(isset($issuespage)){ ?>
    @yield('reply')
<?php }
else if(isset($feedpage)){

}

else {
    ?>
    <br><br>
    <div class="tabs-striped tabs-background-dark tabs-color-assertive">
        <div class="tabs" style="position:fixed;">
            @yield('footer')
        </div>
    </div>
<?php } ?>
<nav id="menu">
    <ul>
        @if(Auth::check())
            @if(Auth::user()->type == 'admin' || Auth::user()->type == 'manager')
                <li><a href="/logout">Sign out</a></li>
            @else
                <li><a href="/tutorial">Tutorial</a></li>
                <li><a href="#">My Account</a>
                    <ul>
                        <li><a href="/terms">Terms and Policies</a></li>
                        <li><a href="/privacypolicy">Privacy Policy</a></li>
                        <li><a href="/changepassword">Change Password</a></li>
                    </ul>
                </li>
                <li><a href="http://www.skosay.com/?view_full_site=true">Desktop Site</a></li>
                <li><a href="/logoutUser">Sign out</a></li>
            @endif
        @elseif(isset($_COOKIE['guestId']) || isset($_COOKIE['fbId']))
            @if (isset($deptCounter))
            <li><a href="../feedback/logoutUser">Sign out Feedback</a></li>
            @else
            <li><a href="/logoutUser">Sign out</a></li>
            @endif
        @endif
    </ul>
</nav>
</body>
@if(Auth::check())
    @if(($join == 0) && (Auth::user()->type == 'consumer'))
        <script>
            setInterval(function(){getCount()},5000);
            function getCount(){
                $.ajax({
                    type: 'GET',
                    url: '/getcount',
                    success: function(count){
                        if(count != 0)
                        {
                            
                            $('#notifDiv').empty().append('<a href="/message" class="badge-header" style="color: #E8E8E8;"><span id="notifcount" class="badge-text-header">'+count+'</span></a>');
                        }
                        else
                        {
                            $('#notifDiv').empty();
                        }
                    }
                });
            }
        </script>
    @elseif(($join == 0) && (Auth::user()->type == 'admin' || Auth::user()->type == 'manager'))
        <script>
            setInterval(function(){getCount()},5000);
            function getCount(){
                $.ajax({
                    type: 'GET',
                    url: '/getcount',
                    success: function(count){
                        if(count != 0)
                        {
                            
                            $('#notifDiv').empty().append('<a class="badge-header" style="color: #E8E8E8;"><span id="notifcount" class="badge-text-header">'+count+'</span></a>');
                        }
                        else
                        {
                            $('#notifDiv').empty();
                        }
                    }
                });
            }
        </script>
@endif
@else
    @if($join == 0 && isset($_COOKIE['fbId']))
        <script>
            mixpanel.track("Signed in with Facebook");
            setInterval(function(){getCount()},5000);
            function getCount(){
                $.ajax({
                    type: 'GET',
                    url: '/getcount',
                    success: function(count){
                        if(count != 0)
                        {
                            
                            $('#notifDiv').empty().append('<a href="/message" class="badge-header" style="color: #E8E8E8;"><span id="notifcount" class="badge-text-header">'+count+'</span></a>');
                        }
                        else
                        {
                            $('#notifDiv').empty();
                        }
                    }
                });
            }
        </script>
    @endif
@endif





<script>
var mySub = document.getElementsByClassName("mm-subopen");
mySub.style.width = "100%";

/*Authored by Niko*/
</script>

</html>