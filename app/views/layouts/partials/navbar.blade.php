<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">SKOSAY <small>Beta</small></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="{{Request::is('/') ? 'active':''}}"><a href="/">Home</a></li>
            <li class="{{Request::is('/about') ? 'active':''}}"><a href="/about">About</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
   
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>