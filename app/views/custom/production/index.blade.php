<?php  $ctr = 'custom.' . App::environment() . '.default' ;
$value = $_SERVER['SERVER_NAME'];

?>
@extends($ctr)

@section('content')

<!-- style only for this page -->
<style>
    .item{
        padding: 12px;
        display:inline;
    }
</style>
<!-- end style -->

<script>
    setTimeout(function()
    {
        $('#button1').click();
    },5000);

    //initialization
    google.maps.event.addDomListener(window, 'load', initialize);

</script>


<?php
$page = 1;
$sc=  DB::select('select latitude,longitude from customers;');
//DB::select('select storename from customers;'); //retrieve skosay sustomers from database
?>
 
<script>
    var data = {{json_encode($sc)}}; //convert to JSON
    sessionStorage.sc= JSON.stringify(data);//store it as string using sessionStorage
</script> 



<div id="gmap_canvas" style="display: none;"></div>
<div class="item item-divider" style="display: none;">
    <input type="hidden" id="lat" name="lat" value="0" />
    <input type="hidden" id="lng" name="lng" value='0' />
    <div id="button1" class="button button-block button-positive" onclick="findPlaces('location'); return false;">Search</div>
</div>


<div class="list" id="demoDay" style="display:none;">
    <form method="POST" action="feedback" id="Demo">
        <input type="hidden" name="question" value="1" />
        <input type="hidden" name="iconsrc" value="images/Demo.PNG" />
        <input type="hidden" name="pId" value="12345" />
        <input type="hidden" name="pType" value="Demo'" />
        <input type="hidden" name="pAddress" value="The ARK Challenge Central Arkansas Demo Day 2014" />
        <input type="hidden" name="pName" value="The ARK Challenge Central Arkansas Demo Day 2014" />
        <button type="submit" class="item item-thumbnail-left" style="text-align:left; padding-left:25px; width: 100%;">    <img src="images/Demo.png" style="width:50px;">
            <img src="images/right.png" style="height:30px;vertical-align:center;" align="right">
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font style="font-size:15px;vertical-align:top;">The ARK Challenge Central Arkansas Demo Day 2014</font>
            <br />
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font color="grey" style="font-size:12px;vertical-align:top;">The ARK Challenge Central Arkansas Demo Day 2014</font>
        </button>
    </form>
</div>


<!-- MODAL -->

@include('layouts.popup')

<!-- END OF MODAL -->

<!-- NSAA Meeting
<div class="list" id="demoDay" style="padding:0px;margin-top:0px;padding-top:0px;margin-bottom:0px;padding-bottom:0px;">
    <form method="POST" action="feedback" id="Demo">
        <input type="hidden" name="question" value="1" />
        <input type="hidden" name="iconsrc" value="images/Info.png" />
        <input type="hidden" name="pId" value="54321" />
        <input type="hidden" name="pType" value="Demo'" />
        <input type="hidden" name="pAddress" value="Natural State Angel Association Meeting" />
        <input type="hidden" name="pName" value="Arvest Ballpark in Springdale" />
        <button type="submit" class="item item-thumbnail-left" style="text-align:left; padding-left:25px; width: 100%;">    
        	<img src="images/Info.png" style="width:50px;">
            <img src="images/right.png" style="height:30px;vertical-align:center;" align="right">
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font style="font-size:15px;vertical-align:top;">Natural State Angel Association Meeting</font>
            <br />
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font color="grey" style="font-size:12px;vertical-align:top;">Arvest Ballpark in Springdale</font>
        </button>
    </form>
</div> -->
<div class="list" id="places" style="padding:0px;padding-top:0px;margin-top:0px;padding-bottom:0px;">
    <center>
        <br><br>
        <h4>Finding locations near you...</h4>
        <h5>Please wait</h5>
        <br>
        <img src="images/loader2.gif" style="width:50px;">
        <br/><br/>
        <img src="images/google.png" style="width:110px;">
    </center>
</div>

<script>
function getText(divname)
{
    var html = document.getElementById('places').innerHTML;
    if(html == 'undefined'){
        oldhtml = $('#places').html();
        var newhtml = '<br><br><center><h4>Sorry! Nothing was found!</h4><h5>Make sure you have internet connection<br/>and your location services are enabled.</h5><br/><img src="images/google.png" style="width:110px;"><br/><span class="ion-refresh" style="font-size: 40px; margin: 0.2em;" onclick="location.reload();"></span><h5>Tap to refresh.</h5><br/></center>';
        $('#places').html(newhtml);

    }
}window.setTimeout(getText, 5200);   
</script>
<div class="list" id="loadDiv" style="margin-top: 4.5em; display:none;">
    <center>
        <h4>Finding locations near you...</h4>
        <h5>Please wait</h5>
        <br>
        <img src="images/loader2.gif" style="width:50px;">
        <br/><br/>
        <img src="images/google.png" style="width:110px;">
    </center>
</div>
<div class="list" id="errorDiv" style="margin-top: 4.5em; display: none;">
    <center>
        <h4>Sorry! Nothing was found!</h4>
        <h5>Make sure you have internet connection<br/>and your location services are enabled.</h5><br/>
        <img src="images/google.png" style="width:110px;"><br/>
        <span class="ion-refresh" style="font-size: 40px; margin: 0.2em;" onclick="location.reload(); document.getElementById('errorDiv').style.display = 'none'; document.getElementById('loadDiv').style.display = 'none';"></span>
        <h5>Tap to refresh.</h5>
        <br/>
    </center>
</div>

@stop
@section('footer')
<?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop