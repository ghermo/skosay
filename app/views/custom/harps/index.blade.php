<?php  $ctr = 'custom.' . App::environment() . '.default' ;?>
@extends($ctr)

@section('content')
<!-- style only for this page -->
<style>
    .item{
        padding: 12px;
        display:inline;
    }
</style>
<!-- end style -->

<script>
    setTimeout(function()
    {
        $('#button1').click();
    },5000);

    

</script>

<?php $page = 1; ?>

<div id="gmap_canvas" style="display: none;"></div>
<div class="item item-divider" style="display: none;">
    <input type="hidden" id="lat" name="lat" value="0" />
    <input type="hidden" id="lng" name="lng" value='0' />
    <div id="button1" class="button button-block button-positive" onclick="findPlaces('location'); return false;">Search</div>
</div>

<?php $stores = DB::table('stores')->get(); ?>


<div class="list" id="demoDay">
    @foreach ($stores as $store)
    <form method="POST" action="feedback" id="{{ $store->storename }}">
        <input type="hidden" name="question" value="1" />
        <input type="hidden" name="iconsrc" value="{{ $store->photo }}" />
        <input type="hidden" name="pId" value="{{ $store->id }}" />
        <input type="hidden" name="pType" value="Walmart'" />
        <input type="hidden" name="pAddress" value="{{ $store->address }}" />
        <input type="hidden" name="pName" value="{{ $store->storename }}" />
        <button type="submit" class="item item-thumbnail-left" style="text-align:left; padding-left:25px; width: 100%;">    
            <img src="{{ $store->photo }}" style="width:50px;">
            <img src="images/right.PNG" style="height:30px;vertical-align:center;" align="right">
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font style="font-size:15px;vertical-align:top;">{{ $store->storename }}</font>
            <br />
            <img src="images/spcr.png" style="width:40px;padding-right:30px;">
            <font color="grey" style="font-size:12px;vertical-align:top;">
                {{ $store->address }}</font>
        </button>
    </form>
    @endforeach
</div>

<div class="list" id="places" style="display: none;"></div>

<div class="list" id="errorDiv" style="margin-top: 4.5em; display: none;">
    <center>
        <h4>Sorry! Nothing was found!</h4>
        <h5>Make sure you have internet connection<br/>and your location services are enabled.</h5><br/>
        <img src="images/google.png" style="width:110px;"><br/>
        <span class="ion-refresh" style="font-size: 40px; margin: 0.2em;" onclick="location.reload(); document.getElementById('errorDiv').style.display = 'none'; document.getElementById('loadDIv').style.display = '';"></span>
        <h5>Tap to refresh.</h5>
        <br/>
    </center>
</div>
<!-- MODAL -->

@include('layouts.popup')

<!-- END OF MODAL -->


@stop

@section('footer')
<?php include(app_path().'/views/layouts/footer.blade.php'); ?>
@stop