<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user-groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('groupid')->references('id')->on('groups')->nullable();
			$table->string('userid')->references('id')->on('users')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user-groups');
	}

}
