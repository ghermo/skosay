<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('fbid')->default('0');
			$table->string('email')->unique();
			$table->string('name')->nullable();
			$table->string('mobile')->nullable();
			$table->string('type');
			$table->integer('secret_id')->nullable();
			$table->string('secret_answer')->nullable();
			$table->string('password');
			$table->string('confirmation')->nullable();
			$table->string('confirmation_code')->nullable();
			$table->string('forgot')->nullable();
			$table->string('forgot_code')->nullable();
			$table->timestamps();
            $table->string('remember_token')->default(true);
            $table->string('district_manager')->nullable();
            $table->integer('notiFlag')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
