<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('department-groups', function(Blueprint $table)
        {
            $table->increments('id');
			$table->string('groupid')->references('id')->on('groups')->nullable();
			$table->string('deptid')->references('id')->on('departments')->nullable();
			$table->timestamps();
        });
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
