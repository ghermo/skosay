<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('store-groups', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('groupid')->references('id')->on('groups')->nullable();
            $table->string('storeid')->references('id')->on('stores')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('store-groups');
	}

}
