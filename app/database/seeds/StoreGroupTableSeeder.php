<?php
class StoreGroupTableSeeder extends Seeder {

    public function run()
    {
       DB::table('store-groups')->delete();

        // Seeding of Groups
        $stgroup = new StoreGroup;
        $stgroup->groupid = '2';
        $stgroup->storeid = '1';
        $stgroup->save();

        $stgroup = new StoreGroup;
        $stgroup->groupid = '3';
        $stgroup->storeid = '2';
        $stgroup->save();

    }
}