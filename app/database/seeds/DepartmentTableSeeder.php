<?php
class DepartmentTableSeeder extends Seeder {

    public function run()
    {
       DB::table('departments')->delete();

        // 2nd St. Rogers 1-3
        $department = new Department;
        $department->name = "Grocery";
        $department->storeid = '6564199c87d584b7562ea486c494bf705fc9c530';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Pharmacy";
        $department->storeid = '6564199c87d584b7562ea486c494bf705fc9c530';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Bakery";
        $department->storeid = '6564199c87d584b7562ea486c494bf705fc9c530';
        $department->qGroupId = '1';
        $department->save();

        
        // Springdale - Gutensohn 4-6
        $department = new Department;
        $department->name = "Grocery";
        $department->storeid = 'f4247ee8325dff8ca981d39d197f62697a98c265';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Pharmacy";
        $department->storeid = 'f4247ee8325dff8ca981d39d197f62697a98c265';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Bakery";
        $department->storeid = 'f4247ee8325dff8ca981d39d197f62697a98c265';
        $department->qGroupId = '1';
        $department->save();


        //Butterfield Coach - Springdale 7-9
        $department = new Department;
        $department->name = "Grocery";
        $department->storeid = '4e2f82e15df904a171819d6d15ed9bdf842a16be';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Pharmacy";
        $department->storeid = '4e2f82e15df904a171819d6d15ed9bdf842a16be';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Bakery";
        $department->storeid = '4e2f82e15df904a171819d6d15ed9bdf842a16be';
        $department->qGroupId = '1';
        $department->save();

        //test for BO's Cafe 10 - 12
        $department = new Department;
        $department->name = "Dept A";
        $department->storeid = '1e6dbb99c2148e8970987847904021cdf5d6315d';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Dept B";
        $department->storeid = '1e6dbb99c2148e8970987847904021cdf5d6315d';
        $department->qGroupId = '1';
        $department->save();

        $department = new Department;
        $department->name = "Dept C";
        $department->storeid = '1e6dbb99c2148e8970987847904021cdf5d6315d';
        $department->qGroupId = '1';
        $department->save();

        //Green Leaf Grill 13-14
        $department = new Department;
        $department->name = "Grill Department";
        $department->storeid = '64d802a654ce1724a445e37bb13c2b77fd8b7d4c';
        $department->qGroupId = '5';
        $department->save();

        $department = new Department;
        $department->name = "Catering Department";
        $department->storeid = '64d802a654ce1724a445e37bb13c2b77fd8b7d4c';
        $department->qGroupId = '6';
        $department->save();




        
    }
}