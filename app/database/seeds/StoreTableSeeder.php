<?php
class StoreTableSeeder extends Seeder {

    public function run()
    {
       DB::table('stores')->delete();

        $store = new Store;

        // Seeding of Administrator account
        $store->id = '1';
        $store->storename = 'WalMart';
        $store->address = '75 North East St., Fayetteville, AR 72701. Walton Enterprises , 125 W. Central Ave. #218, Bentonville, AR 72712.';
		$store->photo = 'images/walter64.png';
        $store->save();

		$store = new Store;
		$store->id = '2';
        $store->storename = 'Bed,Bath and Beyond';
        $store->address = '3816 North Mall Avenue Fayetteville, Arkansas 72703';
		$store->photo = 'images/bed64.png';
        $store->save();

		$store = new Store;
		$store->id = '3';
        $store->storename = 'Target';
        $store->address = '3545 N Shiloh Dr, Fayetteville, AR 72703-5359';
		$store->photo = 'images/target64.png';
        $store->save();

        $store = new Store;
        $store->id = '12345';
        $store->storename = 'The ARK Challenge Central Arkansas Demo Day 2014';
        $store->address = 'The ARK Challenge Central Arkansas Demo Day 2014';
        $store->photo = 'images/Demo.png';
        $store->qGroupId = '3';
        $store->save();

        $store = new Store;
        $store->id = 'bd3676e10b603acd4d2e6303d887c4e63297c391';
        $store->storename = "M'Pressed Cleaners";
        $store->address = '813 West Central Avenue, Bentonville';
        $store->photo = 'images/Business.png';
        $store->qGroupId = '4';
        $store->save();

        $store = new Store;
        $store->id = 'aa6feee413162b463b9971fa6d7ca16ed14550c2';
        $store->storename = "M'Pressed Cleaners";
        $store->address = '2410 Southwest 14th Street #2, Bentonville';
        $store->photo = 'images/Business.png';
        $store->qGroupId = '4';
        $store->save();

        $store = new Store;
        $store->id = '1e6dbb99c2148e8970987847904021cdf5d6315d';
        $store->storename = "Bo's Coffee Club - Katipunan Branch";
        $store->address = '329-G B. Gonzales, Lungsod Quezon';
        $store->photo = 'images/cafe.png';
        $store->qGroupId = '1';
        $store->hasDept = 1;
        $store->save();

        $store = new Store;
        $store->id = '64d802a654ce1724a445e37bb13c2b77fd8b7d4c';
        $store->storename = "Green Leaf Grill";
        $store->address = '601 South Gaines Street, Little Rock';
        $store->photo = 'images/deptstore.png';
        $store->qGroupId = '1';
        $store->hasDept = 1;
        $store->save();


        $store = new Store;
        $store->id = '6ef7a471e2039a67a3c5c511c409ae0e23612701';
        $store->storename = "Northwest Arkansas Regional Airport";
        $store->address = '1 Airport Boulevard, Bentonville ';
        $store->photo = 'images/Airport.png';
        $store->qGroupId = '7';
        $store->hasDept = 0;
        $store->save();

        $store = new Store;
        $store->id = '54321';
        $store->storename = 'Natural State Angel Association Meeting';
        $store->address = 'Natural State Angel Association Meeting';
        $store->photo = 'images/Demo.png';
        $store->qGroupId = '8';
        $store->save();

/*
        $store = new Store;
        $store->id = 'bf3e468fa11ced029810958f3d0b184f3f310375';
        $store->storename = "Northwest Arkansas Regional Airport";
        $store->address = 'Bentonville ';
        $store->photo = 'images/Airport.png';
        $store->qGroupId = '1';
        $store->hasDept = 0;
        $store->save();
*/
        
//        $store = new Store;
//        $store->id = '72776779490fe0b3aa6247c940ebfa22d1c5ca5f';
//        $store->photo = 'images/restaurant.png';
//        $store->storename = 'Petron';
//        $store->qGroupId = '1';
//        $store->save();
//
//        $store = new Store;
//        $store->id = 'c38c868347a6b946d83c696541e8d9bb618788d6';
//        $store->photo = 'images/cafe.png';
//        $store->storename = 'Xocolat';
//        $store->qGroupId = '2';
//        $store->save();
    }
}