<?php
class QuestionTableSeeder extends Seeder {

    public function run()
    {
       DB::table('questions')->delete();

        // STORE 1
        $question = new Question;
        $question->orderId= 2;
        $question->question = 'Would you choose to do business with this location again?';
        $question->save();

        $question = new Question;
        $question->orderId = 1;
        $question->question = 'Were you greeted by an employee upon entry to the location?';
        $question->save();

        $question = new Question;
        $question->orderId = 3;
        $question->question = 'Would you recommend this business to friends and family?';
        $question->save();

        $question = new Question;
        $question->orderId = 5;
        $question->question = 'Is this a sample question #3?';
        $question->save();

        $question = new Question;
        $question->orderId = 4;
        $question->question = 'Is this a sample question #5?';
        $question->save();

        // STORE 2
        $question = new Question;
        $question->orderId= 3;
        $question->question = 'Is this a sample question #2?';
        $question->save();

        $question = new Question;
        $question->orderId = 2;
        $question->question = 'Is this a sample question #3?';
        $question->save();

        $question = new Question;
        $question->orderId = 4;
        $question->question = 'Is this a sample question #5?';
        $question->save();

        $question = new Question;
        $question->orderId = 1;
        $question->question = 'Is this a sample question #4?';
        $question->save();

        $question = new Question;
        $question->orderId = 5;
        $question->question = 'Is this a sample question #1?';
        $question->save();

        //DEMO QUESTIONS LIL ROCK ID=11-13
        $question = new Question;
        $question->orderId = 1;
        $question->question = 'Is this your first ARK Challenge Demo Day?';
        $question->save();

        $question = new Question;
        $question->orderId = 2;
        $question->question = 'Have you ever been a part of a startup?';
        $question->save();

        $question = new Question;
        $question->orderId = 3;
        $question->question = 'Are you an investor?';
        $question->save();

        //M'Pressed Cleaners id = 14 - 16
        $question = new Question;
        $question->orderId = 1;
        $question->question = "Are you friends with M'Pressed on Facebook?";
        $question->save();

        $question = new Question;
        $question->orderId = 2;
        $question->question = "Were the M'Pressed staff friendly?";
        $question->save();

        $question = new Question;
        $question->orderId = 3;
        $question->question = 'Did you have any issues?';
        $question->save();


        //Grean Leaf - Grill Department 17 - 19
        $question = new Question;
        $question->orderId = 1;
        $question->question = "Were you satisfied with your meal?";
        $question->save();

        $question = new Question;
        $question->orderId = 2;
        $question->question = "Were you satisfied with the Green Leaf Grill’s service/staff?";
        $question->save();

        $question = new Question;
        $question->orderId = 3;
        $question->question = 'Were you satisfied with the Green Leaf Grill’s dining area?';
        $question->save();


        //Grean Leaf - Catering Department: 20 - 22
        $question = new Question;
        $question->orderId = 1;
        $question->question = "Was the food delivered on time?";
        $question->save();

        $question = new Question;
        $question->orderId = 2;
        $question->question = "Was the order correctly fulfilled?";
        $question->save();

        $question = new Question;
        $question->orderId = 3;
        $question->question = 'Was the quantity of food good?';
        $question->save();


        //Northwest Arkansas Regional Airport: 23 - 25
        $question = new Question;
        $question->orderId = 1;
        $question->question = "Is this your first time to visit XNA?";
        $question->save();

        $question = new Question;
        $question->orderId = 2;
        $question->question = "Are you from Northwest Arkansas?";
        $question->save();

        $question = new Question;
        $question->orderId = 3;
        $question->question = 'Would you fly through XNA again?';
        $question->save(); 


    }
}