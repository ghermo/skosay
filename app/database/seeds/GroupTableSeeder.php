<?php
class GroupTableSeeder extends Seeder {

    public function run()
    {
       DB::table('groups')->delete();

        // Seeding of Groups
        $group = new Group;
        $group->groupname = 'Walmart';
        $group->save();

        $group = new Group;
        $group->groupname = 'Petron';
        $group->save();

        $group = new Group;
        $group->groupname = 'BosCafe';
        $group->save();

        $group = new Group;
        $group->groupname = 'BosCafe Dept A';
        $group->save();

        $group = new Group;
        $group->groupname = 'BosCafe Dept B';
        $group->save();

        $group = new Group;
        $group->groupname = 'BosCafe Dept C';
        $group->save();

        $group = new Group;
        $group->groupname = 'Green Leaf Grill';
        $group->save();

        $group = new Group;
        $group->groupname = 'Green Leaf Grill: Grill Department';
        $group->save();

        $group = new Group;
        $group->groupname = 'Green Leaf Grill: Catering Department';
        $group->save();

        $group = new Group;
        $group->groupname = 'XNA Airport';
        $group->save();
    }
}