<?php
class UserTableSeeder extends Seeder {

    public function run()
    {
       DB::table('users')->delete();

        // Seeding of Administrator account
        $user = new User;
        $user->email = 'admin1@admin.com';
        $user->name = "admin1";
        $user->type = 'admin';
        $user->password = Hash::make('admin1');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'admin2@admin.com';
        $user->name = "admin2";
        $user->type = 'admin';
        $user->password = Hash::make('admin2');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'admin3@admin.com';
        $user->name = "admin3";
        $user->type = 'admin';
        $user->password = Hash::make('admin3');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        //Manager Account
        $user = new User;
        $user->email = 'manage1@manage.com';
        $user->name = "manage1";
        $user->type = 'manager';
        $user->password = Hash::make('manage1');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'manage2@manage.com';
        $user->name = "manage2";
        $user->type = 'manager';
        $user->password = Hash::make('manage2');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'manage3@manage.com';
        $user->name = "manage3";
        $user->type = 'manager';
        $user->password = Hash::make('manage3');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        //DISTRICT MANAGER ACCOUNT
        $user = new User;
        $user->email = 'district1@manage.com';
        $user->name = "district1";
        $user->type = 'manager';
        $user->password = Hash::make('district1');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '1';
        $user->save();

		//Consumer Account
        $user = new User;
		$user->email = 'consumer1@consumer.com';
        $user->name = "consumer1";
        $user->type = 'consumer';
        $user->password = Hash::make('consumer1');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'consumer2@consumer.com';
        $user->name = "consumer2";
        $user->type = 'consumer';
        $user->password = Hash::make('consumer2');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'consumer3@consumer.com';
        $user->name = "consumer3";
        $user->type = 'consumer';
        $user->password = Hash::make('consumer3');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        
        $user = new User;
        $user->email = 'jlcortsa@gmail.com';
        $user->name = "Jay Castro";
        $user->type = 'consumer';
        $user->password = Hash::make('test1234');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->mobile = '+14792364199';
        $user->save();
        
        $user = new User;
        $user->email = 'justin@skosay.com';
        $user->name = "Justin Urso";
        $user->type = 'consumer';
        $user->password = Hash::make('test1234');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->mobile = '+14794590191';
        $user->save();
        
        $user = new User;
        $user->email = 'jason@skosay.com';
        $user->name = "Jason Kohrig";
        $user->type = 'consumer';
        $user->password = Hash::make('test1234');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->mobile = '+14795307216';
        $user->save();


        $user = new User;
        $user->email = 'smanager@manager.com';
        $user->name = "Store Manager";
        $user->type = 'manager';
        $user->password = Hash::make('smanager');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'ktrowe@arkbluecross.com';
        $user->name = "Kristen Rowe";
        $user->type = 'manager';
        $user->password = Hash::make('arkmngr_kr');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'stephanie.roberts@compass-usa.com';
        $user->name = "Stephanie Roberts";
        $user->type = 'manager';
        $user->password = Hash::make('arkmngr_sr');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'jpcheek@arkbluecross.com';
        $user->name = "James Cheek";
        $user->type = 'manager';
        $user->password = Hash::make('arkmngr_jc');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();

        $user = new User;
        $user->email = 'semrussel@gmail.com';
        $user->name = "Russel John";
        $user->type = 'manager';
        $user->password = Hash::make('srllc00!');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->mobile = '+639061539155';
        $user->save();
        
        $user = new User;
        $user->email = 'shiela@solutionsresource.com';
        $user->name = "Shiela Marie";
        $user->type = 'manager';
        $user->password = Hash::make('srllc00!');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->mobile = '+639309642116';
        $user->save();


        $user = new User;
        $user->email = 'client@client.com';
        $user->name = "client";
        $user->type = 'consumer';
        $user->password = Hash::make('client');
        $user->confirmation = 1;
        $user->confirmation_code = 'SEEDEDACCOUNT';
        $user->forgot = 0;
        $user->forgot_code = 'SEEDEDACCOUNT';
        $user->district_manager = '0';
        $user->save();


    }
}