<?php
class QuestionGroupsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('question-groups')->delete();

        // STORE 1 + GROUP 1
        $question = new QuestionGroup;
        $question->qid = '1';
        $question->qgroupid = '1';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '2';
        $question->qgroupid = '1';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '3';
        $question->qgroupid = '1';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '4';
        $question->qgroupid = '2';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '5';
        $question->qgroupid = '2';
        $question->save();

        // STORE 2 + GROUP 2
        $question = new QuestionGroup;
        $question->qid = '6';
        $question->qgroupid = '2';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '7';
        $question->qgroupid = '2';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '8';
        $question->qgroupid = '2';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '9';
        $question->qgroupid = '2';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '10';
        $question->qgroupid = '2';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '11';
        $question->qgroupid = '3';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '12';
        $question->qgroupid = '3';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '13';
        $question->qgroupid = '3';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '14';
        $question->qgroupid = '4';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '15';
        $question->qgroupid = '4';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '16';
        $question->qgroupid = '4';
        $question->save();

        //Grean Leaf: Grill Department Connection
        $question = new QuestionGroup;
        $question->qid = '17';
        $question->qgroupid = '5';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '18';
        $question->qgroupid = '5';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '19';
        $question->qgroupid = '5';
        $question->save();


        //Grean Leaf: Catering Department Connection
        $question = new QuestionGroup;
        $question->qid = '20';
        $question->qgroupid = '6';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '21';
        $question->qgroupid = '6';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '22';
        $question->qgroupid = '6';
        $question->save();


        //Northwest Arkansas Regional Airport Connection
        $question = new QuestionGroup;
        $question->qid = '23';
        $question->qgroupid = '7';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '24';
        $question->qgroupid = '7';
        $question->save();

        $question = new QuestionGroup;
        $question->qid = '25';
        $question->qgroupid = '7';
        $question->save();

    }
}