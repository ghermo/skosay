<?php
class QgroupsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('qgroups')->delete();

        $question = new Qgroup;
        $question->groupName = 'Default Question Group';
        $question->save();

        $question = new Qgroup;
        $question->groupName = 'Question Group 2';
        $question->save();

        $question = new Qgroup;
        $question->groupName = 'Demo Day Little Rock';
        $question->save();

        $question = new Qgroup;
        $question->groupName = "M'Pressed Cleaners";
        $question->save();

        $question = new Qgroup;
        $question->groupName = "Grean Leaf Grill - Grill Department";
        $question->save();

        $question = new Qgroup;
        $question->groupName = "Grean Leaf Grill - Catering Department";
        $question->save();

        $question = new Qgroup;
        $question->groupName = "Northwest Arkansas Regional Airport";
        $question->save();

    }
}