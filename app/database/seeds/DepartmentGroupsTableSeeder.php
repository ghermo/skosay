<?php
class DepartmentGroupsTableSeeder extends Seeder {

    public function run()
    {
       DB::table('department-groups')->delete();

        // 2nd St. Rogers
        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '10';
        $dgroup->groupid = '4';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '11';
        $dgroup->groupid = '5';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '12';
        $dgroup->groupid = '6';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '10';
        $dgroup->groupid = '3';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '11';
        $dgroup->groupid = '3';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '12';
        $dgroup->groupid = '3';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '13';
        $dgroup->groupid = '7';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '14';
        $dgroup->groupid = '7';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '13';
        $dgroup->groupid = '8';
        $dgroup->save();

        $dgroup = new DepartmentGroup;
        $dgroup->deptid = '14';
        $dgroup->groupid = '9';
        $dgroup->save();


        
    }
}