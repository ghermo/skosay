<?php
class MessageTableSeeder extends Seeder {

    public function run()
    {
       DB::table('messages')->delete();

        $message = new Message;

        // Seeding of Administrator account
        $message->id = '1';
        $message->userid = '3';
		$message->issueid = '1';
		$message->message = 'YES;YES;NO;Sample Comment';
        $message->save();

		$message = new Message;
		$message->id = '2';
        $message->userid = '1';
		$message->issueid = '1';
		$message->message = 'Sample Message of Admin';
        $message->save();



    }
}