<?php
class CustomerTableSeeder extends Seeder {

    public function run()
    {
       DB::table('customers')->delete();

        $customer = new Customer;
        $customer->storename = 'Xocolat';
        $customer->save();

        $customer = new Customer;
        $customer->storename = 'Northwest Arkansas Regional Airport';
        $customer->save();

        $customer = new Customer;
        $customer->storename = "M'Pressed Cleaners";
        $customer->save();

        $customer = new Customer;
        $customer->storename = "Slim Chickens";
        $customer->save();
        
        $customer = new Customer;
        $customer->storename = "Slim Chicken's Restaurant";
        $customer->save();
    }
}