<?php
class OauthClientsSeeder extends Seeder {

    public function run()
    {
       DB::table('oauth_clients')->delete();

		$clients = new Clients;
		$clients->id = 'client@client.com';
        $clients->secret = '12345';
        $clients->name = 'client';
        $clients->save();

        $clients = new Clients;
		$clients->id = 'admin1@admin.com';
        $clients->secret = '12345';
        $clients->name = 'admin1';
        $clients->save();

        $clients = new Clients;
        $clients->id = 'admin2@admin.com';
        $clients->secret = '12345';
        $clients->name = 'admin2';
        $clients->save();

        $clients = new Clients;
        $clients->id = 'admin3@admin.com';
        $clients->secret = '12345';
        $clients->name = 'admin3';
        $clients->save();

        $clients = new Clients;
		$clients->id = 'manage1@manage.com';
        $clients->secret = '12345';
        $clients->name = 'manage1';
        $clients->save();

        $clients = new Clients;
        $clients->id = 'manage2@manage.com';
        $clients->secret = '12345';
        $clients->name = 'manage2';
        $clients->save();

        $clients = new Clients;
        $clients->id = 'manage3@manage.com';
        $clients->secret = '12345';
        $clients->name = 'manage3';
        $clients->save();

        $clients = new Clients;
        $clients->id = 'smanager@manager.com';
        $clients->secret = '12345';
        $clients->name = 'smanager';
        $clients->save();



    }
}