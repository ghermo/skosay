<?php
class HarpsStoreTableSeeder extends Seeder {

    public function run()
    {
        /* START HARPS STORES */
        $photo = 'images/Supermarket.png';
        $harpsfoodstore = 'Harps Food Store';//repeating store names

        //start of Arkansas
        $store = new Store;
        $store->id = 'ChIJ7YCcvgS4y4cRMKEXHF9Mgc4';
        $store->storename = 'Harps Food Store';
        $store->address = '25 Highway 64 West Alma, AR 72921';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ34VJrtCi04cR1yJ6WhCqkpg';
        $store->storename = 'Harps Food Store';
        $store->address = '232 Hwy 167 Bald Knob, AR 72010';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ87MzfufE04cRwzrBDeQ6dEs';
        $store->storename = $harpsfoodstore;
        $store->address = '1740 Batesville Blvd. Batesville, AR 72501';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJZzSdBzsDyYcRd2IG3luK4Og';
        $store->storename = $harpsfoodstore;
        $store->address = '404 Town Center NE Bella Vista, AR 72714';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
        
        $store = new Store;
        $store->id = 'ChIJn0twDY9RzYcRwc6TQm41b-0';
        $store->storename = $harpsfoodstore;
        $store->address = '1515 Highway 5 Benton, AR 72019';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//5
        $store = new Store;
        $store->id = 'ChIJXZZE55kayYcRgPwokVg0LWY';
        $store->storename = $harpsfoodstore;
        $store->address = '1209 N. Walton Blvd Bentonville, AR 72712';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJNZSkVVvFzocRyakPbZHRDrg';
        $store->storename = 'Price Cutter Food Warehouse';
        $store->address = '326 Eureka Avenue Berryville, AR 72616';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJtcOkrgslzocRqiuLZt9LVPk';
        $store->storename = $harpsfoodstore;
        $store->address = '939 Central Blvd. Bull Shoals, AR 72619';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ8ZNM-KDv0ocRtYEU75RIL3A';
        $store->storename = $harpsfoodstore;
        $store->address = '801 North 2nd Street, Cabot, AR 72023';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJD0oshj-T0YcRPXt46VoGNE4';
        $store->storename = $harpsfoodstore;
        $store->address = '2002 Arkansas 56, Calico Rock, AR 72519';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//10
        $store = new Store;
        $store->id = 'ChIJMemqyGEPyYcRypWjUcd5RHw';
        $store->storename = $harpsfoodstore;
        $store->address = '710 West Centerton Boulevard, Centerton, AR 72719';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJcb-iAKJrzIcRMIHT3Ur7sws';
        $store->storename = $harpsfoodstore;
        $store->address = '502 South Crawford Street Clarksville, AR 72830';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'EiczODAgSGFya3JpZGVyIFN0LCBDb253YXksIEFSIDcyMDMyLCBVU0E';
        $store->storename = 'Price Cutter Food Warehouse';
        $store->address = '380 Harkrider Conway, AR 72032';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJCXpGX92D0ocRyr2U6uUaoqo';
        $store->storename = $harpsfoodstore;
        $store->address = '1120 E. German Lane Conway, AR 72032';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ98touhRY1ocRPI1LDpCBluA';
        $store->storename = $harpsfoodstore;
        $store->address = '301 N. Missouri Ave. Suite 2 Corning, AR 72422';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//15
        $store = new Store;
        $store->id = 'ChIJ27xRmDNUzIcRJdWT9g-fH50';
        $store->storename = $harpsfoodstore;
        $store->address = '612 Union Street Dardanelle, AR 72834';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

/*Harps Arkansas Dover no longi lati
        $store = new Store;
        
        $store->storename = $harpsfoodstore;
        $store->address = '8810 Market Street
Dover, AR 72837';
        $store->photo = $photo;
        $store->save();
  */  
        $store = new Store;
        $store->id = 'ChIJExsCA8BcyYcRSpDk5h1n0Gc';
        $store->storename = $harpsfoodstore;
        $store->address = '1951 N. Center Street Elkins, AR 72727';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJb2_dmetuyYcRsphcRpp39w8';
        $store->storename = $harpsfoodstore;
        $store->address = '1189 No. Garland Fayetteville, AR 72703';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJz-zP0_1ryYcRgdupIuN_yyk';
        $store->storename = $harpsfoodstore;
        $store->address = '3055 College Avenue, Fayetteville, AR 72703';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJWzjkvHJpyYcRQKlUTpWEaT0';
        $store->storename = 'Harps Marketplace';
        $store->address = '1780 North Crossover Rd. Fayetteville, AR 72703';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//20
        $store = new Store;
        $store->id = 'ChIJ_aAPc0JuyYcR0azBIEKTsMU';
        $store->storename = $harpsfoodstore;
        $store->address = '1274 North Colorado Drive Fayetteville, AR 72704';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJkWzlVVGyy4cRBAm-qGp_2jE';
        $store->storename = 'Harps Marketplace';
        $store->address = '3401 South 74th Street Fort Smith, AR 72903';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJZWspZaVSyocR3uW_B9ghIZI';
        $store->storename = $harpsfoodstore;
        $store->address = '2507 Market Trace Fort Smith, AR 72908';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJp8ZawLJMyocRZAa1-KnUw2U';
        $store->storename = 'Harps Marketplace';
        $store->address = '3100 Grand Avenue Fort Smith, AR 72904';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJBdX5sGXCzocRoxkz-4jStc0';
        $store->storename = $harpsfoodstore;
        $store->address = '309 East Main Green Forest, AR 72638';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//25
        $store = new Store;
        $store->id = 'ChIJsbvZs31-0ocR6DYi0oXTLqk';
        $store->storename = $harpsfoodstore;
        $store->address = '15 North Broadview Street Greenbrier, AR 72058';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJb8ClkKZNzYcRVokJyJGvC2Q';
        $store->storename = $harpsfoodstore;
        $store->address = '6039 Highway 67 Haskell, AR 72015';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ2Xqop-9G0ocR6DZ-FaUtZdg';
        $store->storename = $harpsfoodstore;
        $store->address = '901 W. Main Street Heber Springs, AR 72543';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'Ei41NTM2IENlbnRyYWwgQXZlLCBMYWtlIEhhbWlsdG9uLCBBUiA3MTkxMywgVVNB';
        $store->storename = $harpsfoodstore;
        $store->address = '5536 Central Avenue Hot Springs, AR 71913';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJcVjsOhDVzIcRCJS69WwRg3E';
        $store->storename = $harpsfoodstore;
        $store->address = '146 Thornton Ferry Rd Hot Springs, AR 71913';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
  //30  
        $store = new Store;
        $store->id = 'ChIJAbth0y-zzocR3V1GiGTHFKc';
        $store->storename = $harpsfoodstore;
        $store->address = '115 Lee St, HWY 412 & HWY 23, Huntsville, AR 72740';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJEc1xYMyFzocRg8yS-lyh9lk';
        $store->storename = $harpsfoodstore;
        $store->address = '613 E Court St, Jasper, AR 72641';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJYRGoeM4u1IcR3osCCeA6A8s';
        $store->storename = $harpsfoodstore;
        $store->address = '2005 Harrisburg Road Jonesboro, AR 72401';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ6_DQUE6CyYcRMfQrzwSQTLQ';
        $store->storename = $harpsfoodstore;
        $store->address = '718 W. Pridemore Drive Lincoln, AR 72744';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;// no id
        $store->storename = $harpsfoodstore;
        $store->address = '700 Hwy 65 North Marshall, AR 72650';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//35
        $store = new Store;
        $store->id = 'ChIJL6m5hKud0ocRHLMGvqcxI7Q';
        $store->storename = $harpsfoodstore;
        $store->address = '582 HWY 365 Mayflower, AR 72106';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
    
        $store = new Store;
        $store->id = 'ChIJWYJHSHV1zYcRk-ewWYgG2zI';
        $store->storename = $harpsfoodstore;
        $store->address = '100 S. St. Joseph St Morrilton, AR 72110';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
    
        $store = new Store;
        $store->id = 'EicyMDg5IFcgUmQsIE1vdW50YWluIEhvbWUsIEFSIDcyNjUzLCBVU0E';
        $store->storename = $harpsfoodstore;
        $store->address = '2089 HWY 62 West Mountain Home, AR 72653';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
    
        $store = new Store;
        $store->id = 'ChIJgQSqy8Lb0YcRCvo14kFfwbo';
        $store->storename = $harpsfoodstore;
        $store->address = '924 East Hwy 62 Mountain Home, AR 72653';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
    
        $store = new Store;
        $store->id = 'ChIJ7eEIF_gg0ocRbQI9MShqhco';
        $store->storename = $harpsfoodstore;
        $store->address = '907 East Main Mountain View, AR 72560';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//40
        $store = new Store;
        $store->id = 'ChIJH7M8lCl6yYcRnk2W5lh66BI';
        $store->storename = $harpsfoodstore;
        $store->address = '319 East Buchanan Street Prairie Grove , AR 72753';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
    
        $store = new Store;
        $store->id = 'ChIJMxvK7V4V1ocRBlEvhj3PSPw';
        $store->storename = $harpsfoodstore;
        $store->address = '807 E. 9th St. Rector, AR 72461';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJZ0EuqA4XyYcRGY4OQJKEs38';
        $store->storename = $harpsfoodstore;
        $store->address = '715 North 2nd Street Rogers, AR 72756';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJde9G8TAXyYcR-e9l3jz_TZo';
        $store->storename = 'Price Cutter Food Warehouse';
        $store->address = '103 North Dixieland Road Rogers, AR 72756';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ-8yvbGlTzIcRiL0xApTV79c';
        $store->storename = $harpsfoodstore;
        $store->address = '100 S. Knoxville Russellville, AR 72801';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//45
        $store = new Store;
        $store->id = 'ChIJ8YEfIM2r04cRQXkLrrBwKvo';
        $store->storename = $harpsfoodstore;
        $store->address = '2525 W. Beebe-Capps Expressway Searcy , AR 72143';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJOYtkPguYyYcRN16Dt0fZCGM';
        $store->storename = 'Harps';
        $store->address = '201 Hwy. 412 West Siloam Springs, AR 72761';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJp2AhtLMUyYcRcj16bM1_TOw';
        $store->storename = $harpsfoodstore;
        $store->address = '1308 N. Thompson Springdale, AR 72764';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJl7RCoENryYcR3DS8jMDGgQQ';
        $store->storename = 'Price Cutter Food Warehouse';
        $store->address = '1101 S. Thompson Springdale, AR 72764';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJaVfUmutqyYcRufoZaElZwZM';
        $store->storename = $harpsfoodstore;
        $store->address = '1945 Butterfield Coach Road Springdale, AR 72764';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//50
        $store = new Store;
        $store->id = 'ChIJ824vN7tsyYcRWByiuqR3Ets';
        $store->storename = $harpsfoodstore;
        $store->address = '2894 West Sunset Springdale, AR 72762';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJFTlwyDltyYcRzzTdrpyHdZU';
        $store->storename = $harpsfoodstore;
        $store->address = '1007 Jones Road Springdale, AR 72762';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJdaCoZjK0y4cRCYWJDEwsEDE';
        $store->storename = 'Price Cutter Food Warehouse';
        $store->address = '120 Cloverleaf Plaza Van Buren, AR 72956';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ4x8r9CKM0ocRAaqEZNJq8_I';
        $store->storename = $harpsfoodstore;
        $store->address = '1048 Main Street Vilonia, AR 72173';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJfz5Yz2sTy4cR7fbEVOY-rfs';
        $store->storename = $harpsfoodstore;
        $store->address = '148 West 2nd Street Waldron, AR 72958';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//55
        $store = new Store;
        $store->id = 'ChIJV2JK4X5jyYcR8sISeKbU0hs';
        $store->storename = $harpsfoodstore;
        $store->address = '266 Three Dog Lane West Fork, AR 72774';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJc0LlZRkWzocR7kN2PUEGWdI';
        $store->storename = $harpsfoodstore;
        $store->address = '200 N. Wickersham Street Yellville, AR 72687';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//57 added out of 58 in arkansas



//start of Missouri        
        $store = new Store;
        $store->id = 'ChIJLQUUQ555yIcRguxWnypSngcf';
        $store->storename = $harpsfoodstore;
        $store->address = '25685 Fir Road Airport Drive (Joplin), MO 64801';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJG_YCPkrX0IcR4wG93Mt7c4I';
        $store->storename = $harpsfoodstore;
        $store->address = '605 West Hwy 160 Alton, MO 65606';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJARI_-nd3eIgRjyqHQNTYL7I';
        $store->storename = $harpsfoodstore;
        $store->address = '4 S. Catapla St. Dexter, MO 63841';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJvbUZI94d14cRJMCeEheBwRI';
        $store->storename = $harpsfoodstore;
        $store->address = '1401 Hillcrest Plaza Doniphan, MO 63935';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJQfFrpaAf1ocRd_UNni-qErM';
        $store->storename = $harpsfoodstore;
        $store->address = '1780 First Street Kennett, MO 63857';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//5
        $store = new Store;
        $store->id = 'ChIJP2DsQFKAeIgRHN8FQio8-bM';
        $store->storename = $harpsfoodstore;
        $store->address = '1003 N. Douglas Malden, MO 63863';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();


        $store = new Store;
        $store->id = 'ChIJORbSdl5TyIcRIBy1EuuvXz4';
        $store->storename = $harpsfoodstore;
        $store->address = '210 West Main Noel , MO 64854';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJhygQ6RGw14cR0n07T1RqJa8';
        $store->storename = $harpsfoodstore;
        $store->address = '1301 Shelby Road Poplar Bluff, MO 63901';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJr6Sx80Uf0YcR_ScAAxKrmIc';
        $store->storename = $harpsfoodstore;
        $store->address = '129 East Walnut Thayer, MO 65791';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//9 out of 9 added for Missouri

//start of Oklahoma
        $store = new Store;
        $store->id = 'ChIJabkUHqW1t4cRhjzFMuF8vjQ';
        $store->storename = $harpsfoodstore;
        $store->address = '300 E. Layton / Hwy 66 Chelsea , OK 74016';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJv3KaCafitYcR5CfVDl8fWSM';
        $store->storename = $harpsfoodstore;
        $store->address = '1010 E. Poplar Fort Gibson, OK 74434';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJV8XLwjr0tYcRDsqyNyXufk4';
        $store->storename = $harpsfoodstore;
        $store->address = '603 North Main Street, Gore, OK 74435';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJpT_VTMs1yIcRqjWSTjGTZGc';
        $store->storename = $harpsfoodstore;
        $store->address = '1310 South Main Grove, OK 74344';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ_VAUz_1CtocRwWBIF3qCNsg';
        $store->storename = $harpsfoodstore;
        $store->address = '30750 Oklahoma 88, Inola, OK 74036';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
//5
        $store = new Store;
        $store->id = 'ChIJQ9Sd9ssjtocRGOfBLbA2P7o';
        $store->storename = $harpsfoodstore;
        $store->address = 'Intersection Hwy 82/33 Locust Grove, OK 74352';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJETz0i472yocREDRaiT3xWmo';
        $store->storename = 'Price Cutter Food Warehouse';
        $store->address = '2219 No. Broadway Poteau , OK 74953';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJC_ZpBtAltocRNzlN8ahhlJg';
        $store->storename = $harpsfoodstore;
        $store->address = '111 E. Ferry Salina, OK 74365';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();

        $store = new Store;
        $store->id = 'ChIJ3QddNxm7uIcRUrvKAh-KYo4';
        $store->storename = $harpsfoodstore;
        $store->address = '519 HWY 59 North Westville, OK 74965';
        $store->photo = $photo;
        $store->domain = 'harps';
        $store->save();
        /* END HARPS STORES*/
    }
}