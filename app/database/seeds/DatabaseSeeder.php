<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->command->info('User table has been seeded!');

        $this->call('StoreTableSeeder');
        $this->command->info('Store table has been seeded!');

        $this->call('QuestionTableSeeder');
		$this->command->info('Question table has been seeded!');

		$this->call('GroupTableSeeder');
		$this->command->info('Group table has been seeded!');

		$this->call('StoreGroupTableSeeder');
		$this->command->info('StoreGroup table has been seeded!');

		$this->call('UserGroupTableSeeder');
		$this->command->info('UserGroup table has been seeded!');

		//$this->call('SQuestionTableSeeder');
		//$this->command->info('SQuestion table has been seeded!');

        $this->call('QgroupsTableSeeder');
        $this->command->info('qgroups table has been seeded!');

        $this->call('QuestionGroupsTableSeeder');
        $this->command->info('question-groups table has been seeded!');

        $this->call('CustomerTableSeeder');
        $this->command->info('customers table has been seeded!');

        $this->call('DepartmentTableSeeder');
        $this->command->info('Department table has been seeded!');

        $this->call('DepartmentGroupsTableSeeder');
        $this->command->info('DepartmentGroups table has been seeded!');

//		$this->call('IssueTableSeeder');
//		$this->command->info('Issue table has been seeded!');


//		$this->call('MessageTableSeeder');
//		$this->command->info('Message table has been seeded!');
	
		$this->call('OauthClientsSeeder');
		$this->command->info('oauth_clients has been seeded!');
	}

}
